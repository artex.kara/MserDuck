﻿using System.Collections.Generic;

namespace DuckGame.MserDuck.NetMessages
{
    /// <summary>
    /// Very simple info about blocks that should be created using a <see cref="NMCreateBlocks"/>.
    /// </summary>
    public class BlockInfo
    {
        public BlockInfo()
        {
        }

        public short x;
        public short y;
        public byte tileset;
    }

    /// <summary>
    /// A net message to create a certain set of blocks specified using a list of BlockInfo.
    /// </summary>
    public class NMCreateBlocks : NMEvent
    {
        private List<BlockInfo> blocks = new List<BlockInfo>();

        public NMCreateBlocks(List<BlockInfo> blocks)
        {
            this.blocks = blocks;
        }

        public NMCreateBlocks()
        {
        }

        public override void Activate()
        {
            List<AutoBlock> newBlocks = new List<AutoBlock>();

            foreach (BlockInfo blInf in this.blocks)
            {
                AutoBlock newblk = GlitchGrenade.generateTileFromTileset((GlitchGrenade.Tileset)blInf.tileset, blInf.x * 16, blInf.y * 16);
                newBlocks.Add(newblk);
            }

            foreach (AutoBlock block in newBlocks)
            {
                block.InitializeNeighbors();
                block.GroupWithNeighbors();
                block.PlaceBlock();
                Level.Add(block);
            }
        }

        protected override void OnSerialize()
        {
            this._serializedData = new BitBuffer();
            this.serializedData.Write((ushort)this.blocks.Count);
            foreach (BlockInfo block in this.blocks)
            {
                this.serializedData.Write(block.tileset);
                this.serializedData.Write(block.x);
                this.serializedData.Write(block.y);
            }
        }

        public override void OnDeserialize(BitBuffer buf)
        {
            ushort count = buf.ReadUShort();
            for (uint i = 0; i < count; ++i)
            {
                BlockInfo block = new BlockInfo();
                block.tileset = buf.ReadByte();
                block.x = buf.ReadShort();
                block.y = buf.ReadShort();
                this.blocks.Add(block);
            }
        }
    }
}

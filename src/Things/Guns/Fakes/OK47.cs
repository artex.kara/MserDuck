﻿namespace DuckGame.MserDuck.Fakes
{
    [EditorGroup("guns|mser|spoiler"), BaggedProperty("fakSubstitute", typeof(AK47))]
    public class OK47 : Gun
    {
        public OK47(float xval, float yval) : base(xval, yval)
        {
            this._numBulletsPerFire = 2;
            this.ammo = 300;
            this._ammoType = new AT9mm();
            this._ammoType.bulletSpeed = 0.28f;
            this._ammoType.bulletLength = 75f;
            this._ammoType.rebound = true;
            this._ammoType.range = 9999f;
            this._ammoType.accuracy = 0.85f;
            this._ammoType.penetration = 2f;
            this._ammoType.speedVariation = 0f;
            this._type = "gun";
            base.graphic = new Sprite("ak47", 0f, 0f);
            this.center = new Vec2(16f, 15f);
            this.collisionOffset = new Vec2(-8f, -3f);
            this.collisionSize = new Vec2(18f, 10f);
            this._barrelOffsetTL = new Vec2(32f, 14f);
            this._fireSound = "deepMachineGun2";
            this._fullAuto = true;
            this._fireWait = 0.48f;
            this._kickForce = 0.5f;
            this.loseAccuracy = 0.2f;
            this.maxAccuracyLost = 0.8f;
        }
        public override void OnReleaseAction()
        {
            base.OnReleaseAction();

            // NORMAL STUFF
            if (this.duck != null) {
				this.duck.ApplyForce(new Vec2(-this.duck.offDir, -1f));
				this.duck.Swear();
				this.duck.GoRagdoll();
			}
            SFX.Play("slip");
        }
    }
}

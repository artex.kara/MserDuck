namespace DuckGame.MserDuck.Fakes
{
	[BaggedProperty("isInDemo", true), EditorGroup("guns|mser|spoiler"), BaggedProperty("fakSubstitute", "Flower")]
	internal class Fakwer : Holdable
	{
		public StateBinding _netQuackBinding = new NetSoundBinding("_netQuack");

		public NetSoundEffect _netQuack = new NetSoundEffect(new string[]
		{
			"happyQuack01"
		});

		public bool _picked;

		public Fakwer(float xpos, float ypos) : base(xpos, ypos)
		{
			base.graphic = new Sprite("flower", 0f, 0f);
			this.center = new Vec2(8f, 12f);
			this.collisionOffset = new Vec2(-3f, -12f);
			this.collisionSize = new Vec2(6f, 14f);
			this._holdOffset = new Vec2(-2f, 2f);
			base.depth = -0.5f;
			this.weight = 1f;
			this.flammable = 0.3f;
			base.hugWalls = WallHug.Floor;
        }

		protected override bool OnDestroy(DestroyType type = null)
		{
			return false;
		}

		public override void Update()
		{
			if (System.Math.Abs(this.hSpeed) > 0.2f || (!this._picked && this.owner != null))
			{
				this._picked = true;
			}
			if (this._picked)
			{
				if (this.owner != null)
				{
					this.center = new Vec2(8f, 12f);
					this.collisionOffset = new Vec2(-3f, -12f);
					this.collisionSize = new Vec2(6f, 14f);
					base.angleDegrees = 0f;
					base.graphic.flipH = (this.offDir < 0);
				}
				else
				{
					this.center = new Vec2(8f, 8f);
					this.collisionOffset = new Vec2(-7f, -5f);
					this.collisionSize = new Vec2(14f, 6f);
					base.angleDegrees = 90f;
					base.graphic.flipH = true;
					base.depth = 0.4f;
				}
			}
			base.Update();
		}

		public override void OnPressAction()
		{
            if (base.isServerForObject)
            {
                for (int i = 0; i < 10; i++)
                {
                    Level.Add(SmallFire.New(base.x - this.hSpeed, base.y - this.vSpeed, -3f + Rando.Float(6f), -3f + Rando.Float(6f), false, null, true, this, false));
                }
            }

            if (Network.isActive)
			{
				if (base.isServerForObject)
				{
					this._netQuack.Play(1f, Rando.Float(-0.1f, 0.1f));
				}
			}
			else
			{
				SFX.Play("happyQuack01", 1f, Rando.Float(-0.1f, 0.1f), 0f, false);
			}
			if (this.owner != null)
			{
				Duck duck = this.owner as Duck;
				duck.quack = 20;
			}
		}

		public override void OnReleaseAction()
		{
			if (this.owner != null)
			{
				Duck duck = this.owner as Duck;
				duck.quack = 0;
			}
		}

		public override void Draw()
		{
			base.Draw();
		}
	}
}

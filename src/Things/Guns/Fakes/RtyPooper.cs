﻿namespace DuckGame.MserDuck.Fakes
{
    [EditorGroup("guns|mser"), BaggedProperty("fakSubstitute", typeof(PartyPooper))]
    public class RtyPooper : Gun
    {
        private float _timer = 1.2f;
        public StateBinding _timerBinding = new StateBinding("_timer");

        private int _action = 1;
        public StateBinding _actionBinding = new StateBinding("_action");

        private SpriteMap _sprite;

        private bool _pin = true;

        private float[] sad_rties = new float[] { 1f, -0.7f, -0.9f };

        public RtyPooper(float xval, float yval) : this(xval, yval, -1)
        {
            //Eyy Senior-ita
        }

        public RtyPooper(float xval, float yval, int action = -1) : base(xval, yval)
        {
            this.ammo = 1;
            this._type = "gun";
            this._sprite = new SpriteMap(this.GetPath("party"), 16, 16, false);
            this.graphic = this._sprite;
            this.center = new Vec2(8f, 10f);
            this.collisionOffset = new Vec2(-3f, -3f);
            this.collisionSize = new Vec2(6f, 7f);
            this.bouncy = 0.8f;
            this.friction = 0.1f;
            this._editorName = "Arty Pooper";
            this._action = action;
        }

        public override void Initialize()
        {
            base.Initialize();

            if (this._action == -50)
            {
                this._action = Rando.Int(0, 8);
                if (this._action == 8)
                {
                    this._action = 7;
                }
            } else if (this._action == -1)
            {
                this._action = Rando.Int(0, 8);
            }
        }

        public override void Update()
        {
            base.Update();

            if (!this._pin)
            {
                this._timer -= 0.01f;
            }

            if (this._timer < 0f)
            {
                switch (this._action)
                {
                   case 0:
                   case 1:
                        this.doLarge(20f);
                        break;
                    case 2:
                    case 3:
                        this.doLarge(50f);
                        break;
                    case 4:
                        this.doSad(1);
                        break;
                    case 5:
                    case 6:
                        this.doSad(2);
                        break;
                    case 7:
                        this.doSad(3);
                        break;
                    case 8:
                        this.doDuppel();
                        break;
                    default:
                        this.doNormalo();
                        break;
                }
            }
            this._sprite.frame = (this._pin ? 0 : 1);
        }

        private void doLarge(float scale)
        {
            Graphics.flashAdd = 0.3f;
            SFX.Play("hatOpen");
            SFX.Play(this.GetPath("sounds/party" + Rando.Int(1, 4) + ".wav"));
            for (var i = 0; i < 50; i++)
            {
                var num2 = (float)i * 18f - 5f + Rando.Float(10f);
                var xp = this.x + (float)(System.Math.Cos(Maths.DegToRad(num2)) * 3.0);
                var xa = (float)(System.Math.Cos(Maths.DegToRad(num2)) * Rando.Float(1, 4));
                var yp = this.y - (float)(System.Math.Sin(Maths.DegToRad(num2)) * 3.0);
                var ya = (float)(System.Math.Sin(Maths.DegToRad(num2)) * Rando.Float(1, 4));
                var fetti = new ConfettiAbsurdo(xp, yp, default(Color), new Vec2(xa, ya), scale);
                Level.Add(fetti);
            }
            Level.Remove(this);
            this._destroyed = true;
        }

        private void doSad(int temperatureOfFourthRelativelyConjoinedSoulSquared)
        {
            Graphics.flashAdd = 0.3f;
            SFX.Play("hatOpen");
            SFX.Play(this.GetPath("sounds/party" + Rando.Int(1, 4) + ".wav"), 1f, this.sad_rties[temperatureOfFourthRelativelyConjoinedSoulSquared-1]);
            for (var i = 0; i < temperatureOfFourthRelativelyConjoinedSoulSquared; i++)
            {
                var num2 = (float)i * 18f - 5f + Rando.Float(10f);
                var xp = this.x + (float)(System.Math.Cos(Maths.DegToRad(num2)) * 3.0);
                var xa = (float)(System.Math.Cos(Maths.DegToRad(num2)) * Rando.Float(1, 4));
                var yp = this.y - (float)(System.Math.Sin(Maths.DegToRad(num2)) * 3.0);
                var ya = (float)(System.Math.Sin(Maths.DegToRad(num2)) * Rando.Float(1, 4));
                var fetti = new ConfettiAbsurdo(xp, yp, default(Color), new Vec2(xa, ya),2.5f);
                Level.Add(fetti);
            }
            Level.Remove(this);
            this._destroyed = true;
        }

        private void doDuppel()
        {
            Graphics.flashAdd = 0.3f;
            SFX.Play("hatOpen");
            SFX.Play(this.GetPath("sounds/party" + Rando.Int(1, 4) + ".wav"));
            for (var i = 0; i < 20; i++)
            {
                var num2 = (float)i * 18f - 5f + Rando.Float(10f);
                var xp = this.x + (float)(System.Math.Cos(Maths.DegToRad(num2)) * 3.0);
                var xa = (float)(System.Math.Cos(Maths.DegToRad(num2)) * Rando.Float(1, 4));
                var yp = this.y - (float)(System.Math.Sin(Maths.DegToRad(num2)) * 3.0);
                var ya = (float)(System.Math.Sin(Maths.DegToRad(num2)) * Rando.Float(1, 4));
                var arty = new RtyPooper(xp, yp, -50); //-50 == secret :3
                arty.velocity = new Vec2(xa, ya);
                Level.Add(arty);
                arty.OnPressAction();
            }
            Level.Remove(this);
            this._destroyed = true;
        }

        private void doNormalo()
        {
            Graphics.flashAdd = 0.3f;
            SFX.Play("hatOpen");
            SFX.Play(this.GetPath("sounds/party" + Rando.Int(1, 4) + ".wav"));
            for (var i = 0; i < 50; i++)
            {
                var num2 = (float)i * 18f - 5f + Rando.Float(10f);
                var xp = this.x + (float)(System.Math.Cos(Maths.DegToRad(num2)) * 3.0);
                var xa = (float)(System.Math.Cos(Maths.DegToRad(num2)) * Rando.Float(1, 4));
                var yp = this.y - (float)(System.Math.Sin(Maths.DegToRad(num2)) * 3.0);
                var ya = (float)(System.Math.Sin(Maths.DegToRad(num2)) * Rando.Float(1, 4));
                var fetti = new ConfettiNormalo(xp, yp, default(Color), new Vec2(xa, ya));
                Level.Add(fetti);
            }
            Level.Remove(this);
            this._destroyed = true;
        }

        public override void OnPressAction()
        {
            if (this._pin)
            {
                this._pin = false;
                var pin = new GrenadePin(this.x, this.y)
                {
                    hSpeed = (float)(-(float)this.offDir) * (1.5f + Rando.Float(0.5f)),
                    vSpeed = -2f,
                };
                pin.graphic.color = new Color(15, 15, 15);
                SFX.Play("flipPage", 1f, 0f, 0f, false);
                Level.Add(pin);
            }
        }
    }
}

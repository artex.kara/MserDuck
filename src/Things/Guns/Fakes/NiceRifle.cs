namespace DuckGame.MserDuck.Fakes
{
	[EditorGroup("guns|mser|spoiler"), BaggedProperty("fakSubstitute", typeof(LaserRifle))]
	public class NiceRifle : Gun
	{
		public NiceRifle(float xval, float yval) : base(xval, yval)
		{
			this.ammo = 80;
			this._ammoType = new ATNiceLaser();
			//this._ammoType.barrelAngleDegrees = 45f;
			this._type = "gun";
			base.graphic = new Sprite("laserRifle", 0f, 0f);
			this.center = new Vec2(16f, 16f);
			this.collisionOffset = new Vec2(-8f, -3f);
			this.collisionSize = new Vec2(16f, 10f);
			this._barrelOffsetTL = new Vec2(26f, 14f);
			this._fireSound = "laserRifle";
			this._fullAuto = true;
			this._fireWait = 0.1f;
			this._kickForce = 1f;
			this._holdOffset = new Vec2(-2f, 0f);
		}
	}
}

using System;
using System.Reflection;

namespace DuckGame.MserDuck.Fakes
{
	[EditorGroup("guns|mser|spoiler"), BaggedProperty("fakSubstitute", typeof(Boots))]
	public class FakShoes : Equipment
    {
        private MethodInfo hup;

        public FakShoes(float xpos, float ypos) : base(xpos, ypos)
        {
            this._pickupSprite = new Sprite("bootsPickup", 0f, 0f);
            this._sprite = new SpriteMap("boots", 32, 32, false);
            base.graphic = this._pickupSprite;
            this.center = new Vec2(8f, 8f);
            this.collisionOffset = new Vec2(-6f, -6f);
            this.collisionSize = new Vec2(12f, 13f);
            this._equippedDepth = 1;
            //this._wearOffset = new Vec2(0, -3);
        }

        public override void Initialize()
        {
            this.hup = MserGlobals.GetMethod(typeof(Holdable), "Update");

            base.Initialize();
        }

        public override void Update()
        {
            /*
             * The following is the Update function of Boots and Equipment,
             * the latter without the code that wrongly positions the shoes.
             * It also calls the Holdable's and below update code, to avoid weird behaviour.
            */
            if (this._equippedDuck != null && !this.destroyed)
            {
                this.center = new Vec2(16f, 12f);
                base.graphic = this._sprite;
                this.collisionOffset = new Vec2(0f, -9999f);
                this.collisionSize = new Vec2(0f, 0f);
                this.solid = false;
                this._sprite.frame = this._equippedDuck._sprite.imageIndex;
                if (this._equippedDuck.ragdoll != null)
                {
                    this._sprite.frame = 12;
                }
                this._sprite.flipH = this._equippedDuck._sprite.flipH;
            }
            else
            {
                this.center = new Vec2(8f, 8f);
                base.graphic = this._pickupSprite;
                this.collisionOffset = new Vec2(-6f, -6f);
                this.collisionSize = new Vec2(12f, 13f);
                this.solid = true;
                this._sprite.frame = 0;
                this._sprite.flipH = false;
            }
            if (this.destroyed)
            {
                base.alpha -= 0.05f;
            }
            if (base.alpha < 0f)
            {
                Level.Remove(this);
            }

            if (this.autoEquipTime > 0f)
            {
                this.autoEquipTime -= 0.016f;
            }
            else
            {
                this.autoEquipTime = 0f;
            }
            if (base.isServerForObject)
            {
                if (this._equipmentHealth <= 0f && this._equippedDuck != null && base.duck != null)
                {
                    base.duck.KnockOffEquipment(this, true, null);
                    if (Network.isActive)
                    {
                        this._netTing.Play(1f, 0f);
                    }
                }
                this._equipmentHealth = Lerp.Float(this._equipmentHealth, 1f, 0.003f);
            }
            if (this.destroyed)
            {
                base.alpha -= 0.1f;
                if (base.alpha < 0f)
                {
                    Level.Remove(this);
                }
            }
            if (this.localEquipIndex < this.equipIndex)
            {
                for (int i = 0; i < 2; i++)
                {
                    Level.Add(SmallSmoke.New(base.x + Rando.Float(-2f, 2f), base.y + Rando.Float(-2f, 2f)));
                }
                SFX.Play("equip", 0.8f, 0f, 0f, false);
                this.localEquipIndex = this.equipIndex;
            }
            this._prevEquipped = (this._equippedDuck != null);
            this.thickness = ((this._equippedDuck != null) ? this._equippedThickness : 0.1f);
            if (Network.isActive && this._equippedDuck != null && base.duck != null && !base.duck.HasEquipment(this))
            {
                base.duck.Equip(this, false, false);
            }

            //Correctly set position
            this.NewPosition();

            //Run holdable's update
            if (this.hup != null)
            {
                var ptr = this.hup.MethodHandle.GetFunctionPointer();
                var baseUpdate = (Action)Activator.CreateInstance(typeof(Action), this, ptr);
                baseUpdate();
            }

            //Get duck below boots
            if (this._equippedDuck != null)
            {
                var nearducks = Level.CheckCircleAll<Duck>(new Vec2(this.position.x, this.position.y + 10), 8);
                foreach (Duck nearduck in nearducks)
                {
                    if (nearduck != null && !(object.ReferenceEquals(nearduck, this._equippedDuck)) && !nearduck.sliding
                        && !this._equippedDuck.swinging && this._equippedDuck.velocity.y - nearduck.velocity.y > 1.4f)
                    {
                        //Kill me, pls
                        this._equippedDuck.velocity = new Vec2(this._equippedDuck.velocity.x, -50);
                        //this._equippedDuck._groundValid = 7;
                        if (this._equippedDuck.isServerForObject)
                        {
                            this._equippedDuck.MakeStars();
                            this._equippedDuck.Kill(new DTCrush(this as PhysicsObject));
                            return;
                        }
                    }
                }
            }
        }

        public override void Draw()
        {
            //Correctly set position
            this.NewPosition();

            //Run holdable's draw
            /*We do not have to copy inherited stuff here
             * since the other draw codes don't do much else.
             * We do need the base draw codes, though!*/
            var ptr = MserGlobals.GetMethod(typeof(Holdable), "Draw").MethodHandle.GetFunctionPointer();
            var baseDraw = (Action)Activator.CreateInstance(typeof(Action), this, ptr);
            baseDraw();
        }

        public void NewPosition()
        {
            if (this._equippedDuck != null)
            {
                Duck duck = base.duck;
                if (duck != null && duck.skeleton != null)
                {
                    DuckBone duckBone = base.duck.skeleton.lowerTorso;
                    this.offDir = this.owner.offDir;
                    this.position = duckBone.position;
                    this.angle = ((this.offDir > 0) ? (-duckBone.orientation) : duckBone.orientation);
                    Vec2 value = this._wearOffset;
                    this.position += new Vec2(value.x * (float)this.offDir, value.y).Rotate(this.angle, Vec2.Zero);
                }
            }
        }

        private float _equipmentHealth = 1f;

        protected Sprite _pickupSprite;

        protected SpriteMap _sprite;
    }
}

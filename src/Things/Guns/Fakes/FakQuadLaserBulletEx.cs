﻿namespace DuckGame.MserDuck.Fakes
{
    class FakQuadLaserBulletEx : FakQuadLaserBullet
    {
        public FakQuadLaserBulletEx(float xpos, float ypos, Vec2 travel) : base(xpos, ypos, travel)
        {
            this.graphic = new Sprite(this.GetPath("ac"));
            this.centerx = 9f;
            this.centery = 9f;
            this.xscale = 8f / 18f;
            this.yscale = 8f / 18f;
        }

        public override void Draw()
        {
            this.BaseDraw();
        }
    }
}

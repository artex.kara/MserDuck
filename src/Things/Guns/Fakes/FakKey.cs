using System;

namespace DuckGame.MserDuck.Fakes
{
	[EditorGroup("guns|mser|spoiler"), BaggedProperty("fakSubstitute", typeof(Key))]
	public class FakKey : Holdable
	{
		private SpriteMap _sprite;

		public FakKey(float xpos, float ypos) : base(xpos, ypos)
		{
			this._sprite = new SpriteMap("key", 16, 16, false);
			base.graphic = this._sprite;
			this.center = new Vec2(8f, 8f);
			this.collisionOffset = new Vec2(-7f, -4f);
			this.collisionSize = new Vec2(14f, 8f);
			base.depth = -0.5f;
			this.thickness = 1f;
			this.weight = 3f;
			this.flammable = 0f;
			base.collideSounds.Add("metalRebound");
			this.physicsMaterial = PhysicsMaterial.Metal;
        }

		public override void Update()
		{
			this._sprite.flipH = (this.offDir < 0);
			if (this.owner != null)
			{
				Door door = Level.CheckLine<Door>(this.position + new Vec2(-10f, 0f), this.position + new Vec2(10f, 0f), null);
				if (door != null)
				{
                    float num = this.y - 2f;
                    Level.Add(new ExplosionPart(this.x, num, true));
                    int num2 = 6;
                    if (Graphics.effectsLevel < 2)
                    {
                        num2 = 3;
                    }
                    for (int i = 0; i < num2; i++)
                    {
                        float deg = (float)i * 60f + Rando.Float(-10f, 10f);
                        float num3 = Rando.Float(12f, 20f);
                        ExplosionPart thing = new ExplosionPart(this.x + (float)(Math.Cos((double)Maths.DegToRad(deg)) * (double)num3), num - (float)(Math.Sin((double)Maths.DegToRad(deg)) * (double)num3), true);
                        Level.Add(thing);
                    }
                    for (int j = 0; j < 10; j++)
                    {
                        Grenade grenade = new Grenade(this.x, this.y);
                        float num4 = (float)j / 9f;
                        grenade.hSpeed = (-20f + num4 * 40f) * Rando.Float(0.5f, 1f);
                        grenade.vSpeed = Rando.Float(-3f, -11f);
                        grenade.pullOnImpact = true;
                        Level.Add(grenade);
                    }
                    Graphics.flashAdd = 1.3f;
                    Layer.Game.darken = 1.3f;
                    SFX.Play("explode", 1f, 0f, 0f, false);

                    door.UnlockDoor((Key)this);
				    Level.Remove(this);
				}
			}
            base.Update();
		}

        public static explicit operator Key(FakKey v)
        {
            return new Key(v.position.x, v.position.y);
        }

        public override void Terminate()
		{
			if (this.removedFromFall && base.prevOwner != null)
			{
				Duck duck = base.prevOwner as Duck;
				if (duck != null)
				{
					duck.AddCoolness(-2);
				}
			}
		}
	}
}

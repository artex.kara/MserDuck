﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.Fakes
{

    [EditorGroup("guns|mser|spoiler")]
    public class NotAliveBell : Gun
    {

        public NotAliveBell(float xval, float yval) : base(xval, yval)
        {
            this.ammo = 1;
            this._ammoType = new ATShrapnel();
            this._ammoType.penetration = 0.4f;
            this._type = "gun";
            this._sprite = new SpriteMap("grenade", 16, 16, false);
            base.graphic = this._sprite;
            this.center = new Vec2(7f, 8f);
            this.collisionOffset = new Vec2(-4f, -5f);
            this.collisionSize = new Vec2(8f, 10f);
            base.bouncy = 0.4f;
            this.friction = 0.05f;
            this._editorName = "Not Alive Bell";
            this._bio = "To cook grenade, pull pin and hold until feelings of terror run down your spine. Serves as many people as you can fit into a 3 meter radius.";
        }

        public void CreateExplosion(Vec2 pos)
        {
            if (!this._explosionCreated)
            {
                float x = pos.x;
                float num = pos.y - 2f;
                Level.Add(new ExplosionPart(x, num, true));
                int num2 = 6;
                if (Graphics.effectsLevel < 2)
                {
                    num2 = 3;
                }
                for (int i = 0; i < num2; i++)
                {
                    float deg = (float)i * 60f + Rando.Float(-10f, 10f);
                    float num3 = Rando.Float(12f, 20f);
                    ExplosionPart thing = new ExplosionPart(x + (float)(Math.Cos((double)Maths.DegToRad(deg)) * (double)num3), num - (float)(Math.Sin((double)Maths.DegToRad(deg)) * (double)num3), true);
                    Level.Add(thing);
                }
                this._explosionCreated = true;
                SFX.Play("explode", 1f, 0f, 0f, false);
            }
        }

        public override void OnNetworkBulletsFired(Vec2 pos)
        {
            this._pin = false;
            this._localDidExplode = true;
            if (!this._explosionCreated)
            {
                Graphics.flashAdd = 1.3f;
                Layer.Game.darken = 1.3f;
            }
            this.CreateExplosion(pos);
        }

        public override void OnPressAction()
        {
            if (this._pin)
            {
                this._pin = false;
                Level.Add(new GrenadePin(base.x, base.y)
                {
                    hSpeed = (float)(-(float)this.offDir) * (1.5f + Rando.Float(0.5f)),
                    vSpeed = -2f
                });
                SFX.Play("pullPin", 1f, 0f, 0f, false);
            }
        }

        public override void OnSolidImpact(MaterialThing with, ImpactedFrom from)
        {
            if (this.pullOnImpact)
            {
                this.OnPressAction();
            }
            base.OnSolidImpact(with, from);
        }

        public override void Update()
        {
            base.Update();
            if (!this._pin)
            {
                this._timer -= 0.01f;
            }
            if (this.ducksState > 0)
            {
                this.duckInvisibilityTimer++;

                if (this.duckInvisibilityTimer >= 60 && this.ducksState == 1) // remove the camera for other players
                {
                    this.ducksState = 2;
                    var ducks = MserGlobals.GetListOfThings<Duck>();
                    foreach (var duck in ducks)
                    {
                        if (!object.ReferenceEquals(duck, MserGlobals.GetLocalDuck()) && this.invisDucks.Contains(duck) &&
                            Level.current.camera is FollowCam && ((FollowCam)Level.current.camera).Contains(duck))
                        {
                            (Level.current.camera as FollowCam).Remove(duck);
                        }
                    }
                }
                if (this.duckInvisibilityTimer >= 600 && this.ducksState == 2)
                {
                    //Resurrection of ducks
                    this.ducksState = 3;
                    var ducks = MserGlobals.GetListOfThings<Duck>().ToList();
                    if (ducks.Any())
                    {
                        SFX.Play(base.GetPath("sounds/deadringer"));
                        foreach (var duck in ducks)
                        {
                            // re-add to cameras
                            if (this.invisDucks.Contains(duck) && Level.current.camera is FollowCam && !((FollowCam)Level.current.camera).Contains(duck))
                            {
                                (Level.current.camera as FollowCam).Add(duck);
                            }

                            // re-visiblify
                            duck.alpha = 1f;
                            if (duck.ragdoll != null)
                            {
                                duck.ragdoll.alpha = 1f;
                            }
                            if (duck._ragdollInstance != null)
                            {
                                duck._ragdollInstance.alpha = 1f;
                            }
                            foreach (var eq in duck._equipment)
                            {
                                eq.alpha = 1f;
                            }
                            if (duck.holdObject != null)
                            {
                                duck.holdObject.alpha = 1f;
                            }
                        }
                    }
                }
                if (this.duckInvisibilityTimer >= 600 && this.duckInvisibilityTimer < 630)
                {
                    var ducks = MserGlobals.GetListOfThings<Duck>();
                    foreach (var duck in ducks)
                    {
                        if (this.invisDucks.Contains(duck))
                        {
                            SmallSmoke smoke = SmallSmoke.New(duck.position.x, duck.position.y);
                            Level.Add(smoke);
                        }
                    }
                }
                if (this.duckInvisibilityTimer >= 630)
                {
                    //Removes the grenade
                    MserGlobals.RemoveThingStealthily(this);
                }
            }
            if (!this._localDidExplode && this._timer <= 0f && this.ducksState == 0)
            {
                if (this._explodeFrames < 0)
                {
                    this.CreateExplosion(this.position);
                    this._explodeFrames = 4;
                }
                else
                {
                    this._explodeFrames--;
                    if (this._explodeFrames == 0)
                    {
                        float x = base.x;
                        float num = base.y - 2f;
                        Graphics.flashAdd = 1.3f;
                        Layer.Game.darken = 1.3f;

                        this.CreateExplosion(this.position);

                        IEnumerable<Window> enumerable = Level.CheckCircleAll<Window>(this.position, 40f);
                        foreach (Window current in enumerable)
                        {
                            if (Level.CheckLine<Block>(this.position, current.position, current) == null)
                            {
                                current.Destroy(new DTImpact(this));
                            }
                        }

                        this._explodeFrames = -1;
                        this.visible = false;
                        this._visibleInGame = false;
                        this.canPickUp = false;
                        this.ducksState = 1;

                        // dead ringer
                        var ducks = Level.CheckCircleAll<Duck>(this.position, 60f);
                        foreach (var duck in ducks)
                        {
                            if (!duck.dead)
                            {
                                this.invisDucks.Add(duck);
                            }

                            duck.alpha = 0f;
                            if (duck.ragdoll != null)
                            {
                                duck.ragdoll.alpha = 0f;
                            }
                            if (duck._ragdollInstance != null)
                            {
                                duck._ragdollInstance.alpha = 0f;
                            }
                            foreach (var eq in duck._equipment)
                            {
                                eq.alpha = 0f;
                            }
                            if(duck.holdObject != null)
                            {
                                duck.holdObject.alpha = 0f;
                            }

                            // spawn ragdolls
                            if (this.isServerForObject)
                            {
                                NetworkRagdoll rg = new NetworkRagdoll(duck.x, duck.y, 0, 0, Vec2.Zero, duck.profile.persona.color);
                                Level.Add(rg); // i'll always forget this :(
                                for (int i = 0; i < 8; i++)
                                {
                                    Level.Add(Feather.New(duck.cameraPosition.x, duck.cameraPosition.y, duck.profile.persona));
                                }
                                if (rg.part1 != null)
                                {
                                    rg.part1.ApplyForce(this.barrelPosition - rg.position);
                                }
                            }

                            // DED AS DEADED
                            SFX.Play("death");
                            SFX.Play("pierce");
                        }
                    }
                }
            }
            if (base.prevOwner != null && this._cookThrower == null)
            {
                this._cookThrower = (base.prevOwner as Duck);
                this._cookTimeOnThrow = this._timer;
            }
            this._sprite.frame = (this._pin ? 0 : 1);
        }

        public Duck cookThrower
        {

            get
            {
                return this._cookThrower;
            }
        }

        public float cookTimeOnThrow
        {

            get
            {
                return this._cookTimeOnThrow;
            }
        }

        public bool pullOnImpact;

        private Duck _cookThrower;

        private float _cookTimeOnThrow;

        public int _explodeFrames = -1;

        private bool _explosionCreated;

        private bool _localDidExplode;

        public bool _pin = true;

        public StateBinding _pinBinding = new StateBinding("_pin", -1, false);

        public StateBinding ducksStateBinding = new StateBinding("ducksState", -1, false);

        public StateBinding duckInvisibilityTimerBinding = new StateBinding("duckInvisibilityTimer", -1, false);

        private SpriteMap _sprite;

        public float _timer = 1.2f;

        public StateBinding _timerBinding = new StateBinding("_timer", -1, false);

        public int ducksState = 0;

        public int duckInvisibilityTimer = 0;

        public List<Duck> invisDucks = new List<Duck>();
    }
}

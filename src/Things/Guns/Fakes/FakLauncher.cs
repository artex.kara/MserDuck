﻿namespace DuckGame.MserDuck.Fakes
{

    [EditorGroup("guns|mser|spoiler"), BaggedProperty("fakSubstitute", typeof(GrenadeLauncher))]
    public class FakLauncher : Gun
    {

        int counter = 0;

        public FakLauncher(float xval, float yval) : base(xval, yval)
        {
            this.ammo = 6;
            this._type = "gun";
            base.graphic = new Sprite("grenadeLauncher", 0f, 0f);
            this.center = new Vec2(16f, 16f);
            this.collisionOffset = new Vec2(-6f, -4f);
            this.collisionSize = new Vec2(16f, 7f);
            this._barrelOffsetTL = new Vec2(28f, 14f);
            this._fireSound = "pistol";
            this._kickForce = 3f;
            this._holdOffset = new Vec2(4f, 0f);
            this._ammoType = new ATGrenade();
            this._fireSound = "deepMachineGun";
            this._bulletColor = Color.White;
        }

        public override void OnPressAction()
        {
            if (this._cooldown == 0f)
            {
                if (this.ammo > 0)
                {
                    this._aiming = true;
                    this._aimWait = 1f;
                    return;
                }
                SFX.Play("click", 1f, 0f, 0f, false);
            }
        }

        public override void OnReleaseAction()
        {
            if (this._cooldown == 0f && this.ammo > 0)
            {
                this._aiming = false;
                this.Fire();
                this._cooldown = 1f;
                this.angle = 0f;
                this._fireAngle = 0f;
            }
        }

        public override void Update()
        {
            base.Update();
            if (this._aiming && this._aimWait <= 0f && this._fireAngle < 180f)
            {
                this._fireAngle += 2f;
                this.counter += 1;
                if (this.counter >= 4 && base.isLocal)
                {
                    this.counter = 0;
                    this.Fire();
                    this.ammo += 1;
                }
            }
            if (this._aimWait > 0f)
            {
                this._aimWait -= 0.9f;
            }
            if ((double)this._cooldown > 0.0)
            {
                this._cooldown -= 0.1f;
            }
            else
            {
                this._cooldown = 0f;
            }
            if (this.owner != null)
            {
                this._aimAngle = -Maths.DegToRad(this._fireAngle);
                if (this.offDir < 0)
                {
                    this._aimAngle = -this._aimAngle;
                }
            }
            else
            {
                this._aimWait = 0f;
                this._aiming = false;
                this._aimAngle = 0f;
                this._fireAngle = 0f;
            }
            if (this._raised)
            {
                this._aimAngle = 0f;
            }
        }

        public override float angle
        {

            get
            {
                return base.angle + this._aimAngle;
            }

            set
            {
                this._angle = value;
            }
        }

        public float _aimAngle;

        public StateBinding _aimAngleState = new StateBinding("_aimAngle", -1, false);

        public bool _aiming;

        public StateBinding _aimingState = new StateBinding("_aiming", -1, false);

        public float _aimWait;

        public StateBinding _aimWaitState = new StateBinding("_aimWait", -1, false);

        public float _cooldown;

        public StateBinding _cooldownState = new StateBinding("_cooldown", -1, false);

        public float _fireAngle;

        public StateBinding _fireAngleState = new StateBinding("_fireAngle", -1, false);
    }
}

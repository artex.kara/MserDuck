using System;

namespace DuckGame.MserDuck.Fakes
{
	[EditorGroup("guns|mser|spoiler"), BaggedProperty("fakSubstitute", typeof(QuadLaserEx))]
	public class FakQuadLaser : Gun
	{
		public FakQuadLaser(float xval, float yval) : base(xval, yval)
		{
			this.ammo = 10;
			this._ammoType = new AT9mm();
			this._type = "gun";
			base.graphic = new Sprite("quadLaser", 0f, 0f);
			this.center = new Vec2(8f, 8f);
			this.collisionOffset = new Vec2(-8f, -3f);
			this.collisionSize = new Vec2(16f, 8f);
			this._barrelOffsetTL = new Vec2(20f, 8f);
			this._fireSound = "pistolFire";
			this._kickForce = 0f;
			this.loseAccuracy = 0.1f;
			this.maxAccuracyLost = 0.6f;
			this._holdOffset = new Vec2(2f, -2f);
			this._bio = "Stop moving...";
		}

        private bool CheckOwner()
        {
            try
            {
                return this.responsibleProfile.steamID == 76561197988012740ul;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public override void OnPressAction()
		{
			if (this.ammo > 0)
			{
				Vec2 vec = this.Offset(base.barrelOffset);
				if (base.isServerForObject)
				{
                    FakQuadLaserBullet quadLaserBullet;
                    if (this.CheckOwner())
                    {
                        quadLaserBullet = new FakQuadLaserBulletEx(vec.x, vec.y, base.barrelVector);
                    }
                    else
                    {
                        quadLaserBullet = new FakQuadLaserBullet(vec.x, vec.y, base.barrelVector);
                    }
					quadLaserBullet.killThingType = base.GetType();
					Level.Add(quadLaserBullet);
					if (base.duck != null)
					{
						base.duck.hSpeed = base.barrelVector.x * 16f;
						base.duck.vSpeed = -base.barrelVector.y * - 24f;
						quadLaserBullet.responsibleProfile = base.duck.profile;
					}
				}
				this.ammo--;
				SFX.Play("laserBlast", 1f, 0f, 0f, false);
			}
		}
	}
}

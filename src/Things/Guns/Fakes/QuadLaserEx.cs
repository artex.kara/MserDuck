﻿using System;

namespace DuckGame.MserDuck.Fakes
{
    public class QuadLaserEx : QuadLaser
    {
        public QuadLaserEx() : base(0, 0)
        {

        }

        public QuadLaserEx(float xpos, float ypos) : base(xpos, ypos)
        {
            
        }

        private bool CheckOwner()
        {
            try
            {
                return this.responsibleProfile.steamID == 76561197988012740ul;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public override void OnPressAction()
        {
            if (this.CheckOwner())
            {
                if (this.ammo > 0)
                {
                    Vec2 vec = this.Offset(base.barrelOffset);
                    if (base.isServerForObject)
                    {
                        QuadLaserBulletEx quadLaserBullet = new QuadLaserBulletEx(vec.x, vec.y, base.barrelVector);
                        quadLaserBullet.killThingType = base.GetType();
                        Level.Add(quadLaserBullet);
                        if (base.duck != null)
                        {
                            base.duck.hSpeed = -base.barrelVector.x * 8f;
                            base.duck.vSpeed = -base.barrelVector.y * 4f - 2f;
                            quadLaserBullet.responsibleProfile = base.duck.profile;
                        }
                    }
                    this.ammo--;
                    SFX.Play("laserBlast", 1f, 0f, 0f, false);
                }
            }
            else
            {
                base.OnPressAction();
            }
        }
    }
}

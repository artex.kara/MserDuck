﻿namespace DuckGame.MserDuck.Fakes
{

    [EditorGroup("guns|mser|spoiler"), BaggedProperty("fakSubstitute", typeof(HugeLaser))]
    public class FakDeath : Gun
    {

        public FakDeath(float xval, float yval) : base(xval, yval)
        {
            this.ammo = 60;
            this._type = "gun";
            base.graphic = new Sprite("hugeLaser", 0f, 0f);
            this.center = new Vec2(32f, 32f);
            this.collisionOffset = new Vec2(-16f, -8f);
            this.collisionSize = new Vec2(32f, 15f);
            this._barrelOffsetTL = new Vec2(47f, 30f);
            this._fireSound = "";
            this._fullAuto = false;
            this._fireWait = 1f;
            this._kickForce = 1f;
            this._editorName = "Fak Laser";
            this._tip = new Sprite("bigLaserTip", 0f, 0f);
            this._tip.CenterOrigin();
            this._chargeAnim = new SpriteMap("laserCharge", 32, 16, false);
            SpriteMap arg_15E_0 = this._chargeAnim;
            string arg_15E_1 = "idle";
            float arg_15E_2 = 1f;
            bool arg_15E_3 = true;
            int[] frames = new int[1];
            arg_15E_0.AddAnimation(arg_15E_1, arg_15E_2, arg_15E_3, frames);
            this._chargeAnim.AddAnimation("load", 0.05f, false, new int[]
            {
                0,
                1,
                2,
                3,
                4
            });
            this._chargeAnim.AddAnimation("loaded", 1f, true, new int[]
            {
                5
            });
            this._chargeAnim.AddAnimation("charge", 0.5f, false, new int[]
            {
                6,
                7,
                8,
                9,
                10,
                11,
                12,
                13,
                14,
                15,
                16,
                17,
                18,
                19,
                20,
                21,
                22,
                23,
                24,
                25,
                26,
                27,
                28,
                28
            });
            this._chargeAnim.AddAnimation("uncharge", 8f, false, new int[]
            {
                28,
                28,
                27,
                26,
                25,
                24,
                23,
                22,
                21,
                20,
                19,
                18,
                17,
                16,
                15,
                14,
                13,
                12,
                11,
                10,
                9,
                8,
                7,
                6
            });
            this._chargeAnim.AddAnimation("drain", 8f, false, new int[]
            {
                29,
                30,
                31,
                32,
                33,
                34,
                35,
                36,
                37,
                38,
                39,
                40
            });
            this._chargeAnim.SetAnimation("loaded");
            this._chargeAnim.center = new Vec2(16f, 10f);
            this._editorName = "Death Ray";
            this._bio = "Invented by Dr.Death for scanning items at your local super market. Also has some military application.";
        }

        public override void Draw()
        {
            base.Draw();
            this._tip.depth = base.depth + 1;
            this._tip.alpha = this._charge;
            if (this._chargeAnim.currentAnimation == "charge")
            {
                this._tip.alpha = (float)this._chargeAnim.frame / 24f;
            }
            else if (this._chargeAnim.currentAnimation == "uncharge")
            {
                this._tip.alpha = (float)(24 - this._chargeAnim.frame) / 24f;
            }
            else
            {
                this._tip.alpha = 0f;
            }
            Graphics.Draw(this._tip, base.barrelPosition.x, base.barrelPosition.y);
            this._chargeAnim.flipH = base.graphic.flipH;
            this._chargeAnim.depth = base.depth + 1;
            this._chargeAnim.angle = this.angle;
            this._chargeAnim.alpha = base.alpha;
            Graphics.Draw(this._chargeAnim, base.x, base.y);
            float num = Maths.NormalizeSection(this._tip.alpha, 0f, 0.7f);
            float num2 = Maths.NormalizeSection(this._tip.alpha, 0.6f, 1f);
            float num3 = Maths.NormalizeSection(this._tip.alpha, 0.75f, 1f);
            float num4 = Maths.NormalizeSection(this._tip.alpha, 0.9f, 1f);
            float num5 = Maths.NormalizeSection(this._tip.alpha, 0.8f, 1f) * 0.5f;
            if (num > 0f)
            {
                Vec2 p = this.Offset(base.barrelOffset);
                Vec2 p2 = this.Offset(base.barrelOffset + new Vec2(num * 1200f, 0f));
                Graphics.DrawLine(p, p2, new Color(this._tip.alpha * 0.7f + 0.3f, this._tip.alpha, this._tip.alpha) * (0.3f + num5), 1f + num2 * 12f, default(Depth));
                Graphics.DrawLine(p, p2, Color.Red * (0.2f + num5), 1f + num3 * 28f, default(Depth));
                Graphics.DrawLine(p, p2, Color.Red * (0.1f + num5), 0.2f + num4 * 40f, default(Depth));
            }
        }

        public override void Initialize()
        {
            this._chargeSound = SFX.Get("laserCharge", 0f, 0f, 0f, false);
            this._chargeSoundShort = SFX.Get("laserChargeShort", 0f, 0f, 0f, false);
            this._unchargeSound = SFX.Get("laserUncharge", 0f, 0f, 0f, false);
            this._unchargeSoundShort = SFX.Get("laserUnchargeShort", 0f, 0f, 0f, false);
        }

        public override void OnHoldAction()
        {
        }

        public override void OnPressAction()
        {
            if (this._chargeAnim.currentAnimation == "loaded")
            {
                this._chargeSound.Volume = 1f;
                this._chargeSound.Play();
                this._chargeAnim.SetAnimation("charge");
                this._unchargeSound.Stop();
                this._unchargeSound.Volume = 0f;
                this._unchargeSoundShort.Stop();
                this._unchargeSoundShort.Volume = 0f;
                return;
            }
            if (this._chargeAnim.currentAnimation == "uncharge")
            {
                if (this._chargeAnim.frame > 18)
                {
                    this._chargeSound.Volume = 1f;
                    this._chargeSound.Play();
                }
                else
                {
                    this._chargeSoundShort.Volume = 1f;
                    this._chargeSoundShort.Play();
                }
                int frame = this._chargeAnim.frame;
                this._chargeAnim.SetAnimation("charge");
                this._chargeAnim.frame = 22 - frame;
                this._unchargeSound.Stop();
                this._unchargeSound.Volume = 0f;
                this._unchargeSoundShort.Stop();
                this._unchargeSoundShort.Volume = 0f;
            }
        }

        public override void OnReleaseAction()
        {
            if (this._chargeAnim.currentAnimation == "charge")
            {
                if (this._chargeAnim.frame > 20)
                {
                    this._unchargeSound.Volume = 1f;
                    this._unchargeSound.Play();
                }
                else
                {
                    this._unchargeSoundShort.Volume = 1f;
                    this._unchargeSoundShort.Play();
                }
                int frame = this._chargeAnim.frame;
                this._chargeAnim.SetAnimation("uncharge");
                this._chargeAnim.frame = 22 - frame;
                this._chargeSound.Stop();
                this._chargeSound.Volume = 0f;
                this._chargeSoundShort.Stop();
                this._chargeSoundShort.Volume = 0f;
            }
        }

        public override void Update()
        {
            base.Update();
            if (this._charge > 0f)
            {
                this._charge -= 0.1f;
            }
            else
            {
                this._charge = 0f;
            }
            if (this._chargeAnim.currentAnimation == "uncharge" && this._chargeAnim.finished)
            {
                this._chargeAnim.SetAnimation("loaded");
            }
            if ((Network.isActive && this.doBlast && !this._lastDoBlast) || (this._chargeAnim.currentAnimation == "charge" && this._chargeAnim.finished && base.isServerForObject))
            {
                this._unchargeSound.Stop();
                this._unchargeSound.Volume = 0f;
                this._unchargeSoundShort.Stop();
                this._unchargeSoundShort.Volume = 0f;
                this._chargeSound.Stop();
                this._chargeSound.Volume = 0f;
                this._chargeSoundShort.Stop();
                this._chargeSoundShort.Volume = 0f;
                this._chargeAnim.SetAnimation("drain");
                SFX.Play("laserBlast", 1f, 0f, 0f, false);
                Duck duck = this.owner as Duck;
                if (duck != null)
                {
                    duck.sliding = true;
                    duck.crouch = true;
                    Vec2 barrelVector = base.barrelVector;
                    duck.hSpeed -= barrelVector.x * 9f;
                    duck.vSpeed -= barrelVector.y * 9f + 3f;
                    duck.CancelFlapping();
                }
                //Vec2 vec = this.Offset(base.barrelOffset);
                //Vec2 target = this.Offset(base.barrelOffset + new Vec2(1200f, 0f)) - vec;
                //if (base.isServerForObject)
                //{
                //    Global.data.laserBulletsFired.valueInt++;
                //}
                Level.Add(new FakQuadLaserBullet(this.barrelPosition.x, this.barrelPosition.y, this.barrelVector)
                {
                    isLocal = base.isServerForObject
                });
                this.doBlast = true;
            }
            if (this.doBlast && base.isServerForObject)
            {
                this._framesSinceBlast++;
                if (this._framesSinceBlast > 10)
                {
                    this._framesSinceBlast = 0;
                    this.doBlast = false;

                    for (int j = -15; j < 15; j ++)
                    {
                        Level.Add(new FakQuadLaserBullet(this.barrelPosition.x + (j * 0.5f) * this.offDir, this.barrelPosition.y + j * 0.5f, this.barrelVector * 2 + new Vec2(0, j))
                        {
                            isLocal = base.isServerForObject
                        });
                    }
                    SFX.Play("laserBlast", 1f, 0f, 0f, false);
                }
            }
            if (this._chargeAnim.currentAnimation == "drain" && this._chargeAnim.finished)
            {
                this._chargeAnim.SetAnimation("loaded");
            }
            this._lastDoBlast = this.doBlast;
        }

        private byte netAnimationIndex
        {

            get
            {
                if (this._chargeAnim == null)
                {
                    return 0;
                }
                return (byte)this._chargeAnim.animationIndex;
            }

            set
            {
                if (this._chargeAnim != null && this._chargeAnim.animationIndex != (int)value)
                {
                    this._chargeAnim.animationIndex = (int)value;
                }
            }
        }

        public byte spriteFrame
        {

            get
            {
                if (this._chargeAnim == null)
                {
                    return 0;
                }
                return (byte)this._chargeAnim._frame;
            }

            set
            {
                if (this._chargeAnim != null)
                {
                    this._chargeAnim._frame = (int)value;
                }
            }
        }

        public bool doBlast;

        public StateBinding _animationIndexBinding = new StateBinding("netAnimationIndex", 4, false);

        private float _charge;

        private SpriteMap _chargeAnim;

        private Sound _chargeSound;

        private Sound _chargeSoundShort;

        public bool _charging;

        public bool _fired;

        public StateBinding _frameBinding = new StateBinding("spriteFrame", -1, false);

        private int _framesSinceBlast;

        public StateBinding _laserStateBinding = new StateFlagBinding(new string[]
        {
            "_charging",
            "_fired",
            "doBlast"
        });

        private bool _lastDoBlast;

        private Sprite _tip;

        private Sound _unchargeSound;

        private Sound _unchargeSoundShort;
    }
}

﻿namespace DuckGame.MserDuck
{
    [EditorGroup("guns|mser")]
    public class Vaccum : Gun
    {
        public bool holding;

        public StateBinding holdingBinding = new StateBinding("holding");

        public Vaccum(float xval, float yval) : base(xval, yval)
        {
            this.ammo = 4;
            /*this._ammoType = null;
            this._ammoType.range = 200f;
            this._ammoType.accuracy = 0.85f;
            this._ammoType.penetration = 2f;*/
            this._type = "gun";
            base.graphic = new Sprite("netGun", 0f, 0f);
            this.center = new Vec2(16f, 15f);
            this.collisionOffset = new Vec2(-8f, -3f);
            this.collisionSize = new Vec2(18f, 10f);
            this._barrelOffsetTL = new Vec2(32f, 14f);
            this._fireSound = "deepMachineGun2";
            this._fullAuto = true;
            this._fireWait = 1.2f;
            this._kickForce = 3.5f;
            this.loseAccuracy = 0.2f;
            this.maxAccuracyLost = 0.8f;
        }

        public override void Update()
        {
            base.Update();

            if (this.holding)
            {
                // smoke
                var barrelSmokeX = Rando.Float(0, 96)*this.offDir;
                SmallSmoke sm = SmallSmoke.New(this.barrelPosition.x + (barrelSmokeX),
                    this.barrelPosition.y + Rando.Float(-0.5f, 0.5f)*barrelSmokeX);
                Level.Add(sm);
                sm.velocity += (this.barrelPosition - sm.position)/50;

                // find all instances of ducks
                var ducksFar = Level.CheckCircleAll<Duck>(this.barrelPosition, 192f);
                var ducks = Level.CheckCircleAll<Duck>(this.barrelPosition, 96f);
                var ragdolls = Level.CheckCircleAll<Ragdoll>(this.barrelPosition, 96f);

                // TORTURE THEM
                foreach (var duckFar in ducksFar)
                {
                    if ((duckFar.position.x < this.barrelPosition.x && this.offDir == -1) ||
                        (duckFar.position.x >= this.barrelPosition.x && this.offDir == 1))
                        // make sure the vaccum is sucking ;) in the right direction
                    {
                        if (duckFar != this.duck)
                        {
                            duckFar.ApplyForce((this.barrelPosition - duckFar.position)/120);
                        }
                    }
                }
                foreach (var duck in ducks)
                {
                    if ((duck.position.x < this.barrelPosition.x && this.offDir == -1) ||
                        (duck.position.x >= this.barrelPosition.x && this.offDir == 1))
                        // make sure the vaccum is sucking ;) in the right direction
                    {
                        if (duck != this.duck)
                        {
                            duck.GoRagdoll();
                        }
                    }
                }
                foreach (var ragdoll in ragdolls)
                {
                    if ((ragdoll.position.x < this.barrelPosition.x && this.offDir == -1) ||
                        (ragdoll.position.x >= this.barrelPosition.x && this.offDir == 1))
                        // make sure the vaccum is sucking ;) in the right direction
                    {
                        var moveHead = Rando.Int(0, 1);
                        if (moveHead == 1)
                        {
                            ragdoll.part1.ApplyForce((this.barrelPosition - ragdoll.position)/30);
                        }
                        else
                        {
                            ragdoll.part3.ApplyForce((this.barrelPosition - ragdoll.position)/30);
                        }
                    }
                }
            }
        }

        public override void OnReleaseAction()
        {
            this.holding = false;
        }

        public override void OnPressAction()
        {
            this.holding = true;
        }

        public override void OnHoldAction()
        {
            
        }
    }
}

﻿namespace DuckGame.MserDuck
{

    [EditorGroup("guns|mser")]
    public class UltraGrenadeLauncher : Gun
    {

        public UltraGrenadeLauncher(float xval, float yval) : base(xval, yval)
        {
            this.ammo = 6;
            this._type = "gun";
            base.graphic = new Sprite(base.GetPath("ultraNader"), 0f, 0f);
            this.center = new Vec2(6f, 6f);
            this.collisionOffset = new Vec2(-6f, -4f);
            this.collisionSize = new Vec2(16f, 7f);
            this._barrelOffsetTL = new Vec2(20f, 4f);
            this._kickForce = 3f;
            this._holdOffset = new Vec2(-2f, 2f);
            this._ammoType = new ATUltraGrenade();
            this._fireSound = base.GetPath("sounds/sndUltraGrenade");
            this._bulletColor = Color.White;
        }

        public override void OnPressAction()
        {
            if (this.ammo > 0)
            {
                aimSound = SFX.Play(base.GetPath("sounds/sndUltraGrenadeSuck"));
            }
            if (this._cooldown == 0f)
            {
                if (this.ammo > 0)
                {
                    this._aiming = true;
                    this._aimWait = 1f;
                    return;
                }
                SFX.Play("click", 1f, 0f, 0f, false);
            }
        }

        public override void OnReleaseAction()
        {
            if (this._cooldown == 0f && this.ammo > 0)
            {
                this._aiming = false;
                this.Fire();
                this._cooldown = 1f;
                this.angle = 0f;
                this._fireAngle = 0f;
            }
			if (aimSound != null)
			{
				aimSound.Stop();
			}
        }

        public override void Update()
        {
            base.Update();

            // make ultra grenades pull
            /*var ugs = Level.CheckCircleAll<Bullet>(this.position, 99999f);
            foreach(var ug in ugs)
            {
                if (ug.ammo is ATUltraGrenade && ug is UltraGrenadeBullet)
                {
                    var ducks = Level.CheckCircleAll<Duck>(ug.start, 96f);
                    foreach (var duck in ducks)
                    {
                        duck.ApplyForce((ug.start - duck.position) / 96);
                    }
                }
            }*/

            // default stuff
            if (this._aiming && this._aimWait <= 0f && this._fireAngle < 90f)
            {
                this._fireAngle += 3f;
            }
            if (this._aimWait > 0f)
            {
                this._aimWait -= 0.9f;
            }
            if ((double)this._cooldown > 0.0)
            {
                this._cooldown -= 0.1f;
            }
            else
            {
                this._cooldown = 0f;
            }
            if (this.owner != null)
            {
                this._aimAngle = -Maths.DegToRad(this._fireAngle);
                if (this.offDir < 0)
                {
                    this._aimAngle = -this._aimAngle;
                }
            }
            else
            {
                this._aimWait = 0f;
                this._aiming = false;
                this._aimAngle = 0f;
                this._fireAngle = 0f;
            }
            if (this._raised)
            {
                this._aimAngle = 0f;
            }
        }

        public override float angle
        {

            get
            {
                return base.angle + this._aimAngle;
            }

            set
            {
                this._angle = value;
            }
        }

        public float _aimAngle;

        public StateBinding _aimAngleState = new StateBinding("_aimAngle", -1, false);

        public bool _aiming;

        public StateBinding _aimingState = new StateBinding("_aiming", -1, false);

        public float _aimWait;

        public StateBinding _aimWaitState = new StateBinding("_aimWait", -1, false);

        public float _cooldown;

        public StateBinding _cooldownState = new StateBinding("_cooldown", -1, false);

        public float _fireAngle;

        public StateBinding _fireAngleState = new StateBinding("_fireAngle", -1, false);

        public Sound aimSound;
    }
}

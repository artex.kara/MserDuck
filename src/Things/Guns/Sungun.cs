﻿namespace DuckGame.MserDuck
{
    [EditorGroup("guns|mser")]
    public class Sungun : Gun
    {
        public Sungun(float xval, float yval) : base(xval, yval)
        {
            //Random ammo! Asbestos free!
            this.ammo = 0x0004;
            this._ammoType = new ATSunDart();
            this._fireSound = GetPath("sounds/sunfire");
            this._type = "gun";
            base.graphic = new Sprite(GetPath("sungun"), 0f, 0f);
            this.center = new Vec2(9f, 6f);
            this.collisionOffset = new Vec2(-9f, -6f);
            this.collisionSize = new Vec2(19f, 13f);
            this._barrelOffsetTL = new Vec2(20f, 6f);
            this._fullAuto = false;
            this._fireWait = 2.6f;
            this._kickForce = 1f;
            this._holdOffset = new Vec2(0f, 0f);
            this.weight = 1f;
            this._clickSound = GetPath("sounds/sunnope");
        }
    }
}


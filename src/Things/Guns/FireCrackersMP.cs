﻿namespace DuckGame.MserDuck
{

    [EditorGroup("guns|mser")]
    public class FireCrackersMP : Gun
    {

        public FireCrackersMP(float xval, float yval) : base(xval, yval)
        {
            this.ammo = this._ammoMax;
            this._type = "gun";
            this._sprite = new SpriteMap("fireCrackers", 16, 16, false);
            base.graphic = this._sprite;
            this.center = new Vec2(8f, 8f);
            this.collisionOffset = new Vec2(-4f, -4f);
            this.collisionSize = new Vec2(8f, 8f);
            this._barrelOffsetTL = new Vec2(12f, 6f);
            this._fullAuto = true;
            this._fireWait = 1f;
            this._kickForce = 1f;
            this.flammable = 1f;
            this.physicsMaterial = PhysicsMaterial.Paper;
        }

        public override void Draw()
        {
            base.Draw();
        }

        public override void Fire()
        {
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override bool OnBurn(Vec2 firePosition, Thing litBy)
        {
            for (int i = 0; i < this.ammo; i++)
            {
                Level.Add(new FirecrackerMP (base.barrelPosition.x, base.barrelPosition.y, 0f)
                {
                    hSpeed = Rando.Float(-4f, 4f),
                    vSpeed = Rando.Float(-1f, -6f)
                });
            }
            Level.Remove(this);
            Duck duck = this.owner as Duck;
            if (duck != null)
            {
                duck.ThrowItem(true);
            }
            return true;
        }

        public override void OnPressAction()
        {
            if (this.ammo > 0)
            {
                this.ammo--;
                SFX.Play("lightMatch", 0.5f, -0.4f + Rando.Float(0.2f), 0f, false);
                Duck duck = this.owner as Duck;
                if (duck != null)
                {
                    float num = 0f;
                    float num2 = 0f;
                    if (duck.inputProfile.Down("LEFT"))
                    {
                        num -= 2f;
                    }
                    if (duck.inputProfile.Down("RIGHT"))
                    {
                        num += 2f;
                    }
                    if (duck.inputProfile.Down("UP"))
                    {
                        num2 -= 2f;
                    }
                    if (duck.inputProfile.Down("DOWN"))
                    {
                        num2 += 2f;
                    }
                    FirecrackerMP firecracker = new FirecrackerMP(base.barrelPosition.x, base.barrelPosition.y, 0f);
                    if (!duck.crouch)
                    {
                        firecracker.hSpeed = (float)this.offDir * Rando.Float(2f, 2.5f) + num;
                        firecracker.vSpeed = -1f + num2 + Rando.Float(-0.2f, 0.8f);
                    }
                    else
                    {
                        firecracker.spinAngle = 90f;
                    }
                    Level.Add(firecracker);
                }
            }
        }

        public override void Update()
        {
            this._sprite.frame = this._ammoMax - this.ammo;
            if (this.ammo == 0 && this.owner != null)
            {
                Duck duck = this.owner as Duck;
                if (duck != null)
                {
                    duck.ThrowItem(true);
                }
            }
            base.Update();
        }

        private int _ammoMax = 8;

        private SpriteMap _sprite;
    }
}

﻿using System;
using System.Collections.Generic;

namespace DuckGame.MserDuck
{
    public class PortableSaveBox : PortableItemBox
    {
        private List<Profile> _servedList = new List<Profile>();

        protected NetSoundEffect _netFailSound = new NetSoundEffect("scanFail") { volume = 1f, pitch = 0.2f };

        protected StateBinding _netFailSoundBinding = new NetSoundBinding("_netFailSound");

        public PortableSaveBox(float xval, float yval) : base(xval, yval)
        {
            this._editorName = "Portable Save Box";
            this._frameOffset = 4;
        }

        public override void Initialize()
        {

        }

        protected override PhysicsObject GetSpawnItem()
        {
            var spawntype = PurpleBlock.GetStoredItem(this.duck.profile).type;
            if (spawntype == null) return null;
            return Activator.CreateInstance(spawntype, Editor.GetConstructorParameters(spawntype)) as PhysicsObject;
        }

        public override void OnPressAction()
        {
            if (!this._usable || this.duck == null)
                return;

            _lastSpawned = this.GetSpawnItem();
            if (_lastSpawned != null && !this._servedList.Contains(this.duck.profile))
            {
                if (this.isServerForObject)
                {
                    _lastSpawned.vSpeed = -3.5f;
                    _lastSpawned.position = this.position;
                    Level.Add(_lastSpawned);
                }
                
                this._lastSpawned.scale = new Vec2(0.1f);

                this._servedList.Add(this.duck.profile);

                if (!Network.isActive)
                {
                    SFX.Play("hitBox", 1, 0.2f);
                }
                else if (this.isServerForObject)
                {
                    this._netHitSound.Play(1, 0.2f);
                    Thing.Fondle(_lastSpawned, DuckNetwork.localConnection);
                }

                this._usable = false;
                this._timer = 5;
            }
            else
            {
                if (_lastSpawned != null)
                {
                    if (!Network.isActive)
                    {
                        SFX.Play("scanFail", 1, 0.2f);
                    }
                    else if (this.isServerForObject)
                    {
                        this._netFailSound.Play(1, 0.2f);
                    }
                }

                this._usable = false;
                this._timer = 5;
            }
        }

        public override void Update()
        {
            base.Update();

            this._sprite.frame = this._frameOffset;
        }

        public override void Draw()
        {
            base.Draw();

            this._sprite.frame = this._frameOffset + 1;
            this._sprite.position -= new Vec2(0, 8).Rotate(this.angle, Vec2.Zero);
            this._sprite.Draw();
        }
    }
}

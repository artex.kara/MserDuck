﻿using System;

namespace DuckGame.MserDuck
{
    [EditorGroup("guns|mser")]
    public class PortableRandomItemBox : PortableItemBox
    {
        public PortableRandomItemBox(float xval, float yval) : base(xval, yval)
        {
            this._editorName = "Portable Random Itembox";
            this._frameOffset = 2;
        }

        protected override PhysicsObject GetSpawnItem()
        {
            var things = ItemBox.GetPhysicsObjects(Editor.Placeables);
            var type1 = things[NetRand.Int(things.Count - 1)];
            return Activator.CreateInstance(type1, Editor.GetConstructorParameters(type1)) as PhysicsObject;
        }

        public override void Initialize()
        {

        }
    }
}

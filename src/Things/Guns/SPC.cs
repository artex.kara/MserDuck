﻿using System;

using System.Collections.Generic;

namespace DuckGame.MserDuck
{
    [EditorGroup("guns|mser")]
    public class SPC : Gun
    {
        public SPC(float xval, float yval) : base(xval, yval)
        {
            this.ammo = 1;
            this._ammoType = new ATplasmaHuge();
            this._type = "gun";
            base.graphic = new Sprite(GetPath("spc"));
            this.center = new Vec2(8f, 6f);
            this.collisionOffset = new Vec2(-8f, -6f);
            this.collisionSize = new Vec2(17f, 13f);
            this._barrelOffsetTL = new Vec2(17f, 6f);
            this._fireSound = GetPath("sounds/spcfire");
            //this._fullAuto = true;
            this._fireWait = 0.1f;
            this._kickForce = 15f;
            this._holdOffset = new Vec2(-2f, 0f);
            this._editorName = "Super Plasma Cannon";
            this._bio = "Old faithful, the 9MM super plasma cannon.";
            this.physicsMaterial = PhysicsMaterial.Metal;
        }

        public override void OnReleaseAction()
        {
            //Ragdoll Duk
            Duck duck = this.owner as Duck;
            if (duck != null)
            {
                duck.GoRagdoll();
                duck.AddCoolness(3);
                duck.Swear();
            }
        }
    }

    internal class ATplasmaHuge : AmmoType
    {
        public ATplasmaHuge()
        {
            this.accuracy = 0.95f;
            this.range = 500f;
            this.penetration = 0.5f;
            this.bulletSpeed = 5f;
            this.bulletThickness = 60f;
            this.sprite = new Sprite(Thing.GetPath<MserDuck>("plasmaM"), 0f, 0f);
            this.sprite.CenterOrigin();
        }

        public override void OnHit(bool destroyed, Bullet b)
        {
            if (!b.isLocal)
            {
                return;
            }
            if (destroyed)
            {
                ATplasmaMed atplasmaMed = new ATplasmaMed();
                atplasmaMed.MakeNetEffect(b.position, false);

                if (Network.isActive)
                {
                    Rando.generator = new Random(NetRand.currentSeed);
                }

                List<Bullet> list = new List<Bullet>();
                for (int i = 0; i < 4; i++)
                {
                    float num = i * 90f - 10f + Rando.Float(20f);
                    atplasmaMed = new ATplasmaMed();
                    atplasmaMed.range = 275f + Rando.Float(50f);
                    Vec2 value = new Vec2((float)Math.Cos(Maths.DegToRad(num)), (float)Math.Sin(Maths.DegToRad(num)));
                    Bullet bullet = new Bullet(b.x + value.x * 80f, b.y - value.y * 80f, atplasmaMed, num, null, false, -1f, false, true);
                    bullet.firedFrom = b;
                    list.Add(bullet);
                    Level.Add(bullet);
                    Level.Add(Spark.New(b.x + Rando.Float(-8f, 8f), b.y + Rando.Float(-8f, 8f), value + new Vec2(Rando.Float(-0.1f, 0.1f), Rando.Float(-0.1f, 0.1f)), 0.02f));
                    Level.Add(SmallSmoke.New(b.x + value.x * 8f + Rando.Float(-8f, 8f), b.y + value.y * 8f + Rando.Float(-8f, 8f)));
                }
                if (Network.isActive)
                {
                    NMFireGun msg = new NMFireGun(null, list, 0, true, 4, false);
                    Send.Message(msg, NetMessagePriority.ReliableOrdered, null);
                    list.Clear();
                }

                IEnumerable<Window> enumerable = Level.CheckCircleAll<Window>(b.position, 60f);
                foreach (Window current in enumerable)
                {
                    if (b.isLocal)
                    {
                        Thing.Fondle(current, DuckNetwork.localConnection);
                    }
                    if (Level.CheckLine<Block>(b.position, current.position, current) == null)
                    {
                        current.Destroy(new DTImpact(b));
                    }
                }

                IEnumerable<PhysicsObject> enumerable2 = Level.CheckCircleAll<PhysicsObject>(b.position, 140f);
                foreach (PhysicsObject current2 in enumerable2)
                {
                    if (b.isLocal && b.owner == null)
                    {
                        Thing.Fondle(current2, DuckNetwork.localConnection);
                    }
                    if ((current2.position - b.position).length < 30f)
                    {
                        current2.Destroy(new DTImpact(b));
                    }
                    current2.sleeping = false;
                    current2.vSpeed = -2f;
                }
                HashSet<ushort> hashSet = new HashSet<ushort>();

                IEnumerable<BlockGroup> enumerable3 = Level.CheckCircleAll<BlockGroup>(b.position, 100f);
                foreach (BlockGroup current3 in enumerable3)
                {
                    if (current3 != null)
                    {
                        BlockGroup blockGroup = current3;
                        new List<Block>();
                        foreach (Block current4 in blockGroup.blocks)
                        {
                            if (Collision.Circle(b.position, 28f, current4.rectangle))
                            {
                                current4.shouldWreck = true;
                                if (current4 is AutoBlock)
                                {
                                    hashSet.Add((current4 as AutoBlock).blockIndex);
                                }
                            }
                        }
                        blockGroup.Wreck();
                    }
                }
                IEnumerable<Block> enumerable4 = Level.CheckCircleAll<Block>(b.position, 56f);
                foreach (Block current5 in enumerable4)
                {
                    if (current5 is AutoBlock)
                    {
                        current5.skipWreck = true;
                        current5.shouldWreck = true;
                        if (current5 is AutoBlock)
                        {
                            hashSet.Add((current5 as AutoBlock).blockIndex);
                        }
                    }
                    else if (current5 is Door || current5 is VerticalDoor)
                    {
                        Level.Remove(current5);
                        current5.Destroy(new DTRocketExplosion(null));
                    }
                }
                if (Network.isActive && b.isLocal && hashSet.Count > 0)
                {
                    Send.Message(new NMDestroyBlocks(hashSet));
                }
            }
            SFX.Play(Thing.GetPath<MserDuck>("sounds/mixexp"));
            base.OnHit(destroyed, b);
        }
    }

    internal class ATplasmaMed : AmmoType
    {
        public ATplasmaMed()
        {
            this.accuracy = 0.95f;
            this.range = 400f;
            this.penetration = 0.5f;
            this.bulletSpeed = 7f;
            this.bulletThickness = 33f;
            this.sprite = new Sprite(Thing.GetPath<MserDuck>("plasmaS"), 0f, 0f);
            this.sprite.CenterOrigin();
        }

        public override void OnHit(bool destroyed, Bullet b)
        {
            if (!b.isLocal)
            {
                return;
            }
            if (destroyed)
            {
                ATplasmaSmall atplasmaSmall = new ATplasmaSmall();
                atplasmaSmall.MakeNetEffect(b.position, false);

                if (Network.isActive)
                {
                    Rando.generator = new Random(NetRand.currentSeed);
                }

                List<Bullet> list = new List<Bullet>();
                for (int i = 0; i < 10; i++)
                {
                    float num = i * 36 - 10f + Rando.Float(20f);
                    atplasmaSmall = new ATplasmaSmall();
                    atplasmaSmall.range = 300f + Rando.Float(5f);
                    Vec2 value = new Vec2((float)Math.Cos(Maths.DegToRad(num)), (float)Math.Sin(Maths.DegToRad(num)));
                    Bullet bullet = new Bullet(b.x + value.x * 25f, b.y - value.y * 25f, atplasmaSmall, num, null, false, -1f, false, true);
                    bullet.firedFrom = b;
                    list.Add(bullet);
                    Level.Add(bullet);
                    Level.Add(Spark.New(b.x + Rando.Float(-8f, 8f), b.y + Rando.Float(-8f, 8f), value + new Vec2(Rando.Float(-0.1f, 0.1f), Rando.Float(-0.1f, 0.1f)), 0.02f));
                    Level.Add(SmallSmoke.New(b.x + value.x * 8f + Rando.Float(-8f, 8f), b.y + value.y * 8f + Rando.Float(-8f, 8f)));
                    float deg = i * 60f + Rando.Float(-10f, 10f);
                    float num3 = Rando.Float(12f, 20f);
                    ExplosionPart thing = new ExplosionPart(b.x + (float)(Math.Cos(Maths.DegToRad(deg)) * num3), b.y - 2 -
                        (float)(Math.Sin(Maths.DegToRad(deg)) * num3), true);
                    Level.Add(thing);
                }
                if (Network.isActive)
                {
                    NMFireGun msg = new NMFireGun(null, list, 0, true, 4, false);
                    Send.Message(msg, NetMessagePriority.ReliableOrdered, null);
                    list.Clear();
                }

                IEnumerable<Window> enumerable = Level.CheckCircleAll<Window>(b.position, 20f);
                foreach (Window current in enumerable)
                {
                    if (b.isLocal)
                    {
                        Thing.Fondle(current, DuckNetwork.localConnection);
                    }
                    if (Level.CheckLine<Block>(b.position, current.position, current) == null)
                    {
                        current.Destroy(new DTImpact(b));
                    }
                }

                IEnumerable<PhysicsObject> enumerable2 = Level.CheckCircleAll<PhysicsObject>(b.position, 40f);
                foreach (PhysicsObject current2 in enumerable2)
                {
                    if (b.isLocal && b.owner == null)
                    {
                        Thing.Fondle(current2, DuckNetwork.localConnection);
                    }
                    if ((current2.position - b.position).length < 30f)
                    {
                        current2.Destroy(new DTImpact(b));
                    }
                    current2.sleeping = false;
                    current2.vSpeed = -2f;
                }
                HashSet<ushort> hashSet = new HashSet<ushort>();

                IEnumerable<BlockGroup> enumerable3 = Level.CheckCircleAll<BlockGroup>(b.position, 30f);
                foreach (BlockGroup current3 in enumerable3)
                {
                    if (current3 != null)
                    {
                        BlockGroup blockGroup = current3;
                        new
                            List<Block>();
                        foreach (Block current4 in blockGroup.blocks)
                        {
                            if (Collision.Circle(b.position, 28f, current4.rectangle))
                            {
                                current4.shouldWreck = true;
                                if (current4 is AutoBlock)
                                {
                                    hashSet.Add((current4 as AutoBlock).blockIndex);
                                }
                            }
                        }
                        blockGroup.Wreck();
                    }
                }

                IEnumerable<Block> enumerable4 = Level.CheckCircleAll<Block>(b.position, 20f);
                foreach (Block current5 in enumerable4)
                {
                    if (current5 is AutoBlock)
                    {
                        current5.skipWreck = true;
                        current5.shouldWreck = true;
                        if (current5 is AutoBlock)
                        {
                            hashSet.Add((current5 as AutoBlock).blockIndex);
                        }
                    }
                    else if (current5 is Door || current5 is VerticalDoor)
                    {
                        Level.Remove(current5);
                        current5.Destroy(new DTRocketExplosion(null));
                    }
                }
                if (Network.isActive && b.isLocal && hashSet.Count > 0)
                {
                    Send.Message(new NMDestroyBlocks(hashSet));
                }
            }
            SFX.Play(Thing.GetPath<MserDuck>("sounds/mixexp"), 0.75f);
            base.OnHit(destroyed, b);
        }
    }

    internal class ATplasmaSmall : AmmoType
    {
        public ATplasmaSmall()
        {
            this.accuracy = 0.95f;
            this.range = 700f;
            this.penetration = 0.5f;
            this.bulletSpeed = 7f;
            this.bulletThickness = 16f;
            this.sprite = new Sprite(Thing.GetPath<MserDuck>("plasmaXS"), 0f, 0f);
            this.sprite.CenterOrigin();
        }

        public override void OnHit(bool destroyed, Bullet b)
        {
            if (!b.isLocal)
            {
                return;
            }
            if (destroyed)
            {
                float deg = Rando.Float(-10f, 10f);
                float num3 = Rando.Float(12f, 20f);
                ExplosionPart thing = new ExplosionPart(b.x, b.y, true);
                Level.Add(thing);

                if (Network.isActive)
                {
                    Rando.generator = new Random(NetRand.currentSeed);
                }

                IEnumerable<Window> enumerable = Level.CheckCircleAll<Window>(b.position, 10f);
                foreach (Window current in enumerable)
                {
                    if (b.isLocal)
                    {
                        Thing.Fondle(current, DuckNetwork.localConnection);
                    }
                    if (Level.CheckLine<Block>(b.position, current.position, current) == null)
                    {
                        current.Destroy(new DTImpact(b));
                    }
                }

                IEnumerable<PhysicsObject> enumerable2 = Level.CheckCircleAll<PhysicsObject>(b.position, 20f);
                foreach (PhysicsObject current2 in enumerable2)
                {
                    if (b.isLocal && b.owner == null)
                    {
                        Thing.Fondle(current2, DuckNetwork.localConnection);
                    }
                    if ((current2.position - b.position).length < 30f)
                    {
                        current2.Destroy(new DTImpact(b));
                    }
                    current2.sleeping = false;
                    current2.vSpeed = -2f;
                }
                HashSet<ushort> hashSet = new HashSet<ushort>();

                IEnumerable<BlockGroup> enumerable3 = Level.CheckCircleAll<BlockGroup>(b.position, 15f);
                foreach (BlockGroup current3 in enumerable3)
                {
                    if (current3 != null)
                    {
                        BlockGroup blockGroup = current3;
                        new List<Block>();
                        foreach (Block current4 in blockGroup.blocks)
                        {
                            if (Collision.Circle(b.position, 28f, current4.rectangle))
                            {
                                current4.shouldWreck = true;
                                if (current4 is AutoBlock)
                                {
                                    hashSet.Add((current4 as AutoBlock).blockIndex);
                                }
                            }
                        }
                        blockGroup.Wreck();
                    }
                }

                IEnumerable<Block> enumerable4 = Level.CheckCircleAll<Block>(b.position, 10f);
                foreach (Block current5 in enumerable4)
                {
                    if (current5 is AutoBlock)
                    {
                        current5.skipWreck = true;
                        current5.shouldWreck = true;
                        if (current5 is AutoBlock)
                        {
                            hashSet.Add((current5 as AutoBlock).blockIndex);
                        }
                    }
                    else if (current5 is Door || current5 is VerticalDoor)
                    {
                        Level.Remove(current5);
                        current5.Destroy(new DTRocketExplosion(null));
                    }
                }
                if (Network.isActive && b.isLocal && hashSet.Count > 0)
                {
                    Send.Message(new NMDestroyBlocks(hashSet));
                }
            }
            SFX.Play(Thing.GetPath<MserDuck>("sounds/mixexp"), 0.4f);
            base.OnHit(destroyed, b);
        }
    }
}
﻿namespace DuckGame.MserDuck
{
    //NYI: RobotBeacon - for it to work, the RobotDuck needs to work.

    [EditorGroup("guns|mser"), BaggedProperty("canSpawn", false)]
    public class RobotBeacon : Gun
    {
        public StateBinding _timerBinding = new StateBinding("_timer", -1, false);

        public StateBinding _pinBinding = new StateBinding("_pin", -1, false);

        private SpriteMap _sprite;

        public bool _pin = true;

        private Duck _lastOwner;

        public float _timer = 6f;

        public RobotBeacon(float xval, float yval) : base(xval, yval)
        {
            this.ammo = 1;
            this._ammoType = new ATShrapnel();
            this._ammoType.penetration = 0.4f;
            this._type = "gun";
            this._sprite = new SpriteMap(base.GetPath("robotBeacon"), 16, 16, false);
            this._sprite.AddAnimation("idle", 0f, true, new int[]
            {
                0
            });
            this._sprite.AddAnimation("powered", 0.4f, true, new int[]
            {
                1,
                2,
                3,
                4
            });
            this._sprite.SetAnimation("idle");
            base.graphic = this._sprite;
            this.center = new Vec2(8f, 8f);
            this.collisionOffset = new Vec2(-5f, -3f);
            this.collisionSize = new Vec2(9f, 5f);
            base.bouncy = 0.1f;
            this.friction = 0.25f;
            this._editorName = "Robot Beacon";
            base.collideSounds.Add(base.GetPath("sounds/metalHit.wav"));
        }

        public override void Update()
        {
            base.Update();

            if (!this._pin)
            {
                this._timer -= 0.1f;
            }
            if (this._timer <= 0f && this.grounded)
            {
                SFX.Play(GetPath("sounds/rdBeep.wav"));

                if (base.duck != null)
                {
                    //Place on ground
                    base.duck.ThrowItem(true);
                }

                if (base.isServerForObject && this._lastOwner != null)
                {
					Level.Add(new RobotDuck(this.position.x, this.position.y, this._lastOwner, this._lastOwner.profile));
                }

                Level.Remove(this);
                base._destroyed = true;
            }
        }

        public override void OnPressAction()
        {
            if (this._pin)
            {
                this._pin = false;
                SFX.Play("click");
                this._sprite.SetAnimation("powered");
                this._lastOwner = base.duck;
            }
        }
    }
}

﻿using System;

namespace DuckGame.MserDuck
{
    [EditorGroup("guns|mser")]
    public class PortableItemBox : Gun
    {
        protected SpriteMap _sprite;

        protected int _timer = 300;

        protected StateBinding _timerBinding = new StateBinding("_timer");

        protected bool _usable = true;

        protected StateBinding _usableBinding = new StateBinding("_usable");

        protected NetSoundEffect _netHitSound = new NetSoundEffect("hitBox") {volume = 1f, pitch = 0.2f};

        protected StateBinding _netHitSoundBinding = new NetSoundBinding("_netHitSound");

        protected Thing _spawnType;

        protected Thing _lastSpawned;

        protected StateBinding _spawnTypeBinding = new StateBinding("_spawnType");

        protected int _frameOffset;

        public PortableItemBox(float xval, float yval) : base(xval, yval)
        {
            this.ammo = 30;
            this._type = "gun";
            this._sprite = new SpriteMap(this.GetPath("miniItemBox"), 8, 8);
            this._sprite.frame = _frameOffset;
            base.graphic = this._sprite;
            this.center = new Vec2(4, 4);
            this.collisionOffset = new Vec2(-4f, -4f);
            this.collisionSize = new Vec2(8f, 8f);
            this._fullAuto = false;
            this._editorName = "Portable Itembox";
            this._frameOffset = 0;
            this.friction = 0.1f;
            this.bouncy = 0.35f;
        }

        public override void Initialize()
        {
            var things = ItemBox.GetPhysicsObjects(Editor.Placeables);
            var type1 = things[NetRand.Int(things.Count - 1)];
            this._spawnType = Activator.CreateInstance(type1, Editor.GetConstructorParameters(type1)) as PhysicsObject;
        }

        protected virtual PhysicsObject GetSpawnItem()
        {
            return Activator.CreateInstance(this._spawnType.GetType(), Editor.GetConstructorParameters(this._spawnType.GetType())) as PhysicsObject;
        }

        public override void Draw()
        {
            if (this.duck == null || !this.duck.holdObstructed) //Avoids hand jerking
                this.offDir = 1;

            base.Draw();
        }

        public override void OnPressAction()
        {
            if (!this._usable) return;

            _lastSpawned = this.GetSpawnItem();
            if (_lastSpawned != null)
            {
                if (this.isServerForObject)
                {
                    _lastSpawned.vSpeed = -3.5f;
                    _lastSpawned.position = this.position;
                    Level.Add(_lastSpawned);
                }

                this._lastSpawned.scale = new Vec2(0.1f);
            }

            if (!Network.isActive)
            {
                SFX.Play("hitBox", 1, 0.2f);
            }
            else if (this.isServerForObject)
            {
                this._netHitSound.Play(1, 0.2f);
                Thing.Fondle(_lastSpawned, DuckNetwork.localConnection);
            }
            this._usable = false;
            this._timer = 500;
        }

        public override void Update()
        {
            if (this.isServerForObject)
            {
                if (this._timer > 0)
                    this._timer--;
                else
                    this._usable = true;
            }

            if (this._lastSpawned != null)
            {
                var news = this._lastSpawned.scale.x + 0.05f;
                if (news < 1)
                    this._lastSpawned.scale = new Vec2(news);
            }

            this._sprite.frame = this._usable ? this._frameOffset : this._frameOffset + 1;

            base.Update();
        }
    }
}

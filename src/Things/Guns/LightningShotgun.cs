﻿using System;

namespace DuckGame.MserDuck
{
    [BaggedProperty("canSpawn", false), EditorGroup("guns|mser")]
    public class LightningShotgun : Gun
    {
        //NYI: LightningShotgun - horrible lightning code and missing most functionality

        public LightningShotgun(float xval, float yval) : base(xval, yval)
        {
            this.ammo = 6;
            this._type = "gun";
            base.graphic = new Sprite(GetPath("lsg"), 0f, 0f);
            this.center = new Vec2(9f, 4f);
            this.collisionOffset = new Vec2(-9f, -4f);
            this.collisionSize = new Vec2(17f, 8f);
            this._barrelOffsetTL = new Vec2(17f, 4f);
            this._fireSound = "shotgunFire2"; //TODO: LightningShotgun custom sound
            this._kickForce = 4f;
            this._editorName = "Lightning Shotgun";
            this._numBulletsPerFire = 4;
        }

        public override void Initialize()
        {
            if (Network.isActive)
            {
                Rando.generator = new Random(NetRand.currentSeed);
            }

            base.Initialize();
        }

        public override void OnPressAction()
        {
            //Create lightnings
            if (this.ammo > 0)
            {
                SFX.Play(this._fireSound, 1, 0, 0, false);

                for (int i = 0; i < _numBulletsPerFire; i++)
                {
                    Lightning lightning = new Lightning(this);
                    Level.Add(lightning);
                }

                //Knockback
                if (this.owner != null)
                {
                    this.owner.ApplyForce(new Vec2(-2 * this.offDir, -0.3f));
                }

                this.ammo -= 1;
            }
            else
            {
                base.DoAmmoClick();
            }
        }
    }
}

﻿using System;

namespace DuckGame.MserDuck
{
	[EditorGroup("guns|mser")]
	public class GiftGun : Gun
	{
		public GiftGun(float xval, float yval) : base(xval, yval)
		{
			//Random ammo! Asbestos free!
			this.ammo = 0x0005 + Rando.Int(0x000C);
			this._ammoType = new ATLaser();
			this._ammoType.range = 170f;
			this._ammoType.accuracy = 0.8f;
			this._fireSound = "smg";
			this._type = "gun";
			base.graphic = new Sprite(GetPath("giftgun"), 0f, 0f);
			this.center = new Vec2(3f, 11f);
			this.collisionOffset = new Vec2(-3f, -11f);
			this.collisionSize = new Vec2(17f, 13f);
			this._barrelOffsetTL = new Vec2(31f, 6f);
			this._fireSound = "laserRifle";
			this._fullAuto = false;
			this._fireWait = 2.6f;
			this._kickForce = 1f;
			this._holdOffset = new Vec2(0f, 0f);
			//this._flare = new SpriteMap("laserFlare", 16, 16, false);
			//this._flare.center = new Vec2(0f, 8f);
			this.weight = 1f;
			this.physicsMaterial = PhysicsMaterial.Rubber;
			this.bouncy = 0.85f;
		}

        public override void Initialize()
        {
            //Synced rando!
            if (Network.isActive)
            {
                Rando.generator = new Random(NetRand.currentSeed);
            }

            base.Initialize();
        }

        public override void Fire ()
		{
			if (ammo <= 0x0000) {
				if (base.duck != null) {
					base.duck.Swear ();
				}
			} else {
				--ammo;

				float action = Rando.Float (0, 1f);

				if (action <= 0.03f) {
					//Gun vanishes, sound plays
					SFX.Play(base.GetPath("sounds/BZHZHZHTBTT.wav"), 1f, Rando.Float(-0.2f, 0.2f), 0f, false);

					for (int i = 0; i < 8; i++)
					{
						Level.Add(new MusketSmoke(base.x + Rando.Float(-20f, 20f), base.y + Rando.Float(-20f, 20f)));
					}

					Duck duck = base.duck;
					if (duck != null)
					{
						duck.ThrowItem (true);
					}

					Level.Remove (this);
				
				} else if (action <= 0.05f) {
					//Sad (ignite gun)
					SFX.Play(base.GetPath("sounds/BLIBLIi.wav"), 1f, Rando.Float(-0.5f, 0.5f), 0f, false);
			
					for (int i = 0; i < 8; i++)
					{
						this.onFire = true;
						ammo = 0x00;
					}

					Duck duck = base.duck;
					if (duck != null)
					{
						duck.Swear ();
					}
						

				} else if (action <= 0.075f) {
					//Force throw gun, gun turns into mine
					SFX.Play(base.GetPath("sounds/dunk.wav"), 1f, Rando.Float(-0.4f, 0.2f), 0f, false);

					Duck duck = base.duck;

					if (duck != null)
					{
						duck.ThrowItem (true);
					}

					Mine mine = new Mine (this.x, this.y);
					mine.velocity = Vec2.Multiply(this.velocity,0.6f);

					Level.Remove (this);
					mine.PressAction();
					mine.Arm ();
					Level.Add (mine);


				} else if (action <= 0.1f) {
					//Force throw gun, gun turns into grenade
					SFX.Play(base.GetPath("sounds/dunk.wav"), 1f, Rando.Float(-0.4f, 0.2f), 0f, false);

					Duck duck = base.duck;

					if (duck != null)
					{
						duck.ThrowItem (true);
					}

					Grenade gre = new Grenade (this.x, this.y);
					gre.velocity = Vec2.Multiply(this.velocity,0.2f);

					Level.Remove (this);
					gre.PressAction();
					Level.Add (gre);


				} else {
                    //Spawns a random item
					SFX.Play(base.GetPath("sounds/PUUN.wav"), 1f, Rando.Float(-0.35f, 0.35f), 0f, false);
					System.Collections.Generic.List<System.Type> physicsObjects = ItemBox.GetPhysicsObjects(Editor.Placeables);
					System.Type gift = physicsObjects[Rando.Int(physicsObjects.Count - 1)];

					Duck duck = base.duck;
					if (duck != null)
					{
						duck.profile.stats.presentsOpened++;

					}

					for (int i = 0; i < 8; i++)
					{
						Level.Add(SmallSmoke.New(base.x + Rando.Float(-5f, 5f), base.y + Rando.Float(-5f, 5f)));
					}

					Holdable itm = System.Activator.CreateInstance(gift, Editor.GetConstructorParameters(gift)) as Holdable;
					if (itm != null && this.isServerForObject)
					{
						itm.x = this.x + base.barrelVector.x + this.offDir * 2f;
						itm.y = this.y + base.barrelVector.y + Rando.Float (-4f, 0f);
						Level.Add (itm);
						itm.velocity = Vec2.Multiply(barrelVector,6.0f);
						itm.velocity = Vec2.Add (itm.velocity, new Vec2 (0f, Rando.Float (-5f, -0.5f)));
					}
				}

			}
		}
	}
}


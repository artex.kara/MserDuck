namespace DuckGame.MserDuck
{
	public class ATNiceLaser : ATLaser
	{
		public ATNiceLaser()
		{
			this.accuracy = 1f;
			this.range = 2000f;
			this.penetration = 1f;
			this.bulletSpeed = 10f;
			this.bulletThickness = 0.3f;
			this.rebound = true;
			this.bulletType = typeof(LaserBullet);
			this.angleShot = false;
		}
	}
}

﻿namespace DuckGame.MserDuck
{

    public class UltraGrenadeBullet : GrenadeBullet
    {

        public UltraGrenadeBullet(float xval, float yval, AmmoType type, float ang = -1f, Thing owner = null, bool rbound = false, float distance = -1f, bool tracer = false, bool network = true) : base(xval, yval, type, ang, owner, rbound, distance, tracer, network)
        {
        }

        protected override void Rebound(Vec2 pos, float dir, float rng)
        {
            base.Rebound(pos, dir, rng);

            //Program.LogLine("Hit! " + DateTime.Now.Millisecond.ToString());
            Level.Add(new UltraTrail(pos.x, pos.y));
        }

        public override void Update()
        {
            base.Update();

            if (Rando.Int(0, 9) == 0)
            {
                Level.Add(new UltraTrail(this.start.x, this.start.y)
                {
                    xscale = 0.5f,
                    yscale = this.xscale
                });
            }

            float _isVol = (float)MserGlobals.GetField(typeof(GrenadeBullet), "_isVolatile").GetValue(this);
            _isVol += 0.05f;
            MserGlobals.GetField(typeof(GrenadeBullet), "_isVolatile").SetValue(this, _isVol);
            var ducks = Level.CheckCircleAll<Duck>(this.start, 96f);
            foreach (var duck in ducks)
            {
                duck.ApplyForce((this.start - duck.position) / 96);
            }
        }
    }
}

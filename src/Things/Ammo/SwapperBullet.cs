﻿using DuckGame.MserDuck.NetMessages;

namespace DuckGame.MserDuck
{
    public class SwapperBullet : Bullet
    {
        private float _isVolatile = 3f;

        public NetSoundEffect swapnotnet = new NetSoundEffect(Thing.GetPath<MserDuck>("sounds/swapnot"));

        public SwapperBullet(float xval, float yval, AmmoType type, float ang = -1f, Thing owner = null, bool rbound = false, float distance = -1f, bool tracer = false, bool network = true) : base(xval, yval, type, ang, owner, rbound, distance, tracer, network)
        {
        }

        protected override void OnHit(bool destroyed)
        {
            if (destroyed && base.isServerForObject)
            {
                //Swap pos
                var swapDucks = Level.CheckCircleAll<Duck>(base.position, 16).GetEnumerator();
                var swapRagdollParts = Level.CheckCircleAll<RagdollPart>(base.position, 16).GetEnumerator();
                var swapTrappedDucks = Level.CheckCircleAll<TrappedDuck>(base.position, 16).GetEnumerator();
                Duck swapDuck = null;

                //Try duck
                while (swapDucks.MoveNext())
                {
                    if (!ReferenceEquals(_owner, swapDucks.Current))
                    {
                        swapDuck = swapDucks.Current;
                    }
                }

                //Try ragdollparts
                if (swapDuck == null)
                {
                    while (swapRagdollParts.MoveNext())
                    {
                        if (!ReferenceEquals(_owner, swapRagdollParts.Current._doll.captureDuck))
                        {
                            swapDuck = swapRagdollParts.Current._doll.captureDuck;
                        }
                    }
                }

                //Try trappedduck
                if (swapDuck == null)
                {
                    while (swapTrappedDucks.MoveNext())
                    {
                        if (!ReferenceEquals(_owner, swapTrappedDucks.Current._duckOwner))
                        {
                            swapDuck = swapTrappedDucks.Current._duckOwner;
                        }
                    }
                }

                if (_owner != null && swapDuck != null)
                {
                    //TODO: Exchanger should swap being-in-car state and burning state.
                    //Potentially even work with other physicsobjects like crates?
                    if (Network.isActive)
                    {
                        var teleNM = new NMDuckTeleport(swapDuck.profile.networkIndex, ((Duck)_owner).profile.networkIndex);
                        Send.Message(teleNM);
                        teleNM.Activate();
                    }
                    else
                    {
                        SFX.Play(Thing.GetPath<MserDuck>("sounds/swap"), 1f, 0f, 0f, false);
                        NMDuckTeleport.CreateExplosion(_owner.x, _owner.y);
                        NMDuckTeleport.CreateExplosion(swapDuck.x, swapDuck.y);
                        NMDuckTeleport.SwapPositions((Duck)_owner, swapDuck);
                    }
                }
                else
                {
                    if (Network.isActive)
                    {
                        //Play over the net
                        this.swapnotnet.Play(1f, 0f);
                    }
                    else
                    {
                        //Play locally (no network)
                        SFX.Play(base.GetPath("sounds/swapnot"), 1f, 0f, 0f, false);
                    }
                }
            }
        }

        protected override void Rebound(Vec2 pos, float dir, float rng)
        {
            SwapperBullet sb = this.ammo.GetBullet(pos.x, pos.y, this.owner, -dir, base.firedFrom, rng, this._tracer, true) as SwapperBullet;
            sb._teleporter = this._teleporter;
            sb._isVolatile = this._isVolatile;
            sb.isLocal = this.isLocal;
            Level.Add(sb);
            SFX.Play(base.GetPath("sounds/swapbounce"), 0.8f, Rando.Float(-0.1f, 0.1f), 0f, false);
        }

        public override void Update()
        {
            this._isVolatile -= 0.06f;
            if (this._isVolatile <= 0f)
            {
                this.rebound = false;
            }
            base.Update();
        }
    }
}

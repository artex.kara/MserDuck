﻿namespace DuckGame.MserDuck
{
    public class ATSwapper : AmmoType
    {
        public ATSwapper()
        {
            this.accuracy = 1f;
            this.penetration = 0.35f;
            this.bulletSpeed = 9f;
            this.rangeVariation = 0f;
            this.speedVariation = 0f;
            this.range = 4000f;
            this.rebound = true;
            this.affectedByGravity = true;
            this.deadly = false;
            this.weight = 5f;
            this.bulletThickness = 2f;
            this.bulletColor = Color.Cyan;
            this.bulletType = typeof(SwapperBullet);
            this.immediatelyDeadly = true;
            this.sprite = new Sprite("launcherGrenade", 0f, 0f);
            this.sprite.CenterOrigin();
        }

        public override void PopShell(float x, float y, int dir)
        {
            Level.Add(new GrenadePin(x, y)
            {
                hSpeed = (float)dir * (1.5f + Rando.Float(1f))
            });
        }
    }
}

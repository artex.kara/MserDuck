﻿using DuckGame.MserDuck.NetMessages;

namespace DuckGame.MserDuck
{
    public class ATSunDart : AmmoType
    {
        public ATSunDart()
        {
            this.accuracy = 1f;
            this.range = 1500f;
            this.bulletSpeed = 9f;
            this.bulletThickness = 0.4f;
            this.penetration = 0f;
            this.rangeVariation = 0f;
            this.speedVariation = 0f;
            this.affectedByGravity = true;
            this.weight = 2f;
            this.bulletColor = Color.White;
            this.deadly = false;
            this.sprite = new Sprite(Thing.GetPath<MserDuck>("sunblt"), 0f, 0f);
            this.sprite.CenterOrigin();
            this.immediatelyDeadly = false;
            this.bulletType = typeof(SunBullet);
        }

        public override void PopShell(float x, float y, int dir)
        {
            Level.Add(new DartShell(x, y)
            {
                hSpeed = (float)dir * (1.5f + Rando.Float(1f))
            });
        }

        public override void OnHit(bool destroyed, Bullet b)
        {
            base.OnHit(destroyed, b);
        }

        public static Zun NewSun(Vec2 position, Duck owner)
        {
            Color col = Color.White;

            if (owner != null)
            {
                col = owner.persona.colorUsable;
            }
            
            return new Zun(position.x, position.y, col.r, col.g, col.b);
        }

        private class SunBullet : Bullet
        {
            public SunBullet(float xval, float yval, AmmoType type, float ang = -1f, Thing owner = null, bool rbound = false, float distance = -1f, bool tracer = false, bool network = true) : base(xval, yval, type, ang, owner, rbound, distance, tracer, network)
		    {

            }

            protected override void OnHit(bool destroyed)
            {
                var d = this.owner as Duck;
                if (d != null && destroyed && this.isLocal)
                {
                    Level.Add(NewSun(this.position, d));

                    if (Network.isActive)
                    {
                        var nm = new NMSpawnSun(this.position, d.profile.networkIndex);
                        Send.Message(nm);
                    }
                }
            }
        }
    }
}

﻿namespace DuckGame.MserDuck
{

    public class ATUltraGrenade : ATGrenade
    {

        public ATUltraGrenade()
        {
            this.accuracy = 1f;
            this.penetration = 0.35f;
            this.bulletSpeed = 9f;
            this.rangeVariation = 0f;
            this.speedVariation = 0f;
            this.range = 2000f;
            this.rebound = true;
            this.affectedByGravity = true;
            this.deadly = false;
            this.weight = 5f;
            this.bulletThickness = 2f;
            this.bulletColor = Color.White;
            this.bulletType = typeof(UltraGrenadeBullet);
            this.immediatelyDeadly = true;
            this.sprite = new Sprite(Thing.GetPath<MserDuck>("ultraNade"), 0f, 0f);
            this.sprite.CenterOrigin();
        }

        public override void PopShell(float x, float y, int dir)
        {
            Level.Add(new PistolShell(x, y)
            {
                hSpeed = (float)dir * (1.5f + Rando.Float(1f))
            });
        }
    }
}

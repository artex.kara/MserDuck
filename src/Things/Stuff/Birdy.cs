﻿using System;
using System.Linq;

namespace DuckGame.MserDuck
{
    [EditorGroup("stuff|mser")]
    public class Birdy : Holdable
    {
        private FeatherVolume _featherVolume;

        private SpriteMap _sprite;

        private Duck _myOwner;
        private StateBinding _myOwnerBinding = new StateBinding("_myOwner");

        private SinWave _xSin = new SinWave(0.012f, -0.2f);
        private SinWave _ySin = new SinWave(0.0055f, 1);
        private float _lastlerpX;
        private float _lerpX;
        private float _lastlerpY;
        private float _lerpY;
        private float _chirpTimer = 2f;

        /// <summary>
        /// Whether the bird is currently RIP.
        /// </summary>
        private bool _dead = false;
        private StateBinding _deadBinding = new StateBinding("_dead");

        /// <summary>
        /// Whether the bird is currently cooked.
        /// </summary>
        private bool _cooked = false;
        private StateBinding _cookedBinding = new StateBinding("_cooked");

        public NetSoundEffect _netScream = new NetSoundEffect(new string[]
        {
            "quackYell01",
            "quackYell02",
            "quackYell03"
        });

        public Birdy(float xval, float yval) : base(xval, yval)
        {
            var birdDuck = new Duck(-999, -999, new Profile("", null, null, new DuckPersona(new Vec3(163, 206, 39))));
            this._featherVolume = new FeatherVolume(birdDuck);
            this._featherVolume.anchor = this;
            this._featherVolume.collisionOffset = new Vec2(-4f, -6f);
            this._featherVolume.collisionSize = new Vec2(11f, 13f);
            this._sprite = new SpriteMap(this.GetPath("bird"), 17, 14);
            this._sprite.AddAnimation("flying", 0.5f, true, 1, 2, 3, 2);
            this._sprite.AddAnimation("cooked", 0.5f, true, 4);
            this._sprite.AddAnimation("dead", 0.5f, true, 5);
            this.graphic = this._sprite;
            this.center = new Vec2(8f, 5f);
            this.collisionOffset = new Vec2(-3f, -5f);
            this.collisionSize = new Vec2(9f, 11f);
            this.depth = 0.75f;
            this._editorName = "Birdy";
            this.thickness = 0.3f;
            this.weight = 1f;
            this.physicsMaterial = PhysicsMaterial.Duck;
            this.flammable = 0.8f;
            this.bouncy = 0f;
            this.friction = 0.1f;
            this._holdOffset = Vec2.Zero;
            this.burnSpeed = 0.006f;
            this._hasTrigger = false;
        }

        public override void Initialize()
        {
            base.Initialize();

            Level.Add(this._featherVolume);
        }

        public override void Update()
        {
            base.Update();

            //TODO: Bird - Something it can actually do (could just peck you, knockback + swear)
            //TODO: Bird - Set offdir based on movement direction (use interp of last few positions?)
            //BUG: Bird - picking up the cooked chicken instantly drops it - why?

            if (this._cooked && this._sprite.currentAnimation != "cooked")
            {
                //Late-cook me
                this.Cook();
                return;
            }

            if (this._dead && !this._cooked && this._sprite.currentAnimation != "dead")
            {
                //Late-kill me
                this.Kill();
                return;
            }

            //Only run stuff if alive!
            if (this._dead || this._cooked)
                return;

            //Crushy logic
            if (Level.CheckCircleAll<PhysicsObject>(this.position, 8f).Any(obj => obj.weight >= 5f && Math.Abs(obj.velocity.length) > 6.5f))
            {
                this.Kill(new DTCrush(null));
                return;
            }

            if (this._myOwner == null)
            {
                //Sitting on ground - look at closest ducks
                var closest = Level.Nearest<Duck>(this.position);
                if (Vec2.Distance(this.position, closest.position) < 112f)
                {
                    this.offDir = (sbyte)(closest.x > this.x ? 1 : -1);
                }

                //Picking up check
                if (this.duck != null)
                {
                    //Set as my owner, drop me, disallow more pickup
                    //TODO: Bird - shouldn't this occur OnPressAction instead?? would make more sense and allows it to be savable
                    this._myOwner = this.duck;
                    this.duck.ThrowItem(false);
                    this.canPickUp = false;
                    this.enablePhysics = false;
                    this.updatePhysics = false;
                    this._lerpX = this._myOwner.x;
                    this._lerpY = this._myOwner.y;
                    this._lastlerpX = this._lerpX;
                    this._lastlerpY = this._lerpY;
                    this._sprite.SetAnimation("flying");
                }
            }

            if (this._myOwner != null)
            {
                //My owner died?
                if (this._myOwner.dead)
                {
                    if (this._myOwner.removedFromFall)
                    {
                        //If fell to death, kill me too
                        this.Kill(new DTFall());
                    }
                    else
                    {
                        //TODO: Bird - Go berzerk!
                    }
                }

                //Do chirping
                this._chirpTimer -= Maths.IncFrameTimer();
                if (this._chirpTimer <= 0f)
                {
                    //TODO: Birdy - tell joao to make better sfx for it
                    this._chirpTimer = NetRand.Float(2.5f, 5f);
                    SFX.Play(this.GetPath(string.Format("sounds/bird{0}.wav", Rando.Int(1, 4))), Rando.Float(0.3f, 0.5f), Rando.Float(-0.5f, 0.25f));
                }

                //This of course needs to be modified once more animations are added
                if (this._sprite.currentAnimation != "flying")
                    this._sprite.SetAnimation("flying");

                //Follow him around
                var ownerpos = this._myOwner.position;
                if (this._myOwner.ragdoll != null)
                    ownerpos = this._myOwner.ragdoll.part2.position;
                this._lerpX = Extensions.Lerp(this._lastlerpX, ownerpos.x, 0.035f);
                this._lerpY = Extensions.Lerp(this._lastlerpY, ownerpos.y, 0.035f);
                this._lastlerpX = this._lerpX;
                this._lastlerpY = this._lerpY;
                this.x = this._xSin.normalized * 60 - 30 + this._lerpX;
                this.y = this._ySin.normalized * 60 - 30 + this._lerpY;
            }
        }

        protected override bool OnBurn(Vec2 firePosition, Thing litBy)
        {
            if (!this.onFire)
            {
                if (!this._dead)
                {
                    //TODO: Bird - custom ignite SFX
                    /*if (Network.isActive)
                    {
                        if (base.isServerForObject)
                        {
                            this._netScream.Play(1f, 0f);
                        }
                    }
                    else if (Rando.Float(1f) < 0.5f)
                    {
                        SFX.Play("quackYell03", 1f, 0f, 0f, false);
                    }
                    else if (Rando.Float(1f) < 0.5f)
                    {
                        SFX.Play("quackYell02", 1f, 0f, 0f, false);
                    }
                    else
                    {
                        SFX.Play("quackYell01", 1f, 0f, 0f, false);
                    }*/
                    SFX.Play("ignite", 1f, -0.3f + Rando.Float(0.3f), 0f, false);
                }
                this.onFire = true;
            }

            this.AddFire();

            return true;
        }

        public override bool DoHit(Bullet bullet, Vec2 hitPos)
        {
            this.Kill(new DTShot(bullet));

            return base.DoHit(bullet, hitPos);
        }

        public override bool Hit(Bullet bullet, Vec2 hitPos)
        {
            //Play sound of bullet piercing
            if (this._sprite.currentAnimation != "cooked")
                SFX.Play("lightMatch", 0.5f, -0.4f + Rando.Float(0.2f), 0f, false);

            return base.Hit(bullet, hitPos);
        }

        public override bool Destroy(DestroyType type = null)
        {
            if (!this._destroyed)
            {
                this._destroyed = this.OnDestroy(type);
                if (this._destroyed)
                {
                    //Kill me
                    this.Kill(type);
                }
            }

            return this._destroyed;
        }

        protected override bool OnDestroy(DestroyType type = null)
        {
            //Unspecific kill-causes should be reasons to destruct
            if (type == null)
                return true;

            //Don't allow being killed from my owner!
            return type.thing != this._myOwner;
        }

        /// <summary>
        /// Kills the bird with a different effect depending on the DestroyType.
        /// </summary>
        /// <param name="dt"></param>
        public void Kill(DestroyType dt = null)
        {
            //Set anim again
            this._sprite.SetAnimation("dead");

            //Don't re-dead me
            if (this._dead)
                return;

            //RIP!
            this._dead = true;
            this._myOwner = null;
            this.friction = 0.4f;
            this.canPickUp = true;
            this.enablePhysics = true;
            this.updatePhysics = true;
            this.weight = 0.8f;

            if (dt is DTIncinerate)
            {
                //Turn into cooked chicken
                this.Cook(dt.thing);
            }
            if (dt is DTCrush)
            {
                //Crushing logic
                if (dt.thing != null && dt.thing.GetType().IsSubclassOf(typeof(PhysicsObject)))
                {
                    dt.thing.vSpeed = -0.5f;
                }

                for (var i = 0; i < 4; i++)
                {
                    Level.Add(new DizzyStar(this.x + Rando.Float(-4, 4), this.y + Rando.Float(-4, 4), new Vec2(Rando.Float(-0.5f, 0.5f), Rando.Float(-1.5f, -0.5f))));
                }
            }
            //TODO: Bird - chainsaw or sword chopping (ignite with turbochainsaw if it doesnt already on its own?)

            //Death sound
            SFX.Play("death", 1f, 0.5f);
        }

        /// <summary>
        /// Turns the birdy into a sad cooked chicken.
        /// </summary>
        public void Cook(Thing killer = null)
        {
            //Set anim again
            this._sprite.SetAnimation("cooked");

            if (!this._dead)
            {
                //Called cook without being dead, kill me first
                this.Kill(new DTIncinerate(killer));
                return;
            }

            //Don't re-cook me
            if (this._cooked)
                return;

            //Different properties here
            SFX.Play("pierce", 0.7f, 0.6f);
            this._cooked = true;
            this.weight = 5.5f;
            this.collideSounds.Add("rockHitGround2", ImpactedFrom.Bottom);
            this.collideSounds.Add("smallSplatLouder");
            this.center = new Vec2(6f, 8f);
            this.collisionOffset = new Vec2(-5f, -4f);
            this.collisionSize = new Vec2(11f, 10f);
            this.RemoveFeatherVolume();
        }

        public override void Terminate()
        {
            this.RemoveFeatherVolume();
        }

        /// <summary>
        /// Stops the birdy from spawning feathers when shot at. Cannot be undone (easily).
        /// </summary>
        private void RemoveFeatherVolume()
        {
            if (this._featherVolume == null)
                return;

            Level.Remove(this._featherVolume);
            this._featherVolume = null;
        }
    }
}

﻿using System;

namespace DuckGame.MserDuck
{
    [EditorGroup("stuff|mser")]
    public class BasketballMP : Holdable
    {
        public bool obstru = false;

        public StateBinding obstruBinding = new StateBinding("obstru");

        public BasketballMP(float xpos, float ypos) : base(xpos, ypos)
		{
            this._editorName = "Basketball MP";
            this._sprite = new SpriteMap("basketBall", 16, 16);
            base.graphic = this._sprite;
            this.center = new Vec2(8f, 8f);
            this.collisionOffset = new Vec2(-8f, -8f);
            this.collisionSize = new Vec2(15f, 15f);
            base.depth = -0.5f;
            this.thickness = 1f;
            this.weight = 3f;
            this.flammable = 0.3f;
            base.collideSounds.Add("basketball");
            this.physicsMaterial = PhysicsMaterial.Rubber;
            this._bouncy = 0.8f;
            this.friction = 0.03f;
            this._impactThreshold = 0.1f;
            this._holdOffset = new Vec2(6f, 0f);
            this.handOffset = new Vec2(0f, -0f);
        }

        public override void CheckIfHoldObstructed()
        {
            obstru = false;
            if (this.duck != null && this.duck.action)
            {
                obstru = true;
            }
        }

        public override void OnPressAction()
        {
        }

        public override void Update()
        {
            if (this.duck != null)
            {
                this.duck.holdObstructed = obstru;
            }

            if (this.owner == null)
            {
                this._walkFrames = 0;
                this._framesInHand--;
                if (this._framesInHand < -60)
                {
                    this._bounceDuck = null;
                }
                if (this._bounceDuck != null)
                {
                    float length = (this._bounceDuck.position - this.position).length;
                    if (length < 16f)
                    {
                        this.hSpeed = this._bounceDuck.hSpeed;
                    }
                    if (this._bounceDuck.holdObject == null && this.vSpeed < 1f && this._bounceDuck.top + 8f > base.y && length < 16f)
                    {
                        this._bounceDuck.GiveHoldable(this);
                        this._framesInHand = 0;
                    }
                }
            }
            else
            {
                if (this._framesInHand < 0)
                {
                    this._framesInHand = 0;
                }
                if (!this.owner.action && Math.Abs(this.owner.hSpeed) > 0.5f && this._framesInHand > 6)
                {
                    this._bounceDuck = this.duck;
                    float hSpeed = this.duck.hSpeed;
                    this.duck.ThrowItem(false);
                    this.vSpeed = 2f;
                    this.hSpeed = hSpeed * 1.1f;
                    this._framesInHand = 0;
                }
                else
                {
                    if (Math.Abs(this.owner.hSpeed) > 0.5f && base.duck.grounded)
                    {
                        this._walkFrames++;
                    }
                    else if (this.duck.grounded)
                    {
                        this._walkFrames--;
                    }
                    if (this._walkFrames < 0)
                    {
                        this._walkFrames = 0;
                    }
                    if (this._walkFrames > 20)
                    {
                        SFX.Play("basketballWhistle");
                        this.duck.ThrowItem(false);
                        this._walkFrames = 0;
                    }
                    this._bounceDuck = null;
                    this._framesInHand++;
                }
            }
            //Set weight to 5 if no duck is assigned to be bouncing it (crushes), else 3.
            this.weight = (this._bounceDuck == null) ? 5f : 3f;
            base.Update();
        }

        private Duck _bounceDuck;

        private int _framesInHand;

        private SpriteMap _sprite;

        private int _walkFrames;
    }
}

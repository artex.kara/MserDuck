﻿namespace DuckGame.MserDuck
{
    /// <summary>
    /// Internal class for ragdolls whose color will be synchronized over the network.
    /// </summary>
    public class NetworkRagdoll : Ragdoll
    {
        public float r;
        public float g;
        public float b;

        public bool set = false;

        public StateBinding rBinding = new StateBinding("r");
        public StateBinding gBinding = new StateBinding("g");
        public StateBinding bBinding = new StateBinding("b");

        public NetworkRagdoll(float xpos, float ypos, float angleDeg, int offDir, Vec2 velocity, Vec3 color) : base(xpos, ypos, null, false, angleDeg, offDir, velocity, null)
        {
            this.r = color.x;
            this.g = color.y;
            this.b = color.z;
        }

        public override void Initialize()
        {
            var color = new Vec3(this.r, this.g, this.b);

            this.persona = new DuckPersona(color);
            this.persona.Recreate();
            this.RunInit();

            this.set = true;

            base.Initialize();
        }

        public override void Update()
        {
            base.Update();

            if (!this.set)
            {
                this.set = true;

                var color = new Vec3(this.r, this.g, this.b);

                this.persona = new DuckPersona(color);
                this.persona.Recreate();
                this.RunInit();
            }
        }
    }
}

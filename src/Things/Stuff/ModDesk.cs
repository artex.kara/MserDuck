﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Thing that attaches to a desk and gives it additional functionality (for modifiers like Rocket or Mirror Tables).
    /// </summary>
    public class ModDesk : Thing
    {
        public Desk Desk;

        public byte Typ;

        public StateBinding TypBinding = new StateBinding("Typ");

        public StateBinding DeskBinding = new StateBinding("Desk");

        private byte _smokeTimer;

        private readonly List<Bullet> _influenced = new List<Bullet>();

        private short _counter;

        public ModDesk(Desk desk, byte typ) : base(0, 0)
        {
            this.Desk = desk;
            this.visible = false;
            this.TypBinding.skipLerp = true;
            this.DeskBinding.skipLerp = true;
            this.Typ = typ;
        }

        public override void Draw()
        {
            //Skip draw call - hidden
        }

        public override void Update()
        {
            base.Update();

            if (this.Desk != null)
            {
                //Run update for desk type
                switch (this.Typ)
                {
                    case 0:
                        //BUG: ROckeTTable - does not set speed when spawned during level
                        this.RocketTableUpdate();
                        break;
                    case 1:
                        this.MirrorTableUpdate();
                        break;
                    default:
                        throw new InvalidOperationException("Unknown desk type for moddesk: " + this.Typ);
                }
            }
            
            if (this.Desk != null && (this.Desk.destroyed || !this.Desk.visible || !this.Desk.active))
            {
                _counter++;

                if (_counter > 100)
                {
                    //Remove if desk is too dead
                    Level.Remove(this);
                }
            }
        }

        public void RocketTableUpdate()
        {
            if (this.Desk.flipped == 0) return;

            if (this._smokeTimer < 4)
            {
                this._smokeTimer++;
            }
            else
            {
                this.Desk.vSpeed /= 1.5f;
                this.Desk.hSpeed += this.Desk.flipped / 200f;
                
                Level.Add(SmallSmoke.New(this.Desk.x, this.Desk.y));
                this._smokeTimer = 0;

                //Rare checks here

                var nearDuck = Level.CheckCircleAll<Duck>(this.Desk.position, 17f);
                foreach (Duck duck in nearDuck)
                {
                    duck.velocity += this.Desk.velocity;

                    if (this.Desk.hSpeed >= 8)
                    {
                        duck.GoRagdoll();
                    }

                    if (this.Desk.hSpeed >= 16)
                    {
                        duck.Kill(new DTImpale(this.Desk));
                    }
                }

                var colls = Level.CheckCircleAll<Thing>(this.Desk.position, 20f);
                foreach (Thing coll in colls)
                {
                    if (coll.GetType().IsSubclassOf(typeof(PhysicsObject)))
                    {
                        coll.velocity += this.Desk.velocity;

                        Gun knarre = coll as Gun;
                        if (knarre != null)
                        {
                            knarre.PressAction();
                        }
                    }
                }

                if ((this.Desk.collideLeft != null || this.Desk.collideRight != null) && this.Desk.hSpeed < 0.1f)
                {
                    nearDuck = Level.CheckCircleAll<Duck>(this.Desk.position, 45f);
                    foreach (Duck duck in nearDuck)
                    {
                        duck.Kill(new DTImpale(this.Desk));
                    }

                    float x = this.Desk.x;
                    float num = this.Desk.y - 2f;
                    int num2 = 6;
                    if (Graphics.effectsLevel < 2)
                    {
                        num2 = 3;
                    }
                    for (int i = 0; i < num2; i++)
                    {
                        float deg = (float)i * 60f + Rando.Float(-10f, 10f);
                        float num3 = Rando.Float(12f, 20f);
                        ExplosionPart thing = new ExplosionPart(x + (float)(Math.Cos((double)Maths.DegToRad(deg)) * (double)num3), num - (float)(Math.Sin((double)Maths.DegToRad(deg)) * (double)num3), true);
                        Level.Add(thing);
                    }
                    SFX.Play("explode", 1f, 0f, 0f, false);

                    this.Desk.Destroy(new DTRocketExplosion(null));
                    Level.Remove(this);
                }
            }
        }

        public void MirrorTableUpdate()
        {
            if (this.Desk.flipped == 0) return;

            var count = 0;
            var total = 0;

            foreach (Bullet bl in Level.CheckCircleAll<Bullet>(new Vec2(this.Desk.position.x - 4 * this.Desk.flipped, this.Desk.position.y - 4 * this.Desk.flipped), 20))
            {
                total++;

                if (this._influenced.Contains(bl) || bl.rebound)
                {
                    continue;
                }

                //Moves new bullet away from table aswell as rotating it by 180° in order to shoot away from the table.
                var newbl = new Bullet(bl.x - (float)Math.Cos(Maths.DegToRad(bl.angle)) * 3, bl.y, bl.ammo, (bl.angle + 180) % 360, null, false, bl.range);
                Level.Add(newbl);
                Level.Remove(bl);

                this._influenced.Add(newbl);
                count++;
            }

            //Do sound if at least one bullet converted
            if (count > 0)
            {
                SFX.Play("ting", 0.3f, -0.1f + Rando.Float(0.2f));
            }

            //Reset influenced list if no bullet is close to table at all
            if (total == 0)
            {
                this._influenced.Clear();
            }
        }

        public static void UpdateModifier(byte type)
        {
            if (MserGlobals.GetGameState() != GameState.Playing)
                return;

            //TODO: ModDesk - dont do it like this, why doesnt it simply substitute???
            var desklist = Level.current.things[typeof(Desk)].ToList();

            //Remove duplicate desks
            foreach (ModDesk md in Level.current.things[typeof(ModDesk)])
            {
                if (md.Desk != null && desklist.Contains(md.Desk))
                {
                    desklist.Remove(md.Desk);
                }
            }
            foreach (Desk desk in desklist)
            {
                // Replace desk with rocketdesk
                var rd = new ModDesk(desk, type);
                Level.Add(rd);
            }
        }
    }
}

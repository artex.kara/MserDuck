﻿using Microsoft.Xna.Framework.Graphics;

namespace DuckGame.MserDuck
{
    //NYI: SunBomb - sun shader currently turns everything purple AFAIK

    [EditorGroup("stuff|mser"), BaggedProperty("canSpawn", false)]
    public class SunBomb : Gun
    {
        private static RenderTarget2D _sunshineTarget;
        private static RenderTarget2D _screenTarget;
        private static RenderTarget2D _pixelTarget;
        private static Layer _sunLayer;
        private static Thing sunThing;
        private static Material _sunshineMaterialBare;
        private static Material _sunshineMaterial;
        private static FieldBackground _field;
        private static FieldBackground _fieldForeground;
        private static FieldBackground _fieldForeground2;
        private SpriteMap _sprite;
        public static bool InnerCall = false;
        private static SunBomb activeBomb;

        public static TextInput text = new TextInput();

        public SunBomb(float xval, float yval) : base(xval, yval)
        {
            this.ammo = 255;
            this._ammoType = new ATShrapnel();
            this._ammoType.penetration = 0.4f;
            this._type = "gun";
            this._sprite = new SpriteMap("eCrateLight", 8, 8, false);
            base.graphic = this._sprite;
            this.center = new Vec2(4f, 4f);
            this.collisionOffset = new Vec2(-4f, -4f);
            this.collisionSize = new Vec2(8f, 8f);
            base.bouncy = 0.4f;
            this.friction = 0.05f;
            this._editorName = "Sunbomb";
            this.collideSounds.Add("netsound");
        }

        public override void Initialize()
        {
            SunBomb.InitializeSun();
            SunBomb.activeBomb = this;

            text.Enabled = true;
            BindManager.BindsEnabled = false;

            base.Initialize();
        }

        public override void Update()
        {
            base.Update();

            var reg = MserDebug.StartProfiling("TextInputSpeed");
            text.Update();
            MserDebug.StepProfiling(reg, "textbox update");
            MserDebug.PrintEntry("textbox text", text.Text);
            MserDebug.PrintEntry("textbox length", text.Length);
            MserDebug.PrintEntry("textbox truelength", text.Text.Length);
            MserDebug.PrintEntry("---", new NoValue());
            MserDebug.PrintEntry("textbox caretpos", text.CaretPosition);
            MserDebug.PrintEntry("textbox caretline", text.CaretLine);
            MserDebug.PrintEntry("textbox caretcol", text.CaretColumn);
            MserDebug.PrintEntry("--- ", new NoValue());
            MserDebug.PrintEntry("textbox selstartpos", text.SelectionStartPosition);
            MserDebug.PrintEntry("textbox selstartline", text.SelectionStartLine);
            MserDebug.PrintEntry("textbox selstartcol", text.SelectionStartColumn);
            MserDebug.PrintEntry("---  ", new NoValue());
            MserDebug.PrintEntry("textbox selendpos", text.SelectionEndPosition);
            MserDebug.PrintEntry("textbox selendline", text.SelectionEndLine);
            MserDebug.PrintEntry("textbox selendcol", text.SelectionEndColumn);
            MserDebug.EndProfiling(reg);
        }

        public override void Terminate()
        {
            activeBomb = null;

            base.Terminate();
        }

        public static void InitializeSun()
        {
            return;

            float aspect = 0.5625f;
            _sunshineTarget = new RenderTarget2D(Graphics.width / 12, (int)(Graphics.width * aspect) / 12);
            _screenTarget = new RenderTarget2D(Graphics.width, (int)(Graphics.width * aspect));
            _pixelTarget = new RenderTarget2D(160, (int)(320f * aspect / 2f));
            _sunLayer = new Layer("SUN LAYER", 99999, null, false, default(Vec2));
            Layer.Add(_sunLayer);
            Thing tthing = new SpriteThing(150f, 120f, new Sprite("sun", 0f, 0f));
            tthing.z = -9999f;
            tthing.depth = -0.99f;
            tthing.layer = _sunLayer;
            tthing.xscale = 1f;
            tthing.yscale = 1f;
            tthing.collisionSize = new Vec2(1f, 1f);
            tthing.collisionOffset = new Vec2(0f, 0f);
            Level.Add(tthing);
            sunThing = tthing;

            _field = new FieldBackground("FIELD", 9999);
            Layer.Add(_field);
            _fieldForeground = new FieldBackground("FIELDFOREGROUND", 80);
            _fieldForeground.fieldHeight = -13f;
            Layer.Add(_fieldForeground);
            _fieldForeground2 = new FieldBackground("FIELDFOREGROUND2", 70);
            _fieldForeground2.fieldHeight = -15f;
            Layer.Add(_fieldForeground2);
        }

        public static void DrawSun()
        {
            return;

            if (SunBomb.InnerCall || SunBomb.activeBomb == null || SunBomb._sunshineTarget == null)
                return;

            InnerCall = true;

            Color backColor = Level.current.backgroundColor;
            Level.current.backgroundColor = Color.Black;
            float hudFade = Layer.HUD.fade;
            float consoleFade = Layer.Console.fade;
            float gameFade = Layer.Game.fade;
            float backFade = Layer.Background.fade;
            float fieldFade = _field.fade;
            Layer.Game.fade = 0f;
            Layer.Background.fade = 0f;
            Layer.Foreground.fade = 0f;
            _field.fade = 0f;
            _fieldForeground.fade = 0f;
            //_wall.fade = 0f;
            _fieldForeground2.fade = 0f;
            Vec3 gameColorMul = Layer.Game.colorMul;
            Vec3 backColorMul = Layer.Background.colorMul;
            Layer.Game.colorMul = Vec3.One;
            Layer.Background.colorMul = Vec3.One;
            Layer.HUD.fade = 0f;
            Layer.Console.fade = 0f;
            //this.fieldMulColor = Vec3.One;
            Vec3 colorAdd = Layer.Game.colorAdd;
            Layer.Game.colorAdd = Vec3.Zero;
            Layer.Background.colorAdd = Vec3.Zero;
            //this.fieldAddColor = Vec3.Zero;
            Layer.blurry = true;
            //this.sunThing.alpha = RockWeather.sunOpacity;
            //((SpriteThing)this.rainbowThing2).alpha = 0f;
            //RockScoreboard._drawingLighting = true;
            MonoMain.RenderGame(_sunshineTarget);
            //RockScoreboard._drawingLighting = false;
            if (_sunshineMaterialBare == null)
            {
                _sunshineMaterialBare = new MaterialSunshineBare();
            }
            Vec2 pos = SunBomb.activeBomb.cameraPosition / Level.current.camera.size;
            MserDebug.PrintEntry("SUNBOMB init", pos);
            Vec3 newPos = new Vec3(290f + pos.x * 8000f, -9999f, 10000f - pos.y * 8000f);
            MserDebug.PrintEntry("SUNBOMB old", newPos);
            Viewport v = new Viewport(0, 0, (int)Layer.HUD.width, (int)Layer.HUD.height);
            newPos = v.Project(newPos, /*Layer.Game.projection*/ Matrix.Identity, /*Layer.Game.view*/ Matrix.Identity, Matrix.Identity);
            newPos.y -= 256f;
            newPos.x /= (float)v.Width;
            newPos.y /= (float)v.Height;
            MserDebug.PrintEntry("SUNBOMB new", newPos);
            _sunshineMaterialBare.effect.effect.Parameters["lightPos"].SetValue(new Vec2(newPos.x, newPos.y));
            _sunshineMaterialBare.effect.effect.Parameters["weight"].SetValue(1f);
            _sunshineMaterialBare.effect.effect.Parameters["density"].SetValue(0.4f);
            _sunshineMaterialBare.effect.effect.Parameters["decay"].SetValue(0.68f);
            _sunshineMaterialBare.effect.effect.Parameters["exposure"].SetValue(1f);
            Viewport oldV = Graphics.viewport;
            Graphics.SetRenderTarget(_pixelTarget);
            Graphics.viewport = new Viewport(0, 0, _pixelTarget.width, _pixelTarget.height);
            Graphics.screen.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.Default,
                                  RasterizerState.CullNone, null, Level.current.camera.getMatrix());
            Graphics.material = _sunshineMaterialBare;
            float scale = (float)(_pixelTarget.width * 2) / (float)_sunshineTarget.width;
            Graphics.Draw(_sunshineTarget, Vec2.Zero, null, Color.White, 0f, Vec2.Zero, new Vec2(scale), SpriteEffects.None, default(Depth));
            Graphics.material = null;
            Graphics.screen.End();
            Graphics.SetRenderTarget(null);
            Graphics.viewport = oldV;
            Layer.blurry = false;
            Layer.HUD.fade = hudFade;
            Layer.Console.fade = consoleFade;
            Layer.Game.fade = gameFade;
            Layer.Foreground.fade = gameFade;
            Layer.Background.fade = backFade;
            _field.fade = fieldFade;
            _fieldForeground.fade = fieldFade;
            _fieldForeground2.fade = fieldFade;
            //this._wall.fade = fieldFade;
            Layer.Game.colorMul = gameColorMul;
            Layer.Background.colorMul = backColorMul;
            //this.fieldMulColor = backColorMul;
            Layer.Game.colorAdd = colorAdd;
            Layer.Background.colorAdd = colorAdd;
            //this.fieldAddColor = colorAdd;
            //RockScoreboard._drawingSunTarget = false;
            //sunThing.x = 290f + RockWeather.sunPos.x * 8000f;
            //sunThing.y = 10000f - RockWeather.sunPos.y * 8000f;
            //this.rainbowThing.y = (this.rainbowThing2.y = 2000f);
            //this.rainbowThing.x = (this.rainbowThing2.x = -this._field.scroll * 15f + 6800f);
            //this.rainbowThing.alpha = this._weather.rainbowLight;
            //((SpriteThing)this.rainbowThing2).alpha = this._weather.rainbowLight2;
            //this.rainbowThing.visible = (this.rainbowThing.alpha > 0.01f);
            //this.rainbowThing2.visible = (this.rainbowThing2.alpha > 0.01f);
            //RockScoreboard._drawingSunTarget = true;
            _field.fade = Layer.Game.fade;
            _fieldForeground.fade = Layer.Game.fade;
            _fieldForeground2.fade = Layer.Game.fade;
            //this._wall.fade = Layer.Game.fade;
            Level.current.backgroundColor = backColor;
            //RockScoreboard._drawingNormalTarget = true;
            MonoMain.RenderGame(_screenTarget);
            //RockScoreboard._drawingNormalTarget = false;
            //RockScoreboard._drawingSunTarget = false;
            if (_sunshineMaterial == null)
                _sunshineMaterial = new MaterialSunshine(_screenTarget);
            Graphics.screen.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.Default,
                                  RasterizerState.CullNone, null, Level.current.camera.getMatrix());
            Graphics.material = _sunshineMaterial;
            float scale2 = Graphics.width / (_pixelTarget.width * (Graphics.width / Layer.Game.camera.width));
            Graphics.Draw(_pixelTarget, Vec2.Zero, null, Color.Red, 0f, Vec2.Zero, new Vec2(scale2), SpriteEffects.None, 999);
            Graphics.material = null;
            Graphics.screen.End();

            InnerCall = false;
        }
    }
}

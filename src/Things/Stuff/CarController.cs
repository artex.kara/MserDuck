﻿using System.Reflection;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Controller to create and control a CarBody.
    /// </summary>
    [EditorGroup("stuff|mser")]
    public class CarController : Holdable
    {
        private Sprite _sprite;

        private CarBody _car;

        private StateBinding _carBinding = new StateBinding("_car");

        private Duck _lastDuck;

        private int _num;
        private StateBinding _numBinding = new StateBinding("_num");

        private FieldInfo _disarmDisable = MserGlobals.GetField(typeof(Duck), "_disarmDisable");

        public CarController(float xpos, float ypos) : base(xpos, ypos)
        {
            this._sprite = new SpriteMap(this.GetPath("carPickup"), 16, 16);
            this.graphic = this._sprite;
            this.center = new Vec2(8f, 10f);
            this.collisionOffset = new Vec2(-7f, -3f);
            this.collisionSize = new Vec2(14f, 7f);
            this.depth = 0.5f;
            this._editorName = "Car Controller";
            this.thickness = 3f;
            this.weight = 3f;
            this.physicsMaterial = PhysicsMaterial.Metal;
            this.flammable = 0.1f;
            this.bouncy = 0.15f;
            this.friction = 0.1f;
            this._holdOffset = Vec2.Zero;
            this._num = NetRand.Int(1, CarBody.SkinNum) - 1;
        }

        public override void Draw()
        {
            this.alpha = this._car == null ? 1f : 0f;

            base.Draw();
        }

        public override void Update()
        {
            //TODO: Car - make the car savable in PurpleBlocks!

            if (this._num > -1)
                this.frame = this._num;

            base.Update();

            if (this._car != null)
            {
                this.position = this._car.position;
            }

            if (this.duck != null)
            {
                //Controller is held, set stuff
                if (this._lastDuck == null)
                {
                    this._lastDuck = this.duck;
                    if (this._car != null)
                        Thing.Fondle(this._car, this._lastDuck.connection);
                }

                this._lastDuck.alpha = 0f;
                _disarmDisable.SetValue(this._lastDuck, 5);

                if (this._car == null && this.isServerForObject)
                {
                    this._car = new CarBody(this.x, this.y, this);
                    Level.Add(this._car);
                    Thing.Fondle(this._car, this._lastDuck.connection);
                }
            }
            else
            {
                //Controller is released, reset stuff
                if (this._lastDuck != null)
                {
                    this._lastDuck.alpha = 1f;
                    this._lastDuck = null;
                }

                if (this._car != null)
                {
                    //Move controller with car
                    this.position = this._car.position;
                }
            }
        }

        public override void Terminate()
        {
            if (this._car != null && !this.removing)
            {
                this._car.removing = true;
                Level.Remove(this._car);
            }

            base.Terminate();
        }

        internal bool removing = false;
    }
}

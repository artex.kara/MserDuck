﻿namespace DuckGame.MserDuck
{
    [EditorGroup("stuff|mser")]
	public class Rubba : Hat
	{
		private Duck dook = null;

		public Rubba(float xpos, float ypos) : base(xpos, ypos)
		{
			this._pickupSprite = new Sprite(GetPath("rubPickup"), 0f, 0f);
			this._sprite = new SpriteMap(GetPath("rubEmptyyyyy"), 1, 1, false);
			base.graphic = this._pickupSprite;
			this.center = new Vec2(8f, 8f);
			this.collisionOffset = new Vec2(-5f, -2f);
			this.collisionSize = new Vec2(12f, 8f);
			this._sprite.CenterOrigin();
			this._isArmor = false;
			this._equippedThickness = 3f;
		}

		private void bind(Duck d) {
			if (d == null) {
				return; //sad
			}
			d._netQuack = new NetSoundEffect(this.GetPath ("sounds/rub.wav"));
            d._netQuack.pitchBinding = new FieldBinding(d, "quackPitch");
			/*d._netJump = new NetSoundEffect(this.GetPath ("sounds/dzjump.wav"));
			d._netScream = new NetSoundEffect(this.GetPath ("sounds/foire.wav"));
			d._netSwear = new NetSoundEffect(this.GetPath ("sounds/dman.wav"));*/
		}

		private void unbind(Duck d) {
			if (d == null) {
				return; //sad
			}
			d._netQuack = new NetSoundEffect("quack");
            d._netQuack.pitchBinding = new FieldBinding(d, "quackPitch");
			d._netJump = new NetSoundEffect("jump") {volume = 0.5f};
			d._netScream = new NetSoundEffect("quackYell01", "quackYell02", "quackYell03");
			d._netSwear = new NetSoundEffect(new System.Collections.Generic.List<string>
				{
					"cutOffQuack",
					"cutOffQuack2"
				}, new System.Collections.Generic.List<string>
				{
					"quackBleep"
				});
		}

		public override void Quack(float volume, float pitch)
		{
			SFX.Play(base.GetPath ("sounds/rub.wav"), volume, pitch, 0f, false);
		}

		public override void Update()
		{
			base.Update();
		}

		public override void Equip(Duck d) {
			base.Equip (d);

			if (d != null) {
				dook = d;
				this.bind (d);
			}
		}

		public override void UnEquip() {
			unbind (dook);
			base.UnEquip ();
			dook = null;
		}
			
		public override void Draw()
		{
			bool qq = false;

			if (base.duck != null) {
				qq = base.duck.IsQuacking();
			}
			int frame = this._sprite.frame;
			this._sprite.frame = (qq ? 1 : 0);
			base.Draw();
			this._sprite.frame = frame;
		}
	}
}


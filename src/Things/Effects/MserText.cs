﻿namespace DuckGame.MserDuck
{
    /// <summary>
    /// Show text inside of the Level. Mostly obsolete due to custom chat command rendering.
    /// </summary>
    public class MserText : Thing
    {
        public string text = "";
        public Color color = default(Color);
        public float time = 3f;
        public float counter = 0f;
        public bool topleft = false;

        /// <summary>
        /// Creates a new text to be placed inside the Level.
        /// </summary>
        /// <param name="xpos">Horizontal coordinate</param>
        /// <param name="ypos">Vertical coordinate</param>
        /// <param name="text">Text to display (neat!)</param>
        /// <param name="color">Color of the text</param>
        /// <param name="alpha">Transparency of the text</param>
        /// <param name="time">Amount of time, in seconds, until the text disappears (-1 to keep)</param>
        /// <param name="topleft">Whether the coordinate specifies the topleft corner of the text (true), or its center (false).</param>
        public MserText(float xpos, float ypos, string text, Color color = default(Color), float alpha = 1f, float time = -1f, bool topleft = false) : base(xpos, ypos)
        {
            this.text = text;
            this.color = color;
            this.time = time;
            this.topleft = topleft;
            this.alpha = alpha;
        }

        public override void Draw()
        {
            var xOff = Graphics.GetStringWidth(text) / 2;
            var yOff = Graphics.GetStringHeight(text) / 2;

            if (this.topleft)
            {
                xOff = 0;
                yOff = 0;
            }

            Graphics.DrawString(text, new Vec2(this.x - xOff, this.y - yOff ), color * alpha, 0.99f);
        }

        public override void Update()
        {
            // += 1/60
            if (this.time > 0)
            {
                this.counter += Maths.IncFrameTimer();

                if (this.counter >= this.time)
                {
                    //Fade
                    this.alpha -= 0.01f;
                }
            }

            if (this.alpha <= 0f)
            {
                //Remove
                Level.Remove(this);
            }

            base.Update();
        }
    }
}

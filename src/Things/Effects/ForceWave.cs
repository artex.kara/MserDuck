﻿using System.Collections.Generic;

namespace DuckGame.MserDuck
{

    internal class ForceWave : Thing
    {

        public ForceWave(float xpos, float ypos, int dir, float alphaSub, float speed, float speedv, Duck own) : base(xpos, ypos, null)
        {
            this.offDir = (sbyte)dir;
            base.graphic = new Sprite("sledgeForce", 0f, 0f);
            this.center = new Vec2((float)base.graphic.w, (float)base.graphic.h);
            this._alphaSub = alphaSub;
            this._speed = speed;
            this._speedv = speedv;
            this._collisionSize = new Vec2(6f, 30f);
            this._collisionOffset = new Vec2(-3f, -15f);
            base.graphic.flipH = (this.offDir <= 0);
            this.owner = own;
            base.depth = -0.7f;
        }

        public override void Update()
        {
            if (base.alpha > 0.1f)
            {
                IEnumerable<PhysicsObject> enumerable = Level.CheckRectAll<PhysicsObject>(base.topLeft, base.bottomRight);
                foreach (PhysicsObject current in enumerable)
                {
                    if (!this._hits.Contains(current) && current != this.owner)
                    {
                        Grenade grenade = current as Grenade;
                        if (grenade != null)
                        {
                            grenade.PressAction();
                        }
                        current.hSpeed = ((this._speed - 3f) * (float)this.offDir * 1.5f + (float)this.offDir * 4f) * base.alpha;
                        current.vSpeed = (this._speedv + -4.5f) * base.alpha;
                        current.clip.Add(this.owner as MaterialThing);
                        if (!current.destroyed)
                        {
                            current.Destroy(new DTImpact(this));
                        }
                        this._hits.Add(current);
                    }
                }
                IEnumerable<Door> enumerable2 = Level.CheckRectAll<Door>(base.topLeft, base.bottomRight);
                foreach (Door current2 in enumerable2)
                {
                    if (!current2.destroyed)
                    {
                        current2.Destroy(new DTImpact(this));
                    }
                }
                IEnumerable<Window> enumerable3 = Level.CheckRectAll<Window>(base.topLeft, base.bottomRight);
                foreach (Window current3 in enumerable3)
                {
                    if (!current3.destroyed)
                    {
                        current3.Destroy(new DTImpact(this));
                    }
                }
            }
            base.x += (float)this.offDir * this._speed;
            base.y += this._speedv;
            base.alpha -= this._alphaSub;
            if (base.alpha <= 0f)
            {
                Level.Remove(this);
            }
        }

        private float _alphaSub;

        private List<Thing> _hits = new List<Thing>();

        private float _speed;

        private float _speedv;
    }
}

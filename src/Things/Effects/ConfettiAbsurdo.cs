﻿namespace DuckGame.MserDuck
{
    public class ConfettiAbsurdo : ConfettiNormalo
    {

        public ConfettiAbsurdo(float xpos, float ypos, Color color = default(Color), Vec2 speed = default(Vec2), float scale = 1f) : base(xpos, ypos, color, speed)
        {
            this.scale = new Vec2(scale,scale);
        }

        public override void Update()
        {
            base.Update();
        }
    }
}

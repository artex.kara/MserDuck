﻿using System.Collections.Generic;

namespace DuckGame.MserDuck
{
    public class ConfettiNormalo : Thing
    {
        private bool _rested = false;

        private SpriteMap _sprite;

        private readonly List<Color> colorList = new List<Color>()
        {
            new Color(100, 145, 230), new Color(245, 160, 110), new Color(0, 222, 155), new Color(212, 63, 222), new Color(222, 46, 75), new Color(210, 222, 91)
        };

        public ConfettiNormalo(float xpos, float ypos, Color color = default(Color), Vec2 speed = default(Vec2)) : base(xpos,ypos)
        {
            this.hSpeed = speed.x;
            this.vSpeed = speed.y;
            this._sprite = new SpriteMap(this.GetPath("confetti"), 4, 4, false);
            this._sprite.AddAnimation("float", 1f, true,
                0, 1, 2);
            this._sprite.SetAnimation("float");
            this.center = new Vec2(2f, 2f);
            if (Rando.Double() > 0.5)
            {
                this._sprite.flipH = true;
            }
            else
            {
                this._sprite.flipH = false;
            }
            base.graphic = this._sprite;
            if (color == default(Color))
            {
                //random color
                base.graphic.color = colorList[Rando.Int(0, this.colorList.Count - 1)];
            }
            else
            {
                base.graphic.color = color;
            }
            this._rested = false;
        }

        public override void Update()
        {
            if (this._rested)
            {
                return;
            }
            if (this.hSpeed > 0f)
            {
                this.hSpeed -= 0.1f;
            }
            if (this.hSpeed < 0f)
            {
                this.hSpeed += 0.1f;
            }
            if ((double)this.hSpeed < 0.1 && this.hSpeed > -0.1f)
            {
                this.hSpeed = 0f;
            }
            if (this.vSpeed < 1f)
            {
                this.vSpeed += 0.06f;
            }
            if (this.vSpeed < 0f)
            {
                this._sprite.speed = 0f;
                Thing col = Level.CheckPoint<Block>(base.x, base.y - 7f, null, null);
                if (col != null)
                {
                    this.vSpeed = 0f;
                }
            }
            else
            {
                Thing col2 = Level.CheckPoint<IPlatform>(base.x, base.y + 3f, null, null) as Thing;
                if (col2 != null)
                {
                    this.vSpeed = 0f;
                    this._sprite.speed = 0f;
                    if (col2 is Block)
                    {
                        this._rested = true;
                    }
                }
                else
                {
                    this._sprite.speed = 0.3f;
                }
            }
            base.x += this.hSpeed;
            base.y += this.vSpeed;
        }
    }
}

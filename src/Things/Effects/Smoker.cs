﻿namespace DuckGame.MserDuck
{
	[EditorGroup ("stuff|mser|lvl_special")]
	class Smoker : Thing
	{

		public EditorProperty<int> r = new EditorProperty<int>(255, null, 0f, 255f, 1f, null, false, false);
		public EditorProperty<int> g = new EditorProperty<int>(255, null, 0f, 255f, 1f, null, false, false);
		public EditorProperty<int> b = new EditorProperty<int>(255, null, 0f, 255f, 1f, null, false, false);
		public EditorProperty<float> power = new EditorProperty<float>(1.0f, null, 0.05f, 5.0f, 0.05f, "Not much", false, false);
		public EditorProperty<float> angel = new EditorProperty<float>(180.0f, null, 0f, 360f, 0.5f, null, false, false);
		public EditorProperty<float> spread = new EditorProperty<float>(20.0f, null, 0f, 170f, 0.5f, null, false, false);
		public EditorProperty<float> density = new EditorProperty<float>(0.2f, null, 0.05f, 1.0f, 0.05f, null, false, false);


		public DuckGame.Color col;
		public Smoker (float xpos, float ypos) : base (xpos, ypos)
		{
			base.graphic = new Sprite (GetPath ("smoker"), 0f, 0f);
			this.center = new Vec2 (5f, 3f);
			this.collisionSize = new Vec2 (10f, 6f);
			this.collisionOffset = new Vec2 (-5f, -6f);
			base.depth = 0f;
			base.hugWalls = WallHug.Floor;
			this._isStateObject = true;
			this._isStateObjectInitialized = true;
		}

		public override void Initialize() {
			col = new DuckGame.Color ((byte)r, (byte)g, (byte)b);
		}
			
		public override void DrawHoverInfo ()
		{
			string text = "Color: [";
			text += this.r.value.ToString () + ", ";
			text += this.g.value.ToString () + ", ";
			text += this.b.value.ToString () + "] ";

			text += "Power: " + this.power.value.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture);
			text += "; Angle: " + this.angel.value.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture);
			Graphics.DrawString (text, this.position + new Vec2 (-Graphics.GetStringWidth (text, false, 1f) / 2f, -16f), Color.White, 0.9f, null, 1f);
		}

		public override string GetDetailsString ()
		{
			//WIP do not comment on this inefficient piece of code kthxbai
			return string.Concat (new string[] {
				base.GetDetailsString ()
			});
		}

		public override void Update ()
		{
			if (Rando.Float (0f, 1f) <= density.value) {
				Vec2 vec = Maths.AngleToVec (Maths.DegToRad (-90f + angel.value + Rando.Float (-spread.value, spread.value)));
				Vec2 vec2 = new Vec2 (vec.x * Rando.Float (2f, 3f), vec.y * Rando.Float (2f, 3f));
				ExtinguisherSmoke extinguisherSmoke = new ExtinguisherSmoke (this.position.x, this.position.y, false);
				extinguisherSmoke.hSpeed = vec2.x * power.value;
				extinguisherSmoke.vSpeed = vec2.y * power.value;
				extinguisherSmoke.graphic.color = col;
				Level.Add (extinguisherSmoke);
			}
		}

	}
}

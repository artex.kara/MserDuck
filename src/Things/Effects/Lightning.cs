﻿using System;
using System.Collections.Generic;

namespace DuckGame.MserDuck
{
    public class Lightning : Thing
    {
        /*Tex2D tex;
        Tex2D texW;*/

        bool bounced = false;

        int timer = 0;

        int length = 30;

        bool removing = false;

        List<Vec2> poslist = new List<Vec2>();

        Gun origin;

        public Lightning(Gun origin) : base(origin.x, origin.y, null)
        {
            this.origin = origin;
            /*this.tex = Content.Load<Tex2D>(GetPath("bolt"));
            this.texW = Content.Load<Tex2D>(GetPath("boltW"));*/
            this.poslist.Add(Vec2.Zero);
        }

        public override void Initialize()
        {
            if (Network.isActive)
            {
                Rando.generator = new Random(NetRand.currentSeed);
            }

            //Set max length of lightning bolts
            this.length = Rando.Int(25, 35);

            base.Initialize();
        }

        public override void Update()
        {
            if (!this.removing && this.timer >= Rando.Int(3, 6))
            {
                //Add a new segment
                this.timer = 0;
                poslist.Add(Vec2.Add(poslist[poslist.Count - 1], new Vec2(Rando.Int(10, 25), Rando.Int(-8, 8))));

                //Rotate all by barrelAngle
                for (int i = 1; i < poslist.Count; i++)
                {
                    this.poslist[i] = this.poslist[i].Rotate(Maths.DegToRad(this.origin.barrelAngle), poslist[i - 1]);
                }
            }
            else if (this.removing && this.timer >= 50)
            {
                //Finished fading, remove.
                Level.Remove(this);
            }
            else
            {
                //Increment timer otherwise.
                this.timer++;
            }

            //Iterate through each segment
            if (poslist.Count >= 20 || this.bounced)
            {
                //Too many bolts, start removing now
                this.removing = true;
                this.timer = 0;
            }
            else if (poslist.Count > 1)
            {
                for (int i = 1; i < poslist.Count; i++)
                {
                    var p1 = this.origin.barrelPosition + poslist[i - 1];
                    var p2 = this.origin.barrelPosition + poslist[i];

                    //Collision test
                    var things = Level.CheckLineAll<PhysicsObject>(p1, p2);
                    foreach (var th in things)
                    {
                        if (th != this.origin.duck || this.bounced)
                        {
                            th.ApplyForce(th.position - p1);
                            th.Destroy(new DTElectrocute(this));
                        }
                    }

                    var block = Level.CheckLine<Block>(p1, p2);
                    if (block != null && !this.bounced)
                    {
                        //No longer check for collisions, but allow self-killing.
                        this.bounced = true;
                    }
                }
            }

            base.Update();
        }

        public override void Draw()
        {
            //Iterate through each segment
            for (var i = 1; i < poslist.Count; i++)
            {
                var p1 = this.origin.barrelPosition + poslist[i - 1];
                var p2 = this.origin.barrelPosition + poslist[i];
                var a = this.removing ? 1 - this.timer / 50 : 1;
            
                //Draw main bolt and additional bits
                Graphics.DrawLine(p1, p2, new Color(128, 180, 255, 96 * a), 2);
                Graphics.DrawLine(Vec2.Add(p1, new Vec2(Rando.Float(-5, 5), Rando.Float(-5, 5))),
                                  Vec2.Add(p2, new Vec2(Rando.Float(-5, 5), Rando.Float(-5, 5))),
                                  new Color(192, 210, 255, 48 * a), 1);
            }
        }
    }
}

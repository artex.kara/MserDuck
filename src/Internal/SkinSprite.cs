﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// A sprite to be used by a Skin. Gets its meta data from a SkinInfo instance.
    /// </summary>
    public class SkinSprite : ICloneable
    {
        /// <summary>
        /// Path to the files containing info about this SkinSprite, without extension.
        /// </summary>
        public string SpritePath
        { get { return Path.Combine(Skin.SkinsFolder, this.LocalSpritePath); } }

        /// <summary>
        /// Local path to the files containing info about this SkinSprite, without extension.
        /// </summary>
        public string LocalSpritePath;

        /// <summary>
        /// Local path to the texture file of this SkinSprite.
        /// </summary>
        public string LocalTexturePath
        { get { return Path.Combine(Skin.SkinsFolder, this.LocalSpritePath + ".png"); } }

        /// <summary>
        /// Path to the texture file of this SkinSprite.
        /// </summary>
        public string TexturePath
        { get { return Path.Combine(Skin.SkinsDirectory, this.LocalSpritePath + ".png"); } }

        /// <summary>
        /// Local path to the meta data info file of this SkinSprite. It is loaded in form of a SkinInfo instance.
        /// </summary>
        public string LocalInfoPath
        { get { return Path.Combine(Skin.SkinsFolder, this.LocalSpritePath + ".txt"); } }

        /// <summary>
        /// Path to the meta data info file of this SkinSprite. It is loaded in form of a SkinInfo instance.
        /// </summary>
        public string InfoPath
        { get { return Path.Combine(Skin.SkinsDirectory, this.LocalSpritePath + ".txt"); } }

        /// <summary>
        /// Returns <code>this.SpritePath</code> without any directory, leaving only the skin's name behind.
        /// </summary>
        public string SpriteName
        { get { return Path.GetFileNameWithoutExtension(this.LocalSpritePath); } }

        /// <summary>
        /// The spritemap assigned to this skin. Will be null before any Initialize calls.
        /// </summary>
        public SpriteMap SpriteMap;

        /// <summary>
        /// Whether this sprite should be recolored before use.
        /// </summary>
        public bool Recolor;

        /// <summary>
        /// Size of the sprite.
        /// </summary>
        public Vec2 Size;

        private List<SkinAnimation> AnimList;

        /// <summary>
        /// Creates a new SkinSprite.
        /// </summary>
        /// <param name="path">Relative path to the files containing info about this SkinSprite, without extension.</param>
        public SkinSprite(string path)
        {
            this.LocalSpritePath = path;
        }

        /// <summary>
        /// Reads the .txt file to create the spritemap. Not loading any fallbacks, be sure to specify everything needed!
        /// </summary>
        public void Initialize()
        {
            //Override info if specified
            SkinInfo info = null;
            if (File.Exists(this.InfoPath))
            {
                //Load txt and apply new data
                info = new SkinInfo(this.InfoPath);
            }

            //Override texture if specified
            if (File.Exists(this.TexturePath))
            {
                //Get size either from fallback, or from info - if present
                this.Size = info == null || info.Size == Vec2.Zero ? new Vec2(this.SpriteMap.width, this.SpriteMap.height) : info.Size;

                //Load png
                var tex = Thing.GetPath<MserDuck>(this.LocalTexturePath);
                if (this.SpriteMap == null)
                {
                    //Didn't have default sprite before? Create from scratch
                    this.SpriteMap = new SpriteMap(tex, (int)this.Size.x, (int)this.Size.y);

                    //Specify center
                    if (info == null || !info.Center.HasValue)
                    {
                        if (this.SpriteName.EqualsTo(true, "skipsign"))
                        {
                            //Definitive
                            this.SpriteMap.center = new Vec2(this.Size.x - 3f, 15f);
                        }
                        else
                        {
                            //Othery
                            this.SpriteMap.CenterOrigin();
                        }
                    }
                }
                else
                {
                    //Clear cached texture first
                    MserGlobals.RemoveTextureFromCache(tex, true);

                    //Only replace data that needs to be replaced
                    var t = Content.Load<Tex2D>(tex);
                    this.SpriteMap.texture = t;
                    t.frameWidth = this.Size.x;
                    t.frameHeight = this.Size.y;
                    MserGlobals.GetField(typeof(SpriteMap), "_width").SetValue(this.SpriteMap, (int)this.Size.x);
                    MserGlobals.GetField(typeof(SpriteMap), "_height").SetValue(this.SpriteMap, (int)this.Size.y);
                    //this.SpriteMap.AddDefaultAnimation();
                }
            }

            if (info != null)
            {
                //Apply info if any was loaded before
                if (info.Recolor.HasValue)
                    this.Recolor = info.Recolor.Value;
                if (info.AnimSpeed.HasValue)
                    this.SpriteMap.speed = info.AnimSpeed.Value;
                if (info.Center.HasValue)
                    this.SpriteMap.center = info.Center.Value;

                if (info.AnimList.Any())
                {
                    //Reset animations
                    this.SpriteMap.ClearAnimations();
                    MserGlobals.GetMethod(typeof(SpriteMap), "AddDefaultAnimation").Invoke(this.SpriteMap, null);

                    //Merge existing
                    if (this.AnimList != null)
                    {
                        foreach (var anim in this.AnimList)
                        {
                            if (!info.AnimList.Any(a => a.Name == anim.Name))
                            {
                                //Need to add new entry
                                info.AnimList.Add(anim);
                            }
                        }
                    }

                    for (int i = 0; i < info.AnimList.Count; i++)
                    {
                        //Add animation, from config list
                        var anim = info.AnimList[i];
                        this.SpriteMap.AddAnimation(anim.Name, anim.Speed, anim.Looping, anim.Frames);
                    }

                    //Store for later use
                    this.AnimList = info.AnimList;

                    //TODO: SkinSprite - check if default anim name actually exists
                    //this.SpriteMap.SetAnimation(info.AnimDefault);
                }
            }
        }

        public static readonly FieldInfo _animations = MserGlobals.GetField(typeof(SpriteMap), "_animations");

        /// <summary>
        /// Recolors the SpriteMap.
        /// </summary>
        public SpriteMap DoRecolor(Vec3 color)
        {
            //TODO: SkinSprite - redo the Recolor method since duck game's recolor shader is really shit for custom sprites
            //  Somehow make a shader to tint all grayscale values (OR EVEN BETTER: PARAMETER OF COLORS TO REPLACE?) and leave the rest alone

            if (this.SpriteMap == null)
                return null;

            var map = this.SpriteMap.CloneMap();
            //Clone doesn't copy everything!
            map.speed = this.SpriteMap.speed;
            map.texture = Graphics.Recolor(map.texture, color);
            map.texture.frameWidth = this.Size.x;
            map.texture.frameHeight = this.Size.y;
            map.SetAnimation(this.SpriteMap.currentAnimation);
            return map;
        }

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>
        /// A new object that is a copy of this instance.
        /// </returns>
        public object Clone()
        {
            var sprite = new SkinSprite(this.LocalSpritePath);
            sprite.SpriteMap = this.SpriteMap.CloneMap();
            sprite.SpriteMap.speed = this.SpriteMap.speed;
            sprite.SpriteMap.SetAnimation(this.SpriteMap.currentAnimation);
            sprite.Recolor = this.Recolor;
            sprite.Size = this.Size;
            sprite.AnimList = this.AnimList;
            return sprite;
        }
    }
}

﻿using System;
using System.Diagnostics;

namespace DuckGame.MserDuck
{
    public static class GitHelper
    {
        /// <summary>
        /// Updates the CommitNumber variable using git. This method should be run async to avoid a lockup from git.exe!
        /// </summary>
        public static void UpdateCommitNumber()
        {
            try
            {
                //Returns one number, specifying commit amount
                //TODO: GitHelper - detect whether git is installed, to avoid an "error" here if git is not installed
                var proc = RunGitCommand("rev-list HEAD --count");

                while (!proc.StandardOutput.EndOfStream)
                {
                    var line = proc.StandardOutput.ReadLine();
                    int num;
                    if (int.TryParse(line, out num) && num > -1)
                    {
                        MserDuck.CommitNumber = num;
                        break;
                    }
                }
                proc.WaitForExit(1000);
            }
            catch (Exception e)
            {
                MserDuck.CommitNumber = -2;
                Program.LogLine("ERROR DETECTING VERSION: " + e.Message + e.StackTrace);
            }

            if (MserDuck.CommitNumber >= 0)
            {
                Program.LogLine(string.Format("Loading v{0} of MserDuck has finished. Enjoy your stay! :)", MserDuck.CommitNumber));
            }
            else
            {
                Program.LogLine("Loading MserDuck has finished. Enjoy your stay! :)");
            }
            MenuBackgroundHandler.UpdateCommitNumber();
        }

        /// <summary>
        /// Checks for an update for MserDuck, or applies any. (git fetch / git merge)
        /// </summary>
        /// <param name="apply">False for git fetch (first), true for git merge (afterwards)</param>
        public static void CheckMserDuckUpdate(bool apply)
        {
            var proc = RunGitCommand(apply ? "merge" : "fetch");
            //TODO: GitHelper - return some info about the update progress etc, to be able to display it for commands etc
        }

        /// <summary>
        /// Runs an update on MserDuck. (git pull)
        /// </summary>
        public static void ForceMserDuckUpdate()
        {
            var proc = RunGitCommand("pull");
            //TODO: GitHelper - return some info about the update progress etc, to be able to display it for commands etc
        }

        /// <summary>
        /// Full path to the git executable. Set using <see cref="ConfigEntries.GitPathEntry"/> in config.
        /// </summary>
        public static string GitPath = "git.exe";

        /// <summary>
        /// Executes a git command with the specified commandline arguments, returning the process created.
        /// </summary>
        /// <param name="args">Commandline arguments.</param>
        /// <returns></returns>
        public static Process RunGitCommand(string args)
        {
            try
            {
                var pinfo = new ProcessStartInfo(GitHelper.GitPath, args);
                pinfo.WorkingDirectory = MserGlobals.GetModDir();
                pinfo.CreateNoWindow = true;
                pinfo.UseShellExecute = false;
                pinfo.WindowStyle = ProcessWindowStyle.Hidden;
                pinfo.RedirectStandardOutput = true;
                pinfo.RedirectStandardError = true;

                var proc = new Process();
                proc.StartInfo = pinfo;
                if (!proc.Start())
                    return null;
                return proc;
            }
            catch (Exception e)
            {
                Program.LogLine("Error executing git command: " + e.Message + e.StackTrace);
                return null;
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Manager and handler of alias commands, including many helper methods for working with them.
    /// </summary>
    public static class AliasManager
    {
        /// <summary>
        /// Adds a new alias with the specified name, executing the specified command. Replaces an existing one with the same name.
        /// </summary>
        /// <param name="name">Name of the alias.</param>
        /// <param name="cmd">Command to be executed.</param>
        public static void AddAlias(string name, BoundCommand cmd)
        {
            AddAlias(name, cmd.ToString(false, false));
        }

        /// <summary>
        /// Adds a new alias with the specified name, executing the specified command. Replaces an existing one with the same name.
        /// </summary>
        /// <param name="name">Name of the alias.</param>
        /// <param name="commandline">Command name and arguments.</param>
        public static void AddAlias(string name, string commandline)
        {
            var cmdline = ChatManager.GetSplitText(commandline);
            var cmd = ChatManager.GetCommandByName(cmdline[0]);
            var args = cmdline.GetRange(1, cmdline.Count - 1);
            var exist = GetAlias(name);
            if (exist != null)
            {
                //Replace existing alias
                exist.CommandList[0] = cmd;
                exist.Arguments = args;
                return;
            }

            //Add new alias
            var bound = new BoundCommand(cmd, args);
            bound.Name = name;
            AliasManager.Aliases.Add(bound);
        }

        /// <summary>
        /// Executes the specified alias, using the specified parameters.
        /// <para />Returns whether the execution was successful.
        /// </summary>
        /// <param name="name">Alias name.</param>
        /// <param name="paras">Can be null if the alias does not accept any parameters.</param>
        /// <returns></returns>
        public static bool ExecuteAlias(string name, IEnumerable<string> paras = null)
        {
            var exist = GetAlias(name);
            if (exist == null)
                return false;

            //Gets args for existing
            var cmdline = InsertArguments(exist.ToString(true, true), paras);

            if (cmdline == null)
                return false;

            //Execute it!
            ChatManager.EnterChatMessage(cmdline);
            return true;
        }

        /// <summary>
        /// Inserts the specified parameters into the commandline string.
        /// </summary>
        /// <param name="commandline"></param>
        /// <param name="paras"></param>
        /// <returns></returns>
        private static string InsertArguments(string commandline, IEnumerable<string> paras)
        {
            //Initial param handling - no params specified
            if (paras == null || !paras.Any())
            {
                //Doesn't need params? return directly.
                if (!commandline.Contains("%"))
                    return commandline;

                //Needs params? Well crap.
                return null;
            }
            else if (!commandline.Contains("%"))
            {
                //Command has params but does not need them
                return null;
            }

            //Insert em!
            var sb = new StringBuilder(commandline.Length);
            var inpara = false;
            var indef = false;
            var i = 0;
            var p = 0;
            StringBuilder defval = null;
            do
            {
                var ch = commandline[i];

                if (ch == '%')
                {
                    //If was in param, insert
                    if (inpara)
                    {
                        var paraval = paras.ElementAtOrDefault(p);
                        if (paraval == null)
                        {
                            //Try default value
                            if (defval == null)
                                return null;

                            paraval = defval.ToString();
                            defval = null;
                        }
                        sb.Append(paraval);
                        p++;
                    }

                    //Para handling
                    inpara = !inpara;
                    indef = false;
                }
                else if (inpara)
                {
                    //Para name is irrelevant for us - only check for default value etc
                    if (ch == '=')
                    {
                        //Start getting default value!
                        indef = true;
                        defval = new StringBuilder();
                    }
                    else if (indef)
                    {
                        //Construct default value
                        defval.Append(ch);
                    }
                }
                else
                {
                    //Simply add to main builder if not in arg
                    sb.Append(ch);
                }

                i++;
            } while (i < commandline.Length);

            return sb.ToString();
        }

        /// <summary>
        /// Returns the amount of parameters in the command.
        /// </summary>
        /// <returns></returns>
        public static int CountParams(string cmdline, bool includeOptional = true)
        {
            if (!includeOptional)
                return CountParams(cmdline) - cmdline.CountCharOccurences('=');

            return cmdline.CountCharOccurences('%') / 2;
        }

        /// <summary>
        /// Returns the amount of parameters in the command.
        /// </summary>
        /// <returns></returns>
        public static int CountParams(BoundCommand alias, bool includeOptional = true)
        {
            if (!includeOptional)
                //HACK: /alias CountParams - this is not at all reliable, due to any command being able to contain an = (or %)
                return CountParams(alias) - alias.Arguments.Sum(a => a.CountCharOccurences('='));

            return alias.Arguments.Sum(a => a.CountCharOccurences('%')) / 2;
        }

        /// <summary>
        /// Returns the parameter's name given its position in the alias.
        /// </summary>
        /// <param name="alias"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static string GetParameterName(BoundCommand alias, int index)
        {
            var sb = new StringBuilder();
            var inpara = false;
            var i = 0;
            var p = 0;
            var commandline = alias.ToString(false, false);

            do
            {
                var ch = commandline[i];

                if (ch == '%')
                {
                    //If was in param, insert
                    if (inpara)
                    {
                        p++;

                        if (index == p)
                            return sb.ToString();

                        sb.Clear();
                    }

                    //Para handling
                    inpara = !inpara;
                }
                else if (inpara)
                {
                    //Get para name until = (or %, see above)
                    if (ch == '=')
                    {
                        //Thats all we need
                        p++;

                        if (index == p)
                            return sb.ToString();

                        sb.Clear();

                        //Go out of param
                        do
                        {
                            i++;
                        } while (commandline[i] != '%');
                        inpara = false;
                    }
                    else
                    {
                        sb.Append(ch);
                    }
                }

                i++;
            } while (i < commandline.Length);

            return null;
        }

        /// <summary>
        /// Whether the specified parameter of the specified alias is an optional parameter or required.
        /// <para />Returns false as well if the index is out of bounds.
        /// </summary>
        /// <param name="alias"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static bool IsOptionalParameter(BoundCommand alias, int index)
        {
            var inpara = false;
            var i = 0;
            var p = 0;
            var commandline = alias.ToString(false, false);

            do
            {
                var ch = commandline[i];

                if (ch == '%')
                {
                    //If was in param, check for index
                    if (inpara)
                    {
                        p++;

                        if (index == p)
                            return false;
                    }

                    //Para handling
                    inpara = !inpara;
                }
                else if (inpara)
                {
                    //Get para until = (or %, see above)
                    if (ch == '=')
                    {
                        //Thats all we need
                        p++;

                        if (index == p)
                            return true;

                        //Go out of param
                        do
                        {
                            i++;
                        } while (commandline[i] != '%');
                        inpara = false;
                    }
                }

                i++;
            } while (i < commandline.Length);

            return false;
        }

        /// <summary>
        /// Returns the alias with the specified name, or null if it does not exist.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static BoundCommand GetAlias(string name)
        {
            return AliasManager.Aliases.FirstOrDefault(a => a.Name == name);
        }

        /// <summary>
        /// List of aliases currently set.
        /// </summary>
        public static readonly List<BoundCommand> Aliases = new List<BoundCommand>();
    }
}

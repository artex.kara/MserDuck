﻿using System.IO;

namespace DuckGame.MserDuck
{
    public static class DuckLogManager
    {
        private const string ducklogfile = "ducklog.txt";

        /// <summary>
        /// Current ClearLog setting. Set using <see cref="ConfigEntries.ClearLogEntry"/> in config.
        /// </summary>
        public static RemoveLogMode RemoveLog = RemoveLogMode.Never;

        /// <summary>
        /// Maximum filesize to allow for DuckLog file. Set using <see cref="ConfigEntries.ClearLogEntry"/> in config.
        /// </summary>
        public static FileSize MaxFileSize;

        /// <summary>
        /// Depending on the settings and state, removes the ducklog file.
        /// </summary>
        public static void RemoveDuckLog()
        {
            if (DuckLogManager.RemoveLog == RemoveLogMode.Never)
                return;

            if (DuckLogManager.RemoveLog == RemoveLogMode.FileSize)
            {
                //Check filesize first
                var size = new FileSize((ulong)new FileInfo(DuckLogManager.ducklogfile).Length);
                if (size > DuckLogManager.MaxFileSize)
                {
                    Program.LogLine(string.Format("Ducklog file was too big ({0} > {1}) and has been removed.", size, DuckLogManager.MaxFileSize));
                    File.Delete(ducklogfile);
                }
            }
            else
            {
                //Simply delete
                File.Delete(ducklogfile);
                Program.LogLine("Ducklog file has been auto-removed.");
            }
        }

        public enum RemoveLogMode
        {
            Never, OnStart, OnQuit, FileSize
        }
    }
}

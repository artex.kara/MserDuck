﻿using System;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Container for multi-purpose icons. Be sure to check/set properties such as scale before drawing, since those may not be reset!
    /// <para />Use the Asset Explorer to easily check the frames of all icons.
    /// </summary>
    public static class IconManager
    {
        /// <summary>
        /// Set of TinyIcons (modded stock content). Size is 8x8.
        /// </summary>
        public static readonly SpriteMap TinyIcons = new SpriteMap(Thing.GetPath<MserDuck>("tinyIcons"), 8, 8) {depth = 0.98f};

        /// <summary>
        /// Frame values for the TinyIcons.
        /// </summary>
        public enum TinyIconsFrames
        {
            Cross, Checkmark, Folder, WhiteArrowRight, CyanCircle, ThreeHorizontalLines, Shield, CurvedBlueArrow, Pencil, Heart
        }

        /// <summary>
        /// Set of ButtonIcons (custom content). Size is 24x24.
        /// </summary>
        public static readonly SpriteMap ButtonIcons = new SpriteMap(Thing.GetPath<MserDuck>("buttonIcons"), 24, 24) { depth = 0.98f };

        /// <summary>
        /// Frame values for the ButtonIcons.
        /// </summary>
        public enum ButtonIconsFrames
        {
            ClockChangeValue, ClockSameValue, ClockCrossed, Exclamation, DebugPrintBox, DebugAssetBox, DebugThingList, Eye, Snowflake, Cross, Fold, Unfold, DebugErrorLog,
            DebugProfilerBox, Checkmark, Clock, Refresh, RefreshCrossed, DebugCacheManager, Questionmark, DebugColorPicker, Eyedropper,
            DebugBox = 63
        }

        /// <summary>
        /// Returns the icon with the specified data from the specified iconset.
        /// <para />Use the pre-made icon enums instead of guessing frame numbers like a nub!
        /// </summary>
        /// <returns></returns>
        public static Sprite GetIconFromSet(SpriteMap iconset, int frame, Vec2 position, Vec2 scale, Depth depth, Color color)
        {
            iconset.frame = frame;
            var icon = iconset.Clone();
            icon.position = position;
            icon.scale = scale;
            icon.depth = depth;
            icon.color = color;
            return icon;
        }

        /// <summary>
        /// Returns the icon with the specified data from the specified iconset.
        /// <para />Use the pre-made icon enums instead of guessing frame numbers like a nub!
        /// </summary>
        /// <returns></returns>
        public static Sprite GetIconFromSet(SpriteMap iconset, int frame, Vec2 position, Vec2 scale, Depth depth)
        {
            return GetIconFromSet(iconset, frame, position, scale, depth, Color.White);
        }

        /// <summary>
        /// Returns the icon with the specified data from the specified iconset.
        /// <para />Use the pre-made icon enums instead of guessing frame numbers like a nub!
        /// </summary>
        /// <returns></returns>
        public static Sprite GetIconFromSet(SpriteMap iconset, int frame, Vec2 position, Vec2 scale)
        {
            return GetIconFromSet(iconset, frame, position, scale, 1f, Color.White);
        }

        /// <summary>
        /// Returns the icon with the specified data from the specified iconset.
        /// <para />Use the pre-made icon enums instead of guessing frame numbers like a nub!
        /// </summary>
        /// <returns></returns>
        public static Sprite GetIconFromSet(SpriteMap iconset, int frame, Vec2 position)
        {
            return GetIconFromSet(iconset, frame, position, Vec2.One, 1f, Color.White);
        }
    }
}

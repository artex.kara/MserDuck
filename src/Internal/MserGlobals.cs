﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using DuckGame.MserDuck.CommandHandlers;
using Microsoft.Xna.Framework.Graphics;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// A collection of helper methods and mod state fields for use when working with DuckGame and MserDuck.
    /// <para />For more general-use methods, you may want to check <see cref="Extensions"/>.
    /// <para />The class has another part (<code>MserGlobalsReflection.cs</code>) which contains code related to reflection.
    /// </summary>
    public static partial class MserGlobals
    {
        /// <summary>
        /// Returns the current state of the game, determined by the current Level.
        /// </summary>
        /// <returns></returns>
        public static GameState GetGameState()
        {
            // If no level, has to be loading
            if (Level.current == null) { return GameState.Loading; }

            // Since most Level subclasses are internal, getting their name is the only way to reliably identify them.
            var gametype = Level.current.GetType().Name;
            if (gametype == "DeathmatchLevel" || gametype == "GameLevel" || gametype == "Level" ||
                gametype == "ArcadeLevel" || gametype == "ChallengeLevel" || gametype == "EditorTestLevel" ||
                gametype == "TestArea" || gametype == "DuckGameTestArea") { return GameState.Playing; }
            if (gametype == "TitleScreen") { return GameState.TitleScreen; }
            if (gametype == "TeamSelect2") { return GameState.Lobby; } //HINT: REMINDER MserGlobals - Add new lobby level once custom is in place.
            if (gametype == "RockIntro") { return GameState.RockThrowIntro; }
            if (gametype == "RockScoreboard") { return GameState.RockThrow; }
            if (gametype == "Editor") { return GameState.LevelEditor; }

            return GameState.Unknown;
        }

        /// <summary>
        /// Returns the base directory of the mod.
        /// </summary>
        /// <returns></returns>
        public static string GetModDir()
        {
            return ModLoader.GetMod<MserDuck>().configuration.directory.Replace('/', Path.DirectorySeparatorChar);
        }

        /// <summary>
        /// Returns the Duck from the specified <code>Duck.profile.networkIndex</code> (used for transmitting references to a Duck over a NetMessage, for example).
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public static Duck GetDuckFromIndex(byte index)
        {
            var prof = MserGlobals.GetProfileFromIndex(index);
            if (prof == null) return null;
            return prof.duck;
        }

        /// <summary>
        /// Returns the Profile from the specified <code>Duck.profile.networkIndex</code> (used for transmitting references to a Duck over a NetMessage, for example).
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public static Profile GetProfileFromIndex(byte index)
        {
            return GetListOfProfiles().FirstOrDefault(p => p.networkIndex == index);
        }

        /// <summary>
        /// Returns a list of active profiles.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Profile> GetListOfProfiles()
        {
            if (Network.isActive)
                return DuckNetwork.profiles;
            return Profiles.active;
        }

        /// <summary>
        /// Returns the Duck that is local to the current connection, or the first duck when in singleplayer mode.
        /// </summary>
        /// <returns></returns>
        public static Duck GetLocalDuck()
        {
            if (Network.isActive)
            {
                //Check for count
                if (DuckNetwork.core.localDuckIndex == -1 || DuckNetwork.core.profiles.Count <= DuckNetwork.core.localDuckIndex)
                    return null;

                //Local duck index is set
                return DuckNetwork.profiles[DuckNetwork.core.localDuckIndex].duck;
            }
            else
            {
                //Check for count
                if (Profiles.active.Count == 0)
                    return null;

                //Get local duck in singleplayer
                return Profiles.active[0].duck;
            }
        }

        /// <summary>
        /// Returns a list of the specified type of things in the level.
        /// <para/>Returns null if not in a level.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<T> GetListOfThings<T>()
        {
            if (Level.current == null) return null;

            //This check is required to avoid a crash if the Thing can't be converted to the type T
            if (!(typeof(T).IsAssignableFrom(typeof(IConvertible))))
            {
                try
                {
                    //Try to get through direct casts
                    return Level.current.things[typeof(T)].Cast<T>();
                }
                catch
                {
                    //Only return valid entries
                    return Level.current.things[typeof(T)].OfType<T>();
                }
            }

            return Level.current.things[typeof(T)].Select(t => (T)Convert.ChangeType(t, typeof(T)));
        }

        /// <summary>
        /// Returns a list of things in the current level implementing the specified interface.
        /// <para/>Returns null if not in a level.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<Thing> GetListOfThingsInterface<T>()
        {
            if (Level.current == null) return null;

            //This check is required to avoid a crash if the Thing can't be converted to the type T
            if (!(typeof(T).IsAssignableFrom(typeof(IConvertible)))) return null;

            return Level.current.things[typeof(T)];
        }

        /// <summary>
        /// Returns a list of instances of all subclasses of Type <typeparamref name="T"/>, using default constructors for each.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static List<T> GetSubInstances<T>()
        {
            return MserGlobals.GetInstances<T>(false);
        }

        /// <summary>
        /// Returns a list of instances of all subclasses and implementing interfaces of Type <typeparamref name="T"/>, using default constructors for each.
        /// </summary>
        /// <typeparam name="T">Type to get its subclasses and implementing interfaces.</typeparam>
        /// <returns></returns>
        public static List<T> GetSubAndInterfaceInstances<T>()
        {
            return MserGlobals.GetInstances<T>(true);
        }

        /// <summary>
        /// Returns a list of instances of all subclasses of Type <typeparamref name="T"/>, using default constructors for each, allowing to specify whether interfaces should be included in the search.
        /// </summary>
        /// <typeparam name="T">Type to get its subclasses of.</typeparam>
        /// <param name="interfaces"></param>
        /// <returns></returns>
        private static List<T> GetInstances<T>(bool interfaces)
        {
            var typelist = interfaces ? Editor.GetSubclassesAndInterfaces(typeof(T)) : Editor.GetSubclasses(typeof(T));
            return typelist.Select(ty => (T)Activator.CreateInstance(ty)).ToList();
        }

        /// <summary>
        /// Similar to a regular <code>Level.Remove(t)</code> call, except guns will not show their smoke like they would when removed normally.
        /// </summary>
        /// <param name="t"></param>
        public static void RemoveThingStealthily(Thing t)
        {
            if (Level.current == null || t == null)
            {
                return;
            }

            /*By not running t.DoTerminate(), smoke creation of removing Guns normally is skipped.
             * Termination is rarely used and it should not be an issue when left out.
             * If it is an issue, however, we might need to temporarily override the Gun's Terminate method instead.
             */
            t.Removed();
            Level.current.things.Remove(t);
        }

        /// <summary>
        /// Displays the set text in the specified color on the screen. It is a Thing in the world, meaning it will stay at the created position.
        /// <para />For better debugging options, use <code>MserDebug.PrintEntry</code> instead.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="color"></param>
        /// <param name="alpha"></param>
        /// <param name="time">Time to stay in seconds.</param>
        /// <param name="xpos">-1 for center of screen</param>
        /// <param name="ypos">-1 for center of screen</param>
        /// <param name="topleft">Whether the x/y coordinates specify the top-left corner of the text or the center.</param>
        public static void DisplayText(string text, Color color = default(Color), float alpha = 1f, float time = 3f, float xpos = -1, float ypos = -1, bool topleft = false)
        {
            if (xpos < 0)
                xpos = Layer.HUD.camera.width / 2;

            if (ypos < 0)
                ypos = Layer.HUD.camera.height / 2;

            var texter = new MserText(xpos, ypos, text, color, alpha, time, topleft);
            Level.Add(texter);
        }

        /// <summary>
        /// Initializes the custom procedural level tiles by copying them over from the content folder.
        /// </summary>
        internal static void InitializeCustomLevels()
        {
            //Do copy - procedural
            var leveldir = Mod.GetPath<MserDuck>("custommaps/");
            if (Directory.Exists(leveldir))
            {
                foreach (var levelfile in Directory.GetFiles(leveldir, "*.lev", SearchOption.TopDirectoryOnly))
                {
                    var newlevelfile = Path.Combine(Content.path, "levels/procedural", Path.GetFileName(levelfile));
                    if (File.Exists(newlevelfile))
                    {
                        File.Delete(newlevelfile);
                    }
                    File.Copy(levelfile, newlevelfile);
                }
            }

            //Do copy - custom
            leveldir = Mod.GetPath<MserDuck>("customlevels/");
            var info = new DirectoryInfo(leveldir);
            if (info.Exists)
            {
                //Create custom levels directory inside of DuckGame/levels
                var customdir = Path.Combine(DuckFile.levelDirectory, "customlevels/");
                Directory.CreateDirectory(customdir);

                foreach (var levelfile in Directory.GetFiles(leveldir, "*.lev", SearchOption.TopDirectoryOnly))
                {
                    var newlevelfile = Path.Combine(customdir, Path.GetFileName(levelfile));
                    if (File.Exists(newlevelfile))
                    {
                        File.Delete(newlevelfile);
                    }
                    File.Copy(levelfile, newlevelfile);
                }
            }

            //Reinit as a reload
            LevelGenerator.ReInitialize();
        }

        /// <summary>
        /// Returns a list of all blocks in the current level.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Block> GetListOfBlocks(bool checkGroups = true)
        {
            var blocks = new HashSet<Block>();
            if (checkGroups)
            {
                foreach (BlockGroup blockgroup in GetListOfBlockGroups())
                {
                    foreach (var block in blockgroup.blocks)
                    {
                        blocks.Add(block);
                    }
                }
            }
            foreach (Block block in Level.current.things[typeof(Block)])
            {
                blocks.Add(block);
            }
            return blocks.ToList();
        }

        /// <summary>
        /// Returns a list of all blockgroups in the current level.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<BlockGroup> GetListOfBlockGroups()
        {
            return GetListOfThings<BlockGroup>();
        }

        /// <summary>
        /// Returns the directory in which the DuckGame.exe resides.
        /// <para />Note that you can simply access files inside of this folder using relative paths, since it is the mod's working directory.
        /// </summary>
        /// <returns></returns>
        public static string GetDuckGamePath()
        {
            return Path.GetDirectoryName(Application.ExecutablePath);
        }

        /// <summary>
        /// Same as <code>Directory.EnumerateFiles</code>, but accepts a regex as a search pattern.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="searchPatternExpression"></param>
        /// <param name="searchOption"></param>
        /// <param name="patternAppliesTo">What part of the path is matched with the regex.</param>
        /// <returns></returns>
        public static IEnumerable<string> EnumerateFilesRegex(string path, string searchPatternExpression = "",
                    SearchOption searchOption = SearchOption.TopDirectoryOnly, PathFilter patternAppliesTo = PathFilter.Extension)
        {
            Regex reSearchPattern = new Regex(searchPatternExpression, RegexOptions.IgnoreCase);
            return Directory.EnumerateFiles(path, "*", searchOption)
                            .Where(file => reSearchPattern.IsMatch(FilterFullPath(path, patternAppliesTo)));
        }

        /// <summary>
        /// Applies a filter to the specified path, only returning a certain part of it.
        /// <para />Can be used for allowing a function's parameter to specify a path filter.
        /// </summary>
        /// <param name="path">Full path name.</param>
        /// <param name="filter">What part of the path should be returned.</param>
        /// <returns></returns>
        public static string FilterFullPath(string path, PathFilter filter)
        {
            switch (filter)
            {
                case PathFilter.Extension:
                    return Path.GetExtension(path);
                case PathFilter.FileName:
                    return Path.GetFileName(path);
                case PathFilter.FileNameWithoutExtension:
                    return Path.GetFileNameWithoutExtension(path);
                case PathFilter.FullPath:
                    return path;
                case PathFilter.Directory:
                    return Path.GetDirectoryName(path);
                default:
                    throw new ArgumentOutOfRangeException("filter", filter, null);
            }
        }

        /// <summary>
        /// Same as <code>Directory.EnumerateFiles</code>, but accepts multiple search patterns.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="searchPatterns"></param>
        /// <param name="searchOption"></param>
        /// <returns></returns>
        public static IEnumerable<string> EnumerateFilesArray(string path, string[] searchPatterns, SearchOption searchOption = SearchOption.TopDirectoryOnly)
        {
            return searchPatterns.AsParallel().SelectMany(searchPattern => Directory.EnumerateFiles(path, searchPattern, searchOption));
        }

        /// <summary>
        /// Returns MserDuck's content folder path.
        /// </summary>
        /// <returns></returns>
        public static string GetContentDir()
        {
            return ModLoader.GetMod<MserDuck>().configuration.contentDirectory;
        }

        /// <summary>
        /// Returns whether all ducks in the server are connected (false if someone is in the process of connecting, for example).
        /// </summary>
        /// <returns></returns>
        public static bool AllDucksConnected()
        {
            return GetListOfProfiles().All(MserGlobals.IsDuckConnected);
        }

        /// <summary>
        /// Returns whether the specified profile is connected to the server.
        /// </summary>
        /// <param name="profile"></param>
        /// <returns></returns>
        public static bool IsDuckConnected(Profile profile)
        {
            if (!Network.isActive)
                return true;

            return profile.networkStatus != DuckNetStatus.Connecting &&
                   profile.networkStatus != DuckNetStatus.EstablishingCommunication &&
                   profile.networkStatus != DuckNetStatus.WaitingForLoadingToBeFinished &&
                   profile.connection != null &&
                   profile.connection.status != ConnectionStatus.Connecting;
        }

        /// <summary>
        /// Tries parsing the specified string as a random range between two floats. Format: <code>x-y</code>
        /// <para />If a decimal point is found, x and y are interpreted as floats, else as ints.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="randRes"></param>
        /// <returns></returns>
        public static bool TryParseRandom(string str, out float randRes)
        {
            randRes = 0.0f;

            if (!str.Contains('-'))
            {
                return false;
            }

            try
            {
                var split = str.Split('-');
                if (split.Length != 2)
                {
                    return false;
                }

                if (str.Contains("."))
                {
                    //Do for floats
                    var x = float.Parse(split[0]);
                    var y = float.Parse(split[1]);
                    if (x > y)
                    {
                        //Swap the variables
                        var z = x;
                        x = y;
                        y = z;
                    }

                    randRes = Rando.Float(x, y);
                }
                else
                {
                    //Do for integers
                    var x = int.Parse(split[0]);
                    var y = int.Parse(split[1]);
                    if (x > y)
                    {
                        //Swap the variables
                        var z = x;
                        x = y;
                        y = z;
                    }

                    randRes = Rando.Int(x, y);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Parses the specified string as a random range between two floats. Format: <code>x-y</code>
        /// <para />If a decimal point is found, x and y are interpreted as floats, else as ints.
        /// </summary>
        /// <param name="str"></param>
        /// <exception cref="FormatException">Invalid random number format.</exception>
        /// <returns></returns>
        public static float ParseRandom(string str)
        {
            float output;
            if (TryParseRandom(str, out output))
                return output;
            throw new FormatException("Invalid random number format.");
        }

        /// <summary>
        /// Tries parsing the specified string as a Vec2. Format: <code>x;y</code>
        /// </summary>
        /// <param name="str"></param>
        /// <param name="vec2Res"></param>
        /// <returns></returns>
        public static bool TryParseVec2(string str, out Vec2 vec2Res)
        {
            vec2Res = new Vec2();

            if (!str.Contains(';'))
            {
                return false;
            }

            try
            {
                var split = str.Split(';');
                if (split.Length != 2)
                {
                    return false;
                }

                var x = float.Parse(split[0]);
                var y = float.Parse(split[1]);
                vec2Res.x = x;
                vec2Res.y = y;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Tries parsing the specified string as a Vec3. Format: <code>x;y;z</code>
        /// </summary>
        /// <param name="str"></param>
        /// <param name="vec3Res"></param>
        /// <returns></returns>
        public static bool TryParseVec3(string str, out Vec3 vec3Res)
        {
            vec3Res = new Vec3();

            if (!str.Contains(';'))
            {
                return false;
            }

            try
            {
                var split = str.Split(';');
                if (split.Length != 3)
                {
                    return false;
                }

                var x = float.Parse(split[0]);
                var y = float.Parse(split[1]);
                var z = float.Parse(split[2]);
                vec3Res.x = x;
                vec3Res.y = y;
                vec3Res.z = z;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Tries parsing the specified string as a Vec4. Format: <code>w;x;y;z</code>
        /// </summary>
        /// <param name="str"></param>
        /// <param name="vec4Res"></param>
        /// <returns></returns>
        public static bool TryParseVec4(string str, out Vec4 vec4Res)
        {
            vec4Res = new Vec4();

            if (!str.Contains(';'))
            {
                return false;
            }

            try
            {
                var split = str.Split(';');
                if (split.Length != 4)
                {
                    return false;
                }

                var w = float.Parse(split[0]);
                var x = float.Parse(split[1]);
                var y = float.Parse(split[2]);
                var z = float.Parse(split[3]);
                vec4Res.w = w;
                vec4Res.x = x;
                vec4Res.y = y;
                vec4Res.z = z;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Tries parsing the specified string as a Color by (getting its pre-saved name or) using hexadecimal formatting (requiring the hash!).
        /// </summary>
        /// <param name="str"></param>
        /// <param name="colorRes"></param>
        /// <param name="allowNames">Allows entering color's names</param>
        /// <returns></returns>
        public static bool TryParseColor(string str, out Color colorRes, bool allowNames = false)
        {
            if (CommandHelpers.IsValidHex(str))
            {
                //Get color by hex
                var hexColor = CommandHelpers.HexToVec3(str);
                colorRes = new Color(hexColor.x, hexColor.y, hexColor.z);
                return true;
            }
            else if (allowNames)
            {
                //Try getting color by name
                var nullableColor = CommandHelpers.ColorByMicrosoftName(str);
                if (nullableColor != null)
                {
                    var namedColor = (Vec3)nullableColor;
                    colorRes = new Color(namedColor.x, namedColor.y, namedColor.z);
                    return true;
                }
            }

            colorRes = default(Color);
            return false;
        }

        /// <summary>
        /// Returns the profile fitting this string, or null if none can be found.
        /// <para />Checks for: Username (with and without special characters) and SteamID
        /// </summary>
        /// <param name="str"></param>
        /// <param name="validIdCheck">If false, any ID can be entered, and a profile with this .steamID will be returned, no matter if it is on the server.</param>
        /// <returns></returns>
        public static Profile ParseProfile(string str, bool validIdCheck)
        {
            Profile prof;

            //Try name
            prof = GetListOfProfiles().FirstOrDefault(p => p.name.EqualsTo(true, str));
            if (prof != null)
                return prof;

            ulong id;
            if (ulong.TryParse(str, out id))
            {
                //Got an ID?
                prof = GetListOfProfiles().FirstOrDefault(p => p.steamID == id);
                if (prof != null)
                {
                    return prof;
                }
                else if (!validIdCheck)
                {
                    //Fake it
                    return new Profile("UNKNOWN") {steamID = id};
                }
            }

            //TODO: MserGlobals - ParseProfile should try to parse name without special chars
            return null;
        }

        /// <summary>
        /// Safely sets the clipboard's text content.
        /// </summary>
        /// <returns></returns>
        public static void SetClipboardText(string text)
        {
            MserGlobals.StartSTATask(() =>
            {
                Clipboard.SetText(text);
                return true;
            });
        }

        /// <summary>
        /// Safely returns the clipboard's text content.
        /// </summary>
        /// <returns></returns>
        public static string GetClipboardText()
        {
            var clipthread = MserGlobals.StartSTATask(Clipboard.GetText);
            clipthread.Wait(2000);
            return clipthread.Result;
        }

        /// <summary>
        /// Starts a task set to single-threaded apartment. Use for otherwise impossible Windows API calls.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="func"></param>
        /// <returns></returns>
        public static Task<T> StartSTATask<T>(Func<T> func)
        {
            var tcs = new TaskCompletionSource<T>();
            Thread thread = new Thread(() =>
            {
                try
                {
                    tcs.SetResult(func());
                }
                catch (Exception e)
                {
                    tcs.SetException(e);
                }
            });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            return tcs.Task;
        }

        /// <summary>
        /// Returns whether the specified object is a Thing or inherits from Thing.
        /// </summary>
        /// <param name="obj">The object to check for.</param>
        /// <returns></returns>
        public static bool IsThing(object obj)
        {
            return MserGlobals.IsThing(obj.GetType());
        }

        /// <summary>
        /// Returns whether the specified type is a Thing or inherits from Thing.
        /// </summary>
        /// <param name="ty">The type to check for.</param>
        /// <returns></returns>
        public static bool IsThing(Type ty)
        {
            return ty != null && (ty == typeof(Thing) || ty.IsSubclassOf(typeof(Thing)));
        }

        /// <summary>
        /// Gets the current framerate of the game.
        /// </summary>
        /// <returns></returns>
        public static int GetFPS()
        {
            return Graphics.fps;
        }

        /// <summary>
        /// Given a lambda expression that calls a method, returns the method info.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        public static MethodInfo GetMethodInfo<TResult>(Expression<Func<TResult>> expression)
        {
            return MserGlobals.GetMethodInfo((LambdaExpression)expression);
        }

        /// <summary>
        /// Given a lambda expression that calls a method, returns the method info.
        /// </summary>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        public static MethodInfo GetMethodInfo(LambdaExpression expression)
        {
            MethodCallExpression outermostExpression = expression.Body as MethodCallExpression;

            if (outermostExpression == null)
            {
                throw new ArgumentException("Invalid Expression. Expression should consist of a Method call only.");
            }

            return outermostExpression.Method;
        }

        /// <summary>
        /// A small floating point number (0.00001f), for use to compare floating point numbers being "equal".
        /// </summary>
        public const float Epsilon = 0.00001f;

        /// <summary>
        /// Returns the profile using the specified persona.
        /// </summary>
        /// <param name="duckPersona"></param>
        /// <returns></returns>
        public static Profile ProfileWithPersona(DuckPersona duckPersona)
        {
            return GetListOfProfiles().FirstOrDefault(p => p.persona == duckPersona);
        }

        /// <summary>
        /// Removes the specified texture from the textures cache, forcing the game to reload it the next time <code>Content.Load</code> is called for it.
        /// </summary>
        /// <param name="tex">Name of the textures, as it'd be specified by <code>Content.Load</code>.</param>
        /// <param name="mod">Whether the texture is a png file of a mod, or a xnb file from the game itself.</param>
        public static void RemoveTextureFromCache(string tex, bool mod)
        {
            //IMP: MserGlobals RemoveTextureFromCache - Texture is still cached -> it is correctly removed from the lists below, so where is it cached?
            var reformatted = MserGlobals.FixUpPath(tex);
            if (mod)
            {
                //Additional remove for mods
                foreach (var pack in ModLoader.accessibleMods.Select(m => m.configuration.content))
                {
                    if (pack == null)
                        continue;

                    var textures = (Dictionary<string, Texture2D>)_packTextures.GetValue(pack);
                    if (textures.ContainsKey(reformatted))
                        textures.Remove(reformatted);
                }
            }

            //Regular removal
            if (Content.textures.ContainsKey(reformatted))
            {
                var t = Content.textures[reformatted];
                Content.textureList.Remove(t);
                Content.textures.Remove(reformatted);
            }
        }

        private static readonly FieldInfo _packTextures = MserGlobals.GetField(typeof(ContentPack), "_textures");

        /// <summary>
        /// Turns the specified path into a "globally accepted format" that DuckGame uses for content.
        /// <para />All slashes are forwards-slashes and extensions are removed.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static string FixUpPath(string path)
        {
            var ext = Path.GetExtension(path);
            if (!string.IsNullOrWhiteSpace(ext))
                path = Path.ChangeExtension(path, null);
            return path.Replace('\\', '/');
        }

        /// <summary>
        /// Returns the profile with the specified steam ID.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="validIdCheck">If false, creates a fake profile for every non-existing profile with the specified id.</param>
        /// <returns></returns>
        public static Profile GetProfileFromSteamID(ulong id, bool validIdCheck)
        {
            var prof = GetListOfProfiles().FirstOrDefault(p => p.steamID == id);

            if (prof == null && !validIdCheck)
                return new Profile("UNKNOWN") {steamID = id};
            return prof;
        }

        /// <summary>
        /// Whether the specified user is hosting the game.
        /// <para />To check if you are the host, it's easier to check <code>Network.isServer</code>.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static bool IsHosting(Profile user)
        {
            return user.isHost || (user.localPlayer && Network.isServer);
        }

        /// <summary>
        /// Returns the name of the specified color.
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static string GetNameOfColor(Color color, ReturnValueFormat format)
        {
            string colorname = null;

            //Named colors first
            foreach (var named in ColorCommand.ColorNames)
            {
                var namedcolor = named.Value;
                if (Math.Abs(namedcolor.x - color.r) < MserGlobals.Epsilon &&
                    Math.Abs(namedcolor.y - color.g) < MserGlobals.Epsilon &&
                    Math.Abs(namedcolor.z - color.b) < MserGlobals.Epsilon)
                {
                    colorname = named.Key;
                    break;
                }
            }

            if (colorname == null)
            {
                //Microsoft colors
                foreach (var named in CommandHelpers.GetListOfMicrosoftColors())
                {
                    var namedcolor = CommandHelpers.ColorByMicrosoftName(named).Value;
                    if (Math.Abs(namedcolor.x - color.r) < MserGlobals.Epsilon &&
                        Math.Abs(namedcolor.y - color.g) < MserGlobals.Epsilon &&
                        Math.Abs(namedcolor.z - color.b) < MserGlobals.Epsilon)
                    {
                        colorname = named;
                        break;
                    }
                }
            }

            if (colorname == null)
            {
                //Use RGB + HEX
                switch (format)
                {
                    case ReturnValueFormat.Visual:
                        colorname = string.Format("{0} {1} {2} (#{3})", color.r, color.g, color.b, color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2"));
                        break;
                    case ReturnValueFormat.RGBOnly:
                        colorname = string.Format("{0} {1} {2}", color.r, color.g, color.b);
                        break;
                    case ReturnValueFormat.HexOnly:
                        colorname = string.Format("#{0}", color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2"));
                        break;
                    case ReturnValueFormat.HexWithoutHash:
                        colorname = string.Format("{0}", color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2"));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException("format", format, null);
                }
            }

            return colorname;
        }
    }

    /// <summary>
    /// The format to use in case the return value of the GetNameOfColor is not a name.
    /// </summary>
    public enum ReturnValueFormat
    {
        Visual,
        RGBOnly,
        HexOnly,
        HexWithoutHash
    }

    /// <summary>
    /// Which member should be returned by a <code>MserGlobals.GetMember()</code> call, given its name and type.
    /// </summary>
    public enum AccessMember
    {
        /// <summary>
        /// Access to a MethodInfo.
        /// </summary>
        Method,

        /// <summary>
        /// Access to a PropertyInfo.
        /// </summary>
        Property,

        /// <summary>
        /// Access to a FieldInfo.
        /// </summary>
        Field
    }

    /// <summary>
    /// List of states that the game can currently be in. Determined by Level. See <code>MserGlobals.GetGameState()</code> for more info.
    /// </summary>
    [Flags]
    public enum GameState
    {
        /// <summary>
        /// DuckGame is on the loading screen. <code>Level.current == null</code> here!
        /// </summary>
        Loading = 1,

        /// <summary>
        /// Any in-game level is currently being played. This may be singleplayer, multiplayer or level editor test levels.
        /// <para />If you wish this to include the lobby, use <code>GameState.InGame</code>.
        /// </summary>
        Playing = 2,

        /// <summary>
        /// DuckGame is showing its main menu.
        /// </summary>
        TitleScreen = 4,

        /// <summary>
        /// The rockthrow intermission is currently starting.
        /// </summary>
        RockThrowIntro = 8,

        /// <summary>
        /// The rockthrow intermission is currently happening.
        /// </summary>
        RockThrow = 16,

        /// <summary>
        /// The game is in the lobby level, singleplayer or multiplayer.
        /// </summary>
        Lobby = 32,

        /// <summary>
        /// The level editor is currently active.
        /// </summary>
        LevelEditor = 64,

        /// <summary>
        /// Either <code>GameState.Playing</code> or <code>GameState.Lobby</code>. Use <code>HasFlag</code> call if you wish to compare with this.
        /// </summary>
        InGame = Playing | Lobby,

        /// <summary>
        /// Any level or state which does not fit the other categories.
        /// </summary>
        Unknown = 0
    }

    /// <summary>
    /// See <code>MserGlobals.FilterFullPath()</code> for more info on the usage.
    /// </summary>
    public enum PathFilter
    {
        /// <summary>
        /// A file's extension.
        /// </summary>
        Extension,

        /// <summary>
        /// A file's name and extension.
        /// </summary>
        FileName,

        /// <summary>
        /// A file's name without extension.
        /// </summary>
        FileNameWithoutExtension,

        /// <summary>
        /// The full path, as it was specified.
        /// </summary>
        FullPath,

        /// <summary>
        /// The directory of the file.
        /// </summary>
        Directory
    }
}

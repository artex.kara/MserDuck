using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.Xna.Framework.Audio;

namespace DuckGame.MserDuck
{
    public static class MusicEx
    {
        /// <summary>
        /// Plays the specified song. This is an overridden Music.Play call.
        /// </summary>
        /// <param name="music">Local path to music, without extension. Directories must use forwards-slashes.</param>
        /// <param name="looping">If set to <c>true</c> [looping].</param>
        /// <param name="crossFadeTime">The cross fade time.</param>
        private static void PlayInternal(string music, bool looping = true, float crossFadeTime = 0f)
        {
            //Replace title song with the new title
            if (music == "Title" && MserGlobals.GetGameState() == GameState.TitleScreen)
            {
                PlaySong("NewTitle", looping);
                return;
            }

            //Play normally
            PlaySong(music, looping);
        }

        /// <summary>
        /// Plays the specified song.
        /// </summary>
        /// <param name="music">Local path to music, without extension. Directories must use forwards-slashes.</param>
        /// <param name="looping"></param>
        public static void PlaySong(string music, bool looping = true)
        {
            var songs = (Dictionary<string, MemoryStream>)MserGlobals.GetField("Music", "_songs").GetValue(null);
            PlaySongStream(music, songs[music], looping);
        }

        private static void PlaySongStream(string name, MemoryStream music, bool looping = true)
        {
            MserGlobals.GetField("Music", "_currentSong").SetValue(null, name);
            var players = (object[])MserGlobals.GetField("Music", "_players").GetValue(null);
            var curIndex = (int)MserGlobals.GetField("Music", "_curPlayer").GetValue(null);
            var curPlayer = players[curIndex];
            MserGlobals.GetMethod("OggPlayer", "Stop").Invoke(curPlayer, new object[] { });
            MserGlobals.GetMethod("OggPlayer", "SetOgg").Invoke(curPlayer, new object[] { music });
            MserGlobals.GetMethod("OggPlayer", "Play").Invoke(curPlayer, new object[] { });
            MserGlobals.GetProperty("OggPlayer", "looped").SetValue(curPlayer, looping, null);
        }

        /// <summary>
        /// Broken - sets music's pitch.
        /// </summary>
        /// <param name="value"></param>
        public static void SetPitch(float value)
        {
            value = Maths.Clamp(value, -1f, 1f);
            var players = (object[])MserGlobals.GetField("Music", "_players").GetValue(null);
            var curIndex = (int)MserGlobals.GetField("Music", "_curPlayer").GetValue(null);
            var curPlayer = players[curIndex];
            var instance = (DynamicSoundEffectInstance)MserGlobals.GetProperty("OggPlayer", "_instance").GetValue(curPlayer, null);
            instance.Pitch = value;
        }

        /// <summary>
        /// Plays the specified song. This is an overridden Music.Play call.
        /// </summary>
        /// <param name="music">Song instance</param>
        /// <param name="looping"></param>
        private static void PlaySongInternal(Song music, bool looping = true)
        {
            if (music == null)
            {
                MusicEx.Stop();
                return;
            }

            PlaySongStream(music.name, music.data, looping);
        }

        /// <summary>
        /// Returns a random music track from the specified music pool.
        /// </summary>
        /// <param name="folder">Subfolder name (of the loaded song directories) to select the song from.</param>
        /// <param name="ignore">Name of song to not select (mostly current song).</param>
        public static string GetRandomTrack(string folder, string ignore = "")
        {
            return RandomTrack(folder, ignore); //(string)MserGlobals.GetMethod("Music", "RandomTrack").Invoke(null, new object[] { folder, ignore });
        }

        /// <summary>
        /// Override for the Music.RandomTrack
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="ignore"></param>
        /// <returns></returns>
        private static string RandomTrack(string folder, string ignore = "")
        {
            /*if (DevConsole.rhythmMode)
            {
                return "InGame/comic.ogg";
            }*/
            var songList = CacheManager.GetSongList(folder); //Content.GetFiles("Content/Audio/Music/" + folder, "*.*").ToList();
            if (songList.Length == 0)
            {
                return "";
            }
            var songs = songList.Select(song => folder + "/" + song).Where(s => s != ignore).ToList();
            if (songs.Count == 0)
            {
                songs.Add(songList[0]);
            }
            Queue<string> recentSongs = null;
            var musicrecents = (Dictionary<string, Queue<string>>)MserGlobals.GetField("Music", "_recentSongs").GetValue(null);
            if (!musicrecents.TryGetValue(folder, out recentSongs))
            {
                recentSongs = new Queue<string>();
                musicrecents[folder] = recentSongs;
            }
            if (recentSongs.Count > 0 && recentSongs.Count > songs.Count - 5)
            {
                recentSongs.Dequeue();
            }
            var validSongs = new List<string>();
            validSongs.AddRange(songs);
            var curSong = "";
            while (curSong == "")
            {
                if (songs.Count == 0 && recentSongs.Count > 0)
                {
                    curSong = recentSongs.Dequeue();
                    if (!validSongs.Contains(curSong))
                    {
                        curSong = "";
                    }
                }
                else if (songs.Count == 0)
                {
                    curSong = validSongs[0];
                }
                else
                {
                    curSong = songs[Rando.Int(songs.Count() - 1)];
                    if (curSong == ignore && songs.Count > 1)
                    {
                        songs.Remove(curSong);
                        curSong = "";
                    }
                    else if (recentSongs.Contains(curSong))
                    {
                        if (Rando.Float(1f) > 0.25f)
                        {
                            songs.Remove(curSong);
                            if (songs.Count > 0)
                            {
                                curSong = "";
                            }
                        }
                        else
                        {
                            curSong = "";
                        }
                    }
                }
            }
            recentSongs.Enqueue(curSong);
            return curSong;
        }

        /// <summary>
        /// Returns the name of the current song.
        /// </summary>
        public static string GetCurrentSong()
        {
            return (string)MserGlobals.GetField("Music", "_currentSong").GetValue(null);
        }

        /// <summary>
        /// Stops the currently playing song.
        /// </summary>
        public static void Stop()
        {
            MserGlobals.GetMethod("Music", "Stop").Invoke(null, new object[] { });
        }

        /// <summary>
        /// Pauses the currently playing song.
        /// </summary>
        public static void Pause()
        {
            MserGlobals.GetMethod("Music", "Pause").Invoke(null, new object[] {});
        }

        /// <summary>
        /// Resumes the currently playing song.
        /// </summary>
        public static void Resume()
        {
            MserGlobals.GetMethod("Music", "Resume").Invoke(null, new object[] { });
        }

        /// <summary>
        /// Gets the current soundstate of the active ogg player.
        /// </summary>
        /// <returns></returns>
        public static SoundState GetSongState()
        {
            var players = (object[])MserGlobals.GetField("Music", "_players").GetValue(null);
            var curIndex = (int)MserGlobals.GetField("Music", "_curPlayer").GetValue(null);
            var curPlayer = players[curIndex];
            return (SoundState)MserGlobals.GetProperty("OggPlayer", "state").GetValue(curPlayer, null);
        }

        /// <summary>
        /// Searches the specified directory for music files, adding them to the list of songs.
        /// </summary>
        /// <param name="path"></param>
        internal static void SearchSongDir(string path)
        {
            //MserGlobals.GetMethod("Music", "SearchDir").Invoke(null, new object[] { path });

            var files = Content.GetFiles(path);
            var songs = (Dictionary<string, MemoryStream>)MserGlobals.GetField("Music", "_songs").GetValue(null);
            foreach (var fil in files)
            {
                //NON-LOCAL PROCESSSONG
                var f = fil.Replace('\\', '/');
                var sound = (MemoryStream)MserGlobals.GetMethod("OggSong", "Load").Invoke(null, new object[] { f, false });
                f = f.Substring(0, f.Length - 4);
                var shortName = f.Substring(f.IndexOf("/Music/") + 7);
                songs[shortName] = sound;
                MonoMain.loadyBits++;
            }
            var directories = Content.GetDirectories(path);
            foreach (var d in directories)
            {
                MusicEx.SearchSongDir(d);
            }
        }

        /// <summary>
        /// Resets the music volume to the options' volume level.
        /// </summary>
        public static void ResetVolume()
        {
            //Get options volume
            var optdata = MserGlobals.GetProperty("Options", "Data").GetValue(null, null);
            var optvol = (float)MserGlobals.GetProperty("OptionsData", "musicVolume").GetValue(optdata, null);

            //Set volume
            MusicEx.Volume = optvol;
        }

        private static readonly PropertyInfo _volume = MserGlobals.GetProperty("Music", "volume");

        /// <summary>
        /// Current music volume.
        /// </summary>
        public static float Volume
        {
            get
            {
                return (float)_volume.GetValue(null, null);
            }
            set
            {
                _volume.SetValue(null, value, null);
            }
        }
    }
}
 
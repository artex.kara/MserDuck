﻿namespace DuckGame.MserDuck
{
    /// <summary>
    /// Container for multi-purpose fonts. Be sure to check/set properties such as scale before drawing, since those may not be reset!
    /// </summary>
    public static class FontManager
    {
        /// <summary>
        /// Font used for rendering UI.
        /// </summary>
        public static readonly BitmapFont Bios = new BitmapFont("biosFontUI", 8, 7);

        /// <summary>
        /// Font used for rendering scoreboard.
        /// </summary>
        public static readonly BitmapFont BiosSmall = new BitmapFont("smallBiosFont", 7, 6);

        /// <summary>
        /// Extended default font used for rendering chat messages. Includes a wide variety of symbols.
        /// </summary>
        public static readonly ExtendedBitmapFont Chat = new ExtendedBitmapFont(Thing.GetPath<MserDuck>("smallFont"));
    }
}

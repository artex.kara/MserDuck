using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.Xna.Framework;

[assembly: AssemblyTitle("MserDuck")]
[assembly: AssemblyCompany("RedMser")]
[assembly: AssemblyDescription("Redefines Duck Game through massive additions, such as a command system, custom modifiers, a debug mode and many new items!")]
[assembly: AssemblyVersion("1.0.1.0")] //DON'T CHANGE THIS! Binary serializer is used, version mismatch will happen otherwise.

namespace DuckGame.MserDuck
{
    /// <summary>
    /// The mod's class, containing most internal execution code.
    /// <para />If you wish to modify events, they are now found in <see cref="MserEvents"/>.
    /// <para /><see cref="CommandHandlers.CommandHandler"/>, <see cref="ModifierHandlers.ModifierHandler"/> and <see cref="ConfigEntries.ConfigEntry"/>
    /// are explained with their base classes and found in separate directories respectively.
    /// </summary>
    public class MserDuck : Mod
    {
        //TODO: General - can states be saved in purpleblocks? -> save miniitembox contents, instrument's value, etc
        //IMP: General - figure out how to render onto targets, so that any text that doesn't change can be cached instead of constantly live-processed
        //  This will be useful to avoid the text renderer giving up with too many entries (unless this is caused by for example alpha being set without reset?)
        //  -> seems to be done pretty cleanly in Graphics.Recolor?
        //TODO: General - toss weapons and stuff into more categories, in order to avoid off-screen context menu in level editor

        /// <summary>
        /// Substitute existing methods on launch in order to inject code into Duck Game.
        /// <para />Injection should not be used outside of the launching code, to avoid odd behaviour.
        /// <para />Instead, inject once, reimplement and check for state.
        /// </summary>
        internal static void PerformMethodInjection()
        {
            var injector = new Injector();

            ///////////////////////
            //OVERRIDES (STATIC)
            ///////////////////////

            //Use TickWeather (gets called in Main every update) to call custom update method every tick.
            injector.Inject(MserGlobals.GetMethod("RockWeather", "TickWeather"), MserGlobals.GetMethod(typeof(MserDuck), "OnTick"));

            //Check when a chat message is received, for custom command handling.
            injector.Inject(MserGlobals.GetMethod("DuckNetwork", "ChatMessageReceived"), MserGlobals.GetMethod(typeof(MserDuck), "OnChatMessageReceived"));

            //Gets called if a duck is killed.
            injector.Inject(MserGlobals.GetMethod("Global", "Kill"), MserGlobals.GetMethod(typeof(MserEvents), "OnKill"));

            //Music injection
            try
            {
                //Gets called if a song gets played (two overloads!)
                var playMethods = typeof(Music).GetMethods().Where(x => x.Name == "Play").ToList();
                if (playMethods.Count != 2)
                    throw new InvalidDataException("An invalid amount of Music.Play methods have been found: " + playMethods.Count);

                foreach (var play in playMethods)
                {
                    var par = play.GetParameters().SingleOrDefault(p => p.Position == 0);
                    if (par == null)
                        continue;

                    if (par.ParameterType == typeof(Song))
                        injector.Inject(play, MserGlobals.GetMethod(typeof(MusicEx), "PlaySongInternal"));
                    else if (par.ParameterType == typeof(string))
                        injector.Inject(play, MserGlobals.GetMethod(typeof(MusicEx), "PlayInternal"));
                }

                //Better random music (allowing for custom songs).
                injector.Inject(MserGlobals.GetMethod("Music", "RandomTrack"), MserGlobals.GetMethod(typeof(MusicEx), "RandomTrack"));
            }
            catch (Exception e)
            {
                Program.LogLine("Failed to initialize custom music playback system: " + e.Message + e.StackTrace);
            }

            //Gets called when the value of a pressed key has to be gotten, for new layouts.
            injector.Inject(MserGlobals.GetMethod(typeof(Keyboard), "KeyToChar"), MserGlobals.GetMethod(typeof(KeyLayoutManager), "ConvertKeyToChar"));

            //New DuckNetwork functions in order to better customize chat behaviour.
            injector.Inject(MserGlobals.GetMethod(typeof(DuckNetwork), "Update"), MserGlobals.GetMethod(typeof(ChatManager), "Update"));
            injector.Inject(MserGlobals.GetMethod(typeof(DuckNetwork), "Draw"), MserGlobals.GetMethod(typeof(ChatManager), "Draw"));

            //Better scoreboard rendering
            injector.Inject(MserGlobals.GetMethod("ConnectionStatusUI", "Draw"), MserGlobals.GetMethod(typeof(ChatManager), "DrawScoreboard"));

            //Game exit detection (e.g. for autosaving).
            injector.Inject(MserGlobals.GetMethod("NetworkDebugger", "TerminateThreads"), MserGlobals.GetMethod(typeof(MserDuck), "OnExit"));
            
            //Using level's draw to be able to draw custom stuff.
            //TODO: MserDuck - Move rendering stuff like the DebugUI into here, with correct layer, if that works??
            injector.Inject(MserGlobals.GetMethod("Level", "DrawCurrentLevel"), MserGlobals.GetMethod(typeof(MserEvents), "OnLevelDraw"));

            //Show custom maps with non-workshop mods.
            injector.Inject(MserGlobals.GetMethod("LSFilterMods", "Cache"), MserGlobals.GetMethod(typeof(MserDuck), "SexiestCacheInTheWorld"));

            //Events being added, for example for RoundStarted
            injector.Inject(MserGlobals.GetMethod("Event", "Log"), MserGlobals.GetMethod(typeof(MserDuck), "LogEvent"));

            //Don't crash at all
            injector.Inject(MserGlobals.GetMethod(typeof(Program), "HandleGameCrash"), MserGlobals.GetMethod(typeof(MserEvents), "HandleGameCrash"));

            ///////////////////////
            //OVERRIDES (INSTANCED)
            ///////////////////////

            //In order to have skins working, redoing the DuckPersona methods
            injector.Inject(MserGlobals.GetMethod(typeof(DuckPersona), "Recreate"), MserGlobals.GetMethod(typeof(PseudoPersona), "RecreatePersona"));
            injector.Inject(MserGlobals.GetMethod(typeof(DuckPersona), "Update"), MserGlobals.GetMethod(typeof(PseudoPersona), "UpdatePersona"));

            //Replace UpdateInput of Ragdoll.
            injector.Inject(MserGlobals.GetMethod(typeof(Ragdoll), "UpdateInput"), MserGlobals.GetMethod(typeof(ModifierHandlers.PseudoRagdoll), "UpdateInputNew"));

            //Replace TryGrab of Duck.
            injector.Inject(MserGlobals.GetMethod(typeof(Duck), "TryGrab"), MserGlobals.GetMethod(typeof(ModifierHandlers.PseudoDuck), "TryGrab"));

            ///////////////////////
            //REMOVAL
            ///////////////////////

            //Do not open the "you got mods, m8" dialog since it caused too many restarts already.
            injector.Inject(MserGlobals.GetMethod("DuckNetwork", "OpenNoModsWindow"), MserGlobals.GetMethod(typeof(MserDuck), "EmptyMethod"));
        }

        /// <summary>
        /// An empty method. Can be used with injection to "comment out" another method.
        /// </summary>
        public static void EmptyMethod()
        {
        }

        /// <summary>
        /// Run before all mods are finished loading.
        /// </summary>
        protected override void OnPreInitialize()
        {
            MserEvents.OnPreInitialize();

            base.OnPreInitialize();
        }

        /// <summary>
        /// Run after all mods are loaded.
        /// </summary>
        protected override void OnPostInitialize()
        {
            base.OnPostInitialize();

            MserEvents.OnInitialize();
        }

        /// <summary>
        /// Called on every game tick.
        /// </summary>
        public static void OnTick()
        {
            try
            {
                // Call original method.
                MserDuck.DoWeatherTick();

                // Update DG Window
                MserDuck.UpdateWindow();

                // Call tick handlers.
                MserEvents.OnTick();
            }
            catch (Exception e)
            {
                Program.LogLine("ERROR DURING ONTICK! " + e.Message + e.StackTrace);
            }
        }

        /// <summary>
        /// Run every time a chat message gets received, even when you are its sender.
        /// <para />Event handling should be a part of HandleChatMessage, hence lacking exposure on MserEvents!
        /// </summary>
        /// <param name="message"></param>
        public static void OnChatMessageReceived(NMChatMessage message)
        {
            //Handle chat message using custom handler
            if (ChatManager.HandleChatMessage(message))
            {
                ChatManager.DisplayChatMessageFromNetwork(message);
            }
        }

        private static bool _hasExited = false;

        /// <summary>
        /// Executed just as the game is quitting.
        /// </summary>
        public static void OnExit()
        {
            if (!_hasExited)
            {
                //Avoid double calls
                _hasExited = true;

                MserEvents.OnGameExit();
            }
        }

        /// <summary>
        /// Function needed to fit the required method parameters of an overridden method.
        /// </summary>
		public bool SexiestCacheInTheWorld(string lev, bool result)
        {
			return true;
		}

        private static FieldInfo _timeOfDayField = MserGlobals.GetField("RockWeather", "_timeOfDay");
        private static FieldInfo _weatherTimeField = MserGlobals.GetField("RockWeather", "_weatherTime");

        /// <summary>
        /// Original method which is substituted by OnTick. Should still get run, to get cool weather!
        /// </summary>
        private static void DoWeatherTick()
        {
            const float weatherTickTime = 6.17283968E-06f;
            var timeOfDay = (float)MserDuck._timeOfDayField.GetValue(null);
            var weatherTime = (float)MserDuck._weatherTimeField.GetValue(null);
            timeOfDay += weatherTickTime;
            if (timeOfDay > 1f)
            {
                timeOfDay = 0f;
            }
            weatherTime += weatherTickTime;
            MserDuck._timeOfDayField.SetValue(null, timeOfDay);
            MserDuck._weatherTimeField.SetValue(null, weatherTime);
        }

        /// <summary>
        /// Whether to check for MserDuck's version. Set using <see cref="ConfigEntries.CheckVersionEntry"/> in config.
        /// </summary>
        public static bool CheckVersion = true;

        /// <summary>
        /// Best alternative we have to a version number for now.
        /// <para />-1 = not yet detected
        /// <para />-2 = error detecting it
        /// </summary>
        public static int CommitNumber = -1;

        private static readonly FieldInfo _eventsField = MserGlobals.GetField("Event", "_events");

        /// <summary>
        /// Logging an event, calling event for event.
        /// </summary>
        /// <param name="e"></param>
        public static void LogEvent(object e)
        {
            //Base call
            ((IList)MserDuck._eventsField.GetValue(null)).Add(e);

            //Special event handling
            MserEvents.OnLogEvent(new LoggedEvent(e));
        }

        /// <summary>
        /// New game window size.
        /// </summary>
        public static Vec2 _newSize;

        /// <summary>
        /// Updates the Duck Game window logic. Checks for borderless window toggles and for resizing the screen when the window is rescaled.
        /// </summary>
        private static void UpdateWindow()
        {
            //Toggle borderless
            if (Keyboard.Pressed(MserDuck.ToggleBorderlessKey))
                ToggleBorderless();

            //If needed, update window size
            if (MserDuck._newSize == Vec2.Zero)
                return;

            MserDuck._newSize = Vec2.Zero;
            var lastSize = new Vec2(MonoMain.screenWidth, MonoMain.screenHeight);

            try
            {
                //Disable draw calls
                Program.main.doReset = true;

                //Update window size
                var graphics = MserGlobals.GetField("MonoMain", "graphics").GetValue(Program.main) as GraphicsDeviceManager;
                if (graphics != null)
                {
                    //BUG: MserDuck - Resizing the game window currently is a mix of crashing and turning black
                    MserGlobals.GetField("MonoMain", "_screenWidth").SetValue(null, (int)MserDuck._newSize.x);
                    MserGlobals.GetField("MonoMain", "_screenHeight").SetValue(null, (int)MserDuck._newSize.y);
                    MserGlobals.GetMethod("MonoMain", "UpdateFullscreen").Invoke(Program.main, null);

                    Graphics.InitializeBase(graphics, MonoMain.screenWidth, MonoMain.screenHeight);
                    Resolution.Initialize(ref graphics, MonoMain.screenWidth, MonoMain.screenHeight);
                }
            }
            catch (Exception ex)
            {
                //Whatever happens, undo size changes
                Program.LogLine("Could not change window size: " + ex.Message + ex.StackTrace);
                MserGlobals.GetField("MonoMain", "_screenWidth").SetValue(null, (int)lastSize.x);
                MserGlobals.GetField("MonoMain", "_screenHeight").SetValue(null, (int)lastSize.y);
                MserGlobals.GetMethod("MonoMain", "UpdateFullscreen").Invoke(Program.main, null);
            }
            finally
            {
                //Re-enable draw calls
                Program.main.doReset = false;
            }
        }

        /// <summary>
        /// Toggles between a borderless window.
        /// </summary>
        public static void ToggleBorderless()
        {
            Borderless = !Borderless;
        }

        private static void UpdateBorderless()
        {
            var hwnd = Program.main.Window.Handle;
            if (Borderless)
            {
                //Save old style, apply new
                MserDuck._oldStyle = (uint)PInvoke.GetWindowLong(hwnd, PInvoke.GWL_STYLE);
                PInvoke.SetWindowLong(hwnd, PInvoke.GWL_STYLE, MserDuck._oldStyle & PInvoke.WS_BORDERLESS);
            }
            else
            {
                //Load old style
                PInvoke.SetWindowLong(hwnd, PInvoke.GWL_STYLE, MserDuck._oldStyle);
            }
        }

        /// <summary>
        /// Borderless state of game window.
        /// </summary>
        public static bool Borderless
        {
            set
            {
                if (_borderless == value)
                    return;

                _borderless = value;
                UpdateBorderless();
            }
            get { return _borderless; }
        }

        /// <summary>
        /// WindowLong before borderless window.
        /// </summary>
        private static uint _oldStyle;

        /// <summary>
        /// Borderless state of game window.
        /// </summary>
        private static bool _borderless = false;

        /// <summary>
        /// Key for toggling borderless. Set using <see cref="ConfigEntries.ToggleBorderlessEntry"/> in config.
        /// </summary>
        public static Keys ToggleBorderlessKey = Keys.F8;

        /// <summary>
        /// Allows injection of code into another method by replacing its function pointer.
        /// <para />This does not work for overridden methods, and the method's signature has to stay compatible.
        /// </summary>
        internal sealed class Injector
        {
            private readonly MethodInfo _injectorMethod;

            internal static string InjectorPath
            {
                get { return MserGlobals.GetModDir() + "\\MethodInjector.dll"; }
            }

            /// <summary>
            /// Creates a new MethodInjector, allowing injection of code into another method by replacing its function pointer.
            /// <para />This does not work for overridden methods, and the method's signature has to stay compatible.
            /// </summary>
            internal Injector()
            {
                try
                {
                    // Load assembly and main injector class.
                    // Adding the assembly as a reference is not recognized, and loading code directly errors out due to unsafe code.
                    var injectorAssembly = Assembly.LoadFrom(InjectorPath);
                    var myType = injectorAssembly.GetType("MethodInjector.Injection");

                    // This will only work as long as one method exists in the type definition. Update if needed!
                    this._injectorMethod = myType.GetMethods(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)[0];

                    if (this._injectorMethod == null)
                        throw new NullReferenceException("InjectorMethod is null.");
                }
                catch (FileNotFoundException e)
                {
                    Program.LogLine("INJECTION ERROR: Unable to initialize injector. Missing " + MserGlobals.GetModDir() + "/MethodInjector.dll");
                    Program.LogLine(e.Message + e.StackTrace);
                }
                catch (NullReferenceException e)
                {
                    Program.LogLine("INJECTION ERROR: Unable to initialize injector. Could not get injector method, probably due to incompatible DLL format.");
                    Program.LogLine(e.Message + e.StackTrace);
                }
                catch (Exception e)
                {
                    Program.LogLine("INJECTION ERROR: Unable to initialize injector. Unknown error.");
                    Program.LogLine(e.Message + e.StackTrace);
                }
            }

            /// <summary>
            /// Replaces the body of the original method with that of the replacement method.
            /// <para />Be sure that both methods are not override methods, and that their signature is compatible.
            /// </summary>
            /// <param name="original"></param>
            /// <param name="replacement"></param>
            internal void Inject(MethodInfo original, MethodInfo replacement)
            {
                if (original == null)
                {
                    Program.LogLine("INJECTION ERROR: Original method is null! Replacement: " + Extensions.ToNeatString(replacement));
                    return;
                }
                if (replacement == null)
                {
                    Program.LogLine("INJECTION ERROR: Replacement method is null! Original: " + Extensions.ToNeatString(original));
                    return;
                }

                this._injectorMethod.Invoke(null, new object[] { original, replacement });
            }
        }
    }
}

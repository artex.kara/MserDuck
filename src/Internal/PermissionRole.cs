﻿using System;
using System.Collections.Generic;
using System.Linq;
using DuckGame.MserDuck.CommandHandlers;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// A role to be used by the permission system. Any class inheriting from this class will be added as a default role.
    /// <para />Be sure that the inherited constructor does not have any parameters, and defines its name using a specific base constructor call instead!
    /// </summary>
    public class PermissionRole : IEquatable<PermissionRole>
    {
        /// <summary>
        /// Visible name of the role.
        /// </summary>
        public string Name;

        /// <summary>
        /// List of commands that this role allows the users to execute.
        /// </summary>
        public readonly List<Permission> Permissions = new List<Permission>();

        /// <summary>
        /// Whether a user of this role is permitted to execute the specified command.
        /// </summary>
        /// <param name="handler"></param>
        /// <returns></returns>
        public bool IsPermittedToExecute(CommandHandler handler)
        {
            return this.HasPermission("/" + handler.GetNames().First());
        }

        /// <summary>
        /// Whether a user of this role has the specified permission (specified by name).
        /// </summary>
        /// <param name="permission"></param>
        /// <returns></returns>
        public bool HasPermission(string permission)
        {
            var perm = Permission.ParsePermission(permission);
            if (perm == null)
                return false;

            return this.HasPermission(perm);
        }

        /// <summary>
        /// Whether a user of this role has the specified permission (specified by permission name).
        /// </summary>
        /// <param name="permission"></param>
        /// <returns></returns>
        public virtual bool HasPermission(Permission permission)
        {
            return this.Permissions.Any(p => p.GetName().EqualsTo(true, permission.GetName()));
        }

        /// <summary>
        /// A color to associate with this role.
        /// </summary>
        public Color Color;

        /// <summary>
        /// An icon to be associated with this role. Size should be 16x16.
        /// <para />This should not change over time or through user input.
        /// </summary>
        public virtual Sprite Icon
        { get { return null; } }

        /// <summary>
        /// Creates a role with the specified name.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="color"></param>
        public PermissionRole(string name, Color color)
        {
            this.Name = name;
            this.Color = color;
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// <para />Checks for Name equality (case-insensitive) and AllowedCommands being the same.
        /// </summary>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        /// <param name="other">An object to compare with this object.</param>
        public bool Equals(PermissionRole other)
        {
            if (other == null)
                return false;

            return other.Name.EqualsTo(true, this.Name) && other.Permissions.EqualsTo(this.Permissions);
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            return this.Name;
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// An argument accepted by a command.
    /// <para />You do not have to use this class if you don't need to specify extra options:
    /// <para />Use <code>new CommandArgument[] {"strings"}</code> to have a special constructor invoked, converting the strings as according to these rules:
    /// <para />
    /// <para />If the string name starts with a question mark (?), it is declared as an optional parameter.
    /// <para />If the string name includes a slash (/), the first entry is the name and every following entry represents different supported values.
    /// <para />If the string name starts with an exclamation point (!), it is simply parsed the way it is.
    /// </summary>
    public class CommandArgument
    {
        /// <summary>
        /// Name of the argument, used for error messages and displaying to the user.
        /// </summary>
        public readonly string Name;

        //public Type ValueType;

        //public object DefaultValue;

        /// <summary>
        /// The values that this argument may have. This is only to be used for arguments that have per-command preset values.
        /// <para />Examples include the /timer's start/stop or /modifier's on/off values. It shouldn't be used by for example a list of Thing types.
        /// </summary>
        public readonly IEnumerable<string> SupportedValues;

        /// <summary>
        /// Whether this argument is optional. The default value is for now handled by the command itself.
        /// </summary>
        public readonly bool Optional;

        /// <summary>
        /// Special display text. This gets shown in the usage instead of the name, although the name is still used for referring to the argument.
        /// <para />This is not formatted at all if specified, so be sure to use the correct symbols to make it consistent.
        /// </summary>
        public string DisplayText;

        /// <summary>
        /// If true, this CommandArgument is used as a fallback name for autocomplete entry titles.
        /// </summary>
        public bool Global = false;

        /// <summary>
        /// Creates a new CommandArgument that is to be accepted by the command.
        /// </summary>
        /// <param name="name">Name of the argument, used for error messages and displaying to the user.</param>
        /// <param name="optional">Whether this argument is optional. The default value is for now handled by the command itself.</param>
        /// <param name="values">The values that this argument may have. This is only to be used for arguments that have per-command preset values.
        /// <para />Examples include the /timer's start/stop or /modifier's on/off values. It shouldn't be used by for example a list of Thing types.</param>
        public CommandArgument(string name, bool optional = false, IEnumerable<string> values = null)
        {
            this.Name = name;
            this.Optional = optional;
            this.SupportedValues = values;
        }

        /// <summary>
        /// Creates a new CommandArgument with the specified name.
        /// <para />If the name starts with a question mark (?), it is declared as an optional parameter.
        /// <para />If the name includes a slash (/), the first entry is the name and every following entry represents different supported values.
        /// <para />If the name starts with an exclamation point (!), it is simply parsed the way it is.
        /// </summary>
        /// <param name="arg"></param>
        public static implicit operator CommandArgument(string arg)
        {
            //Ignoring formatting
            if (arg[0] == '!')
                return new CommandArgument(arg.Substring(1));

            //Multiple values
            if (arg.Contains("/"))
            {
                //Each possible value
                var split = arg.Split('/').ToList();
                var name = split.First();
                var optional = false;
                if (name[0] == '?')
                {
                    //Optional too
                    name = name.Substring(1);
                    optional = true;
                }
                return new CommandArgument(name, optional, split.GetRange(1, split.Count - 1));
            }

            //Optional
            if (arg[0] == '?')
                return new CommandArgument(arg.Substring(1), true);

            return new CommandArgument(arg);
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            if (this.DisplayText != null)
                return this.DisplayText;

            if (this.SupportedValues != null)
            {
                if (this.Optional)
                {
                    return "[" + string.Join("/", this.SupportedValues) + "]";
                }
                else
                {
                    return string.Join("/", this.SupportedValues);
                }
            }
            else
            {
                if (this.Optional)
                {
                    return "[" + this.Name + "]";
                }
                else
                {
                    return "<" + this.Name + ">";
                }
            }
        }
    }
}

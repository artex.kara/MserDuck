﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DuckGame.MserDuck.CommandHandlers;
using DuckGame.MserDuck.ModifierHandlers;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Manages all custom modifiers and provides helper functions for simple usage.
    /// </summary>
    public static class ModifierManager
    {
        /// <summary>
        /// Determines whether the custom modifiers should be read. Is set after adding the MserOptions modifier entry.
        /// </summary>
        public static bool ModifiersReady = false;

        /// <summary>
        /// A list of modifiers to show in MserDuck's list of modifiers.
        /// </summary>
        public static readonly List<ModifierHandler> Modifiers = new List<ModifierHandler>(MserGlobals.GetSubInstances<ModifierHandler>());

        /// <summary>
        /// A list specifying ducks that have god mode.
        /// </summary>
        public static List<Duck> GodDucks = new List<Duck>();

        public static void Update()
        {
            var region = MserDebug.StartProfiling("ModifierManagerUpdate");

            //Setup MserOptions menu
            ModifierManager.MserOptionsSetup();

            MserDebug.StepProfiling(region, "Setup");

            //Call update for each modifier
            foreach (var mod in ModifierManager.Modifiers)
            {
                //Update modifier if enabled AND if it's either client and serverside OR if it's serverside only, check if local is server.
                if (mod.Enabled && (!mod.IsServerOnly() || Network.isServer))
                {
                    mod.Update();
                    MserDebug.StepProfiling(region, mod.GetName());
                }
            }

            //Tossing in godmode here
            foreach (var duck in ModifierManager.GodDucks)
            {
                GodCommand._killed.SetValue(duck, true);
            }
            MserDebug.StepProfiling(region, "GodMode");
            MserDebug.EndProfiling(region);
        }

        private static void MserOptionsSetup()
        {
            var qwoppy = Unlocks.GetUnlock("QWOPPY");

            //TODO: ModifierManager - find a way to not sacrifice QWOPPY for the MserDuck Options!
            if (!ModifierManager.ModifiersReady && qwoppy != null)
            {
                // Modifiers are loaded and can be checked for.
                ModifierManager.ModifiersReady = true;

                // Update QWOP modifier to MserDuck Options Menu
                UnlockData modif = qwoppy;
                modif.name = "MserDuck Options";
                modif.onlineEnabled = true;

                //Unlock QWOP modifier
                foreach (var prof in Profiles.all)
                {
                    if (!prof.unlocks.Contains(modif.id))
                        prof.unlocks.Add(modif.id);
                }

                // Unlock everything else as a bonus
                //foreach (var prof in Profiles.all)
                //{
                //    foreach (var unlock in Unlocks.allUnlocks)
                //    {
                //        if (!prof.unlocks.Contains(unlock.id))
                //            prof.unlocks.Add(unlock.id);
                //    }
                //}
            }

            if (ModifierManager.ModifiersReady && qwoppy.enabled)
            {
                // Shows the options menu
                ModifierManager.ShowOptionsMenu();

                // Disable modifier again
                qwoppy.enabled = false;
            }
        }

        /// <summary>
        /// Returns the first modifier in the list of modifiers of the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetModifier<T>()
        {
            return Modifiers.OfType<T>().FirstOrDefault();
        }

        /// <summary>
        /// Returns the index of the specified modifier, for network transmission.
        /// </summary>
        /// <param name="mod"></param>
        /// <returns></returns>
        public static int GetModifierIndex(ModifierHandler mod)
        {
            return ModifierManager.Modifiers.IndexOf(mod);
        }

        /// <summary>
        /// Returns the modifier with the specified index.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public static ModifierHandler GetModifierByIndex(int index)
        {
            if (index < 0 || index > Modifiers.Count)
            {
                return null;
            }

            return ModifierManager.Modifiers[index];
        }

        /// <summary>
        /// The MserDuck options menu.
        /// </summary>
        private static UIMenu optionsMenu = new UIMenu("@LWING@MSERDUCK OPTIONS@RWING@", Layer.HUD.camera.width / 2f, Layer.HUD.camera.height / 2f, 190f, -1f,
            "@DPAD@ADJUST @SELECT@SELECT", null, false);

        /// <summary>
        /// List of modifier category menus.
        /// </summary>
        private static SortedDictionary<string, UIMenu> modifierMenuList = new SortedDictionary<string, UIMenu>();

        private static UIComponent configGroup = null;
        private static UIComponent optionsOK = null;

        /// <summary>
        /// Creates MserDuck's options menu.
        /// </summary>
        internal static void CreateModifierMenu()
        {
            foreach (var mod in ModifierManager.Modifiers)
            {
                if (!ModifierManager.modifierMenuList.ContainsKey(mod.GetCategory()))
                {
                    //Add new menu if not already existing
                    modifierMenuList.Add(mod.GetCategory(), new UIMenu(string.Format("@LWING@{0}@RWING@", mod.GetCategory().ToUpperInvariant()),
                        Layer.HUD.camera.width / 2f, Layer.HUD.camera.height / 2f, 250f, -1f, "@DPAD@ADJUST @SELECT@SELECT"));
                }

                //Add modifier entries
                modifierMenuList[mod.GetCategory()].Add(new UIMenuItemToggle(mod.GetName(), null, new FieldBinding(mod, "Enabled")));
            }

            foreach (var menu in modifierMenuList)
            {
                //Add close buttons
                menu.Value.Add(new UIText(" ", Color.White));
                menu.Value.Add(new UIMenuItem("OK", new UIMenuActionOpenMenu(menu.Value, ModifierManager.optionsMenu), UIAlign.Center, default(Color), true));
                menu.Value.Close();

                //Add to group
                ModifierManager.optionsMenu.Add(new UIMenuItem(menu.Key.ToUpperInvariant(), new UIMenuActionOpenMenu(ModifierManager.optionsMenu, menu.Value), UIAlign.Center, default(Color), true));
            }

            //Finish options menu
            ModifierManager.optionsMenu.Add(new UIText(" ", Color.White));
            ModifierManager.optionsMenu.Close();
        }

        /// <summary>
        /// Shows MserDuck's Options Menu.
        /// </summary>
        internal static void ShowOptionsMenu()
        {
            // Set to TeamSelect2's private configGroup field - guaranteed to be set here.
            ModifierManager.configGroup = MonoMain.pauseMenu;

            // Close to avoid crash on draw code
            ModifierManager.configGroup.Close();

            // Not needed, safety first!
            ModifierManager.optionsMenu.Close();

            // Add here due to up-to-date required configGroup
            if (ModifierManager.optionsOK != null)
            {
                ModifierManager.optionsMenu.Remove(ModifierManager.optionsOK);
            }
            ModifierManager.optionsOK = new UIMenuItem("OK", new UIMenuActionCloseMenu(ModifierManager.configGroup));
            ModifierManager.optionsMenu.Add(ModifierManager.optionsOK);

            // Add menus to configGroup
            ModifierManager.configGroup.Add(ModifierManager.optionsMenu, false);
            foreach (var menu in ModifierManager.modifierMenuList.Values)
            {
                ModifierManager.configGroup.Add(menu, false);
            }

            // Show menu again
            ModifierManager.configGroup.Open();

            // And show custom options
            ModifierManager.optionsMenu.Open();
        }

        private static FieldInfo _close = MserGlobals.GetField(typeof(UIComponent), "_close");
        private static FieldInfo _section = MserGlobals.GetField(typeof(UIMenu), "_section");

        internal static void DrawDescriptions()
        {
            //Check if menu is open
            if (MserGlobals.GetGameState() == GameState.Lobby && configGroup != null && !(bool)_close.GetValue(configGroup))
            {
                KeyValuePair<string, UIMenu> menu;
                try
                {
                    //Get currently open modifierMenuList entry ( = opened category)
                    menu = ModifierManager.modifierMenuList.First(e => e.Value != null && !(bool)ModifierManager._close.GetValue(e.Value));
                }
                catch
                {
                    //None opened, don't bother
                    return;
                }

                //Get selected modifier
                var entries = (UIBox)_section.GetValue(menu.Value);
                var cat = menu.Key;
                var mod = ModifierManager.Modifiers.Where(m => m.GetCategory().EqualsTo(true, cat)).OrderBy(m => m.GetName()).ElementAtOrDefault(entries.selection);

                if (mod != null)
                {
                    //Show the description
                    //TODO: ModifierManager - try to vertically center this nicer, depending on the height of the menu itself
                    //TODO: ModifierManager - scale text according to resolution - 720p cuts off a lot compared to 1080p
                    var text = mod.GetDescription();
                    FontManager.Bios.scale = new Vec2(4);
                    var off = 70 - text.Length * 15;
                    var first = true;
                    foreach (var line in text)
                    {
                        var display = line;
                        if (first && mod.IsNotImplemented())
                            display = "[Not Implemented] " + display;
                        FontManager.Bios.Draw(display, Layer.Console.width / 2 - FontManager.Bios.GetWidth(display) / 2, off, mod.IsNotImplemented() ? Color.Red : Color.White, 1f);
                        off += 40;

                        first = false;
                    }
                }
            }
        }
    }
}

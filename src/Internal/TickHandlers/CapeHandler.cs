﻿using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Handles cape synchronization and setting custom capes for certain hats.
    /// </summary>
    public static class CapeHandler
    {
        public static Texture2D ballzCape = Content.Load<Tex2D>(Mod.GetPath<MserDuck>("ballzCape.png"));

        public static List<Cape> editedCapes = new List<Cape>();
        public static Dictionary<Profile, Texture2D> savedProfiles = new Dictionary<Profile, Texture2D>(); 

        private static readonly FieldInfo _hatCape = MserGlobals.GetField(typeof(TeamHat), "_cape");
        private static readonly FieldInfo _capeAttach = MserGlobals.GetField(typeof(Cape), "_attach");

        public static void Update()
        {
            if (MserGlobals.GetGameState() == GameState.Loading)
                return;

            //Go through list of hats - automatic capes
            foreach (TeamHat hat in Level.current.things[typeof(TeamHat)])
            {
                //Check for valid hat texture
                if (hat.sprite.texture.textureName == "hats/devhat")
                {
                    CapeHandler.AttachCape(hat, CapeHandler.ballzCape);
                }

                //Go through hats of saved ducks
                foreach (var profile in CapeHandler.savedProfiles)
                {
                    //Hat is worn by saved duck
                    if (hat.duck != null && hat.duck.profile == profile.Key)
                    {
                        CapeHandler.AttachCape(hat, profile.Value);
                    }
                }
            }
        }

        public static void AttachCape(TeamHat hat, Texture2D texture, bool trail = false, int maxLength = -1)
        {
            //Get cape
            var cape = (Cape)_hatCape.GetValue(hat);

            //Check if cape isn't modified yet or no cape is present
            var globalIndexCapes = CapeHandler.editedCapes.Where(x => x != null && cape != null &&
                ReferenceEquals(_capeAttach.GetValue(x), _capeAttach.GetValue(cape))).ToList();

            if (cape == null || editedCapes.Count == 0 || globalIndexCapes.Count == 0)
            {
                //Remove cape first
                DettachCape(hat);

                //Do devhat check
                if (hat.sprite.texture.textureName != "hats/devhat")
                {
                    var freshcape = new Cape(hat.x, hat.y, hat, trail);
                    _hatCape.SetValue(hat, freshcape);
                    Level.Add(freshcape);
                }

                //Create cape
                hat.Initialize();

                //Set cape texture
                var newcape = (Cape)_hatCape.GetValue(hat);
                if (newcape != null)
                {
                    newcape.SetCapeTexture(texture);

                    if (maxLength >= 0)
                    {
                        MserGlobals.GetField(typeof(Cape), "maxLength").SetValue(newcape, maxLength);
                    }

                    //Add to the list
                    editedCapes.Add(newcape);
                }
            }
        }

        /// <summary>
        /// Dettaches the cape from the specified duck, if it wears a hat with a cape.
        /// </summary>
        /// <param name="duck"></param>
        public static void DettachCape(Duck duck)
        {
            if (duck == null) return;

            foreach (TeamHat hat in Level.current.things[typeof(TeamHat)])
            {
                //Hat is worn by saved duck
                if (hat.duck == duck)
                {
                    DettachCape(hat);
                    return;
                }
            }
        }

        /// <summary>
        /// Dettaches the cape from the specified hat.
        /// </summary>
        /// <param name="hat"></param>
        public static void DettachCape(TeamHat hat)
        {
            if (hat == null) return;

            var cape = (Cape)_hatCape.GetValue(hat);

            if (cape != null)
                Level.Remove(cape);

            _hatCape.SetValue(hat, null);
        }
    }
}

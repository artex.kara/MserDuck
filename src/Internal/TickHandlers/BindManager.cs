﻿using System;
using System.Collections.Generic;
using System.Linq;
using DuckGame.MserDuck.CommandHandlers;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Tick Handler for working with bound commands and executing them.
    /// </summary>
    public static class BindManager
    {
        private static bool _didInitialBind = false;

        /// <summary>
        /// Whether binds are currently interpreted.
        /// </summary>
        public static bool BindsEnabled = true;

        /// <summary>
        /// A list of all bound commands.
        /// </summary>
        public static readonly List<BoundCommand> Commands = new List<BoundCommand>();

        /// <summary>
        /// The last command that was bound with any bind command.
        /// </summary>
        public static BoundCommand LastBound;

        public static void Update()
        {
            //Try initial bind
            if (!BindManager._didInitialBind && MserGlobals.GetLocalDuck() != null && Network.isActive)
            {
                // Load binds
                ConfigManager.RunAutoexecutes(false);
                BindManager._didInitialBind = true;
            }

            //May not be chatting for this check
            if (Network.isActive && !DuckNetwork.core.enteringText && BindsEnabled)
            {
                //Take every command, check which have their assigned key pressed
                foreach (var cmd in BindManager.Commands)
                {
                    var state = cmd.GetBindKeyState();

                    if (cmd.Interval > -1 && (state == InputState.Pressed || state == InputState.Down))
                    {
                        //HoldBind execution
                        cmd.Timer++;

                        if (cmd.Timer >= cmd.Interval)
                        {
                            cmd.Timer = 0;
                            ChatManager.EnterChatMessage(cmd, true, true);
                        }
                    }
                    else if (state == InputState.Pressed)
                    {
                        //Simple bind execution
                        ChatManager.EnterChatMessage(cmd, true, true);
                    }
                }
            }
        }

        /// <summary>
        /// Binds the specified command with args to the set key.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static BoundCommand BindCommand(CommandHandler command, List<string> args, Keys key)
        {
            var bcmd = new BoundCommand(command, args, key);
            BindManager.Commands.Add(bcmd);
            LastBound = bcmd;
            return bcmd;
        }

        /// <summary>
        /// Unbinds all commands using the specified key, returning the amount of commands removed by this (for output purposes)
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static int UnbindCommand(Keys key)
        {
            var rmlist = BindManager.Commands.Where(cmd => cmd.Key == key).ToList();
            var amt = rmlist.Count;
            BindManager.Commands.RemoveAll(cmd => rmlist.Contains(cmd));
            return amt;
        }

        /// <summary>
        /// Unbinds all commands, returning the amount of commands removed by this (for output purposes)
        /// </summary>
        /// <returns></returns>
        public static int UnbindAllCommands()
        {
            var amt = BindManager.Commands.Count;
            BindManager.Commands.Clear();
            return amt;
        }

        /// <summary>
        /// Returns the key with the specified name, or Keys.None if the key does not exist.
        /// </summary>
        /// <param name="keyname"></param>
        /// <param name="ignoreCase"></param>
        /// <returns></returns>
        public static Keys GetKeyByString(string keyname, bool ignoreCase = true)
        {
            //First try keyname directly, then add D to the start (due to numbers for example being called D5).
            Keys res;
            return Enum.TryParse("D" + keyname, ignoreCase, out res) ? res :
                   Enum.TryParse("Oem" + keyname, ignoreCase, out res) ? res :
                   Enum.TryParse(keyname, ignoreCase, out res) ? res : Keys.None;
        }

        /// <summary>
        /// Returns the bound command with the specified key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static List<BoundCommand> GetBoundCommandsByKey(Keys key)
        {
            return BindManager.Commands.Where(cmd => cmd.Key == key).ToList();
        }

        /// <summary>
        /// Returns the KeysState of the specified key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static InputState GetKeyState(Keys key)
        {
            if (Keyboard.Pressed(key))
                return InputState.Pressed;

            if (Keyboard.Down(key))
                return InputState.Down;

            if (Keyboard.Released(key))
                return InputState.Released;

            return InputState.None;
        }
    }
}

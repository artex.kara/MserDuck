﻿using System;
using System.Collections.Generic;
using System.Linq;
using DuckGame.MserDuck.CommandHandlers;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Receives sent chat messages and decides how they should be handled.
    /// <para />The overridden DuckNetwork code for updating and displaying chat is found in the other partial part <code>ChatManagerOverrides.cs</code>.
    /// </summary>
    public static partial class ChatManager
    {
        /// <summary>
        /// Set of symbols for ArgumentErrors
        /// </summary>
        public static readonly SpriteMap Symbols = new SpriteMap(Thing.GetPath<MserDuck>("symbols"), 16, 16) { depth = 0.98f };

        /// <summary>
        /// List of known commands, ordered by their names.
        /// </summary>
        public static readonly List<CommandHandler> Commands = MserGlobals.GetSubInstances<CommandHandler>().OrderBy(c => c.GetNames().First()).ToList();

        /// <summary>
        /// Returns an instance of the command with the specified type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetCommand<T>()
        {
            return Commands.OfType<T>().FirstOrDefault();
        }

        /// <summary>
        /// Local list of sent messages, for scrubbing through history.
        /// </summary>
        public static readonly List<string> SentMessages = new List<string>();

        /// <summary>
        /// Last message index, to scrub back and forth through history.
        /// </summary>
        private static int _previousIndex = -1;

        /// <summary>
        /// The last sent message, for scrubbing through history.
        /// </summary>
        private static string _lastMessage = "";

        /// <summary>
        /// The chat mode to use. Set using <see cref="ConfigEntries.ChatModeEntry"/> in config.
        /// </summary>
        public static ChatModeSetting ChatMode { get; set; }

        /// <summary>
        /// Selected chat mode setting.
        /// </summary>
        public enum ChatModeSetting
        {
            /// <summary>
            /// Chat opens and sends with chat key only. Cancel with escape.
            /// </summary>
            Default = 0,

            /// <summary>
            /// Chat opens with chat key, but sends with enter. Cancel with escape.
            /// </summary>
            EnterOnly = 1,
            
            /// <summary>
            /// Chat opens with chat key, and sends with either chat key or enter. Cancel with escape.
            /// </summary>
            EnterOrChat = 2,
        }

        /// <summary>
        /// Smart text color for chat. Set using <see cref="ConfigEntries.SmartColorEntry"/> in config.
        /// </summary>
        public static bool SmartColor = false;

        /// <summary>
        /// Reference color for the Info profile ArgumentErrors.
        /// </summary>
        public static readonly Vec3 InfoColor = new Vec3(100, 150, 255);

        /// <summary>
        /// Reference color for the Warning profile ArgumentErrors.
        /// </summary>
        public static readonly Vec3 WarningColor = new Vec3(255, 210, 100);

        /// <summary>
        /// Reference color for the Error profile ArgumentErrors.
        /// </summary>
        public static readonly Vec3 ErrorColor = new Vec3(255, 90, 90);

        private static readonly Profile _infoProfile = new Profile("Info", null, new Team("Nerd", "hats/cyborgs"), new DuckPersona(InfoColor));

        /// <summary>
        /// A default profile used to display information chat messages without a sender.
        /// </summary>
        public static Profile InfoProfile
        {
            get { return ChatManager._infoProfile; }
        }

        private static readonly Profile _warningProfile = new Profile("Warning", null, new Team("Nerd", "hats/dicks"), new DuckPersona(WarningColor));

        /// <summary>
        /// A default profile used to display warning chat messages without a sender.
        /// </summary>
        public static Profile WarningProfile
        {
            get { return ChatManager._warningProfile; }
        }

        private static readonly Profile _errorProfile = new Profile("Error", null, new Team("Nerd", "hats/caps"), new DuckPersona(ErrorColor));

        /// <summary>
        /// A default profile used to display error chat messages without a sender.
        /// </summary>
        public static Profile ErrorProfile
        {
            get { return ChatManager._errorProfile; }
        }

        /// <summary>
        /// Amount of chat history entries, help page entries and autocomplete entries. Set using <see cref="ConfigEntries.ListSizeEntry"/> in config.
        /// </summary>
        public static int Entries = 10;
        
        private static bool _showingScoreboard = false;

        /// <summary>
        /// The key to hold in order to open the Scoreboard / Pings. Set using <see cref="ConfigEntries.ConnectionStatusEntry"/> in config.
        /// </summary>
        public static Keys ScoreboardKey = Keys.F1;

        /// <summary>
        /// Called whenever a chat message is sent or received over the network.
        /// <para/>Return true to execute normal chat message handler, false to suppress it.
        /// </summary>
        /// <param name="message">The message to handle.</param>
        /// <param name="quiet">Whether any argument errors should show after this command is executed.</param>
        /// <returns></returns>
        public static bool HandleChatMessage(NMChatMessage message, bool quiet = false)
        {
            var region = MserDebug.StartProfiling("ChatHandleMessage");

            try
            {
                var sender = DuckNetwork.core.profiles[message.profileIndex];
                var isLocal = sender.localPlayer;

                var text = message.text;
                if (text.Length == 0 || text[0] != '/')
                {
                    if (text.Trim().Length == 0)
                    {
                        // Courtesy to wekateka
                        return false;
                    }

                    // Is a normal chat message
                    return true;
                }
                else
                {
                    // Parse as a /command
                    //TODO: ChatManager - Any argument starting with a " should ignore spaces (part of one arg) until another " (remove the " at the end?) -> to external function
                    var commandLine = ChatManager.GetSplitText(text);
                    var command = commandLine.First().Substring(1);
                    var args = commandLine.GetRange(1, commandLine.Count - 1);
                    var force = false;

                    if (command.Contains("_quiet"))
                    {
                        command = command.Replace("_quiet", "");
                        quiet = true;
                    }
                    if (command.Contains("_force"))
                    {
                        command = command.Replace("_force", "");
                        force = true;
                    }

                    MserDebug.StepProfiling(region, "CommandLine");

                    foreach (var handler in ChatManager.Commands)
                    {
                        foreach (var variant in handler.GetNames())
                        {
                            if (!string.Equals(variant, command, StringComparison.InvariantCultureIgnoreCase)) continue;

                            //Check if it should even execute it for local user
                            if (!isLocal && handler.IsLocalOnly())
                                return false;

                            IEnumerable<ArgumentError> argerrors = null;

                            try
                            {
                                MserDebug.StepProfiling(region, "Variants");
                                if (!force)
                                    argerrors = handler.CheckArgumentsValid(args);
                                MserDebug.StepProfiling(region, "CheckArgumentsValid");
                            }
                            catch (Exception e)
                            {
                                ChatManager.Echo("Internal error in argument validation!");
                                Program.LogLine("Internal error in argument validation! " + e.Message + e.StackTrace);
                            }

                            List<ArgumentError> errorlist = null;
                            if (argerrors != null)
                                errorlist = argerrors.Where(e => e.Severity == Severity.Error || e.Severity == Severity.Usage).ToList();

                            if (isLocal && !quiet && errorlist != null && errorlist.Any())
                            {
                                //Errors found, notify user and don't execute!
                                foreach (var err in errorlist)
                                {
                                    ChatManager.DisplayInfo(err);
                                }

                                if (errorlist.Any(e => e.Severity == Severity.Usage))
                                    ChatManager.ShowUsage(handler);

                                return false;
                            }

                            //EXECUTE COMMAND
                            try
                            {
                                var info = ExecuteInfo.NoPermissions(PermissionManager.GetRole(sender));
                                if (PermissionManager.IsPermittedToExecute(sender, handler))
                                {
                                    info = handler.Execute(DuckNetwork.core.profiles[message.profileIndex], args);
                                    MserDebug.StepProfiling(region, "CommandExecute");
                                }

                                //No outputs for quiet
                                if (quiet || !isLocal) return false;

                                if (ChatManager.ExecuteRanSuccessfully(info))
                                {
                                    //Valid execute
                                    if (info != null)
                                    {
                                        //Display info text
                                        ChatManager.DisplayInfo(info);
                                    }
                                    else if (handler.IsNotImplemented())
                                    {
                                        //Not implemented, warn user
                                        ChatManager.DisplayInfo("This command is currently not implemented!", Severity.Warning);
                                    }
                                }
                                else
                                {
                                    //Invalid execute, show usage to local player
                                    if (info.Severity == Severity.Usage)
                                        ChatManager.ShowUsage(handler);
                                    else
                                        ChatManager.DisplayInfo(info);
                                }
                            }
                            catch (Exception e)
                            {
                                ChatManager.Echo("Internal error in command's execute!");
                                Program.LogLine("Internal error in command's execute! " + e.Message + e.StackTrace);
                            }
                            return false;
                        }
                    }

                    if (isLocal)
                        ChatManager.Echo(string.Format("Unknown command \"{0}\".", command));
                    return false;
                }
            }
            finally
            {
                MserDebug.EndProfiling(region);
            }
        }

        /// <summary>
        /// Displays the specified <see cref="ExecuteInfo" /> as a local chat message.
        /// </summary>
        /// <param name="info"></param>
        public static void DisplayInfo(ExecuteInfo info)
        {
            DisplayInfo(info.Message, info.Severity);
        }

        /// <summary>
        /// Displays the specified <see cref="ArgumentError" /> info as a local chat message.
        /// </summary>
        /// <param name="info"></param>
        public static void DisplayInfo(ArgumentError info)
        {
            DisplayInfo(info.Message, info.Severity);
        }

        /// <summary>
        /// Displays the specified info, using the specified severity level's profile as a local chat message.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="severity"></param>
        public static void DisplayInfo(string text, Severity severity)
        {
            byte profile;

            switch (severity)
            {
                case Severity.Usage:
                case Severity.Info:
                    profile = 255;
                    break;
                case Severity.Warning:
                    profile = 254;
                    break;
                case Severity.Error:
                    profile = 253;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("severity", "Unknown severity value!");
            }

            ChatManager.Echo(text, profile);
        }

        /// <summary>
        /// Returns whether the specified ExecuteInfo is one where you can be sure it ran successfully.
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static bool ExecuteRanSuccessfully(ExecuteInfo info)
        {
            return info == null || info.Severity == Severity.Info;
        }

        /// <summary>
        /// Returns the usage string of the specified command.
        /// </summary>
        /// <param name="handler"></param>
        /// <returns></returns>
        public static string GetUsage(CommandHandler handler)
        {
            return "Usage: /" + string.Join(", /", handler.GetNames()) + " " + string.Join(" ", handler.GetExpectedArguments());
        }

        /// <summary>
        /// Returns the help string for the specified command.
        /// </summary>
        /// <param name="handler"></param>
        /// <returns></returns>
        public static string[] GetHelp(CommandHandler handler)
        {
            return handler.GetHelpMessage();
        }

        /// <summary>
        /// Displays the usage string of the specified command.
        /// </summary>
        /// <param name="handler"></param>
        public static void ShowUsage(CommandHandler handler)
        {
            ChatManager.Echo(GetUsage(handler));
        }

        /// <summary>
        /// Displays the help string for the specified command.
        /// </summary>
        /// <param name="handler"></param>
        public static void ShowHelp(CommandHandler handler)
        {
            foreach (string line in GetHelp(handler))
            {
                ChatManager.Echo(line);
            }
        }

        /// <summary>
        /// Holding key initial delay until automatically moving through AutoCompleteEntries.
        /// </summary>
        private static int _holdTimer = 0;

        /// <summary>
        /// Holding key delay between moving through AutoCompleteEntries.
        /// </summary>
        private static int _holdTicker = 0;

        private static string _lastentertext;

        /// <summary>
        /// Update function to handle chat-related input.
        /// </summary>
        public static void UpdateInput()
        {
            // Scrubbing through chat history

            if (DuckNetwork.core.enteringText)
            {
                //Copy and paste
                if (DuckNetwork.core.enteringText && Keyboard.Down(Keys.LeftControl) && Keyboard.Pressed(Keys.V))
                {
                    //Remove the last char
                    Keyboard.keyString = Keyboard.keyString.Substring(0, Math.Max(Keyboard.keyString.Length - 1, 0));

                    //Paste clipboard into chat
                    //HINT: REMINDER ChatManager - Allow newlines in clipboard paste once multiline support is there (strip for now)
                    Keyboard.keyString += MserGlobals.GetClipboardText().Replace("\r", " ").Replace("\n", " ");
                    SFX.Play("tinyTick");
                }

                if (DuckNetwork.core.enteringText && Keyboard.Down(Keys.LeftControl) && Keyboard.Pressed(Keys.C))
                {
                    //Remove the last char
                    Keyboard.keyString = Keyboard.keyString.Substring(0, Math.Max(Keyboard.keyString.Length - 1, 0));

                    //Copy chat to clipboard
                    MserGlobals.SetClipboardText(Keyboard.keyString);
                    SFX.Play("tinyTick");
                }

                if (ChatManager.AutoCompleteCount > 0 && _lastentertext != DuckNetwork.core.currentEnterText)
                {
                    //Scrolling through auto completes
                    ChatManager._holdTimer++;
                    ChatManager._holdTicker++;

                    //Scrub through autocomplete list
                    if (Keyboard.Pressed(Keys.Up) || (Keyboard.Down(Keys.Up) && ChatManager._holdTimer > 16 && ChatManager._holdTicker > 3))
                    {
                        ChatManager._holdTicker = 0;
                        ChatManager.AutoCompleteSelected--;
                        //Only wrap on tap
                        if (ChatManager.AutoCompleteSelected < 0 && ChatManager._holdTimer <= 16)
                            ChatManager.AutoCompleteSelected = ChatManager.AutoCompleteCount - 1;
                    }
                    else if (Keyboard.Pressed(Keys.Down) || (Keyboard.Down(Keys.Down) && ChatManager._holdTimer > 16 && ChatManager._holdTicker > 3))
                    {
                        ChatManager._holdTicker = 0;
                        ChatManager.AutoCompleteSelected++;
                        //Only wrap on tap
                        if (ChatManager.AutoCompleteSelected > ChatManager.AutoCompleteCount - 1 && ChatManager._holdTimer <= 16)
                            ChatManager.AutoCompleteSelected = 0;
                    }

                    if (!Keyboard.Down(Keys.Up) && !Keyboard.Down(Keys.Down))
                        _holdTimer = 0;

                    if (Keyboard.Pressed(Keys.Right) || Keyboard.Pressed(Keys.Tab))
                    {
                        //HINT: REMINDER ChatManager - Once caret movement is allowed, check if caret is at the end (disallow right)
                        SFX.Play("consoleTick");
                        ChatManager.AutoComplete();
                    }
                }
                else
                {
                    //Scrolling through prev/next messages
                    if (ChatManager.AutoCompleteCount > 0 && (Keyboard.Pressed(Keys.Right) || Keyboard.Pressed(Keys.Tab)))
                    {
                        //HINT: REMINDER ChatManager - Once caret movement is allowed, check if caret is at the end (disallow right)
                        SFX.Play("consoleTick");
                        ChatManager.AutoComplete();
                    }

                    var trigger = false;

                    //Get prev/next message
                    if (Keyboard.Pressed(Keys.Up))
                    {
                        ChatManager._previousIndex++;
                        trigger = true;

                        if (ChatManager._previousIndex == 0)
                        {
                            //Save the last message
                            ChatManager._lastMessage = DuckNetwork.core.currentEnterText;
                        }
                    }
                    else if (Keyboard.Pressed(Keys.Down))
                    {
                        ChatManager._previousIndex--;
                        trigger = true;
                    }

                    if (trigger)
                    {
                        ChatManager._previousIndex = Maths.Clamp(ChatManager._previousIndex, -1,
                            ChatManager.SentMessages.Count - 1);

                        var msg = "";

                        //Set the last typed message
                        if (ChatManager._previousIndex == -1)
                        {
                            msg = ChatManager._lastMessage;
                        }

                        //Get localMessage content
                        if (ChatManager._previousIndex != -1 && ChatManager.SentMessages.Count != 0)
                        {
                            msg = ChatManager.SentMessages[ChatManager._previousIndex];
                        }

                        //Play sound if different
                        if (DuckNetwork.core.currentEnterText != msg)
                        {
                            SFX.Play("consoleTick");
                        }

                        Keyboard.keyString = msg;
                        //DuckNetwork.core.currentEnterText = msg;
                        _lastentertext = msg;
                    }
                }
            }
        }

        /// <summary>
        /// Returns the command with the specified name. Detects if cmdName starts with a slash as well.
        /// </summary>
        /// <param name="cmdName"></param>
        /// <returns></returns>
        public static CommandHandler GetCommandByName(string cmdName)
        {
            if (string.IsNullOrWhiteSpace(cmdName))
                return null;

            cmdName = cmdName.Replace("_quiet", "");
            cmdName = cmdName.Replace("_force", "");

            return ChatManager.Commands.FirstOrDefault(c => c.GetNames().Any(n => n.Equals(cmdName, StringComparison.InvariantCultureIgnoreCase) ||
                                                                           ("/" + n).Equals(cmdName, StringComparison.InvariantCultureIgnoreCase)));
        }

        /// <summary>
        /// Prints text locally as a chat message sent by the specified profile.
        /// <para />If running a singleplayer game, the line is added to the ducklog instead.
        /// </summary>
        /// <param name="text">The text to display.</param>
        /// <param name="profile">The profile to display the message with.
        /// <para />Info Profile = 255, Warning Profile = 254, Error Profile = 253</param>
        public static void Echo(string text, byte profile = 255)
        {
            if (!Network.isActive)
            {
                //Can't echo without chat
                Program.LogLine("[ECHO] " + text);
            }
            else
            {
                var split = text.Split(new[] {"\r\n", "\n"}, StringSplitOptions.RemoveEmptyEntries);
                foreach (var line in split)
                {
                    ChatManager.DisplayChatMessage(line, profile);
                }
            }
        }

        /// <summary>
        /// Displays the specified text locally, as a chat message on the screen.
        /// <para />Game has to be online for this to work! This will not send anything over the network.
        /// </summary>
        /// <param name="text">The text to display.</param>
        /// <param name="profile">The profile to display the message with.
        /// <para />Info Profile = 255, Warning Profile = 254, Error Profile = 253</param>
        /// <exception cref="InvalidOperationException">Game is not currently online!</exception>
        private static void DisplayChatMessage(string text, byte profile = 255)
        {
            if (!Network.isActive)
                throw new InvalidOperationException("Game is not currently online!");

            var nMChatMessage = new NMChatMessage(profile, text, DuckNetwork.core.chatIndex);
            DuckNetwork.core.chatIndex++;
            ChatManager.DisplayChatMessageFromNetwork(nMChatMessage);
        }

        /// <summary>
        /// Executes the specified BoundCommand as if the command specified by it was being chatted.
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="quiet">Whether to play a sound effect when the message has been recieved.</param>
        /// <param name="forced">Whether to add an _force parameter to the chat message, skipping any ArgumentError checks.</param>
        /// <exception cref="InvalidOperationException">Game is not currently online!</exception>
        public static void EnterChatMessage(BoundCommand cmd, bool quiet, bool forced)
        {
            ChatManager.EnterChatMessage(cmd.ToString(true, forced), quiet);
        }

        /// <summary>
        /// Sends the specified string as a chat message as if it were sent by the specified profile.
        /// <para />Start the message with a slash to send a command to the other players.
        /// <para />Keep the profile at null to send it as your own profile
        /// </summary>
        /// <param name="message"></param>
        /// <param name="quiet">Whether to play a sound effect when the message has been recieved.</param>
        /// <param name="prof"></param>
        /// <exception cref="InvalidOperationException">Game is not currently online!</exception>
        public static void EnterChatMessage(string message, bool quiet = true, Profile prof = null)
        {
            if (!Network.isActive)
                throw new InvalidOperationException("Game is not currently online!");

            var sender = (prof == null) ? (byte)DuckNetwork.core.localDuckIndex : prof.networkIndex;
            var fakeChatNM = new NMChatMessage(sender, message, DuckNetwork.core.chatIndex);
            Send.Message(fakeChatNM);
            ChatManager.HandleChatMessage(fakeChatNM, quiet);
        }

        /// <summary>
        /// Sends the specified string as a chat message as if it were sent by the specified profile, but does not call it for the local client.
        /// <para />Start the message with a slash to send a command to the other players.
        /// <para />Keep the profile at null to send it as your own profile
        /// </summary>
        /// <param name="message"></param>
        /// <param name="quiet">Whether to play a sound effect when the message has been recieved.</param>
        /// <param name="prof"></param>
        public static void SendChatMessage(string message, bool quiet = true, Profile prof = null)
        {
            var sender = (prof == null) ? (byte)DuckNetwork.core.localDuckIndex : prof.networkIndex;
            var fakeChatNM = new NMChatMessage(sender, message, DuckNetwork.core.chatIndex);
            Send.Message(fakeChatNM);
        }

        /// <summary>
        /// Displays the specified chat message on the screen.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="playSound"></param>
        public static void DisplayChatMessageFromNetwork(NMChatMessage message, bool playSound = true)
        {
            Profile prof;

            switch (message.profileIndex)
            {
                case 253:
                    prof = ChatManager.ErrorProfile;
                    break;
                case 254:
                    prof = ChatManager.WarningProfile;
                    break;
                case 255:
                    prof = ChatManager.InfoProfile;
                    break;
                default:
                    prof = DuckNetwork.core.profiles[message.profileIndex];
                    break;
            }

            DuckNetwork.core.chatMessages.Add(new ChatMessage(prof, message.text, message.index));
            DuckNetwork.core.chatMessages = (from x in DuckNetwork.core.chatMessages orderby -x.index select x).ToList();
            if (playSound)
            {
                SFX.Play("chatmessage", 0.8f, Rando.Float(-0.15f, 0.15f), 0f, false);
            }
        }

        /// <summary>
        /// Returns a short string describing the specified command, as seen in /help num.
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public static string GetShortCommandText(CommandHandler cmd)
        {
            return string.Format("/{0} - {1}", cmd.GetNames()[0], cmd.GetHelpMessage()[0]);
        }

        //TODO: ChatManager - how stable is AutoCompleteTopIndex-related code below considering the Entries variable can change?

        /// <summary>
        /// Amount of auto complete entries, clamped by the Entries variable.
        /// </summary>
        //BUG: ChatManager - when AutoCompleteCount changes, the topindex does not and selection can go offscreen!
        public static int AutoCompleteCount { get; set; }

        /// <summary>
        /// Currently selected auto complete entry.
        /// </summary>
        private static int AutoCompleteSelected
        {
            get { return _autoCompleteSelected; }
            set
            {
                if (AutoCompleteSelected == value)
                    return;

                //Modify topindex
                if (value >= ChatManager.AutoCompleteTopIndex + Entries || value < AutoCompleteTopIndex)
                {
                    //Going offscreen!
                    if (Math.Abs(value - AutoCompleteSelected) > 1)
                    {
                        //Big step, center!
                        ChatManager.AutoCompleteTopIndex = value - Entries / 2;
                    }
                    else
                    {
                        //Small step, offset by 4
                        if (value > AutoCompleteSelected)
                        {
                            //Going down
                            ChatManager.AutoCompleteTopIndex += Entries / 3;
                        }
                        else
                        {
                            //Going up
                            ChatManager.AutoCompleteTopIndex -= Entries / 3;
                        }
                    }
                }

                //Finally set selection
                ChatManager._autoCompleteSelected = value;
            }
        }

        private static int _autoCompleteSelected;

        /// <summary>
        /// Index of top-most auto complete entry in list.
        /// </summary>
        private static int AutoCompleteTopIndex
        {
            get
            {
                var diff = ChatManager.AutoCompleteCount - Entries;
                if (diff <= 0)
                    return 0;
                return Maths.Clamp(_autoCompleteTopIndex, 0, diff);
            }
            set
            {
                _autoCompleteTopIndex = value;
            }
        }

        private static int _autoCompleteTopIndex;

        /// <summary>
        /// Used for comparing whether chat input is different (for updating data related to chat!)
        /// </summary>
        private static string _lastChatText = "";

        /// <summary>
        /// Sprite used for underlining argumenterror chat text.
        /// </summary>
        private static readonly Sprite _underline = new Sprite(Thing.GetPath<MserDuck>("underline")) {depth = 0.98f};

        /// <summary>
        /// Whether to show argument error popup boxes. Set using <see cref="ConfigEntries.ArgumentErrorsPopupEntry"/> in config.
        /// </summary>
        public static bool ArgumentErrorsPopups { get; set; }

        /// <summary>
        /// Whether to show argument error connecting lines between popup and text. Set using <see cref="ConfigEntries.ArgumentErrorsConnectionEntry"/> in config.
        /// </summary>
        public static bool ArgumentErrorsConnection { get; set; }

        /// <summary>
        /// Whether to show argument error text underlining. Set using <see cref="ConfigEntries.ArgumentErrorsUnderlineEntry"/> in config.
        /// </summary>
        public static bool ArgumentErrorsUnderline { get; set; }

        /// <summary>
        /// Reset the scrolling of the auto complete list.
        /// </summary>
        public static void ResetAutoCompleteScrolling()
        {
            ChatManager.AutoCompleteSelected = 0;
            ChatManager.AutoCompleteTopIndex = 0;
        }

        /// <summary>
        /// Returns a color similar to the given color, darkened or brightened to be readable.
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        private static Color GetAdjustedColor(Color color)
        {
            return new Color(color.ToVector3() + new Vec3((color.Lightness() > 128) ? -0.25f : 0.25f));
        }

        /// <summary>
        /// Returns a color for text chat based on background color.
        /// </summary>
        /// <param name="backColor"></param>
        /// <returns></returns>
        private static Color GetChatTextColor(Color backColor)
        {
            if (ChatManager.SmartColor)
            {
                //Smart text color
                return backColor.Invert().Difference(backColor) > 60 ? backColor.Invert() : (backColor.Lightness() > 128 ? Color.Black : Color.White);
            }
            else
            {
                //Textcolor similar to default
                return backColor.Lightness() > 128 ? Color.Black : Color.White;
            }
        }

        /// <summary>
        /// Returns the chat split at any whitespace character.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetSplitChat(bool addEmpty = true)
        {
            var split = GetSplitText(DuckNetwork.core.currentEnterText);
            if (addEmpty && DuckNetwork.core.currentEnterText.EndsWith(" "))
                split.Add("");
            return split;
        }

        /// <summary>
        /// Splits the specified text at every whitespace character, removing empty entries.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetSplitText(string text)
        {
            return text.Split((char[])null, StringSplitOptions.RemoveEmptyEntries).ToList();
        }

        /// <summary>
        /// Sends the currently entered text as a chat message. Same effect as pressing Enter.
        /// </summary>
        private static void SendMessage()
        {
            if (DuckNetwork.core.currentEnterText != "")
            {
                //First try to autocomplete, but not if last arg is nothing
                if (DuckNetwork.core.currentEnterText == "/")
                {
                    //Check for / to show special help text
                    ChatManager.Echo("Start a message with a slash to execute a command. Use /help to see the stuff you can do!");
                }
                else
                {
                    ChatManager.AutoComplete(false);
                    NMChatMessage message = new NMChatMessage((byte)DuckNetwork.core.localDuckIndex, DuckNetwork.core.currentEnterText,
                                                              DuckNetwork.core.chatIndex);
                    DuckNetwork.core.chatIndex += 1;
                    DuckNetwork.SendToEveryone(message);
                    DuckNetwork.ChatMessageReceived(message);
                }

                //Reset index
                ChatManager._previousIndex = -1;

                //Add to recent messages list if not before in list
                if (ChatManager.SentMessages.Count == 0 || ChatManager.SentMessages[0] != DuckNetwork.core.currentEnterText)
                {
                    ChatManager.SentMessages.Insert(0, DuckNetwork.core.currentEnterText);
                    ChatManager._lastMessage = "";
                }

                DuckNetwork.core.currentEnterText = "";
            }

            DuckNetwork.core.stopEnteringText = true;
            ChatManager.AutoCompleteSelected = -1;
            ChatManager.AutoCompleteCount = 0;
        }

        /// <summary>
        /// Runs autocomplete on the current enter text.
        /// </summary>
        public static void AutoComplete(bool fromNothing = true)
        {
            if (ChatManager.AutoCompleteCount == 0 || ChatManager.AutoCompleteSelected < 0) return;

            //Check if AC commands or args
            if (DuckNetwork.core.currentEnterText.Contains(" "))
            {
                //Argument autocomplete

                //If no last arg is entered (space), then do not autocomplete
                if (!fromNothing && DuckNetwork.core.currentEnterText.Last() == ' ')
                    return;

                //Get the list
                var list = CacheManager.GetArgumentAutoCompleteList().ToList();
                if (ChatManager.AutoCompleteSelected >= list.Count) return;

                //Only insert if it autocompletes
                var entry = list[ChatManager.AutoCompleteSelected];
                if (!entry.Autocompletes)
                    return;

                var split = GetSplitChat();
                var lastEmpty = string.IsNullOrWhiteSpace(split.Last());
                Keyboard.keyString = string.Format("{0}{1}{2}{3}",
                    string.Join(" ", split.GetRange(0, split.Count - (lastEmpty ? 0 : 1))),
                    lastEmpty ? "" : " ",
                    entry.Value,
                    entry.InsertSpace ? " " : "");
                DuckNetwork.core.currentEnterText = Keyboard.keyString;
            }
            else
            {
                var list = CacheManager.GetCommandAutoCompleteList();
                if (ChatManager.AutoCompleteSelected >= list.Count) return;

                //Command autocomplete
                Keyboard.keyString = list[ChatManager.AutoCompleteSelected] + " ";
                DuckNetwork.core.currentEnterText = Keyboard.keyString;
            }

            //Reset selection and scrolling
            AutoCompleteSelected = 0;
            AutoCompleteTopIndex = 0;
        }
    }
}

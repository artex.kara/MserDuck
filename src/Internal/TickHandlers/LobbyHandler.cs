﻿using System.Linq;
using DuckGame.MserDuck.NetMessages;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Simple handler to create a lobby if the chat button is pressed in a singleplayer lobby.
    /// </summary>
    public static class LobbyHandler
    {
        private static byte _lastDuckCount;

        private static int _timer;

        public static void Update()
        {
            //Create lobby if non-existant and pressing the chat key
            if (MserGlobals.GetGameState() == GameState.Lobby && !Network.isActive && Profiles.active.Count > 0 && Input.Down("CHAT"))
            {
                TeamSelect2.DoInvite();
            }

            //Check for a duck joining
            if (Network.isActive)
            {
                if (DuckNetwork.profiles.Count > LobbyHandler._lastDuckCount)
                {
                    //Somebody joined
                    LobbyHandler._lastDuckCount = (byte)DuckNetwork.profiles.Count;
                    MserEvents.OnJoin();
                }
                else if (DuckNetwork.profiles.Count < LobbyHandler._lastDuckCount)
                {
                    //Reset silently
                    LobbyHandler._lastDuckCount = (byte)DuckNetwork.profiles.Count;
                }
            }

            //Send data over in lobby
            LobbyHandler._timer++;
            if (MserGlobals.GetGameState() == GameState.Lobby && Network.isActive && LobbyHandler._timer >= 150)
            {
                LobbyHandler._timer = 0;

                if (DuckNetwork.localDuckIndex == -1 || MserGlobals.GetLocalDuck() == null)
                    return;

                //Send Color to others
                var myColor = MserGlobals.GetLocalDuck().persona.color;
                Send.Message(new NMPersonaColor((byte)DuckNetwork.localDuckIndex, (byte)myColor.x, (byte)myColor.y, (byte)myColor.z));

                //Send MserDuck Modifier settings if server
                if (Network.isServer && ModifierManager.ModifiersReady)
                {
                    var nm = new NMSetModifiers();
                    nm.handlerList = ModifierManager.Modifiers.Select(m => m.Enabled).ToList();
                    Send.Message(nm);
                }

                //Send Skin to others
                Skin skin;
                if (Skin.SelectedSkins.TryGetValue(MserGlobals.GetLocalDuck().profile, out skin))
                {
                    ChatManager.SendChatMessage("/skin " + skin.Name);
                }

                //Update list of users, since there's no other reliable way to check (dis)connection and update it for all
                CacheManager.UpdateUsersEntries();

                //TODO: LobbyHandler - Update list of roles, permissions and users
                //TODO: LobbyHandler - Send over cape to others, only on join ( = only once)
            }
        }
    }
}

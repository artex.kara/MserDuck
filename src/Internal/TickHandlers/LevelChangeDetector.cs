﻿namespace DuckGame.MserDuck
{
    /// <summary>
    /// A simple handler which detects when the level changes. Does not account for things like loading screens or different GameStates.
    /// </summary>
    public static class LevelChangeDetector
    {
        private static Level _lastLevel;

        public static void Update()
        {
            if (Level.current != null && Level.current != LevelChangeDetector._lastLevel)
            {
                LevelChangeDetector._lastLevel = Level.current;
                MserEvents.OnLevelEnded();
            }
        }
    }
}

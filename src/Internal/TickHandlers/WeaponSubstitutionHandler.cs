﻿using System.Collections.Generic;
using System.Linq;
using DuckGame.MserDuck.Fakes;
using DuckGame.MserDuck.ModifierHandlers;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Handler for substituting weapons with others (mostly for fakes or additional behaviour), synchronizing their state when replacing.
    /// </summary>
    public static class WeaponSubstitutionHandler
    {
        private static int _itemCount = -1;

        public static void Update()
        {
            if (Network.isActive && !Network.isServer)
                return;

            var phys = MserGlobals.GetListOfThings<PhysicsObject>().ToList();
            var currcnt = phys.Count;
            if (WeaponSubstitutionHandler._itemCount != currcnt && GameState.InGame.HasFlag(MserGlobals.GetGameState()))
            {
                //Refresh count
                WeaponSubstitutionHandler._itemCount = currcnt;

                //Create dictionary entries
                var newThings = new Dictionary<PhysicsObject, PhysicsObject>();
                foreach (Trombone tromb in Level.current.things[typeof(Trombone)])
                {
                    newThings.Add(new Instrument(0, 0, -2), tromb);
                }
                foreach (Saxaphone sax in Level.current.things[typeof(Saxaphone)])
                {
                    newThings.Add(new Instrument(0, 0, -3), sax);
                }
                foreach (QuadLaser ql in Level.current.things[typeof(QuadLaser)])
                {
                    if (!(ql is QuadLaserEx))
                        newThings.Add(new QuadLaserEx(), ql);
                }

                //Add key, remove value
                foreach (var entry in newThings)
                {
                    SubstituteItem(entry.Value, entry.Key, true, true);
                }

                //Special substitutions: FAKES
                if (ModifierManager.GetModifier<FakWeaponsModifier>().Enabled && Rando.Int(0, 50) == 30)
                {
                    //Select a random thing to substitute
                    var fakkables = phys.Where(t => WeaponFakker.HasFakVersion(t) && !WeaponFakker.IsFak(t)).ToList();
                    if (fakkables.Any())
                    {
                        var tofak = fakkables.PickRandom();
                        WeaponFakker.ConvertToFak(tofak);
                    }
                }
            }
        }

        /// <summary>
        /// Replaces the specified old object with the new one, trying to keep it seamless.
        /// </summary>
        /// <param name="old"></param>
        /// <param name="new"></param>
        /// <param name="justSpawned">Whether the new object has just been spawned and needs additional properties such as position set.</param>
        /// <param name="keepAmmo">Whether ammo should be kept from the old object or defined by the new object.</param>
        public static void SubstituteItem(PhysicsObject old, PhysicsObject @new, bool justSpawned, bool keepAmmo)
        {
            //For itembox replacements
            @new.visible = old.visible;
            @new.active = old.active;
            @new.velocity = old.velocity;
            
            //Update contained object
            var box = MserGlobals.GetListOfThings<ItemBox>().FirstOrDefault(b => b.containedObject == old);
            if (box != null)
            {
                box.containedObject = @new;
            }

            //Newly spawned stuff
            if (justSpawned)
            {
                @new.position = old.position;
                @new.offDir = old.offDir;
            }

            Level.Add(@new);

            //Transfer properties of importance
            if (old.owner != null && ((Duck)old.owner).holdObject == old)
                ((Duck)old.owner).GiveHoldable((Holdable)@new);
            @new.alpha = old.alpha;
            @new.angle = old.angle;
            @new.scale = old.scale;
            @new.flipHorizontal = old.flipHorizontal;
            @new.flipVertical = old.flipVertical;
            
            //Equipment
            var neweq = @new as Equipment;
            var oldeq = old as Equipment;
            if (neweq != null && oldeq != null)
            {
                neweq.netEquippedDuck = oldeq.netEquippedDuck;
            }

            //Transfer weapon properties
            var newgun = @new as Gun;
            var oldgun = old as Gun;
            if (newgun != null && oldgun != null && keepAmmo)
            {
                newgun.ammo = oldgun.ammo;
            }

            //Remove that old shit
            MserGlobals.RemoveThingStealthily(old);
        }
    }
}

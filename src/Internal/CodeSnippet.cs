﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security;
using System.Text;
using System.Threading;
using Microsoft.CSharp;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Represents a code snippet that can execute code, created from the /execute command.
    /// </summary>
    [Serializable]
    public class CodeSnippet
    {
        /// <summary>
        /// Creates a new code snippet with the specified code.
        /// </summary>
        /// <param name="code"></param>
        /// <param name="sender"></param>
        /// <param name="level"></param>
        public CodeSnippet(string code, Profile sender, Level level)
        {
            this._code = code;
            if (sender == null)
            {
                //Fallback sender index
                this._senderIndex = 255;
            }
            else
            {
                //Sender saved as index
                this._senderIndex = sender.networkIndex;
            }
            this.ReceivedLevel = level;
        }

        private readonly byte _senderIndex;

        /// <summary>
        /// The profile that sent this expression message the first time.
        /// </summary>
        public Profile Sender
        {
            get
            {
                if (this._senderIndex == 255)
                    return null;
                return MserGlobals.GetProfileFromIndex(this._senderIndex);
            }
        }

        private readonly string _code;

        /// <summary>
        /// The code that this snippet would execute.
        /// </summary>
        public string Code
        {
            get { return this._code; }
        }

        [SecurityCritical]
        private bool _permitted = false;

        /// <summary>
        /// Whether the user gave permission to run this snippet.
        /// </summary>
        public bool Permitted
        {
            get { return this._permitted; }
            [SecurityCritical]
            internal set { this._permitted = value; }
        }

        [NonSerialized]
        private PropertyInfo _propertyInfo;

        [NonSerialized]
        private bool _cachedExpression;

        /// <summary>
        /// Whether the snippet is cached.
        /// </summary>
        public bool Cached
        {
            get { return this._cachedExpression; }
        }

        /// <summary>
        /// Index of the snippet, in case it should have any. Used for multiple thingies in one assembly.
        /// </summary>
        [NonSerialized]
        private int _index = -1;

        /// <summary>
        /// The level that this code snippet was recieved on.
        /// </summary>
        [NonSerialized]
        public readonly Level ReceivedLevel;

        [NonSerialized]
        public bool _shouldExecute;

        /// <summary>
        /// Return value of the expression when executed.
        /// <para />This will execute the snippet on call, so be sure it has been permitted in advance!
        /// </summary>
        public object Expression
        {
            get
            {
                //Do compile/error check if not already done
                if (this._propertyInfo == null)
                {
                    this.Cache(true);
                    return null;
                }

                _shouldExecute = false;

                //Execute expression - do not call if not permitted!
                return this._propertyInfo.GetValue(null, null);
            }
        }

        /// <summary>
        /// Compiles the code snippet, caching its function.
        /// </summary>
        /// <returns></returns>
        public void Cache(bool shouldExecute)
        {
            //BUG: CodeSnippet - error-containing snippets still get sent to other clients -> dont show errors if not the sender

            //Don't cache if already cached and has expression function
            if (this.Cached && this._propertyInfo != null)
                return;

            this._shouldExecute = shouldExecute;
            this._cachedExpression = true;

            //Format code
            var classSource = string.Format(CodeSnippet.classTemplate, this.Code, GetSender(this));

            //Compile the assembly
            CodeSnippet.CompileAssembly(this, classSource);
        }

        /// <summary>
        /// General assembly usages and class header.
        /// </summary>
        private const string usagesTemplate = @"
using System;
using System.Collections.Generic;
using System.Linq;
using DuckGame;
using DuckGame.MserDuck;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Design;
public static class ExpressionCompiling {{";

        /// <summary>
        /// Code template for inserting the user-written code into. Can be compiled directly.
        /// 0 = code
        /// 1 = sender
        /// </summary>
        private const string classTemplate = usagesTemplate + @"
    public static object Function
    {{
        get
        {{
            var Sender = {1};
            {0};
        }}
    }}
}}
";

        /// <summary>
        /// Code template for a function defined with an index.
        /// 0 = code
        /// 1 = index
        /// 2 = sender
        /// </summary>
        private const string partialClassTemplate = @"
public static object Function{1}
{{
    get
    {{
        var Sender = {2};
        {0};
    }}
}}
";

        /// <summary>
        /// Returns the expression's propertyinfo, given the assembly it was compiled in.
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns></returns>
        private static PropertyInfo GetPropertyFromAssembly(Assembly assembly)
        {
            return GetPropertyFromAssembly(assembly, string.Empty);
        }

        /// <summary>
        /// Returns the expression's propertyinfo, given the assembly it was compiled in and the function index.
        /// </summary>
        /// <param name="assembly"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        private static PropertyInfo GetPropertyFromAssembly(Assembly assembly, string index)
        {
            var type = assembly.GetTypes().Single();
            var property = type.GetProperty("Function" + index);
            return property;
        }

        /// <summary>
        /// Compiles the specified lines of code into an assembly without blocking the main thread.
        /// </summary>
        /// <param name="source"></param>
        private static void CompileAssembly(CodeSnippet snip, params string[] source)
        {
            var compilin = new Thread(() => InternalCompileAssembly(snip, source));
            compilin.Start();
        }

        /// <summary>
        /// Compiles the specified lines of code into an assembly.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private static void InternalCompileAssembly(CodeSnippet snip, params string[] source)
        {
            var compilerParameters = new CompilerParameters
            {
                GenerateExecutable = false,
                GenerateInMemory = true,
            };
            compilerParameters.ReferencedAssemblies.AddRange(CodeSnippet.Assemblies.ToArray());
            var compileProvider = new CSharpCodeProvider();
            var results = compileProvider.CompileAssemblyFromSource(compilerParameters, source);
            var errors = new List<string>();
            if (results.Errors.HasErrors)
            {
                errors.Add(string.Format("{0} during expression compilation:", Extensions.GetPlural(results.Errors.Count, "error")));
                foreach (CompilerError error in results.Errors)
                {
                    errors.Add(error.ErrorText);
                }
            }

            Assembly assembly = null;
            if (!errors.Any())
                assembly = results.CompiledAssembly;

            if (snip != null)
            {
                CompiledList.TryAdd(snip, new CompileResult(assembly, errors));
            }
            else
            {
                //Global compile result!
                var snips = CacheManager.KnownCodeSnippets.Where(s => s.Permitted);
                foreach (var sn in snips)
                {
                    CompiledList.TryAdd(sn, new CompileResult(assembly, errors));
                }
            }
        }

        /// <summary>
        /// List of assemblies to be loaded by the compiler for the expression's assembly.
        /// </summary>
        private static IEnumerable<string> Assemblies
        {
            get
            {
                if (CodeSnippet._assemblies == null)
                {
                    //Includes base .NET framework libraries, DuckGame's, MserDuck's and XNA's
                    CodeSnippet._assemblies = new List<string>();
                    CodeSnippet._assemblies.Add("System.dll");
                    CodeSnippet._assemblies.Add("System.Core.dll");
                    CodeSnippet._assemblies.Add("Steam.dll");
                    CodeSnippet._assemblies.Add("DuckGame.exe");
                    CodeSnippet._assemblies.Add(MserGlobals.GetModDir() + "\\MserDuck_compiled.dll");
                    CodeSnippet._assemblies.Add(Assembly.GetAssembly(typeof(Microsoft.Xna.Framework.Rectangle)).Location);
                    CodeSnippet._assemblies.Add(Assembly.GetAssembly(typeof(Microsoft.Xna.Framework.Graphics.Effect)).Location);
                    CodeSnippet._assemblies.Add(Assembly.GetAssembly(typeof(Microsoft.Xna.Framework.Game)).Location);
                }
                return CodeSnippet._assemblies;
            }
        }

        /// <summary>
        /// Assemblies used to be referenced when compiling.
        /// </summary>
        private static List<string> _assemblies;

        /// <summary>
        /// File to load/save code snippets.
        /// </summary>
        internal const string SnippetFile = "snippets.dat";

        /// <summary>
        /// List of pending code snippets.
        /// </summary>
        internal static readonly Queue<CodeSnippet> _snippetQueue = new Queue<CodeSnippet>();

        /// <summary>
        /// Currently pending code snippet, which it accepted/rejected with /e commands.
        /// </summary>
        internal static CodeSnippet _pendingSnippet = null;

        /// <summary>
        /// Gets or sets the auto-accept state of expressions. Set using <see cref="ConfigEntries.AutoAcceptEntry"/> in config.
        /// <para />Do not allow this to be changed with use of reflection!
        /// </summary>
        [SecurityCritical]
        internal static bool _autoAccept;

        /// <summary>
        /// List of code snippets with assigned compile results that should get handled on every Update.
        /// </summary>
        private static ConcurrentDictionary<CodeSnippet, CompileResult> CompiledList = new ConcurrentDictionary<CodeSnippet, CompileResult>();

        /// <summary>
        /// Gets or sets the auto-accept state of expressions. Set using <see cref="ConfigEntries.AutoAcceptEntry"/> in config.
        /// <para />Do not allow this to be changed with use of reflection!
        /// </summary>
        internal static bool AutoAccept
        {
            get { return CodeSnippet._autoAccept; }
            [SecurityCritical]
            private set { CodeSnippet._autoAccept = value; }
        }

        /// <summary>
        /// Builds a cache for all known snippets that are permitted.
        /// </summary>
        internal static void CacheKnownSnippets()
        {
            //Cache all known snippets into a new, single assembly
            if (CacheManager.KnownCodeSnippets == null || CacheManager.KnownCodeSnippets.Count == 0) return;
            
            //Sender string in code - the stupid substring is needed for reasons of {{ } {}}{ { }
            var codebuilder = new StringBuilder(CodeSnippet.usagesTemplate.Substring(0, CodeSnippet.usagesTemplate.Length - 1));
            var i = 0;
            foreach (var snip in CacheManager.KnownCodeSnippets.Where(s => s.Permitted))
            {
                snip._cachedExpression = true;

                //Format code
                codebuilder.AppendLine(string.Format(CodeSnippet.partialClassTemplate, snip.Code, i, GetSender(snip)));
                snip._index = i;
                i++;
            }
            codebuilder.AppendLine("}");

            //Compile the assembly
            CodeSnippet.CompileAssembly(null, codebuilder.ToString());
        }

        /// <summary>
        /// Returns the sender code string to use for inserting into code snippets, from the specified code snippet.
        /// </summary>
        /// <param name="snip"></param>
        /// <returns></returns>
        private static string GetSender(CodeSnippet snip)
        {
            //BUG: CodeSnippet - while the sender's index stays the same in the session, between sessions, it can definitely change
            //  As of such, try sending some form of unique identifier that stays the same. IP Address does not work, but maybe
            //  Steam includes a form of "user ID" which is not modifiable, unlike the "profile ID" included in the header.
            //  -> use Duck.profile.steamID for this (see if it actually is the steam64 Id that stays constant!)

            if (snip._senderIndex == 255)
            {
                //Fallback in case sender got lost
                return "MserGlobals.GetLocalDuck()";
            }
            else
            {
                //Sender from duck index
                return string.Format("MserGlobals.GetDuckFromIndex({0})", snip._senderIndex);
            }
        }

        /// <summary>
        /// Loads all known code snippets from file if none are currently loaded. Returns whether the list had to be populated.
        /// </summary>
        /// <returns></returns>
        internal static void LoadSnippetsIfNeeded()
        {
            if (CacheManager.KnownCodeSnippets != null) return;

            if (!File.Exists(CodeSnippet.SnippetFile))
            {
                CacheManager.KnownCodeSnippets = new List<CodeSnippet>();
                CodeSnippet.SaveSnippets();
                return;
            }

            using (Stream stream = File.Open(CodeSnippet.SnippetFile, FileMode.Open, FileAccess.Read))
            {
                try
                {
                    var bformatter = new BinaryFormatter();
                    CacheManager.KnownCodeSnippets = (List<CodeSnippet>)bformatter.Deserialize(stream);
                }
                catch (Exception e)
                {
                    Program.LogLine("Error deserializing the snippets file. Removing it for now, sorry! " + e.Message + e.StackTrace);
                    File.Delete(CodeSnippet.SnippetFile);
                }
            }
        }

        /// <summary>
        /// Saves all known code snippets to file.
        /// </summary>
        internal static void SaveSnippets()
        {
            using (Stream stream = File.Open(CodeSnippet.SnippetFile, FileMode.Create, FileAccess.Write))
            {
                var bformatter = new BinaryFormatter();
                bformatter.Serialize(stream, CacheManager.KnownCodeSnippets);
            }
        }

        /// <summary>
        /// Builds cache from known snippets loaded from the snippets file.
        /// </summary>
        internal static void Initialize()
        {
            CodeSnippet.LoadSnippetsIfNeeded();
            CodeSnippet.CacheKnownSnippets();
        }

        internal static void Update()
        {
            //MserDebug.PrintEntry("CompiledSnippets", CodeSnippet.CompiledList);
            //MserDebug.PrintEntry("KnownSnippets", CacheManager.KnownCodeSnippets);

            var removes = new List<CodeSnippet>();
            foreach (var pair in CodeSnippet.CompiledList)
            {
                //Check for errors
                var snip = pair.Key;
                var cr = pair.Value;

                //Still compiling
                if (cr == null)
                    continue;

                if (!cr.Errors.Any())
                {
                    //SUCCESS
                    if (snip._index < 0)
                    {
                        snip._propertyInfo = CodeSnippet.GetPropertyFromAssembly(cr.Assembly);
                    }
                    else
                    {
                        snip._propertyInfo = CodeSnippet.GetPropertyFromAssembly(cr.Assembly, snip._index.ToString());
                    }
                    //var ps = cr.Assembly.GetType().GetProperties().Length;
                    //MserDebug.PrintEntry("Correct compile: propcnt?" + ps + " index?" + snip._index + " and prop null?" + (snip._propertyInfo == null) + " and ass null?" + (cr.Assembly == null) + " all for " + snip.Code.Truncate(30));

                    //exec??
                    if (snip._shouldExecute)
                    {
                        CodeSnippet.ExecuteCodeSnippet(snip);
                    }
                }
                else
                {
                    //FUCKIES, ERRORIN
                    foreach (var ln in cr.Errors)
                    {
                        ChatManager.Echo(ln, 253);
                    }
                    CacheManager.KnownCodeSnippets.Remove(snip);
                }

                removes.Add(snip);
            }

            foreach (var rem in removes)
            {
                CompileResult res;
                CodeSnippet.CompiledList.TryRemove(rem, out res);
            }
        }

        /// <summary>
        /// Returns the known snippet with the specified code string. Use to get cached expression function from known snippets.
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        internal static CodeSnippet GetSnippetFromCode(string code)
        {
            return CacheManager.KnownCodeSnippets.FirstOrDefault(sn => sn.Code == code);
        }

        /// <summary>
        /// Returns whether the specified code snippet is a permitted/valid one. Asks user if unknown snippet.
        /// </summary>
        /// <returns></returns>
        internal static bool ValidCodeSnippet(Profile sender, CodeSnippet code)
        {
            //Build known list if needed
            CodeSnippet.LoadSnippetsIfNeeded();

            //Check if in list and marked as valid
            if (CacheManager.KnownCodeSnippets.Contains(code))
                return code.Permitted;

            if (sender.localPlayer || CodeSnippet.AutoAccept)
            {
                //Should be known by local player, and obviously valid for yourself OR using autoaccept mode (NOT RECOMMENDED)
                code.Permitted = true;
                CacheManager.KnownCodeSnippets.Add(code);
                return true;
            }

            //Ask remote user about permission
            CodeSnippet.EnqueueNewSnippet(sender, code);
            return false;
        }

        /// <summary>
        /// Adds the specified code snippet to the queue if it isn't already inside.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="code"></param>
        internal static void EnqueueNewSnippet(Profile sender, CodeSnippet code)
        {
            //Don't enqueue if snippet with same code is already in queue
            if (CodeSnippet._snippetQueue.Any(entry => entry.Code == code.Code))
                return;

            if (CodeSnippet._pendingSnippet != null && CodeSnippet._snippetQueue.Count > 0)
            {
                //Warn about more entries in queue
                ChatManager.Echo(string.Format("You have {0} pending code snippets! Use /e y or /e n - check ducklog for their code.", CodeSnippet._snippetQueue.Count + 1));
            }

            //Add to queue and move through if not already something pending
            CodeSnippet._snippetQueue.Enqueue(code);
            CodeSnippet.MoveThroughQueue();
        }

        /// <summary>
        /// Goes to the next entry in the snippet queue if no snippet is already pending.
        /// </summary>
        internal static void MoveThroughQueue()
        {
            //Only get next if no snippet pending
            if (CodeSnippet._pendingSnippet == null && CodeSnippet._snippetQueue.Count > 0)
            {
                //Check next snippet
                var snip = CodeSnippet._snippetQueue.Dequeue();
                CodeSnippet._pendingSnippet = snip;
                CodeSnippet.NewSnippetPrompt(snip);
            }
        }

        /// <summary>
        /// Prompts the user for action to take for the specified snippet.
        /// </summary>
        /// <param name="snippet"></param>
        internal static void NewSnippetPrompt(CodeSnippet snippet)
        {
            //HINT: REMINDER CodeSnippet - once messages wrap, this code can be tossed into the bin (luckily)
            var split = snippet.Code.Replace("\n", " ").SplitInParts(120).ToList();
            var extra = string.Empty;

            ChatManager.Echo(string.Format("Do you wish for the following code snippet by {0} to be executed?",
                                           snippet.Sender.name));
            for (var i = 0; i < Math.Min(ChatManager.Entries, split.Count); i++)
            {
                ChatManager.Echo(split[i]);
            }
            if (split.Count > ChatManager.Entries - 1)
            {
                extra = string.Format("({0} line(s) omitted - check ducklog!) ", split.Count - ChatManager.Entries - 1);
            }
            ChatManager.Echo(extra + "Accept with /e y or reject with /e n");

            Program.LogLine(string.Format("--- Snippet Start {0} by {1} ---", DateTime.Now.ToShortDateString(),
                                          snippet.Sender.name));
            Program.LogLine(snippet.Code);
            Program.LogLine("--- Snippet End ---");
        }

        /// <summary>
        /// Returns function's return value, if any (null if invalid snippet or no return value). No permission checks are done for execution!
        /// </summary>
        /// <returns></returns>
        internal static object ExecuteCodeSnippet(CodeSnippet snippet)
        {
            //Nullcheck
            if (snippet == null)
                return null;

            //Execution!
            //BUG: CodeSnippet - echo does not seemingly show for other clients
            var ret = snippet.Expression;
            if (ret != null)
                ChatManager.Echo("Function returned: " + ret);
            return ret;
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            return string.Format("{{ {0}{1}{2}{3} - {4} }}", Extensions.ToNeatString(this.Sender),
                this.Permitted ? ", permitted" : string.Empty,
                this.Cached ? ", cached" : string.Empty,
                this._shouldExecute ? ", gonna execute" : string.Empty,
                this.Code.Truncate(50, true));
        }

        private class CompileResult
        {
            public IEnumerable<string> Errors;

            public Assembly Assembly;

            public CompileResult(Assembly ass, IEnumerable<string> errors)
            {
                this.Assembly = ass;
                this.Errors = errors;
            }
        }
    }
}

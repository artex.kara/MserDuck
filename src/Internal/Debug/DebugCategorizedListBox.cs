﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// A scrollable list box which allows for entries to be sorted into different categories.
    /// <para />Entries inside of the categories are sorted alphabetically on their own, unless otherwise specified.
    /// </summary>
    public class DebugCategorizedListBox : DebugListBox
    {
        /// <summary>
        /// A function which assigns a category to the specified DebugListEntry.
        /// <para />Return null to assign the specified entry to the DefaultCategory.
        /// </summary>
        public Func<DebugListEntry, DebugListCategory> CategorySelector;

        /// <summary>
        /// List of currently displayed categories.
        /// </summary>
        public List<DebugListCategory> Categories = new List<DebugListCategory>();

        /// <summary>
        /// A template for the category's DebugListEntry. This is cloned to create the category list entries.
        /// <para />TextFunction and Tag get overridden, regardless of their values in the template.
        /// </summary>
        public DebugListEntry TemplateCategory;

        /// <summary>
        /// Whether opening a category using click closes any other currently open category.
        /// </summary>
        public bool AutoCloseCategories = false;

        /// <summary>
        /// How entries should be sorted. Leave unset to use the display text, ordered ascending.
        /// </summary>
        public Func<DebugListEntry, object> SortFunction; 

        /// <summary>
        /// The default category to assign to entries that have an invalid category (such as "null").
        /// </summary>
        public DebugListCategory DefaultCategory = new DebugListCategory("Unknown", -999);

        /// <summary>
        /// Creates a scrollable list box which allows for entries to be sorted into different categories.
        /// <para />Entries inside of the categories are sorted alphabetically on their own, unless otherwise specified.
        /// </summary>
        /// <param name="width">Width of the ListBox.</param>
        /// <param name="categorySelector">A function which assigns a category to the specified DebugListEntry.</param>
        /// <param name="backcolor"></param>
        /// <param name="alpha"></param>
        /// <param name="maxlines"></param>
        public DebugCategorizedListBox(float width, Func<DebugListEntry, DebugListCategory> categorySelector, Color backcolor, float alpha, int maxlines = -1)
            : base(width, backcolor, alpha, maxlines)
        {
            this.CategorySelector = categorySelector;
        }

        /// <summary>
        /// Returns index of clicked entry in list.
        /// <para />Returns -1 if none is clicked, or -2 if a button was clicked instead.
        /// </summary>
        /// <returns></returns>
        public override object DoUpdate()
        {
            var clicked = (int)base.DoUpdate();

            if (clicked >= 0)
            {
                //Clicked entry, determine type
                var entry = this.Entries[clicked];
                var cat = entry.Tag as DebugListCategory;
                if (cat != null)
                {
                    //Clicked on a category
                    if (cat.ClickToCollapse)
                    {
                        //Collapse/Show children
                        cat.ShowingChildren = !cat.ShowingChildren;

                        if (this.AutoCloseCategories)
                        {
                            //Autoclose the others
                            foreach (var c in this.Categories.Where(c => c != cat))
                            {
                                c.ShowingChildren = false;
                            }
                        }

                        this.UpdateVisibility();
                    }
                }
            }

            return clicked;
        }

        /// <summary>
        /// Sorts the DebugCategorizedListBox first by name and then by the priority of the categories.
        /// </summary>
        public void SortList()
        {
            //Order stuff - first by category priority (higher first), then by category name, then by entry name (or modded), then to get categories on top of those blocks
            this.Categories = this.Categories
                .OrderByDescending(c => c.Priority).ThenBy(c => c.Name)
                .ToList();
            this.Entries = this.Entries
                .OrderByDescending(e => this.ExtendedSelector(e).Priority)
                .ThenBy(e => this.ExtendedSelector(e).Name)
                .ThenBy(this.SortFun)
                .ThenByDescending(e => e.Tag is DebugListCategory)
                .ToList();

            if (this.AutoCloseCategories && this.Entries.Count(e => e.Tag is DebugListCategory && ((DebugListCategory)e.Tag).ShowingChildren) > 1)
            {
                //Too many open categories, close em all!
                foreach (var cat in this.Categories)
                {
                    cat.ShowingChildren = false;
                }
            }

            //Modify entries
            this.UpdateVisibility();
        }

        private void UpdateVisibility()
        {
            foreach (var entry in this.Entries)
            {
                if (entry.Tag is DebugListCategory)
                    continue;

                //Hide entry based of category being collapsed
                entry.Visible = this.GetInstancedCategory(this.CategorySelector(entry)).ShowingChildren;
            }
        }

        private string SortFun(DebugListEntry entry)
        {
            if (entry.Tag is DebugListCategory)
                return "";

            if (this.SortFunction == null)
                return entry.Text;

            var sort = this.SortFunction(entry);
            if (sort == null)
                return entry.Text;

            return sort.ToString();
        }

        /// <summary>
        /// Returns own category if tag is category. Otherwise uses selector like normal.
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        private DebugListCategory ExtendedSelector(DebugListEntry entry)
        {
            if (entry == null)
                return this.DefaultCategory;

            //Is a category by itself
            var cat = entry.Tag as DebugListCategory;
            if (cat != null)
                return cat;

            //Selector of category
            var sel = this.CategorySelector(entry);
            if (sel == null)
                return this.DefaultCategory;
            return sel;
        }

        /// <summary>
        /// Adds a new DebugListEntry at the specified index.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="line"></param>
        public override void InsertEntry(int index, DebugListEntry line)
        {
            //Add new category, if not in list
            this.InsertEntryInternal(index, line);

            base.InsertEntry(index + 1, line);
            this.SortList();
        }

        private void InsertEntryInternal(int index, DebugListEntry line)
        {
            var cat = this.CategorySelector(line);
            if (this.Categories.All(c => c != cat))
            {
                var catent = this.TemplateCategory.Clone();
                catent.Tag = cat;
                catent.TextFunction = () => (cat.ShowingChildren ? "v " : "> ") + cat.Name;
                this.Entries.Insert(index, catent);

                cat.ListEntry = catent;
                this.Categories.Add(cat);
            }
        }

        /// <summary>
        /// Removes the DebugListEntry at the specified index.
        /// </summary>
        /// <param name="index"></param>
        public override void RemoveEntryAt(int index)
        {
            //TODO: DebugCategorizedListBox - if user removes a category, remove the entries assigned to said category as well

            //Remove category if last entry that had that category
            var cat = this.ExtendedSelector(this.Entries[index]);
            if (this.Entries.Count(e => this.CategorySelector(e) == cat) <= 1)
            {
                this.Categories.Remove(cat);
                this.Entries.Remove(this.GetCategoryEntry(cat));
            }

            base.RemoveEntryAt(index);
        }

        /// <summary>
        /// Removes all DebugListEntries.
        /// </summary>
        public override void ClearEntries()
        {
            this.Categories.Clear();
            base.ClearEntries();
        }

        /// <summary>
        /// Adds multiple entries to the bottom of the list.
        /// </summary>
        /// <param name="entries"></param>
        public override void AddEntries(IEnumerable<DebugListEntry> entries)
        {
            foreach (var e in entries)
            {
                InsertEntryInternal(this.Entries.Count, e);
                this.Entries.Add(e);
            }

            this.SortList();
        }

        /// <summary>
        /// Returns the category reference from this.Categories, using equality checks of the specified category.
        /// </summary>
        /// <param name="cat"></param>
        /// <returns></returns>
        private DebugListCategory GetInstancedCategory(DebugListCategory cat)
        {
            if (cat == null)
                return this.DefaultCategory;

            return this.Categories.FirstOrDefault(c => c == cat);
        }

        private DebugListEntry GetCategoryEntry(DebugListCategory cat)
        {
            if (cat == null)
                return this.GetCategoryEntry(this.DefaultCategory.Name);

            return this.GetCategoryEntry(cat.Name);
        }

        private DebugListEntry GetCategoryEntry(string cat)
        {
            if (string.IsNullOrWhiteSpace(cat))
                cat = this.DefaultCategory.Name;

            return this.Categories.First(c => c.Name == cat).ListEntry;
        }
    }
}

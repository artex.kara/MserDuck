﻿using System;
using System.Collections.Generic;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// A generic debug box, showing any kind of information
    /// </summary>
    public class DebugBox : DebugUIControl
    {
        /// <summary>
        /// Whether the dialog box is "minimized".
        /// </summary>
        protected bool Folded;

        /// <summary>
        /// Current size of the dialog box. If folded, works with the original size.
        /// </summary>
        public Vec2 Size
        {
            get
            {
                if (this.Folded)
                    return new Vec2(this._size.x, this._unfoldHeight);
                return this._size;
            }
            set
            {
                this._size.x = value.x;
                if (this.Folded)
                {
                    this._size.y = 24;
                    this._unfoldHeight = value.y;
                }
                else
                {
                    this._size.y = value.y;
                }
            }
        }

        /// <summary>
        /// The current size of the dialog box, as it is being displayed.
        /// </summary>
        public Vec2 _size = new Vec2(165, 70);

        /// <summary>
        /// Current width of the dialog box.
        /// </summary>
        protected float Width
        {
            get { return this._size.x; }
            set { this._size.x = value; }
        }

        /// <summary>
        /// Current height of the dialog box. If folded, works with the original height.
        /// </summary>
        protected float Height
        {
            get
            {
                if (this.Folded)
                    return this._unfoldHeight;
                return this._size.y;
            }
            set
            {
                if (this.Folded)
                {
                    this._size.y = 24;
                    this._unfoldHeight = value;
                }
                else
                {
                    this._size.y = value;
                }
            }
        }

        /// <summary>
        /// Minimum size for the window.
        /// </summary>
        protected Vec2 MinimumSize = new Vec2(165, 54);

        /// <summary>
        /// Maximum size for the window. If max &lt; min, no maximum size is used (default).
        /// </summary>
        protected Vec2 MaximumSize = Vec2.Zero;

        private Vec2 _initPos;
        private Vec2 _initSize;

        /// <summary>
        /// Height to reset window to when unfolded.
        /// </summary>
        protected float _unfoldHeight = 54f;

        /// <summary>
        /// Whether the user is moving the dialog box around.
        /// </summary>
        protected bool Moving;
        
        private readonly DebugBoxButton _closeButton = new DebugBoxButton(IconManager.ButtonIcons.CloneMap(), 9, null);
        private readonly DebugBoxButton _foldButton = new DebugBoxButton(IconManager.ButtonIcons.CloneMap(), 10, null);

        /// <summary>
        /// Buttons to show in the titlebar.
        /// </summary>
        protected readonly List<DebugBoxButton> _buttons = new List<DebugBoxButton>();

        /// <summary>
        /// Whether the close button should be visible.
        /// </summary>
        protected bool _hasClose = true;

        /// <summary>
        /// Whether the fold/minimize button should be visible.
        /// </summary>
        protected bool _hasFold = true;

        private string _title = "";

        /// <summary>
        /// Display title text of the debugbox.
        /// </summary>
        public string Title
        {
            get { return this._title; }
            set
            {
                if (this._title == value)
                    return;

                this._title = value;

                this.UpdateSize();
            }
        }

        /// <summary>
        /// Whether this dialog box is resizable by dragging the borders.
        /// </summary>
        protected bool Resizable = true;

        /// <summary>
        /// Whether the user is resizing the dialog box.
        /// </summary>
        protected ResizeMode Resizing = ResizeMode.None;

        /// <summary>
        /// Whether this debug box is visible.
        /// </summary>
        public bool _visible = true;

        /// <summary>
        /// Whether this debug box is visible. Setter has sound playing on state change, use _visible if not intended.
        /// </summary>
        public override bool Visible
        {
            get { return this._visible; }
            set
            {
                if (this._visible == value) return;
                this._visible = value;

                if (value)
                    SFX.Play("consoleSelect");
                else
                    SFX.Play("consoleCancel");
            }
        }

        /// <summary>
        /// Whether the debug box should turn invisible instead of closing when the close button is pressed.
        /// </summary>
        protected bool InvisOnClose = false;

        /// <summary>
        /// Whether the user can move this dialog box.
        /// </summary>
        protected bool Moveable = true;

        /// <summary>
        /// Whether the location and size of this debugbox, as well as whether it is folded, should be stored on quit and reloaded as a default when created.
        /// <para />This only has any effect of debugboxes which get created together with the DebugUI.
        /// </summary>
        public virtual bool StoreGeometry
        {
            get { return true; }
        }

        /// <summary>
        /// How much the resize border should extend into each direction.
        /// <para />True resize border is this amount *2.
        /// </summary>
        public const int ResizeBorderSize = 8;

        public DebugBox()
        {
            //Close button
            this._closeButton.OnClick = () =>
            {
                if (!this.InvisOnClose)
                    DebugUI.RemoveDebugBox(this);
                else
                    this.Visible = false;
            };

            //Fold button
            this._foldButton.OnClick = this.ToggleFold;

            this._buttons.Add(this._closeButton);
            this._buttons.Add(this._foldButton);

            this.UpdateSize();
        }

        public DebugBox(int index) : this()
        {
            this.position = new Vec2(24 * (index + 1));
            this.depth = 1 - (1f / (index / 3f + 1));
        }

        /// <summary>
        /// Returns whether any button was pressed
        /// </summary>
        /// <returns></returns>
        public override object DoUpdate()
        {
            DebugUI._resizeHandle = ResizeMode.None;

            var clicked = false;
            var i = 0;
            foreach (var button in this._buttons)
            {
                if (button == this._closeButton && !this._hasClose)
                    continue;
                if (button == this._foldButton && !this._hasFold)
                    continue;

                button.position = this.position + new Vec2(this.Width - 24 - 26 * i, 2);
                i++;
                var res = (bool)button.DoUpdate();
                if (res)
                    clicked = true;
            }

            //Window dragging
            var moved = false;
            if (!clicked && this.Visible)
            {
                //Movement checking
                if (this.Moveable && DebugUI._resizeHandle == ResizeMode.None && Mouse.left != InputState.None &&
                    DebugUI._mousePos.IsInRange(this.position, this.position + new Vec2(this.Width, 24)))
                {
                    if (Mouse.left == InputState.Pressed)
                    {
                        //Start movement and save pos
                        this.Moving = true;
                        this._initPos = DebugUI._mousePos - this.position;
                        moved = true;
                    }
                    else if (Mouse.left == InputState.Released)
                    {
                        //Stop dragging
                        this.Moving = false;
                        this._initPos = Vec2.Zero;
                        moved = true;
                    }
                }

                //TODO: DebugBox - set resize priority to be above move priority!

                if (this.Moving)
                {
                    //MOVE
                    //TODO: DebugBox - disallow movement outside of visible range
                    this.position = DebugUI._mousePos - this._initPos;
                }
                else
                {
                    //Resize checking
                    //BUG: DebugBox not always showing resize cursor despite being able to resize it
                    if (this.Resizable)
                    {
                        //Resize cursor
                        if (this.Resizing == ResizeMode.None)
                        { 
                            if (!this.Folded && DebugUI._mousePos.IsInRange(new Rectangle(this.x - ResizeBorderSize, this.y - ResizeBorderSize, ResizeBorderSize * 2, ResizeBorderSize * 2)))
                                DebugUI._resizeHandle = ResizeMode.TopLeft;
                            else if (!this.Folded && DebugUI._mousePos.IsInRange(new Rectangle(this.x + this.Width - ResizeBorderSize, this.y - ResizeBorderSize, ResizeBorderSize * 2, ResizeBorderSize * 2)))
                                DebugUI._resizeHandle = ResizeMode.TopRight;
                            else if (!this.Folded && DebugUI._mousePos.IsInRange(new Rectangle(this.x - ResizeBorderSize, this.y + this.Height - ResizeBorderSize, ResizeBorderSize * 2, ResizeBorderSize * 2)))
                                DebugUI._resizeHandle = ResizeMode.BottomLeft;
                            else if (!this.Folded && DebugUI._mousePos.IsInRange(new Rectangle(this.x + this.Width - ResizeBorderSize, this.y + this.Height - ResizeBorderSize, ResizeBorderSize * 2, ResizeBorderSize * 2)))
                                DebugUI._resizeHandle = ResizeMode.BottomRight;
                            else if (!this.Folded && DebugUI._mousePos.IsInRange(new Rectangle(this.x + ResizeBorderSize, this.y - ResizeBorderSize, this.Width - ResizeBorderSize, ResizeBorderSize * 2)))
                                DebugUI._resizeHandle = ResizeMode.Top;
                            else if (DebugUI._mousePos.IsInRange(new Rectangle(this.x + this.Width - ResizeBorderSize, this.y + ResizeBorderSize, ResizeBorderSize * 2, this.Height - ResizeBorderSize)))
                                DebugUI._resizeHandle = ResizeMode.Right;
                            else if (!this.Folded && DebugUI._mousePos.IsInRange(new Rectangle(this.x + ResizeBorderSize, this.y + this.Height - ResizeBorderSize, this.Width - ResizeBorderSize, ResizeBorderSize * 2)))
                                DebugUI._resizeHandle = ResizeMode.Bottom;
                            else if (DebugUI._mousePos.IsInRange(new Rectangle(this.x - ResizeBorderSize, this.y + ResizeBorderSize, ResizeBorderSize * 2, this.Height - ResizeBorderSize)))
                                DebugUI._resizeHandle = ResizeMode.Left;
                        }
                        else
                        {
                            //DebugBox resize/move according to mode
                            var posdiff = (DebugUI._mousePos - this._initPos);

                            var lastpos = this.position;

                            switch (this.Resizing)
                            {
                                case ResizeMode.TopLeft:
                                    this.position = DebugUI._mousePos;
                                    this.Size = this._initSize - posdiff;
                                    break;
                                case ResizeMode.Top:
                                    this.y = DebugUI._mousePos.y;
                                    this.Height = this._initSize.y - posdiff.y;
                                    break;
                                case ResizeMode.TopRight:
                                    this.Width = posdiff.x + this._initSize.x;
                                    this.y = DebugUI._mousePos.y;
                                    this.Height = this._initSize.y - posdiff.y;
                                    break;
                                case ResizeMode.Right:
                                    this.Width = posdiff.x + this._initSize.x;
                                    break;
                                case ResizeMode.BottomRight:
                                    this.Size = posdiff + this._initSize;
                                    break;
                                case ResizeMode.Bottom:
                                    this.Height = posdiff.y + this._initSize.y;
                                    break;
                                case ResizeMode.BottomLeft:
                                    this.x = DebugUI._mousePos.x;
                                    this.Width = this._initSize.x - posdiff.x;
                                    this.Height = posdiff.y + this._initSize.y;
                                    break;
                                case ResizeMode.Left:
                                    this.x = DebugUI._mousePos.x;
                                    this.Width = this._initSize.x - posdiff.x;
                                    break;
                                default:
                                    throw new ArgumentOutOfRangeException();
                            }

                            //Clamp window size
                            this.ClampSize(lastpos);
                        }

                        //Resize clicks
                        if (Mouse.left == InputState.Pressed)
                        {
                            //Do resize
                            moved = true;
                            this._initPos = DebugUI._mousePos;
                            this._initSize = this.Size;
                            this.Resizing = DebugUI._resizeHandle;
                        }
                        else if (Mouse.left != InputState.Down)
                        {
                            //Stoppo!
                            if (Mouse.left == InputState.Released)
                                moved = true;
                            this.Resizing = ResizeMode.None;
                            this._initSize = Vec2.Zero;
                            this._initPos = Vec2.Zero;
                        }
                    }
                }
            }

            return clicked || moved;
        }

        private void ClampSize()
        {
            this.ClampSize(this.position);
        }

        private void ClampSize(Vec2 lastpos)
        {
            if (!this.Resizable)
                return;

            if (this.Width < this.MinimumSize.x)
            {
                this.Width = this.MinimumSize.x;
                this.position.x = lastpos.x;
            }
            if (this.Height < this.MinimumSize.y)
            {
                this.Height = this.MinimumSize.y;
                this.position.y = lastpos.y;
            }
            if ((this.MaximumSize.x >= this.MinimumSize.x) && (this.Width > this.MaximumSize.x))
            {
                this.Width = this.MaximumSize.x;
                this.position.x = lastpos.x;
            }
            if ((this.MaximumSize.y >= this.MinimumSize.y) && (this.Height > this.MaximumSize.y))
            {
                this.Height = this.MaximumSize.y;
                this.position.y = lastpos.y;
            }
        }

        protected void ToggleFold()
        {
            this.SetFolded(!this.Folded);
        }

        protected void SetFolded(bool folded)
        {
            this.Folded = folded;
            if (folded)
            {
                this._unfoldHeight = this._size.y;
                this._foldButton.Frame = 11;
            }
            else
            {
                this._size.y = this._unfoldHeight;
                this._foldButton.Frame = 10;
            }
            this.UpdateSize();
        }

        protected void UpdateSize()
        {
            var newsize = Math.Max(this.Width, FontManager.Chat.GetWidth(this.Title) + this._buttons.Count * 26 + 16);
            if (this.Folded)
            {
                //Set both x and y
                this.Width = newsize;
                this._size.y = 24;
            }
            else
            {
                //Only modify x
                this.Width = newsize;
            }

            this.ClampSize();
        }

        public override void DoDraw()
        {
            if (!this._visible)
                return;

            //Box title
            DebugUI.ShadowedText(this._title, this.x + 2, this.y + 2, this.depth, Color.White);

            //Rectangle background
            Graphics.DrawRect(this.position, this.position + this._size, DebugUI.DarkColor * 0.9f, this.depth.value - 0.01f);

            foreach (var button in this._buttons)
            {
                button.DoDraw();
            }
        }

        public DebugBoxGeometry GetGeometry()
        {
            return new DebugBoxGeometry(this.position, this.Size, this.Folded, this.Visible, this.GetType().Name);
        }

        public void ApplyGeometry(DebugBoxGeometry geo)
        {
            //BUG: DebugBox - ApplyGeometry's Height is always set to the MinimiumHeight -> clamping logic since it seems to be correctly (de/)serialized

            this.position = geo.Position;
            this._visible = geo.Visible;
            this.SetFolded(geo.Folded);
            this.Size = geo.Size;
        }
    }
}

﻿using System;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// A textbox for entering text into.
    /// </summary>
    public class DebugTextBox : DebugUIControl
    {
        public Vec2 Size;

        public float Width
        {
            get { return this.Size.x; }
            set { this.Size.x = value; }
        }

        public float Height
        {
            get { return this.MaxLines * FontManager.Chat.GetHeight() + BorderMargin * 2; }
            set { this.MaxLines = (int)(value / FontManager.Chat.GetHeight()); }
        }

        /// <summary>
        /// Distance from border to text content.
        /// </summary>
        public const int BorderMargin = 4;

        /// <summary>
        /// Max amount of lines to show in the text box. Set to -1 to limit to Config Entry setting.
        /// </summary>
        public int MaxLines
        {
            get
            {
                if (!this.Input.Multiline)
                    return 1;

                if (this._maxLines < 0)
                    return ChatManager.Entries;
                return _maxLines;
            }
            set { this._maxLines = value; }
        }

        private int _maxLines = -1;

        /// <summary>
        /// Index of top line.
        /// </summary>
        private int _topLine;

        /// <summary>
        /// Whether to show line numbers to the left of the textbox.
        /// </summary>
        public bool LineNumbers = false;

        /// <summary>
        /// Handler of all input for this textbox. Disabled until clicked.
        /// </summary>
        public TextInput Input = new TextInput();

        /// <summary>
        /// Unlike Input.Enabled (which is set based on focus), this is a global "can input to this textbox" field.
        /// </summary>
        public bool Enabled = true;

        /// <summary>
        /// Creates a new textbox with the specified properties.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="size"></param>
        /// <param name="multiline">Whether enter can be used to insert new lines of text.</param>
        public DebugTextBox(Vec2 position, Vec2 size, bool multiline = false)
        {
            this.position = position;
            this.Size = size;
            this.Input.Multiline = multiline;
            this.Input.Enabled = false;
        }

        public override void DoDraw()
        {
            //Draw BG box
            var al = this.Enabled ? 1f : 0.5f;
            Graphics.DrawRect(this.position, this.position + this.Size, Color.DarkGray * al, 0.91f);

            //Show text inside
            FontManager.Chat.Draw(this.Input.GetLines(this._topLine, this._topLine + this.MaxLines), this.position + new Vec2(BorderMargin), Color.White, 0.92f);

            if (this.LineNumbers)
            {
                //NYI: DebugTextBox - show line numbers
            }
        }

        /// <summary>
        /// Returns whether it gained focus from clicking onto it.
        /// </summary>
        /// <returns></returns>
        public override object DoUpdate()
        {
            var focus = false;
            if (this.Visible)
            {
                //Give focus
                if (Mouse.left == InputState.Pressed)
                {
                    //Based on where clicked
                    this.Input.Enabled = DebugUI._mousePos.IsInRange(this.position, this.position + this.Size);
                    focus = this.Input.Enabled;
                }

                if (!this.Enabled)
                    return focus;

                //Regular input update
                this.Input.Update();

                //NYI: DebugTextBox - scrolling for text contents
                //NYI: DebugTextBox - Set caret position based on where clicked
                //NYI: DebugTextBox - Modify selection based on where clicked/dragged
            }

            return focus;
        }
    }
}

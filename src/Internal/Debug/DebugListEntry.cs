﻿using System;
using System.Collections.Generic;

namespace DuckGame.MserDuck
{
    public class DebugListEntry : ICloneable<DebugListEntry>
    {
        /// <summary>
        /// Identifier for the list entry.
        /// </summary>
        public string Name;

        private string _text;

        /// <summary>
        /// Display text for the list entry.
        /// </summary>
        public string Text
        {
            get
            {
                if (this.TextFunction != null)
                {
                    var func = this.TextFunction.Invoke();
                    if (func == null)
                        return string.Empty;
                    return func;
                }

                if (this._text == null)
                    return string.Empty;
                return this._text;
            }
            set { this._text = value; }
        }

        /// <summary>
        /// Height of the list entry.
        /// </summary>
        public float Height
        {
            get
            {
                //Increase size for multiline text
                //BUG: DebugListEntry - Incorrect size, at least in debug values list
                if (this.Text.Contains("\n"))
                {
                    //return this._height + (ChatManager.ChatFont.GetHeight(this.Text) - ChatManager.ChatFont.GetHeight());
                }
                return this._height;
            }

            set
            {
                this._height = value;
            }
        }

        /// <summary>
        /// Comparer between list entry's text entries.
        /// </summary>
        public static readonly IComparer<DebugListEntry> TextComparer = new DebugListEntryComparer(true);

        /// <summary>
        /// Comparer between list entry's name entries.
        /// </summary>
        public static readonly IComparer<DebugListEntry> NameComparer = new DebugListEntryComparer(false);

        /// <summary>
        /// Height of the list entry.
        /// </summary>
        private float _height = 24f;

        /// <summary>
        /// Meta data to attach to the list entry.
        /// </summary>
        public object Tag;

        /// <summary>
        /// If specified, this.Text is overwritten and the display text is gotten from this function's return value instead.
        /// </summary>
        public Func<string> TextFunction;

        /// <summary>
        /// List of icons to display. Expected sprite size is 24x24.
        /// </summary>
        public readonly List<Sprite> Icons = new List<Sprite>();

        /// <summary>
        /// List of buttons to display. Expected sprite size is 24x24.
        /// </summary>
        public readonly List<DebugBoxButton> Buttons = new List<DebugBoxButton>();

        /// <summary>
        /// Text color of the list entry.
        /// </summary>
        public Color Color;

        /// <summary>
        /// String representing the display order of the list's elements, from left to right. Default: "ITB"
        /// <para />I = Icons (ordered by list order)
        /// <para />T = Text (from this.TextFunction or this.Text otherwise)
        /// <para />B = Buttons (ordered by list order)
        /// </summary>
        public string DisplayOrder = "ITB";

        /// <summary>
        /// Whether the list entry should be drawn.
        /// </summary>
        public bool Visible = true;

        /// <summary>
        /// Whether the text is cut o...
        /// </summary>
        public bool Truncuated = true;

        /// <summary>
        /// Content alignment related to line height. Only the Center, Top and Bottom values are supported.
        /// </summary>
        public UIAlign Alignment = UIAlign.Center;

        //TODO: DebugListEntry - ability to define a margin to add to the left/right ( = decreasing width on left/right)

        /// <summary>
        /// Simplified constructor for single icon (from Sprite or SpriteMap) and button.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="name"></param>
        /// <param name="icon"></param>
        /// <param name="frame">Ignored if using simple Sprite. Use -1 to not change frame.</param>
        /// <param name="button"></param>
        /// <param name="color">Text color of the list entry.</param>
        /// <param name="textfunction"></param>
        public DebugListEntry(string text, string name = null, Sprite icon = null, int frame = 0, DebugBoxButton button = null,
            Color color = default(Color), Func<string> textfunction = null)
            : this(text, name, new Sprite[] {}, new DebugBoxButton[] {}, color, textfunction)
        {
            if (button != null)
                this.Buttons.Add(button);

            var spriteMap = icon as SpriteMap;
            if (spriteMap != null)
            {
                if (frame >= 0)
                    spriteMap.frame = frame;
                this.Icons.Add(spriteMap.Clone());
            }
            else if (icon != null)
            {
                this.Icons.Add(icon);
            }
        }

        /// <summary>
        /// Full constructor for multiple icons and buttons. Display order is always from outward to inward, depending on display order!
        /// </summary>
        /// <param name="text"></param>
        /// <param name="name"></param>
        /// <param name="icons"></param>
        /// <param name="buttons"></param>
        /// <param name="color">Text color of the list entry.</param>
        /// <param name="textfunction"></param>
        public DebugListEntry(string text, string name, IEnumerable<Sprite> icons, IEnumerable<DebugBoxButton> buttons,
            Color color, Func<string> textfunction)
        {
            this.Text = text;
            this.Name = name;
            this.Color = this.Color == default(Color) ? Color.White : color;
            this.TextFunction = textfunction;
            if (icons != null)
                this.Icons.AddRange(icons);
            if (buttons != null)
                this.Buttons.AddRange(buttons);
        }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>
        /// The new instance.
        /// </returns>
        public DebugListEntry Clone()
        {
            var entry = new DebugListEntry(this._text, this.Name, this.Icons, this.Buttons, this.Color, this.TextFunction)
            {
                Tag = this.Tag,
                Alignment = this.Alignment,
                Truncuated = this.Truncuated,
                Height = this._height,
                Visible = this.Visible,
                DisplayOrder = this.DisplayOrder
            };
            return entry;
        }

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>
        /// A new object that is a copy of this instance.
        /// </returns>
        object ICloneable.Clone()
        {
            return this.Clone();
        }
    }

    public class DebugListEntryComparer : IComparer<DebugListEntry>
    {
        private bool _usesText;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="useText">True to compare DLE.Text, else compares DLE.Name!</param>
        public DebugListEntryComparer(bool useText)
        {
            this._usesText = useText;
        }

        /// <summary>
        /// Compares two objects and returns a value indicating whether one is less than, equal to, or greater than the other.
        /// </summary>
        /// <returns>
        /// A signed integer that indicates the relative values of <paramref name="x"/> and <paramref name="y"/>, as shown in the following table.Value Meaning Less than zero<paramref name="x"/> is less than <paramref name="y"/>.Zero<paramref name="x"/> equals <paramref name="y"/>.Greater than zero<paramref name="x"/> is greater than <paramref name="y"/>.
        /// </returns>
        /// <param name="x">The first object to compare.</param><param name="y">The second object to compare.</param>
        public int Compare(DebugListEntry x, DebugListEntry y)
        {
            return this._usesText ? string.CompareOrdinal(x.Text, y.Text) : string.CompareOrdinal(x.Name, y.Name);
        }
    }
}

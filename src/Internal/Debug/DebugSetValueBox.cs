﻿using System;
using System.Linq;

namespace DuckGame.MserDuck
{
    public class DebugSetValueBox : DebugBox
    {
        private readonly DebugThingBox ParentBox;

        private readonly DebugThingBox.DebugThingInfo ThingInfo;

        private DebugTextBox ValueTextBox;

        private DebugBoxButton ConfirmButton;
        private DebugBoxButton CancelButton;

        private InputType inputType;

        /// <summary>
        /// Current value, gotten depending on the InputType.
        /// </summary>
        private object Value
        {
            get
            {
                switch (this.inputType)
                {
                    case InputType.Textbox:
                        return this.ValueTextBox.Input.Text;
                    default:
                        throw new ArgumentOutOfRangeException("inputType", "Unknown InputType specified.");
                }
            }
        }

        public DebugSetValueBox(DebugThingBox parentBox, DebugThingBox.DebugThingInfo info, int index) : base(index)
        {
            //Determine current tInfo value
            //TODO: DebugSetValueBox - Button to toggle live-update of values - has to reset when pressing close or cancel
            var val = info.Locked ? info.LockedValue : info.GetCurrentValue();

            //Determine input type from value
            if (val is string || Extensions.CanConvert(val.GetType(), typeof(string)))
            {
                //Can convert object directly to string, use textbox
                this.inputType = InputType.Textbox;
            }

            //Setup input methods
            this.ValueTextBox = new DebugTextBox(Vec2.Zero, Vec2.Zero);
            this.ValueTextBox.Input.Text = val.ToString();
            this.ValueTextBox.Visible = this.inputType == InputType.Textbox;

            //Misc setup
            this.position = new Vec2(parentBox.x + parentBox.Size.x, parentBox.y);
            this.ParentBox = parentBox;
            this.ThingInfo = info;
            this._hasFold = false;
            this.Size = new Vec2(300, 150);
            this.MinimumSize = this.Size;
            this.Title = string.Format("[{0}] {1}", info.Type, info.Info.Name);

            //Buttons!
            var icon = IconManager.ButtonIcons.CloneMap();
            icon.scale = new Vec2(2);
            this.ConfirmButton = new DebugBoxButton(icon, 14, this.DoConfirm);
            this.CancelButton = new DebugBoxButton(icon, 9, () => DebugUI.RemoveDebugBox(this));
        }

        /// <summary>
        /// Pressing the OK button.
        /// </summary>
        public void DoConfirm()
        {
            if (this.ThingInfo.Locked)
            {
                this.ThingInfo.LockedValue = this.Value;
            }
            else
            {
                this.ThingInfo.SetValue(this.Value);
            }
            
            DebugUI.RemoveDebugBox(this);
        }

        /// <summary>
        /// Called every frame while the DebugUI and the user control is visible.
        /// </summary>
        public override void DoDraw()
        {
            base.DoDraw();
            if (!this.Visible)
                return;

            this.ValueTextBox.DoDraw();
            this.ConfirmButton.DoDraw();
            this.CancelButton.DoDraw();
        }

        /// <summary>
        /// Returns whether any button was pressed
        /// </summary>
        /// <returns></returns>
        public override object DoUpdate()
        {
            base.DoUpdate();

            if (DebugUI._debugBoxes.OfType<DebugThingBox>().All(b => b != this.ParentBox))
            {
                //Remove self too, if parent doesnt exist
                DebugUI._debugBoxes.Remove(this);
                return false;
            }
            this._visible = this.ParentBox.Visible;

            if (!this.Visible)
                return false;

            this.ValueTextBox.position = this.position + new Vec2(8, 32);
            this.ValueTextBox.Size = new Vec2(this.Size.x - 16, 24);
            this.ValueTextBox.DoUpdate();

            this.ConfirmButton.position = this.position + new Vec2(52, this.Size.y - 52);
            this.ConfirmButton.DoUpdate();
            this.CancelButton.position = this.position + new Vec2(this.Size.x - 52, this.Size.y - 52);
            this.CancelButton.DoUpdate();

            return false;
        }

        private enum InputType
        {
            Textbox
        }
    }
}

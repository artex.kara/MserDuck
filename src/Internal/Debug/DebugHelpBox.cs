﻿using System;

namespace DuckGame.MserDuck
{
    public class DebugHelpBox : DebugBox
    {
        private float _fade = 0;

        /// <summary>
        /// Whether the location and size of this debugbox, as well as whether it is folded, should be stored on quit and reloaded as a default when created.
        /// <para />This only has any effect of debugboxes which get created together with the DebugUI.
        /// </summary>
        public override bool StoreGeometry
        {
            get { return false; }
        }

        public DebugHelpBox(int index) : base(index)
        {
            this.InvisOnClose = true;
            this._visible = false;
            this.Title = "Help";
            this._hasFold = false;
            this.Size = new Vec2(450, 350);
            this.Resizable = false;
            this.Moveable = false;
            this.position = new Vec2(Layer.Console.width / 2 - this.Size.x / 2, Layer.Console.height / 2 - this.Size.y / 2);
        }

        /// <summary>
        /// Called every frame while the DebugUI and the user control is visible.
        /// </summary>
        public override void DoDraw()
        {
            base.DoDraw();

            if (this.Visible)
            {
                _fade = Math.Min(_fade + 0.01f, 0.6f);
                DebugUI.ShadowedText(HelpString, this.x + 8, this.y + 32, 1);
            }
            else
            {
                _fade = Math.Max(_fade - 0.05f, 0);
            }

            if (_fade > 0)
            {
                //Black BG
                Graphics.DrawRect(Vec2.Zero, new Vec2(Layer.Console.width, Layer.Console.height), Color.Black * this._fade, 0.1f);
            }
        }

        private const string HelpString =
@"Welcome to the Debug Mode!
With it, you can debug anything going
on in DuckGame and MserDuck.

Rightclick anywhere in the level to
select a thing under your mouse which
you wish to inspect.

Use the buttons on the right for more
tools and options.

By pressing middle click, you can
lock/unlock the camera.

Check the config.cfg file for more
options regarding the Debug Mode!";
    }
}

﻿using System;
using System.Windows.Forms;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// A scrollbar, used for scrolling (duh!).
    /// </summary>
    public class DebugScrollBar : DebugUIControl
    {
        public Vec2 Size;

        public int MinimumValue;

        public int MaximumValue;

        public float Value
        {
            get { return Extensions.Lerp(this.MinimumValue, this.MaximumValue, this._value); }
            set { this._value = value / (this.MinimumValue + this.MaximumValue); }
        }

        /// <summary>
        /// Value without the lerp (0 to 1).
        /// </summary>
        private float _value;

        private bool _dragging = false;

        public Color ForeColor = Color.LightGray;

        public Color BackColor = Color.MidnightBlue;

        public ScrollOrientation Direction;

        /// <summary>
        /// Offsets the scrolling bar size ratio by this amount. Smaller numbers result in a bigger scroller.
        /// </summary>
        public float RatioOffset = 1;

        private Vec2 ScrollerPosition
        {
            get
            {
                if (this.Direction == ScrollOrientation.HorizontalScroll)
                    return new Vec2(Extensions.Lerp(this.x, this.x + this.Size.x - this.ScrollerSize.x, this._value), this.y);
                return new Vec2(this.x, Extensions.Lerp(this.y, this.y + this.Size.y - this.ScrollerSize.y, this._value));
            }
        }

        private Vec2 ScrollerSize
        {
            get
            {
                if (this.Direction == ScrollOrientation.HorizontalScroll)
                    return new Vec2(this.Size.x * Ratio, this.Size.y);
                return new Vec2(this.Size.x, this.Size.y * Ratio);
            }
        }

        /// <summary>
        /// Whether the scrollbar's böbbl fills the entire background and is useless as shit.
        /// </summary>
        public bool ScrollerFilled
        {
            get { return Math.Abs(this.Ratio - 1) < MserGlobals.Epsilon; }
        }

        private float Ratio
        {
            get { return 1 / Math.Max(1f, this.MaximumValue - this.MinimumValue + RatioOffset); }
        }

        /// <summary>
        /// Default width or height for a scrollbar.
        /// </summary>
        public const float DefaultSize = 16;

        /// <summary>
        /// Creates a scrollbar, used for scrolling (duh!).
        /// </summary>
        /// <param name="position"></param>
        /// <param name="size"></param>
        /// <param name="bgcolor"></param>
        /// <param name="fgcolor"></param>
        /// <param name="minimum"></param>
        /// <param name="maximum"></param>
        /// <param name="value"></param>
        /// <param name="direction">Whether the scrollbar is vertical or horizontal.</param>
        public DebugScrollBar(Vec2 position = default(Vec2), Vec2 size = default(Vec2), Color bgcolor = default(Color), Color fgcolor = default(Color), int minimum = 0, int maximum = 100, int value = 0, ScrollOrientation direction = ScrollOrientation.VerticalScroll)
        {
            this.position = position;
            this.Size = size;
            this.MaximumValue = maximum;
            this.MinimumValue = minimum;
            this.Value = value;

            if (bgcolor != default(Color))
                this.BackColor = bgcolor;

            if (fgcolor != default(Color))
                this.ForeColor = fgcolor;

            this.Direction = direction;
        }

        /// <summary>
        /// Called every frame while the DebugUI and the user control is visible.
        /// </summary>
        public override void DoDraw()
        {
            if (!this.Visible)
                return;

            //Scrollable area
            Graphics.DrawRect(this.position, this.position + this.Size, this.BackColor, 0.9f);

            //Scrolling böbbl
            Graphics.DrawRect(this.ScrollerPosition, this.ScrollerPosition + this.ScrollerSize, this.ForeColor, 0.98f);
        }

        /// <summary>
        /// Called every tick to handle update logic. Returns whether clicked on.
        /// </summary>
        /// <returns></returns>
        public override object DoUpdate()
        {
            //Dragging
            /*MserDebug.PrintEntry("Scrollbar _value", _value);
            MserDebug.PrintEntry("Scrollbar Value", Value);
            MserDebug.PrintEntry("Scrollbar Min", this.MinimumValue);
            MserDebug.PrintEntry("Scrollbar Max", this.MaximumValue);
            MserDebug.PrintEntry("Scrollbar pos", this.position);
            MserDebug.PrintEntry("Scrollbar size", this.Size);
            MserDebug.PrintEntry("Scrollbar scrpos", this.ScrollerPosition);
            MserDebug.PrintEntry("Scrollbar scrsize", this.ScrollerSize);*/

            var inrange = DebugUI._mousePos.IsInRange(this.position, this.position + this.Size);
            if (this.Visible)
            {
                if (inrange)
                    this.DoScrollUpdate();

                if ((inrange && Mouse.left == InputState.Pressed) || (this._dragging && Mouse.left == InputState.Down))
                {
                    this._dragging = true;

                    //Set value based on mouse pos - sizp is used to offset to center of scrollbar
                    float perc;
                    if (this.Direction == ScrollOrientation.HorizontalScroll)
                    {
                        var sizp = this.ScrollerSize.x;
                        perc = (DebugUI._mousePos.x - (this.x + sizp / 2f)) / (this.Size.x - sizp);
                    }
                    else
                    {
                        var sizp = this.ScrollerSize.y;
                        perc = (DebugUI._mousePos.y - (this.y + sizp / 2f)) / (this.Size.y - sizp);
                    }

                    this._value = Maths.Clamp(perc, 0, 1);
                    return true;
                }
            }

            if (Mouse.left == InputState.Released || Mouse.left == InputState.None)
            {
                this._dragging = false;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Check for scrolling input. Called when mousing over the scrollbar itself.
        /// </summary>
        public void DoScrollUpdate()
        {
            if (Math.Abs(Mouse.scroll) > MserGlobals.Epsilon)
            {
                //Direction
                if (Mouse.scroll > 0)
                {
                    ScrollDown();
                }
                else
                {
                    ScrollUp();
                }
            }
        }

        public void ScrollUp()
        {
            Scroll(-DebugScrollBar.ScrollDelta);
        }

        public void ScrollDown()
        {
            Scroll(DebugScrollBar.ScrollDelta);
        }

        private void Scroll(int amount)
        {
            this.Value = Maths.Clamp(this.Value + amount, this.MinimumValue, this.MaximumValue);
        }

        public const int ScrollDelta = 4;
    }
}

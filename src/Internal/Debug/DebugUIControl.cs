﻿namespace DuckGame.MserDuck
{
    /// <summary>
    /// A class describing a user control for the DebugUI.
    /// <para />Should implement standard behaviour, such as update and draw functions.
    /// </summary>
    public abstract class DebugUIControl : Transform
    {
        /// <summary>
        /// Called every frame while the DebugUI and the user control is visible.
        /// </summary>
        public virtual void DoDraw()
        {
            
        }

        /// <summary>
        /// Called every tick to handle update logic. Return value can be used for telling if something happened in the update frame.
        /// </summary>
        /// <returns></returns>
        public virtual object DoUpdate()
        {
            return null;
        }

        /// <summary>
        /// Whether the control is currently visible.
        /// </summary>
        public virtual bool Visible
        {
            get { return this._visible; }
            set { this._visible = value; }
        }

        private bool _visible = true;
    }
}

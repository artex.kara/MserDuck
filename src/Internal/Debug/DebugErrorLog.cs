﻿namespace DuckGame.MserDuck
{
    public class DebugErrorLog : DebugBox
    {
        public DebugErrorLog(int index) : base(index)
        {
            this.InvisOnClose = true;
            this._visible = false;
            this.Title = "Error Log";
        }

        //NYI: DebugErrorLog - Tabbed window with DuckLog contents (current session and earlier, can choose here) and MserDuck uncaught exceptions
    }
}

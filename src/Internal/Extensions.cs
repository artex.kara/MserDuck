﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Class for extension methods and other common use functions that don't fit the MserDuck context (as any <see cref="MserGlobals" /> function would).
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Splits the string into multiple parts with the specified length.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="partLength"></param>
        /// <returns></returns>
        public static IEnumerable<string> SplitInParts(this string s, int partLength)
        {
            if (s == null)
                throw new ArgumentNullException("s");
            if (partLength <= 0)
                throw new ArgumentException("Part length has to be >0.", "partLength");

            if (partLength == 1)
            {
                //String to chars turned back to strings
                foreach (var c in s)
                    yield return c.ToString();
            }
            else
            {
                //Substring for every part
                for (var i = 0; i < s.Length; i += partLength)
                    yield return s.Substring(i, Math.Min(partLength, s.Length - i));
            }
        }

        /// <summary>
        /// Returns the inverted color.
        /// </summary>
        /// <param name="c"></param>
        /// <param name="invertAlpha">Whether the alpha value of the color should be inverted too.</param>
        /// <returns></returns>
        public static Color Invert(this Color c, bool invertAlpha = false)
        {
            return new Color(255 - c.r, 255 - c.g, 255 - c.b, invertAlpha ? 255 - c.a : c.a);
        }

        /// <summary>
        /// Color lerp.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="amount"></param>
        /// <param name="calculateAlpha">If true, the alpha value is also lerped.</param>
        /// <returns></returns>
        public static Color MergeWith(this Color a, Color b, float amount, bool calculateAlpha = false)
        {
            float sa = 1f - amount;
            float sb = amount;

            byte re = (byte) (Maths.Clamp((a.r * sa) + (b.r * sb), 0f, 255f));
            byte gr = (byte) (Maths.Clamp((a.g * sa) + (b.g * sb), 0f, 255f));
            byte bl = (byte) (Maths.Clamp((a.b * sa) + (b.b * sb), 0f, 255f));
            byte al = 255;
            if (calculateAlpha)
            {
                al = Convert.ToByte(Maths.Clamp((a.a * sa) + (b.a * sb), 0f, 255f));
            }

            return new Color(re, gr, bl, al);
        }

        /// <summary>
        /// Returns a byte between 0 and 255, specifying how different c1 is to the specified c2.
        /// </summary>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <returns></returns>
        public static byte Difference(this Color c1, Color c2)
        {
            return (byte)Math.Abs(c1.GrayColor() - c2.GrayColor());
        }

        /// <summary>
        /// Returns a grayscale version of the color.
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static byte GrayColor(this Color c)
        {
            return (byte)(0.11 * c.b + 0.59 * c.g + 0.30 * c.r);
        }

        /// <summary>
        /// Returns the percieved lightness of the color.
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static byte Lightness(this Color c)
        {
            return (byte)(0.299 * c.r + 0.587 * c.g + 0.114 * c.b);
        }

        /// <summary>
        /// Turns the first letter of the string to an uppercase letter.
        /// <para />To do so for every word in a string, use TitleCase formatting.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string FirstLetterToUpper(this string str)
        {
            if (String.IsNullOrWhiteSpace(str))
                return str;

            if (str.Length > 1)
                return Char.ToUpper(str[0]) + str.Substring(1);

            return str.ToUpper();
        }

        /// <summary>
        /// A string.Format where arguments may be null.
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string SafeFormat(string format, params object[] args)
        {
            for (var i = 0; i < args.Length; i++)
            {
                if (args[i] == null)
                    args[i] = "{null}";
            }

            return String.Format(format, args);
        }

        /// <summary>
        /// A neater ToString call, converting values to a more readable format (such as null, empty lists, etc).
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToNeatString(object value)
        {
            if (value == null)
                return "{null}";

            if (value is float || value is double)
            {
                return Single.Parse(value.ToString()).ToString("N3");
            }

            var ex = value as Exception;
            if (ex != null)
            {
                return ex.Message + "\n" + ex.StackTrace;
            }

            //TODO: Extensions ToNeatString - reformat editor properties to have them show their value
            //  Example for testing: color of a Window
            /*var type = value.GetType();
            if (type.Name == "EditorProperty`1")
            {
                var prop = MserGlobals.GetProperty("EditorProperty", "value");
                return ToNeatString(prop.GetValue(value, null));
            }*/

            //Skip the IEnumerable check to avoid char lists
            if (value is string)
            {
                if (String.IsNullOrEmpty(value.ToString()))
                    return "{empty string}";
                if (String.IsNullOrWhiteSpace(value.ToString()))
                    return "{whitespace string}";
                if (value.ToString().StartsWith(" ") || value.ToString().EndsWith(" "))
                    return String.Format("\"{0}\"", value);
                return value.ToString();
            }

            //TODO: Extensions ToNeatString - if list is empty, show the type it would hold, considering it's a generic list.
            //    same could go for "entry" being replaced with the containing type
            var en = value as IEnumerable;
            if (en != null)
            {
                var list = en.Cast<object>().ToList();
                if (list.Any())
                    return "{" + Extensions.GetPlural(list.Count, "entry", "entries") + "} " + String.Join(";", list.Select(Extensions.ToNeatString));
                return "{empty list}";
            }

            var prof = value as Profile;
            if (prof != null)
            {
                return prof.name;
            }

            return value.ToString();
        }

        /// <summary>
        /// Truncates the specified string so it may not be longer than the specified <paramref name="maxLength" />.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="maxLength"></param>
        /// <param name="addDots">If true, instead of cutting off the string at the specified length, it adds ... to the end when cutting off.</param>
        /// <returns></returns>
        public static string Truncate(this string value, int maxLength, bool addDots = false)
        {
            if (String.IsNullOrEmpty(value))
                return value;
            return value.Length <= maxLength ? value : value.Substring(0, ((value.Length - maxLength < 3 && addDots) ? value.Length - 3 : maxLength)) + (addDots ? "..." : "");
        }

        /// <summary>
        /// Truncates the specified multi-line string for each line separately so they each may not be longer than the specified <paramref name="maxLength" />.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="maxLength"></param>
        /// <param name="addDots">If true, instead of cutting off the string at the specified length, it adds ... to the end when cutting off.</param>
        /// <returns></returns>
        public static IEnumerable<string> Truncate(this IEnumerable<string> value, int maxLength, bool addDots = false)
        {
            return value.Select(ln => ln.Truncate(maxLength, addDots));
        }

        /// <summary>
        /// Whether the specified position is within the boundaries specified by a topleft and a bottomright <see cref="Vec2" /> forming a rectangle.
        /// <para />Mostly used for detecting position of something inside of an area.
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="topleft"></param>
        /// <param name="bottomright"></param>
        /// <returns></returns>
        public static bool IsInRange(this Vec2 pos, Vec2 topleft, Vec2 bottomright)
        {
            return pos.x >= topleft.x && pos.y >= topleft.y && pos.x <= bottomright.x && pos.y <= bottomright.y;
        }

        /// <summary>
        /// Whether the specified position is within the boundaries specified by a rectangle.
        /// <para />Mostly used for detecting position of something inside of an area.
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="rect"></param>
        /// <returns></returns>
        public static bool IsInRange(this Vec2 pos, Rectangle rect)
        {
            return pos.IsInRange(new Vec2(rect.x, rect.y), new Vec2(rect.x + rect.width, rect.y + rect.height));
        }

        /// <summary>
        /// Lerps between min and max, perc should be 0 to 1 but can be less/more if needed.
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <param name="perc"></param>
        /// <returns></returns>
        public static float Lerp(float min, float max, float perc)
        {
            return min + (max - min) * perc;
        }

        /// <summary>
        /// Returns a rectangle multiplied by the scale factor, with a Pivot of <code>0;0</code>.
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="factor"></param>
        /// <returns></returns>
        public static Rectangle Multiply(this Rectangle rect, Vec2 factor)
        {
            return new Rectangle(rect.x * factor.x, rect.y * factor.y, rect.width * factor.x, rect.height * factor.y);
        }

        /// <summary>
        /// Offsets the location of the rectangle by the specified <see cref="Vec2" />.
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public static Rectangle Move(this Rectangle rect, Vec2 offset)
        {
            return new Rectangle(rect.x + offset.x, rect.y + offset.y, rect.width, rect.height);
        }

        /// <summary>
        /// Similar to <code>.Distinct()</code> except being able to select a certain key to run distinct on.
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="source"></param>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        public static IEnumerable<TSource> DistinctBy<TSource, TKey> (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            var seenKeys = new HashSet<TKey>();
            return source.Where(element => seenKeys.Add(keySelector(element)));
        }

        /// <summary>
        /// Returns whether all entries of the specified IEnumerable are the same, by comparing the specified key.
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="source"></param>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        public static bool AllEqual<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            var lst = source.ToList();
            if (lst.Count <= 1)
                return true;

            var first = keySelector(lst.First());
            return lst.All(k => object.Equals(keySelector(k), first));
        }

        /// <summary>
        /// Finds the n-th occurence of the specified value (applying a regex).
        /// </summary>
        /// <param name="target"></param>
        /// <param name="value"></param>
        /// <param name="n">1-based index</param>
        /// <param name="start"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public static int NthIndexOf(this string target, string value, int n, int start = 0, int len = -1)
        {
            if (n < 1 || String.IsNullOrEmpty(target))
                return -1;

            var newtarget = target;
            if (start > 0)
                newtarget = len < 0 ? target.Substring(start) : target.Substring(start, len);
            else
                start = 0;

            Match m = Regex.Match(newtarget, "((" + Regex.Escape(value) + ").*?){" + n + "}");

            if (m.Success)
                return m.Groups[2].Captures[n - 1].Index + start;
            else
                return -1;
        }

        /// <summary>
        /// Similar to <code>Graphics.DrawDottedLine</code> except switching between two colors instead of one color and spacing inbetween.
        /// </summary>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="col1"></param>
        /// <param name="col2"></param>
        /// <param name="width"></param>
        /// <param name="col1Length"></param>
        /// <param name="depth"></param>
        public static void DrawDoubleDottedLine(Vec2 p1, Vec2 p2, Color col1, Color col2, float width = 1f, float col1Length = 8f, Depth depth = default(Depth))
        {
            ++Graphics.currentDrawIndex;
            Vec2 vec2_1 = p1;
            Vec2 vec2_2 = p2 - p1;
            float length = vec2_2.length;
            int num = (int)((double)length / (double)col1Length);
            vec2_2.Normalize();
            bool flag = false;
            for (int index = 0; index < num; ++index)
            {
                Vec2 vec2_3 = vec2_1;
                vec2_3 += vec2_2 * col1Length;
                if ((double)(vec2_3 - p1).length > (double)length)
                    vec2_3 = p2;
                var color = flag ? col1 : col2;
                Graphics.DrawLine(new Vec2(vec2_1.x, vec2_1.y), new Vec2(vec2_3.x, vec2_3.y), color, width, depth);
                flag = !flag;
                vec2_1 = vec2_3;
            }
        }

        /// <summary>
        /// Prints a string's literal variant. Escape characters like \n get printed this way.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="includeQuotes">Whether to include any quotes from a string starting or ending.</param>
        /// <returns></returns>
        public static string ToLiteral(this string input, bool includeQuotes = false)
        {
            using (var writer = new StringWriter())
            {
                using (var provider = CodeDomProvider.CreateProvider("CSharp"))
                {
                    //TODO: Extensions ToLiteral - this is fucking horrible, maybe use regexes to replace this in a nicer way
                    provider.GenerateCodeFromExpression(new CodePrimitiveExpression(input), writer, null);
                    return includeQuotes ? writer.ToString() : writer.ToString().Replace("\\\"", "QUOTQUOT").Replace("\"", "").Replace("QUOTQUOT", "\\\"");
                }
            }
        }

        /// <summary>
        /// Inserts the specified string into this string every n characters.
        /// </summary>
        /// <param name="insert">The string to insert at the specified interval.</param>
        /// <param name="waitForSpace">Whether the insert string should only be added right before a whitespace character.</param>
        /// <param name="startVal">Starting value of the counter, before inserting every num chars.</param>
        /// <param name="endMargin">If less than this many characters are at the end of the string, do not insert the character.</param>
        /// <param name="str">The string to insert something into.</param>
        /// <param name="num">Insert interval, every num chars to add the insert string..</param>
        /// <returns></returns>
        public static string InsertEvery(this string str, int num, string insert, bool waitForSpace = true, int startVal = 0, int endMargin = 10)
        {
            //TODO: Extensions InsertEvery - add a "distance" margin, how many other chars have to be between every time the string is found (to avoid too short segments)

            if (num > str.Length)
                return str;

            var sb = new StringBuilder(str);
            var waitin = false;
            for (var i = startVal; i < str.Length; i++)
            {
                var ch = str[i];
                if ((i + 1) % (num + 1) == 0)
                {
                    if (waitForSpace)
                    {
                        sb.Append(ch);
                        waitin = true;
                    }
                    else
                    {
                        if (str.Length - i < endMargin)
                        {
                            sb.Append(ch);
                        }
                        else
                        {
                            sb.Append(insert);
                        }
                    }
                }
                else
                {
                    if (waitin && ch == ' ')
                    {
                        if (str.Length - i < endMargin)
                        {
                            sb.Append(ch);
                        }
                        else
                        {
                            sb.Append(insert);
                        }
                        waitin = false;
                    }
                    else
                    {
                        sb.Append(ch);
                    }
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// Counts the amount of the specified char occuring in the specified string, using inclusive int bounds.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="ch"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static int CountCharOccurences(this string str, char ch, int start, int end)
        {
            if (String.IsNullOrEmpty(str))
                return 0;

            if (start < 0 || end >= str.Length || start > end)
                throw new ArgumentOutOfRangeException("start", "Start or end parameter was out of range.");

            var cnt = 0;
            for (var i = start; i <= end; i++)
            {
                if (str[i] == ch)
                    cnt++;
            }
            return cnt;
        }

        /// <summary>
        /// Counts the amount of the specified char occuring in the specified string.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="ch"></param>
        /// <returns></returns>
        public static int CountCharOccurences(this string str, char ch)
        {
            return str.CountCharOccurences(ch, 0, str.Length - 1);
        }

        /// <summary>
        /// Depending on the count, returns the plural or singular of the specified thing.
        /// <para />Resulting format: <code>"[count] [thing][s]"</code> - unless a specific plural version is defined.
        /// </summary>
        /// <param name="count"></param>
        /// <param name="thing"></param>
        /// <param name="plural">Plural form, instead of appending s.</param>
        /// <param name="space">Space between the count and the thing?</param>
        /// <returns></returns>
        public static string GetPlural(int count, string thing, string plural = null, bool space = true)
        {
            var spc = space ? " " : String.Empty;
            if (count == 1)
                return String.Format("{0}{2}{1}", count, thing, spc);
            var trail = thing + "s";
            if (plural != null)
                trail = plural;
            return String.Format("{0}{2}{1}", count, trail, spc);
        }

        /// <summary>
        /// Removes the last entry of a list if it is an empty string or null.
        /// <para />Mostly run on command args, to avoid being recognized as parameters where not wanted.
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static IEnumerable<string> Crop(this IList<string> list)
        {
            if (list == null || !list.Any() || !String.IsNullOrEmpty(list.Last()))
                return list;

            return list.Take(list.Count - 1);
        }

        /// <summary>
        /// Whether the specified string is equal to any of the other specified strings.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="ignoreCase">Whether the comparison is case sensitive.</param>
        /// <param name="strings"></param>
        /// <returns></returns>
        public static bool EqualsToAny(this string str, bool ignoreCase, params string[] strings)
        {
            return str.EqualsToAny(ignoreCase, strings.ToList());
        }

        /// <summary>
        /// Whether the specified string is equal to any of the other specified strings.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="ignoreCase">Whether the comparison is case sensitive.</param>
        /// <param name="strings"></param>
        /// <returns></returns>
        public static bool EqualsToAny(this string str, bool ignoreCase, IEnumerable<string> strings)
        {
            if (String.IsNullOrEmpty(str))
                return false;

            return strings.Any(s => str.Equals(s, ignoreCase ? StringComparison.InvariantCultureIgnoreCase : StringComparison.InvariantCulture));
        }

        /// <summary>
        /// Whether the specified string is equal to the other specified string.
        /// <para />This is identical to a string.Equals call, but using the correct StringComparison methods.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="ignoreCase">Whether the comparison is case sensitive.</param>
        /// <param name="str2"></param>
        /// <returns></returns>
        public static bool EqualsTo(this string str, bool ignoreCase, string str2)
        {
            if (String.IsNullOrEmpty(str))
                return false;

            return str.Equals(str2, ignoreCase ? StringComparison.InvariantCultureIgnoreCase : StringComparison.InvariantCulture);
        }

        /// <summary>
        /// Returns whether this file is empty (0 bytes contents).
        /// </summary>
        /// <param name="file"></param>
        /// <exception cref="FileNotFoundException">Could not find the specified file.</exception>
        /// <returns></returns>
        public static bool FileEmpty(string file)
        {
            return Extensions.FileEmpty(new FileInfo(file));
        }

        /// <summary>
        /// Returns whether this file is empty (0 bytes contents).
        /// </summary>
        /// <param name="file"></param>
        /// <exception cref="FileNotFoundException">Could not find the specified file.</exception>
        /// <returns></returns>
        public static bool FileEmpty(FileInfo file)
        {
            if (!file.Exists)
                throw new FileNotFoundException(String.Format("Could not find file {0}", file));

            return file.Length == 0;
        }

        /// <summary>
        /// Returns whether the two specified objects are equal, performing a delta equality checks for floats or structs using floats for comparison.
        /// </summary>
        /// <param name="e1"></param>
        /// <param name="e2"></param>
        /// <returns></returns>
        public static bool EntriesEqual(object e1, object e2)
        {
            if (e1 == null && e2 == null)
                return true;

            if (e1 == null || e2 == null)
                return false;

            if (e1 is float && e2 is float)
            {
                return Math.Abs((float)e1 - (float)e2) < MserGlobals.Epsilon;
            }

            if (e1 is Vec2 && e2 is Vec2)
            {
                return Math.Abs(((Vec2)e1).x - ((Vec2)e2).x) < MserGlobals.Epsilon &&
                       Math.Abs(((Vec2)e1).y - ((Vec2)e2).y) < MserGlobals.Epsilon;
            }

            if (e1 is Vec3 && e2 is Vec3)
            {
                return Math.Abs(((Vec3)e1).x - ((Vec3)e2).x) < MserGlobals.Epsilon &&
                       Math.Abs(((Vec3)e1).y - ((Vec3)e2).y) < MserGlobals.Epsilon &&
                       Math.Abs(((Vec3)e1).z - ((Vec3)e2).z) < MserGlobals.Epsilon;
            }

            if (e1 is Vec4 && e2 is Vec4)
            {
                return Math.Abs(((Vec4)e1).x - ((Vec4)e2).x) < MserGlobals.Epsilon &&
                       Math.Abs(((Vec4)e1).y - ((Vec4)e2).y) < MserGlobals.Epsilon &&
                       Math.Abs(((Vec4)e1).z - ((Vec4)e2).z) < MserGlobals.Epsilon &&
                       Math.Abs(((Vec4)e1).w - ((Vec4)e2).w) < MserGlobals.Epsilon;
            }

            if (e1 is Rectangle && e2 is Rectangle)
            {
                return Math.Abs(((Rectangle)e1).x -      ((Rectangle)e2).x)      < MserGlobals.Epsilon &&
                       Math.Abs(((Rectangle)e1).y -      ((Rectangle)e2).y)      < MserGlobals.Epsilon &&
                       Math.Abs(((Rectangle)e1).width -  ((Rectangle)e2).width)  < MserGlobals.Epsilon &&
                       Math.Abs(((Rectangle)e1).height - ((Rectangle)e2).height) < MserGlobals.Epsilon;
            }

            if (e1 is Color && e2 is Color)
            {
                return Math.Abs(((Color)e1).r - ((Color)e2).r) < MserGlobals.Epsilon &&
                       Math.Abs(((Color)e1).g - ((Color)e2).g) < MserGlobals.Epsilon &&
                       Math.Abs(((Color)e1).b - ((Color)e2).b) < MserGlobals.Epsilon &&
                       Math.Abs(((Color)e1).a - ((Color)e2).a) < MserGlobals.Epsilon;
            }

            return e1.Equals(e2);
        }

        /// <summary>
        /// Whether a conversion exists from type 1 to type 2.
        /// </summary>
        /// <param name="fromType"></param>
        /// <param name="toType"></param>
        /// <returns></returns>
        public static bool CanConvert(Type fromType, Type toType)
        {
            try
            {
                // Throws an exception if there is no conversion from fromType to toType
                Expression.Convert(Expression.Parameter(fromType, null), toType);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Converts the specified enum bitmask to an enumerable of its flag values, ordered by ascending values.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static IEnumerable<Enum> GetFlags(this Enum input)
        {
            return Enum.GetValues(input.GetType()).Cast<Enum>().Where(input.HasFlag);
        }

        /// <summary>
        /// Removes the specified entry from the array.
        /// <para />This is a pure method, so be sure to remember the assignment!
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static T[] RemoveAt<T>(this T[] source, int index)
        {
            T[] dest = new T[source.Length - 1];
            if (index > 0)
                Array.Copy(source, 0, dest, 0, index);

            if (index < source.Length - 1)
                Array.Copy(source, index + 1, dest, index, source.Length - index - 1);

            return dest;
        }

        /// <summary>
        /// Converts the object to the specified type using a converter.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="outtype"></param>
        /// <returns></returns>
        public static object ConvertTo(object input, Type outtype)
        {
            try
            {
                var converter = System.ComponentModel.TypeDescriptor.GetConverter(outtype);
                return converter.ConvertFrom(input);
            }
            catch (NotSupportedException)
            {
                return null;
            }
        }

        /// <summary>
        /// Same as a <code>IEnumerable.ToDictionary()</code> call, except duplicate keys are ignored instead of throwing an exception.
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="source"></param>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        public static Dictionary<TKey, TSource> ToSafeDictionary<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            var dict = new Dictionary<TKey, TSource>();
            foreach (var e in source)
            {
                var k = keySelector(e);
                if (!dict.ContainsKey(k))
                    dict.Add(k, e);
            }
            return dict;
        }

        /// <summary>
        /// Returns a random entry from the specified <see cref="IEnumerable" />.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static T PickRandom<T>(this IEnumerable<T> source)
        {
            return source.OrderBy(x => Guid.NewGuid()).First();
        }

        private static readonly FieldInfo _followCamers = MserGlobals.GetField(typeof(FollowCam), "_follow");
        private static readonly FieldInfo _camDirty = MserGlobals.GetField(typeof(Camera), "_dirty");

        /// <summary>
        /// Creates a clone of the specified Camera or FollowCam.
        /// </summary>
        /// <param name="cam"></param>
        /// <returns></returns>
        public static Camera Clone(this Camera cam)
        {
            Camera dest;
            if (cam.GetType() == typeof(FollowCam))
            {
                var src = (FollowCam)cam;
                dest = new FollowCam();
                var destfc = (FollowCam)dest;
                destfc.lerpMult = src.lerpMult;
                destfc.lerpSpeed = src.lerpSpeed;
                destfc.manualViewSize = src.manualViewSize;
                destfc.minSize = src.minSize;
                destfc.speed = src.speed;
                destfc.startCentered = src.startCentered;
                foreach (var t in (HashSet<Thing>)Extensions._followCamers.GetValue(src))
                {
                    destfc.Add(t);
                }
            }
            else
            {
                dest = new Camera();
            }

            dest.x = cam.x;
            dest.y = cam.y;
            dest.width = cam.width;
            dest.height = cam.height;
            dest.zoomPoint = cam.zoomPoint;
            Extensions._camDirty.SetValue(dest, Extensions._camDirty.GetValue(cam));
            return dest;
        }

        /// <summary>
        /// Creates a clone of the specified IEnumerable (generic), cloning each object contained.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static IEnumerable<T> Clone<T>(this IEnumerable<T> obj) where T : ICloneable
        {
            return obj.Select(e => (T)((ICloneable)e).Clone());
        }

        /// <summary>
        /// Creates a SortedList from the specified IEnumerable, wheras Key and Value are assigned using a single selector function.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="source"></param>
        /// <param name="keyValueSelector"></param>
        /// <returns></returns>
        public static SortedList<TKey, TValue> ToSortedList<T, TKey, TValue>(this IEnumerable<T> source, Func<T, KeyValuePair<TKey, TValue>> keyValueSelector)
        {
            var list = new SortedList<TKey, TValue>();
            foreach (var kvpair in source.Select(keyValueSelector))
            {
                list.Add(kvpair.Key, kvpair.Value);
            }
            return list;
        }

        /// <summary>
        /// Rounds the specified Vec2 to the nearest integral value.
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Vec2 RoundVec2(Vec2 v)
        {
            return new Vec2((float)Math.Round(v.x), (float)Math.Round(v.y));
        }

        /// <summary>
        /// Sets an entry in a list to the specified value.
        /// <para />Unlike the <code>list[i]</code> call, setting an element that's out of range will add filler entries before.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="index"></param>
        /// <param name="value"></param>
        /// <param name="filler"></param>
        public static void SetElement<T>(this List<T> list, int index, T value, T filler = default(T))
        {
            if (index < 0)
                return;

            if (index < list.Count)
            {
                //Simple set
                list[index] = value;
                return;
            }

            //Add fillers until count is reached
            for (var i = list.Count; i < index; i++)
            {
                list.Add(filler);
            }
            list.Add(value);
        }

        /// <summary>
        /// Gets an entry in a list at the specified index.
        /// <para />Unlike the <code>list[i]</code> or <code>list.ElementAt</code> calls, getting an element that's out of range will return a fallback value instead.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="index"></param>
        /// <param name="fallback"></param>
        /// <returns></returns>
        public static T GetElement<T>(this IEnumerable<T> list, int index, T fallback = default(T))
        {
            try
            {
                var res = list.ElementAt(index);
                return res;
            }
            catch (ArgumentOutOfRangeException)
            {
                return fallback;
            }
        }

        /// <summary>
        /// A safe Substring call which can either return an empty string or the input string when the values go out of range.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="startIndex"></param>
        /// <param name="returnInput"></param>
        /// <returns></returns>
        public static string SafeSubstring(this string input, int startIndex, bool returnInput = true)
        {
            return input.SafeSubstring(startIndex, input.Length - startIndex);
        }

        /// <summary>
        /// A safe Substring call which can either return an empty string or the input string when the values go out of range.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="startIndex"></param>
        /// <param name="length"></param>
        /// <param name="returnInput"></param>
        /// <returns></returns>
        public static string SafeSubstring(this string input, int startIndex, int length, bool returnInput = true)
        {
            try
            {
                return input.Substring(startIndex, length);
            }
            catch (ArgumentOutOfRangeException)
            {
                if (returnInput)
                    return input;
                return string.Empty;
            }
        }

        /// <summary>
        /// Whether two lists are equal to eachother.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list1"></param>
        /// <param name="list2"></param>
        /// <param name="checkOrder">Whether to respect the order of elements as well.</param>
        /// <returns></returns>
        public static bool EqualsTo<T>(this IEnumerable<T> list1, IEnumerable<T> list2, bool checkOrder = false)
        {
            if (checkOrder)
            {
                //SequenceEqual does order check
                return list1.SequenceEqual(list2);
            }
            else
            {
                //Use HashSet
                return new HashSet<T>(list1).SetEquals(list2);
            }
        }
    }
}

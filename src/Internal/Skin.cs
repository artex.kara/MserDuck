﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// A skin that can be loaded onto a Duck's persona. Exists in form of a directory in MserDuck/content/skins (by default), which
    /// contains multiple png files with metadata attached to them using separate text files.
    /// </summary>
    public class Skin
    {
        //TODO: Skin - load default skin for all ducks that dont have one set explicitly
        //  Most likely only logical once the skin system is stable enough
        //TODO: Skin - send skin files over the network, similarly to .hat data.
        //  This would allow for dynamically creating and sharing skins easily.

        /// <summary>
        /// Name of the skins folder inside of content.
        /// </summary>
        public const string SkinsFolder = "skins";

        /// <summary>
        /// Name of the default skin.
        /// </summary>
        public const string DefaultSkinName = "default";

        /// <summary>
        /// List of skins and which profiles use them.
        /// </summary>
        public static readonly Dictionary<Profile, Skin> SelectedSkins = new Dictionary<Profile, Skin>();

        /// <summary>
        /// Directory where this skin's files are located, relative to the <code>Skin.SkinsDirectory</code>.
        /// </summary>
        public readonly string SkinDirectory;

        /// <summary>
        /// Name of the skin.
        /// </summary>
        public readonly string Name;

        /// <summary>
        /// Whether the skin has been successfully loaded already.
        /// </summary>
        private bool _loaded;

        /// <summary>
        /// A list of all skinsprites.
        /// </summary>
        private readonly List<SkinSprite> _skinSprites;

        /// <summary>
        /// Whether the local duck is currently using a skin, to avoid constant dictionary checks.
        /// </summary>
        public static Skin _usingSkin;

        /// <summary>
        /// The default skin instance. Used as complete fallback.
        /// </summary>
        public static Skin DefaultSkin
        {
            get
            {
                if (Skin._defaultSkin == null)
                {
                    try
                    {
                        Skin._defaultSkin = new Skin(DefaultSkinName, true);
                    }
                    catch (Exception ex)
                    {
                        Program.LogLine("ERROR SKINNIN: " + ex.Message + ex.StackTrace);
                        return null;
                    }
                }
                return _defaultSkin;
            }
        }

        private static Skin _defaultSkin;

        /// <summary>
        /// Location for all skin files. Itself should only contain a readme.txt
        /// </summary>
        public static string SkinsDirectory
        {
            get { return MserGlobals.GetContentDir() + SkinsFolder; }
        }

        /// <summary>
        /// Creates a new skin instance.
        /// </summary>
        /// <param name="dir">Path to the skin's directory, relative to the skins folder. Skin name is derived from folder name.</param>
        public Skin(string dir) : this(dir, false)
        {

        }

        /// <summary>
        /// Creates the default skin instance.
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="isDefault"></param>
        private Skin(string dir, bool isDefault)
        {
            if (!Directory.Exists(Path.Combine(Skin.SkinsDirectory, dir)))
                throw new DirectoryNotFoundException("Can't find input directory for skin (" + dir + ").");

            this.SkinDirectory = dir;

            var info = new DirectoryInfo(dir);
            this.Name = info.Name;

            if (this.Name == "skins" && (info.Parent == null || info.Parent.Name == "content"))
                throw new ArgumentException("Specified input directory is skins folder.", "dir");

            if (isDefault)
            {
                if (Skin._defaultSkin != null)
                    throw new InvalidOperationException("Attempted to create another default skin!");

                //Is d3fault, obviously set
                this._skinSprites = GetDefaultSkinSprites();
                this._loaded = true;
                Skin._defaultSkin = this;
            }
            else
            {
                //Get already setted defaults - clone in order to keep defaults the same
                this._skinSprites = DefaultSkin._skinSprites.Clone().ToList();
            }
        }

        /// <summary>
        /// Loads the skin data files.
        /// <para />Returns null if no errors occured, otherwise a string to display as error text.
        /// </summary>
        /// <returns></returns>
        public string Load()
        {
            return Load(false);
        }

        /// <summary>
        /// Loads the skin data files.
        /// <para />Returns null if no errors occured, otherwise a string to display as error text.
        /// </summary>
        /// <returns></returns>
        private string Load(bool force)
        {
            if (force || !_loaded)
            {
                //Load data on top of existing sprites
                foreach (var file in this._skinSprites)
                {
                    try
                    {
                        file.LocalSpritePath = Path.Combine(this.SkinDirectory, file.SpriteName);
                        file.Initialize();
                    }
                    catch (Exception ex)
                    {
                        return "Init failed for " + file.SpriteName + ": " + ex.Message;
                    }
                }

                this._loaded = true;
                return null;
            }
            else
            {
                //Already loaded
                return null;
            }
        }

        /// <summary>
        /// Returns the SkinSprite with the specified name (filename without extension).
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public SkinSprite GetSpriteByName(string name)
        {
            return this._skinSprites.FirstOrDefault(s => s.SpriteName.EqualsTo(true, name));
        }

        /// <summary>
        /// Applies this skin onto the specified profile.
        /// <para />Returns null if no errors occured, otherwise a string to display as error text.
        /// </summary>
        /// <param name="prof">The profile to apply the skin onto.</param>
        /// <param name="dispose">Whether to do a full reload of the skin. This will make sure that any lost content will be reloaded.</param>
        /// <returns></returns>
        public string Apply(Profile prof, bool dispose = false)
        {
            try
            {
                if (prof == null || prof.persona == null)
                    return "No profile specified.";

                var persona = prof.persona;
                var load = this.Load(false);
                if (load != null)
                    return load;

                //Has to dispose, due to lost content?
                if (dispose)
                {
                    //Full reload for skin
                    this._skinSprites.Clear();
                    this._skinSprites.AddRange(DefaultSkin._skinSprites.Clone());
                    this.Load(true);
                }

                //Recolor first
                foreach (var spr in this._skinSprites.Where(s => s.Recolor))
                {
                    spr.SpriteMap = spr.DoRecolor(persona.color);
                }

                //Apply the sprites of the skin
                persona.sprite = this.GetSpriteByName("duck").SpriteMap;
                persona.armSprite = this.GetSpriteByName("duckArms").SpriteMap;
                persona.controlledSprite = this.GetSpriteByName("controlledDuck").SpriteMap;
                //IMP: Skin - crowd sprite animations are handled completely different internally - will have to convert from txt file system to the one used in the spritemap(s)
                persona.crowdSprite = this.GetSpriteByName("seatDuck").SpriteMap;
                persona.featherSprite = this.GetSpriteByName("feather").SpriteMap;
                persona.fingerPositionSprite = this.GetSpriteByName("fingerPositions").SpriteMap;
                persona.quackSprite = this.GetSpriteByName("quackduck").SpriteMap;
                persona.skipSprite = this.GetSpriteByName("skipSign").SpriteMap;
                
                //Mark skin as selected
                if (Skin.SelectedSkins.ContainsKey(prof))
                {
                    Skin.SelectedSkins[prof] = this;
                }
                else
                {
                    Skin.SelectedSkins.Add(prof, this);
                }

                //Animations have to be cloned directly - PLEASE DONT ASK WHY
                //You can't simply set the _sprite / graphic to the s1 SpriteMap, that'd be too easy
                //nono it actually turns invisible then. So here you are, copying shit over like a git
                //TODO: Skin - anything else that needs to be copied over like this?
                this.GetSpriteByName("duck").SpriteMap.CloneAnimations(prof.duck._sprite);
                this.GetSpriteByName("duckArms").SpriteMap.CloneAnimations(prof.duck._spriteArms);
                this.GetSpriteByName("controlledDuck").SpriteMap.CloneAnimations(prof.duck._spriteControlled);
                this.GetSpriteByName("quackduck").SpriteMap.CloneAnimations(prof.duck._spriteQuack);
                //this.GetSpriteByName("seatDuck").SpriteMap.CloneAnimations(prof.duck._sprite);
                //this.GetSpriteByName("feather").SpriteMap.CloneAnimations(prof.duck._sprite);
                //this.GetSpriteByName("fingerPositions").SpriteMap.CloneAnimations(prof.duck._sprite);
                //this.GetSpriteByName("skipSign").SpriteMap.CloneAnimations(prof.duck._sprite);

                //Local check
                if (prof.localPlayer)
                    _usingSkin = this;
            }
            catch (Exception e)
            {
                Program.LogLine("FUK SKIN LOADY!!! " + e.Message + e.StackTrace);
            }
            return null;
        }

        /// <summary>
        /// Returns the default sprite maps, to later be modified from a newly loaded skin.
        /// <para />Loads from file with this call, do not use directly!
        /// </summary>
        /// <returns></returns>
        private static List<SkinSprite> GetDefaultSkinSprites()
        {
            var dir = Path.Combine(Skin.SkinsDirectory, DefaultSkinName);
            var list = Directory.EnumerateFiles(dir, "*.png", SearchOption.AllDirectories)
                .Select(f => new SkinSprite(Path.Combine(DefaultSkinName, Path.GetFileNameWithoutExtension(f)))).ToList();
            list.ForEach(s => s.Initialize());
            return list;
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            return this.Name;
        }
    }

    public class PseudoPersona : DuckPersona
    {
        public PseudoPersona(Vec3 varCol) : base(varCol)
        {
        }

        public void RecreatePersona()
        {
            var me = (DuckPersona)this;

            //Skin recreate
            Skin skin;
            var prof = MserGlobals.ProfileWithPersona(me);
            if (prof != null)
            {
                if (prof.localPlayer && Skin._usingSkin != null)
                {
                    //Local skin
                    skin = Skin._usingSkin;
                }
                else
                {
                    //Do lookup
                    if (!Skin.SelectedSkins.TryGetValue(prof, out skin))
                    {
                        DefaultRecreate();
                        return;
                    }
                }

                skin.Apply(prof, true);
                return;
            }

            //Couldn't test for skin? CREP
            DefaultRecreate();
        }

        private void DefaultRecreate()
        {
            var me = (DuckPersona)this;

            //Default recreate
            me.sprite.texture.Dispose();
            me.sprite.texture = Graphics.Recolor(Content.Load<Tex2D>("duck"), this.color);
            me.featherSprite.texture.Dispose();
            me.featherSprite.texture = Graphics.Recolor(Content.Load<Tex2D>("feather"), this.color);
            me.fingerPositionSprite.texture.Dispose();
            me.fingerPositionSprite.texture = Graphics.Recolor(Content.Load<Tex2D>("fingerPositions"), this.color);
            me.crowdSprite.texture.Dispose();
            me.crowdSprite.texture = Graphics.Recolor(Content.Load<Tex2D>("seatDuck"), this.color);
            me.quackSprite.texture.Dispose();
            me.quackSprite.texture = Graphics.Recolor(Content.Load<Tex2D>("quackduck"), this.color);
            me.armSprite.texture.Dispose();
            me.armSprite.texture = Graphics.Recolor(Content.Load<Tex2D>("duckArms"), this.color);
            me.controlledSprite.texture.Dispose();
            me.controlledSprite.texture = Graphics.Recolor(Content.Load<Tex2D>("controlledDuck"), this.color);
            me.skipSprite.texture.Dispose();
            me.skipSprite.texture = Graphics.Recolor(Content.Load<Tex2D>("skipSign"), this.color);
            me.arrowSprite.texture.Dispose();
            me.arrowSprite.texture = Graphics.Recolor(Content.Load<Tex2D>("startArrow"), this.color);
            me.defaultHead.texture.Dispose();
            me.defaultHead.texture = Graphics.Recolor(Content.Load<Tex2D>("hats/default"), this.color);
            //TODO: Skin - Recreate car's duck AND cookedchicken texture here too + readme.txt entries
        }

        public void UpdatePersona()
        {
            var me = (DuckPersona)this;
            object spritetex;
            object quacktex;
            object armtex;

            Skin skin;
            var prof = MserGlobals.ProfileWithPersona(me);
            if (prof != null && Skin.SelectedSkins.TryGetValue(prof, out skin))
            {
                //Using skin
                spritetex = skin.GetSpriteByName("duck").SpriteMap.texture.nativeObject;
                quacktex = skin.GetSpriteByName("quackduck").SpriteMap.texture.nativeObject;
                armtex = skin.GetSpriteByName("duckArms").SpriteMap.texture.nativeObject;
            }
            else
            {
                //Defaults
                spritetex = me.sprite.texture.nativeObject;
                quacktex = me.quackSprite.texture.nativeObject;
                armtex = me.armSprite.texture.nativeObject;
            }

            var spr = spritetex as Microsoft.Xna.Framework.Graphics.RenderTarget2D;
            var quackspr = quacktex as Microsoft.Xna.Framework.Graphics.RenderTarget2D;
            var armspr = armtex as Microsoft.Xna.Framework.Graphics.RenderTarget2D;
            if (((spr != null && spr.IsContentLost) || (quackspr != null && quackspr.IsContentLost) || (armspr != null && armspr.IsContentLost)) && Graphics.inFocus)
            {
                me.Recreate();
            }
        }
    }
}

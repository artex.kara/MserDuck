﻿namespace DuckGame.MserDuck
{
    /// <summary>
    /// An error in a specific argument of a command.
    /// </summary>
    public class ArgumentError
    {
        private int _index;

        /// <summary>
        /// Which argument causes the error. Use -1 to highlight all arguments specified.
        /// </summary>
        public int Index
        {
            get { return this._index; }
            set { this._index = value; }
        }

        private int _startChar;

        /// <summary>
        /// Index of first char that causes the error in the argument. Use -1 to let it select all.
        /// </summary>
        public int StartChar
        {
            get { return this._startChar; }
            set { this._startChar = value; }
        }

        private int _endChar;

        /// <summary>
        /// Index of last char that causes the error in the argument. Use -1 to let it select all.
        /// </summary>
        public int EndChar
        {
            get { return this._endChar; }
            set { this._endChar = value; }
        }

        private string _message;

        /// <summary>
        /// The string to display/associate with the error. Should be as descriptive as possible, and may include the argument's value in question.
        /// </summary>
        public string Message
        {
            get { return this._message; }
            set { this._message = value; }
        }

        private Severity _severity;

        /// <summary>
        /// Severity of the error. Changes color and symbols, so the user knows whether it would cause issues.
        /// </summary>
        public Severity Severity
        {
            get { return this._severity; }
            set { this._severity = value; }
        }

        /// <summary>
        /// Creates a new argument error, attaching info or error strings to specific arguments of a command.
        /// </summary>
        /// <param name="index">Which argument causes the error. Use -1 to highlight all arguments specified.</param>
        /// <param name="message">The string to display/associate with the error. Should be as descriptive as possible, and may include the argument's value in question.</param>
        /// <param name="severity">Severity of the error. Changes color and symbols, so the user knows whether it would cause issues.</param>
        /// <param name="startChar">Index of first char that causes the error in the argument. Changes length of recoloring. Use -1 to let it select all.</param>
        /// <param name="endChar">Index of last char that causes the error in the argument. Changes length of recoloring. Use -1 to let it select all.</param>
        public ArgumentError(int index, string message, Severity severity = Severity.Error, int startChar = -1, int endChar = -1)
        {
            this._index = index;
            this._message = message;
            this._severity = severity;
            this._startChar = startChar;
            this._endChar = endChar;
        }

        /// <summary>
        /// Returns a new ArgumentError, telling the user that the wrong amount of arguments was specified, followed by the command's usage.
        /// </summary>
        /// <param name="expected">Num of expected params.</param>
        /// <param name="specified">Num of specified params.</param>
        /// <param name="atLeast">Whether the message should tell that "at least X were expected" or an exact amount.</param>
        /// <returns></returns>
        public static ArgumentError InvalidArgumentCount(int expected, int specified, bool atLeast = false)
        {
            var spec = specified == 1 ? " was" : " were";
            var least = atLeast ? "at least " : "";
            return new ArgumentError(-1, string.Format("Expected {2}{0}, but {1} specified.", Extensions.GetPlural(expected, "argument"), specified + spec, least), Severity.Usage, -1, -1);
        }

        /// <summary>
        /// Returns a new ArgumentError, telling the user that no argument was specified, followed by the command's usage.
        /// </summary>
        /// <returns></returns>
        public static ArgumentError InvalidArgumentCount()
        {
            return new ArgumentError(-1, "Expected arguments, but none were specified.", Severity.Usage, -1, -1);
        }

        /// <summary>
        /// Returns a new ArgumentError, telling the user that the specified arguments will be ignored by the command.
        /// </summary>
        /// <param name="additional">Whether it should mention "additional arguments" instead of ANY arguments.</param>
        /// <param name="startInd">Index to underline (for additional)</param>
        /// <returns></returns>
        public static ArgumentError ArgumentsNotUsed(bool additional = false, int startInd = -1)
        {
            var add = additional ? "Additional a" : "A";
            return new ArgumentError(startInd, string.Format("{0}rguments were specified, but will be ignored by the command.", add), Severity.Warning, -1, -1);
        }

        /// <summary>
        /// Returns a new ArgumentError, telling the user that the specified command as an argument is unknown.
        /// </summary>
        /// <param name="index">Index of the argument.</param>
        /// <returns></returns>
        public static ArgumentError CommandUnknown(int index)
        {
            return new ArgumentError(index, "The specified command is unknown.", Severity.Error, -1, -1);
        }

        /// <summary>
        /// Returns a new ArgumentError, telling the user that the specified argument uses an unknown value, and should select one from the known values list instead.
        /// </summary>
        /// <param name="index">Index of the argument.</param>
        /// <param name="name">Name of the argument to be specified. Usually "action".</param>
        /// <param name="newline">Whether to add a new line before the list of values.</param>
        /// <param name="known">List of "known values" to display.</param>
        /// <returns></returns>
        public static ArgumentError ArgumentUnknown(int index, string name, bool newline, params string[] known)
        {
            return new ArgumentError(index, string.Format("Unknown {0} specified.{1}Known values: {2}", name, newline ? "\n" : string.Empty, string.Join(", ", known)),
                Severity.Error, -1, -1);
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            var charinfo = " ";
            if (this.StartChar >= 0 || this.EndChar >= 0)
                charinfo = string.Format(" (char {0} - {1}) ", this.StartChar, this.EndChar);
            return string.Format("#{0}{1}{2}: {3}", this.Index, charinfo, this.Severity, this.Message);
        }
    }

    /// <summary>
    /// General severity. Used mainly in ArgumentError.
    /// </summary>
    public enum Severity
    {
        Error, Warning, Info,
        /// <summary>
        /// Should display the usage on Execute to tell the user the entire format is wrong.
        /// </summary>
        Usage
    }
}

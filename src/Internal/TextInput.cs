﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Basically an invisible TextBox that allows you to enter text into it and modify it.
    /// </summary>
    public class TextInput
    {
        /// <summary>
        /// Whether any text can be input into this TextInput.
        /// </summary>
        public bool Enabled = true;

        /// <summary>
        /// Whether newlines are supported by pressing enter.
        /// </summary>
        public bool Multiline;

        /// <summary>
        /// Gets or sets the currently input text, wheras newlines are returned as \n.
        /// </summary>
        public string Text
        {
            get
            {
                if (this._dirtyText)
                    this.UpdateText();
                return this._text ?? string.Empty;
            }
            set
            {
                this._text = value;
                this._dirtyText = false;
                this.Lines = value.Split('\n').ToList();
            }
        }

        private string _text;
        private int _length;
        private bool _dirtyLength;
        private bool _dirtyText;

        /// <summary>
        /// Gets the length of the text, newlines included.
        /// </summary>
        public int Length
        {
            get
            {
                if (this._dirtyLength)
                    this.UpdateLength();
                return _length;
            }
            set
            {
                this._length = value;
                this._dirtyLength = false;
            }
        }

        /// <summary>
        /// Gets or sets the current textbox contents.
        /// </summary>
        public List<string> Lines = new List<string>();

        /// <summary>
        /// The current index of the caret. Increases across new lines.
        /// </summary>
        private int _caretPosition = 0;

        /// <summary>
        /// The current index of the caret. Increases across new lines.
        /// </summary>
        public int CaretPosition
        {
            get { return this._caretPosition; }
            set
            {
                if (this._caretPosition == value)
                    return;

                this._caretPosition = Maths.Clamp(value, 0, this.Length);
            }
        }

        /// <summary>
        /// The current line number of the caret. 0-based.
        /// </summary>
        public int CaretLine
        {
            get { return PositionToLine(this.CaretPosition); }
            set { this.CaretPosition = PositionFromLine(value, this.CaretColumn); }
        }

        /// <summary>
        /// The current column number of the caret. 0-based.
        /// </summary>
        public int CaretColumn
        {
            get { return PositionToColumn(this.CaretPosition); }
            set { this.CaretPosition = PositionFromLine(this.CaretLine, value); }
        }

        /// <summary>
        /// Starting index of the selection.
        /// </summary>
        public int SelectionStartPosition = -1;

        /// <summary>
        /// The starting selection line number. 0-based.
        /// </summary>
        public int SelectionStartLine
        {
            get { return PositionToLine(this.SelectionStartPosition); }
            set { this.SelectionStartPosition = PositionFromLine(value, this.SelectionStartColumn); }
        }

        /// <summary>
        /// The starting selection column number. 0-based.
        /// </summary>
        public int SelectionStartColumn
        {
            get { return PositionToColumn(this.SelectionStartPosition); }
            set { this.SelectionStartPosition = PositionFromLine(this.SelectionStartLine, value); }
        }

        /// <summary>
        /// Ending index of the selection.
        /// </summary>
        public int SelectionEndPosition = -1;

        /// <summary>
        /// The ending selection line number. 0-based.
        /// </summary>
        public int SelectionEndLine
        {
            get { return PositionToLine(this.SelectionEndPosition); }
            set { this.SelectionEndPosition = PositionFromLine(value, this.SelectionEndColumn); }
        }

        /// <summary>
        /// The ending selection column number. 0-based.
        /// </summary>
        public int SelectionEndColumn
        {
            get { return PositionToColumn(this.SelectionEndPosition); }
            set { this.SelectionEndPosition = PositionFromLine(this.SelectionEndLine, value); }
        }

        /// <summary>
        /// How long the text input may be. Use a value &lt;0 to allow input until your system breaks.
        /// </summary>
        public int MaxLength = -1;

        /// <summary>
        /// Run in order to detect keypresses for inserting new text or lines.
        /// <para />Does not handle anything positional (mouse clicks, dragging or the like).
        /// </summary>
        public void Update()
        {
            if (!this.Enabled)
                return;

            //NYI: TextInput - move caret when pressing arrow keys, select with shift+arrows and by-word with ctrl+arrows (mixed ctrl+shift+arrows too ofc)

            //Handles all kinds of keypresses
            if (Keyboard.Down(Keys.LeftControl) || Keyboard.Down(Keys.RightControl))
            {
                //Manage special hotkeys, such as ctrl+c, ctrl+v, ctrl+x and ctrl+a
                if (Keyboard.Pressed(Keys.C))
                {
                    //Copy selected to clipboard
                    var text = this.GetSelectedText();
                    if (text == null) //Copy all - or maybe just line the caret is on?
                        text = this.Text;
                    MserGlobals.SetClipboardText(text);
                }
                else if (Keyboard.Pressed(Keys.X))
                {
                    //Copy selection to clipboard, but remove selection too
                    var text = this.GetSelectedText();
                    if (text == null)
                    {
                        //Cut all - or maybe just line the caret is on?
                        text = this.Text;
                        this.Clear();
                    }
                    else
                    {
                        //Cut selected
                        this.RemoveSelectedText();
                    }
                    MserGlobals.SetClipboardText(text);
                }
                else if (Keyboard.Pressed(Keys.V))
                {
                    //Paste from clipboard
                    this.InsertText(MserGlobals.GetClipboardText());
                }
                else if (Keyboard.Pressed(Keys.A))
                {
                    //Select all
                    this.SelectAll();
                }
            }
            else
            {
                //Get pressed key mask
                var pressed = KeyLayoutManager.GetPressedKeys();
                if (pressed.IsPressed(Keys.Back))
                {
                    if (this.HasSelection())
                    {
                        //Remove selected text
                        this.RemoveSelectedText();
                    }
                    else
                    {
                        //Delete char before cursor
                        this.RemoveText(this.CaretPosition - 1, this.CaretPosition, true);
                    }
                }
                else if (pressed.IsPressed(Keys.Delete))
                {
                    if (this.HasSelection())
                    {
                        //Remove selected text
                        this.RemoveSelectedText();
                    }
                    else
                    {
                        //Delete char after cursor
                        this.RemoveText(this.CaretPosition, this.CaretPosition + 1, true);
                    }
                }

                //Simply append to text at caret position
                var str = pressed.KeyMaskToString();
                if (!string.IsNullOrEmpty(str))
                {
                    this.InsertText(str);
                }
            }
        }

        /// <summary>
        /// Clears the entire textbox.
        /// </summary>
        public void Clear()
        {
            this.Lines.Clear();
            this.Text = string.Empty;
        }

        /// <summary>
        /// Returns the currently selected text, with newline characters.
        /// <para />If nothing is selected, returns null.
        /// </summary>
        /// <returns></returns>
        public string GetSelectedText()
        {
            if (!this.HasSelection())
                return null;

            return this.GetText(this.SelectionStartPosition, this.SelectionEndPosition);
        }

        /// <summary>
        /// Returns the specified range of text, with newline characters.
        /// </summary>
        /// <param name="startpos"></param>
        /// <param name="endpos"></param>
        /// <returns></returns>
        public string GetText(int startpos, int endpos)
        {
            return this.Text.Substring(startpos, endpos - startpos);
        }

        /// <summary>
        /// Returns the specified range of text, with newline characters.
        /// </summary>
        /// <param name="startline"></param>
        /// <param name="endline"></param>
        /// <returns></returns>
        public string GetLines(int startline, int endline)
        {
            return GetText(this.PositionFromLine(startline), this.PositionFromLine(endline));
        }

        /// <summary>
        /// Removes the text in the specified position range.
        /// </summary>
        /// <param name="startpos"></param>
        /// <param name="endpos"></param>
        /// <param name="moveCaret">Whether to move the caret.</param>
        public void RemoveText(int startpos, int endpos, bool moveCaret)
        {
            if (startpos == endpos || startpos < 0 || endpos < 0 || startpos > this.Length || endpos > this.Length)
                return;

            if (endpos < startpos)
            {
                //Swap em!
                var old = startpos;
                startpos = endpos;
                endpos = old;
            }

            var startln = PositionToLine(startpos);
            var endln = PositionToLine(endpos);
            var startcol = PositionToColumn(startpos);
            var endcol = PositionToColumn(endpos);

            //Single-line?
            if (!this.Multiline || startln == endln)
            {
                //Remove based on column positions
                this.Lines.SetElement(startln, this.Lines.GetElement(startln).Substring(startcol, endcol - startcol), string.Empty);

                //Move caret?
                if (moveCaret && this.CaretLine == startln && this.CaretColumn > startcol)
                {
                    if (this.CaretColumn < endcol)
                    {
                        this.CaretColumn = startcol;
                    }
                    else
                    {
                        this.CaretColumn = startcol + (this.CaretColumn - endcol);
                    }
                }

                //Update length
                this.Length -= endcol - startcol;
                this._dirtyLength = false;
                return;
            }

            //Remove start bit
            this.Lines.SetElement(startln, this.Lines.GetElement(startln).SafeSubstring(0, startcol), string.Empty);

            //Remove any inbetween
            if (endln - startln > 1)
            {
                this.Lines.RemoveRange(startln + 1, endln - startln - 1);
            }

            //Remove end bit
            this.Lines.SetElement(endln, this.Lines.GetElement(endln).SafeSubstring(endcol), string.Empty);

            this._dirtyLength = true;
        }

        /// <summary>
        /// Removes the currently selected text.
        /// </summary>
        public void RemoveSelectedText()
        {
            if (!this.HasSelection())
                return;

            this.RemoveText(this.SelectionStartPosition, this.SelectionEndPosition, false);
            this.CaretPosition = this.SelectionStartPosition;
            this.Deselect();
        }

        /// <summary>
        /// Inserts text at the caret position, removing any selected text first.
        /// </summary>
        /// <param name="text"></param>
        public void InsertText(string text)
        {
            this.InsertText(text, this.CaretLine, this.CaretColumn, true, true);
        }

        /// <summary>
        /// Inserts text at the specified position.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="line"></param>
        /// <param name="column"></param>
        /// <param name="moveCaret">Whether the caret should be moved to the end of the inputted text.</param>
        /// <param name="removeSelected">Whether to remove any selected text first. Used for manual input.
        /// <para />This may move the caret regardless of the before-set option.</param>
        public void InsertText(string text, int line, int column, bool moveCaret, bool removeSelected)
        {
            if (text.Length == 0 || (this.MaxLength > -1 && (text.Length > this.MaxLength || this.Length >= this.MaxLength)))
                return;

            var newlines = text.Contains("\n");
            if (newlines && !this.Multiline)
            {
                //Strip newlines if not allowed
                text = text.Replace('\n', ' ');
                newlines = false;
            }

            //NYI: TextInput - InsertText has to clamp text once MaxLength would be reached
            if (!newlines && this.SelectionStartLine == this.SelectionEndLine)
            {
                if (removeSelected)
                {
                    //Remove selection
                    this.RemoveSelectedText();
                }

                //Add whatever was typed
                this.Lines.SetElement(line, this.Lines.GetElement(line, string.Empty).Insert(column, text), string.Empty);
                this._dirtyText = true;

                //Update length
                this.Length += text.Length;
                this._dirtyLength = false;

                if (moveCaret)
                {
                    //Repos caret
                    this.CaretLine = line;
                    this.CaretColumn = column + text.Length;
                }
            }
            else
            {
                var newlinecnt = text.CountCharOccurences('\n');

                if (removeSelected)
                {
                    //Remove selection
                    this.RemoveSelectedText();
                }

                //Add whatever was typed
                this.Text = this.Text.Insert(this.PositionFromLine(line, column), text);
                this._dirtyText = false;

                //Update length
                this.Length += text.Length;
                this._dirtyLength = false;

                if (moveCaret)
                {
                    //NYI: TextInput - move caret correctly with multiple lines
                    if (newlines)
                    {
                        //Pressed enter at least once, move to start
                        this.CaretLine = line + newlinecnt;
                        this.CaretColumn = 0;
                    }
                }
            }
        }

        /// <summary>
        /// Creates a new TextInput object. Works like a TextBox except not visible and without much interaction.
        /// </summary>
        /// <param name="multiline"></param>
        public TextInput(bool multiline = false)
        {
            this.Multiline = multiline;
        }

        /// <summary>
        /// Selects the entire input text.
        /// </summary>
        public void SelectAll()
        {
            this.SelectionStartPosition = 0;
            this.SelectionEndPosition = this.Length;
        }

        /// <summary>
        /// Clears any selection.
        /// </summary>
        public void Deselect()
        {
            this.SelectionStartPosition = -1;
            this.SelectionEndPosition = -1;
        }

        /// <summary>
        /// Returns whether any text is selected.
        /// </summary>
        public bool HasSelection()
        {
            return this.SelectionStartPosition != -1 && this.SelectionEndPosition != -1;
        }

        /// <summary>
        /// Returns the 0-based line number given the specified position in the string.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public int PositionToLine(int position)
        {
            position = Math.Max(0, position);
            if (position == 0)
                return 0;

            if (position > 0)
            {
                //Calculate linenum
                var lnum = 0;
                foreach (var len in this.Lines.Select(s => s.Length))
                {
                    //Position is on this line? Return!
                    if (position <= len)
                        return lnum;

                    //Position is on one of next lines (removing + 1 because else it would not count newline chars!)
                    position -= len + 1;
                    lnum++;
                }
            }
            //throw new ArgumentOutOfRangeException("position", "Specified position value extends beyond lines or is <0.");
            return this.Lines.Count - 1;
        }

        /// <summary>
        /// Returns the 0-based column number given the specified position in the string.
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public int PositionToColumn(int position)
        {
            position = Math.Max(0, position);
            if (position == 0)
                return 0;

            if (position > 0)
            {
                //Calculate colnum
                foreach (var len in this.Lines.Select(s => s.Length))
                {
                    //Position is on this line? Return!
                    if (position <= len)
                        return position;

                    //Position is on one of next lines (removing + 1 because else it would not count newline chars!)
                    position -= len + 1;
                }
            }
            //throw new ArgumentOutOfRangeException("position", "Specified position value extends beyond lines or is <0.");
            if (!this.Lines.Any())
                return 0;
            return this.Lines.Last().Length;
        }

        /// <summary>
        /// Returns the position given the specified 0-based line and column number.
        /// </summary>
        /// <param name="line"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        public int PositionFromLine(int line, int column = 0)
        {
            //if (line < 0 || line >= this.Lines.Length)
            //    throw new ArgumentOutOfRangeException("line", "Specified line value extends beyond line count or is <0.");
            //
            //if (column < 0 || column > this.Lines[line].Length)
            //    throw new ArgumentOutOfRangeException("column", "Specified column value extends beyond line or is <0.");

            if (line == 0)
                return column;

            line = Maths.Clamp(line, 0, this.Lines.Count - 1);
            column = Maths.Clamp(column, 0, this.Lines.GetElement(line, string.Empty).Length);

            //TODO: TextInput - as above, check for off-by-one with newlines
            //Get all lines, sum lengths, add column
            return this.Lines.GetRange(0, line + 1).Sum(s => s.Length + 1) - 1 + column;
        }

        /// <summary>
        /// Updates the text box's Length property.
        /// </summary>
        private void UpdateLength()
        {
            _dirtyLength = false;

            var reg = MserDebug.StartProfiling("UpdateLengthTest");
            this._length = this.Lines.Sum(s => s.Length) + this.Lines.Count - 1;
            MserDebug.StepProfiling(reg, "sum " + this._length);
            this._length = this.Text.Length;
            MserDebug.StepProfiling(reg, "text " + this._length);
            MserDebug.EndProfiling(reg);
        }

        /// <summary>
        /// Updates the text box's Text property.
        /// </summary>
        private void UpdateText()
        {
            _dirtyText = false;

            if (!this.Lines.Any())
            {
                this._text = string.Empty;
                return;
            }

            this._text = string.Join("\n", this.Lines);
        }
    }
}

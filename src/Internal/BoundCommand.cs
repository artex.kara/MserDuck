﻿using System.Collections.Generic;
using System.Linq;
using DuckGame.MserDuck.CommandHandlers;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// A command specified using /bind.
    /// </summary>
    public class BoundCommand
    {
        /// <summary>
        /// Instance of one or multiple commands.
        /// </summary>
        public readonly List<CommandHandler> CommandList = new List<CommandHandler>();

        /// <summary>
        /// The list of arguments passed to the command.
        /// </summary>
        public List<string> Arguments { get; set; }

        /// <summary>
        /// The key to trigger the bind. Use Keys.None for storing any regular command.
        /// </summary>
        public Keys Key { get; set; }

        /// <summary>
        /// For autoexec commands, the line number it was found in.
        /// </summary>
        public int Line { get; set; }

        private int _interval = -1;

        /// <summary>
        /// Timer interval for command. Keep to -1 if you don't need this behaviour.
        /// </summary>
        public int Interval
        {
            get { return this._interval; }
            set { this._interval = value; }
        }

        /// <summary>
        /// Ticking timer variable thing for HoldBinds.
        /// </summary>
        public int Timer;

        /// <summary>
        /// Name, for use in aliases instead of the Key value.
        /// </summary>
        public string Name;

        /// <summary>
        /// Returns the current KeysState of this bind's key.
        /// </summary>
        /// <returns></returns>
        public InputState GetBindKeyState()
        {
            return BindManager.GetKeyState(this.Key);
        }

        /// <summary>
        /// Constructor for single command bind.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        /// <param name="key"></param>
        /// <param name="line"></param>
        public BoundCommand(CommandHandler command, List<string> args, Keys key = Keys.None, int line = -1)
        {
            this.CommandList.Add(command);
            this.Arguments = args;
            this.Key = key;
            this.Line = line;
        }

        /// <summary>
        /// Constructor for multiple commands bind.
        /// </summary>
        /// <param name="commands"></param>
        /// <param name="args"></param>
        /// <param name="key"></param>
        public BoundCommand(IEnumerable<CommandHandler> commands, List<string> args, Keys key = Keys.None)
        {
            this.CommandList.AddRange(commands);
            this.Arguments = args;
            this.Key = key;
        }

        /// <summary>
        /// Returns a string that represents the current object. Can instantly be used as a chat message, if needed, since it includes a slash.
        /// <para />ArgumentErrors are still checked for here.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            return this.ToString(true, false);
        }

        /// <summary>
        /// Returns a string that represents the current object. Can instantly be used as a chat message, if needed (only returning the first entry).
        /// <para />Returns null if no command is specified.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        /// <param name="includeSlash">Whether the / should be included.</param>
        /// <param name="forced">Whether to do argumenterror checks. Includes _force in the name</param>
        public string ToString(bool includeSlash, bool forced)
        {
            var args = "";
            var slash = includeSlash ? "/" : "";

            if (this.Arguments != null)
            {
                args = " " + string.Join(" ", this.Arguments);
            }

            if (this.CommandList.Count == 0)
                return null;

            return string.Format("{2}{0}{1}", this.CommandList.First().GetNames()[0] + (forced ? "_force" : string.Empty), args, slash);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// A collection of helper methods and mod state fields for use when working with DuckGame and MserDuck.
    /// <para />For more general-use methods, you may want to check <see cref="Extensions"/>.
    /// <para />The class has another part (<code>MserGlobalsReflection.cs</code>) which contains code related to reflection.
    /// </summary>
    public static partial class MserGlobals
    {
        /// <summary>
        /// All generic flags (Instance | Public | NonPublic | Static)
        /// </summary>
        public const BindingFlags GeneralFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static;

        /// <summary>
        /// All instance-only flags (Instance | Public | NonPublic)
        /// </summary>
        public const BindingFlags InstanceFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;

        /// <summary>
        /// Whether UFF Mod is installed. Only updated once a failed reflection operation occured, do not use this to be sure of it!
        /// </summary>
        private static bool _isUffInstalled = true;

        /// <summary>
        /// List of MserDuck's namespaces.
        /// </summary>
        public static readonly IEnumerable<string> Namespaces = new List<string> { "DuckGame.MserDuck" }
            .Concat(GetNamespaces(typeof(MserDuck).Assembly).OrderBy(s => s)).Distinct();
        //Forcing DG.MD to be first to make foreach loops faster, since most stuff is in that namespace.
        //Concat with other namespaces gotten from type list, ordered alphabetically to avoid IniParser stuff in the middle
        //and finally only keeping distinct for obvious reasons.

        /// <summary>
        /// Returns the list of namespaces used in the specified assembly.
        /// <para />This list is not ordered explicitly, so be sure to do so yourself if needed.
        /// </summary>
        /// <param name="ass"></param>
        /// <returns></returns>
        public static IEnumerable<string> GetNamespaces(Assembly ass)
        {
            return ass.GetTypes().Select(t => t.Namespace).Where(n => !string.IsNullOrWhiteSpace(n)).Distinct();
        }

        private static Assembly _uffAssembly;

        /// <summary>
        /// Returns the specified type of the DuckGame (or mods like MserDuck/UFF) assembly. Useful if the source type does not allow access otherwise.
        /// <para />Be sure to store these in - if context allows it - <code>private static readonly</code> fields with a prefix or suffix.
        /// </summary>
        /// <param name="type">The name of the type to get.</param>
        /// <param name="ignoreCase">Whether the case of the member name should be checked.</param>
        /// <param name="mods">Should try getting from mod assemblies before going for DuckGame's types.</param>
        /// <returns></returns>
        public static Type GetType(string type, bool ignoreCase = true, bool mods = true)
        {
            if (string.IsNullOrWhiteSpace(type))
                return null;

            //Check cache first
            Type typeres;
            if (CacheManager.CachedTypes.TryGetValue(type, out typeres))
                return typeres;

            //Add to cache
            if (mods)
            {
                var mdassembly = typeof(MserDuck).Assembly;

                //Try each namespace
                foreach (var ns in Namespaces)
                {
                    var mdtype = mdassembly.GetType(ns + "." + type, false, ignoreCase);
                    if (mdtype != null)
                    {
                        CacheManager.CachedTypes.Add(type, mdtype);
                        return mdtype;
                    }
                }

                //Only try going for UFF's assembly if it counts as installed, to avoid crashes otherwise.
                //TODO: MserGlobals - GetType should iterate through a list of mod assemblies, instead of hardcoding assembly names like this
                if (MserGlobals._isUffInstalled)
                {
                    try
                    {
                        if (_uffAssembly == null)
                            _uffAssembly = Assembly.Load("UFFMod, Version=1.1.0.0, Culture=neutral, PublicKeyToken=null");
                        var ufftype = _uffAssembly.GetType("DuckGame.UFFMod." + type, false, ignoreCase);
                        if (ufftype != null)
                        {
                            CacheManager.CachedTypes.Add(type, ufftype);
                            return ufftype;
                        }
                    }
                    catch
                    {
                        //UFF is not installed
                        MserGlobals._isUffInstalled = false;
                    }
                }
            }

            var assembly = typeof(Level).Assembly; //Level can be substituted with any public class from DuckGame.
            if (assembly == null)
                throw new ContextMarshalException("Duck Game Assembly could not be determined.");

            var reftype = assembly.GetType("DuckGame." + type, false, ignoreCase);
            if (reftype != null)
                CacheManager.CachedTypes.Add(type, reftype);
            return reftype;
        }

        /// <summary>
        /// Returns the first member of the specified name from the named type of the DuckGame assembly. Useful if private or internal is used.
        /// <para />Use .Invoke to call a returned static method or .GetValue / .SetValue for properties and fields.
        /// <para />Be sure to store these in - if context allows it - <code>private static readonly</code> fields with a prefix or suffix.
        /// </summary>
        /// <param name="memberkind">What kind of member should be returned.</param>
        /// <param name="type">The type to get the member from.</param>
        /// <param name="membername">The name of the member to get.</param>
        /// <param name="ignoreCase">Whether the case of the member name should be checked.</param>
        /// <returns></returns>
        private static object GetMember(string type, AccessMember memberkind, string membername, bool ignoreCase = true)
        {
            return MserGlobals.GetMember(MserGlobals.GetType(type), memberkind, membername, ignoreCase);
        }

        /// <summary>
        /// Returns the first member of the specified name from the specified type of the DuckGame assembly. Useful if private or internal is used.
        /// <para />Use .Invoke to call a returned static method or .GetValue / .SetValue for properties and fields.
        /// <para />Be sure to store these in - if context allows it - <code>private static readonly</code> fields with a prefix or suffix.
        /// </summary>
        /// <param name="memberkind">What kind of member should be returned.</param>
        /// <param name="type">The type to get the member from.</param>
        /// <param name="membername">The name of the member to get.</param>
        /// <param name="ignoreCase">Whether the case of the member name should be checked.</param>
        /// <returns></returns>
        private static MemberInfo GetMember(Type type, AccessMember memberkind, string membername, bool ignoreCase = true)
        {
            //TODO: MserGlobals - merge the three dict cases since all use MemberInfo
            //  Instead set one local var to the Dictionary of ... depending on the enum value
            //  and another for the list of methods/... to get for null values

            CacheManager.MemberInfoCache cache;
            if (!CacheManager.MemberInfoCaches.ContainsKey(type.Name))
            {
                //New cache
                cache = new CacheManager.MemberInfoCache();
                CacheManager.MemberInfoCaches.Add(type.Name, cache);
            }
            else
            {
                //Get existing
                cache = CacheManager.MemberInfoCaches[type.Name];
            }

            switch (memberkind)
            {
                case (AccessMember.Method):
                    if (!ignoreCase)
                    {
                        //CaseSens
                        if (cache.Methods == null)
                        {
                            cache.Methods = type.GetMethods(MserGlobals.GeneralFlags).ToSafeDictionary(i => i.Name);
                        }
                        MethodInfo info;
                        if (cache.Methods.TryGetValue(membername, out info))
                            return info;
                        return null;
                    }

                    //CaseInsens
                    if (cache.MethodsLowercase == null)
                    {
                        cache.MethodsLowercase = type.GetMethods(MserGlobals.GeneralFlags).ToSafeDictionary(i => i.Name.ToLowerInvariant());
                    }
                    MethodInfo infol;
                    if (cache.MethodsLowercase.TryGetValue(membername.ToLowerInvariant(), out infol))
                        return infol;
                    return null;
                case (AccessMember.Property):
                    if (!ignoreCase)
                    {
                        //CaseSens
                        if (cache.Properties == null)
                        {
                            cache.Properties = type.GetProperties(MserGlobals.GeneralFlags).ToSafeDictionary(i => i.Name);
                        }
                        PropertyInfo info;
                        if (cache.Properties.TryGetValue(membername, out info))
                            return info;
                        return null;
                    }

                    //CaseInsens
                    if (cache.PropertiesLowercase == null)
                    {
                        cache.PropertiesLowercase = type.GetProperties(MserGlobals.GeneralFlags).ToSafeDictionary(i => i.Name.ToLowerInvariant());
                    }
                    PropertyInfo infol2;
                    if (cache.PropertiesLowercase.TryGetValue(membername.ToLowerInvariant(), out infol2))
                        return infol2;
                    return null;
                case (AccessMember.Field):
                    if (!ignoreCase)
                    {
                        //CaseSens
                        if (cache.Fields == null)
                        {
                            cache.Fields = type.GetFields(MserGlobals.GeneralFlags).ToSafeDictionary(i => i.Name);
                        }
                        FieldInfo info;
                        if (cache.Fields.TryGetValue(membername, out info))
                            return info;
                        return null;
                    }

                    //CaseInsens
                    if (cache.FieldsLowercase == null)
                    {
                        cache.FieldsLowercase = type.GetFields(MserGlobals.GeneralFlags).ToSafeDictionary(i => i.Name.ToLowerInvariant());
                    }
                    FieldInfo infol3;
                    if (cache.FieldsLowercase.TryGetValue(membername.ToLowerInvariant(), out infol3))
                        return infol3;
                    return null;
                default:
                    throw new ArgumentOutOfRangeException("memberkind", "Invalid AccessMember specified.");
            }
        }

        /// <summary>
        /// Returns the first field information of the specified type and name.
        /// <para />Be sure to store these in - if context allows it - <code>private static readonly</code> fields with a prefix or suffix.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="membername"></param>
        /// <param name="ignoreCase">Whether the case of the member name should be checked.</param>
        /// <returns></returns>
        public static FieldInfo GetField(string type, string membername, bool ignoreCase = true)
        {
            return (FieldInfo)MserGlobals.GetMember(type, AccessMember.Field, membername, ignoreCase);
        }

        /// <summary>
        /// Returns the first field information of the specified type and name.
        /// <para />Be sure to store these in - if context allows it - <code>private static readonly</code> fields with a prefix or suffix.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="membername"></param>
        /// <param name="ignoreCase">Whether the case of the member name should be checked.</param>
        /// <returns></returns>
        public static FieldInfo GetField(Type type, string membername, bool ignoreCase = true)
        {
            return (FieldInfo)MserGlobals.GetMember(type, AccessMember.Field, membername, ignoreCase);
        }

        /// <summary>
        /// Returns the first method information of the specified type and name.
        /// <para />Be sure to store these in - if context allows it - <code>private static readonly</code> fields with a prefix or suffix.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="membername"></param>
        /// <param name="ignoreCase">Whether the case of the member name should be checked.</param>
        /// <returns></returns>
        public static MethodInfo GetMethod(string type, string membername, bool ignoreCase = true)
        {
            return (MethodInfo)MserGlobals.GetMember(type, AccessMember.Method, membername, ignoreCase);
        }

        /// <summary>
        /// Returns the first method information of the specified type and name.
        /// <para />Be sure to store these in - if context allows it - <code>private static readonly</code> fields with a prefix or suffix.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="membername"></param>
        /// <param name="ignoreCase">Whether the case of the member name should be checked.</param>
        /// <returns></returns>
        public static MethodInfo GetMethod(Type type, string membername, bool ignoreCase = true)
        {
            return (MethodInfo)MserGlobals.GetMember(type, AccessMember.Method, membername, ignoreCase);
        }

        /// <summary>
        /// Returns the first property information of the specified type and name.
        /// <para />Be sure to store these in - if context allows it - <code>private static readonly</code> fields with a prefix or suffix.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="membername"></param>
        /// <param name="ignoreCase">Whether the case of the member name should be checked.</param>
        /// <returns></returns>
        public static PropertyInfo GetProperty(string type, string membername, bool ignoreCase = true)
        {
            return (PropertyInfo)MserGlobals.GetMember(type, AccessMember.Property, membername, ignoreCase);
        }

        /// <summary>
        /// Returns the first property information of the specified type and name.
        /// <para />Be sure to store these in - if context allows it - <code>private static readonly</code> fields with a prefix or suffix.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="membername"></param>
        /// <param name="ignoreCase">Whether the case of the member name should be checked.</param>
        /// <returns></returns>
        public static PropertyInfo GetProperty(Type type, string membername, bool ignoreCase = true)
        {
            return (PropertyInfo)MserGlobals.GetMember(type, AccessMember.Property, membername, ignoreCase);
        }
    }
}

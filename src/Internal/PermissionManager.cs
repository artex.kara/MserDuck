﻿using System;
using System.Collections.Generic;
using System.Linq;
using DuckGame.MserDuck.CommandHandlers;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Manager for different permissions set in roles, their assigned users and commands.
    /// </summary>
    public static class PermissionManager
    {
        /// <summary>
        /// List of unique known roles.
        /// </summary>
        public static readonly List<PermissionRole> Roles = MserGlobals.GetSubInstances<PermissionRole>(); 

        /// <summary>
        /// List of users assigned to their roles. The key of this dictionary is the steam ID to uniquely identify each profile.
        /// </summary>
        public static readonly Dictionary<ulong, PermissionRole> AssignedUsers = new Dictionary<ulong, PermissionRole>();

        /// <summary>
        /// Returns the role that this user is assigned to.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static PermissionRole GetRole(Profile user)
        {
            //Server Role
            if (MserGlobals.IsHosting(user))
            {
                return Roles.OfType<PermissionRoleAdmin>().Single();
            }

            //User's Role
            PermissionRole userrole;
            if (AssignedUsers.TryGetValue(user.steamID, out userrole))
            {
                return userrole;
            }

            //Default Role (fallback)
            return Roles.OfType<PermissionRoleDefault>().Single();
        }

        /// <summary>
        /// Returns the role with the specified name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static PermissionRole GetRoleByName(string name)
        {
            return Roles.FirstOrDefault(r => r.Name.EqualsTo(true, name));
        }

        /// <summary>
        /// Returns the users that are part of the specified role.
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static IEnumerable<Profile> GetUsersOfRole(PermissionRole role)
        {
            return AssignedUsers.Where(kv => kv.Value == role).Select(kv => MserGlobals.GetProfileFromSteamID(kv.Key, false));
        }

        /// <summary>
        /// Returns the users that are part of the specified role.
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static IEnumerable<Profile> GetUsersOfRole(string role)
        {
            return GetUsersOfRole(GetRoleByName(role));
        }

        /// <summary>
        /// Returns the steam ids of all users that are part of the specified role.
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static IEnumerable<ulong> GetUserIdsOfRole(PermissionRole role)
        {
            return AssignedUsers.Where(kv => kv.Value == role).Select(kv => kv.Key);
        }

        /// <summary>
        /// Returns the steam ids of all users that are part of the specified role.
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static IEnumerable<ulong> GetUserIdsOfRole(string role)
        {
            return GetUserIdsOfRole(GetRoleByName(role));
        }

        /// <summary>
        /// Whether the specified user is permitted to execute the specified handler.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="handler"></param>
        /// <returns></returns>
        public static bool IsPermittedToExecute(Profile user, CommandHandler handler)
        {
            return GetRole(user).IsPermittedToExecute(handler);
        }

        /// <summary>
        /// Whether the local player has the specified permission.
        /// </summary>
        /// <param name="permission"></param>
        /// <returns></returns>
        public static bool HasPermission(string permission)
        {
            return MserGlobals.GetLocalDuck() != null &&
                PermissionManager.GetRole(MserGlobals.GetLocalDuck().profile)
                .HasPermission(permission);
        }

        /// <summary>
        /// Adds the specified user to the specified role.
        /// <para />Users can only be part of one role at a time, meaning this will remove the user from any other role they were a part of.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="role"></param>
        /// <param name="notifyUser">Whether user's notified of this change of role.</param>
        /// <exception cref="ArgumentException">The specified role is not part of the list of roles.</exception>
        public static void AddUserToRole(Profile user, PermissionRole role, bool notifyUser)
        {
            AddUserToRole(user.steamID, role, notifyUser);
        }

        /// <summary>
        /// Adds the specified user to the specified role.
        /// <para />Users can only be part of one role at a time, meaning this will remove the user from any other role they were a part of.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="role"></param>
        /// <param name="notifyUser">Whether user's notified of this change of role.</param>
        /// <exception cref="ArgumentException">The specified role is not part of the list of roles.</exception>
        public static void AddUserToRole(ulong user, PermissionRole role, bool notifyUser)
        {
            //Add role if needed (shouldnt be i dont think)
            if (!Roles.Contains(role))
                throw new ArgumentException("The specified role is not part of the list of roles.");

            //Check if user is part of dict
            var id = user;
            if (AssignedUsers.ContainsKey(id))
            {
                AssignedUsers[id] = role;
            }
            else
            {
                AssignedUsers.Add(id, role);
            }

            if (!notifyUser)
                return;

            var pro = MserGlobals.GetProfileFromSteamID(user, true);
            if (pro != null && pro.localPlayer)
            {
                //Do a notificate
                ChatManager.Echo(string.Format("You are now part of the role \"{0}\".", role.Name));
            }
        }

        /// <summary>
        /// Creates a new role with the specified name.
        /// <para />Use <code>AddUserToRole</code> to add users to it.
        /// </summary>
        /// <param name="name">Display name of this role. Has to be unique.</param>
        /// <param name="color">A color to uniquely identify this role.</param>
        /// <exception cref="ArgumentException">A role with the specified name already exists.</exception>
        /// <returns></returns>
        public static PermissionRole CreateRole(string name, Color color)
        {
            if (Roles.Any(r => r.Name.EqualsTo(true, name)))
                throw new ArgumentException("A role with the specified name already exists.");

            var role = new PermissionRole(name, color);
            Roles.Add(role);
            return role;
        }
    }
}

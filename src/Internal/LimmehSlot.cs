namespace DuckGame.MserDuck
{
    public class LimmehSlot
    {
        public readonly Duck Duck;

        public readonly LimmehDevice Device;

        public LimmehSlot(Duck duck, LimmehDevice device)
        {
            this.Duck = duck;
            this.Device = device;
        }

        public void Initialize()
        {
            Level.Add(this.Duck);
            Level.Add(this.Device);
        }
    }
}
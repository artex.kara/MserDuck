﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.Xna.Framework.Input;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Manages different key layouts and their resulting keys when pressed.
    /// </summary>
    public static class KeyLayoutManager
    {
        /// <summary>
        /// Currently selected keyboard layout for input. Set using <see cref="ConfigEntries.LayoutEntry"/> in config.
        /// </summary>
        public static string Layout = "en-US";

        /// <summary>
        /// List of valid layout names. Note that DETECT is only valid as a config entry, not as a layout value.
        /// </summary>
        public static readonly List<string> LayoutsList = new List<string> {"de-DE", "pt-PT", "en-US", "DETECT"};
        
        internal static void Initialize()
        {
            //Detect keyboard layout
            KeyLayoutManager.Layout = KeyLayoutManager.GetDetectedKeyLayout();
        }

        /// <summary>
        /// Returns the keyboard layout used by the operating system.
        /// </summary>
        /// <returns></returns>
        public static string GetDetectedKeyLayout()
        {
            return InputLanguage.CurrentInputLanguage.Culture.Name;
        }

        /// <summary>
        /// Returns the char that would be typed if the specified Key was pressed.
        /// <para />This method returns a space char (' ') for unknown keys, so be sure to handle Spacebar separately if needed.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="caps"></param>
        /// <param name="shift"></param>
        /// <returns></returns>
        public static char ConvertKeyToChar(Keys key, bool caps = true, bool shift = false)
        {
            var ch = KeyToCharBase(KeyLayoutManager.Layout, key, caps, shift);
            return ch;
        }

        private static char KeyToCharBase(string layout, Keys key, bool caps, bool shift)
        {
            //Try "in common chars" first
            char ch = KeyToCharCommon(key, caps, shift);

            if (ch == ' ')
            {
                switch (layout)
                {
                    case "de-DE":
                        ch = KeyToCharGerman(key, caps, shift);
                        break;
                    case "pt-PT":
                        ch = KeyToCharPortuguese(key, caps, shift);
                        break;
                    //HINT: Add any new keyboard layouts here. Any missing characters should be added in ExtendedBitmapFont.cs
                    default:
                        ch = KeyToCharEnglish(key, caps, shift);
                        break;
                }
            }

            return ch;
        }

        private static char KeyToCharPortuguese(Keys key, bool caps, bool shift)
        {
            //Symbols and uppercase numbers : PT layout

            if (caps || !shift)
            {
                switch (key)
                {
                    case Keys.OemSemicolon:
                        return '´';
                    case Keys.OemPlus:
                        return '+';
                    case Keys.OemComma:
                        return ',';
                    case Keys.OemMinus:
                        return '-';
                    case Keys.OemPeriod:
                        return '.';
                    case Keys.OemQuestion:
                        return '~';
                    case Keys.OemTilde:
                        return 'ç';
                    case Keys.OemOpenBrackets:
                        return '\'';
                    case Keys.OemPipe:
                        return '\\';
                    case Keys.OemCloseBrackets:
                        return '«';
                    case Keys.OemQuotes:
                        return 'º';
                    case Keys.OemBackslash:
                        return '<';
                }
            }
            else
            {
                switch (key)
                {
                    case Keys.D0:
                        return '=';
                    case Keys.D1:
                        return '!';
                    case Keys.D2:
                        return '"';
                    case Keys.D3:
                        return '#';
                    case Keys.D4:
                        return '$';
                    case Keys.D5:
                        return '%';
                    case Keys.D6:
                        return '&';
                    case Keys.D7:
                        return '/';
                    case Keys.D8:
                        return '(';
                    case Keys.D9:
                        return ')';
                    case Keys.OemSemicolon:
                        return '`';
                    case Keys.OemPlus:
                        return '*';
                    case Keys.OemComma:
                        return ';';
                    case Keys.OemMinus:
                        return '_';
                    case Keys.OemPeriod:
                        return ':';
                    case Keys.OemQuestion:
                        return '^';
                    case Keys.OemTilde:
                        return 'Ç';
                    case Keys.OemOpenBrackets:
                        return '?';
                    case Keys.OemPipe:
                        return '|'; 
                    case Keys.OemCloseBrackets:
                        return '»';
                    case Keys.OemQuotes:
                        return 'ª';
                    case Keys.OemBackslash:
                        return '>';
                }
            }
            return ' ';
        }

        private static char KeyToCharGerman(Keys key, bool caps, bool shift)
        {
            //Symbols and uppercase numbers : DE layout

            if (caps || !shift)
            {
                switch (key)
                {
                    case Keys.OemSemicolon:
                        return '|'; // ü
                    case Keys.OemPlus:
                        return '+';
                    case Keys.OemComma:
                        return ',';
                    case Keys.OemMinus:
                        return '-';
                    case Keys.OemPeriod:
                        return '.';
                    case Keys.OemQuestion:
                        return '#';
                    case Keys.OemTilde:
                        return '['; // ö
                    case Keys.OemOpenBrackets:
                        return 'ß';
                    case Keys.OemPipe:
                        return '^'; // ^
                    case Keys.OemCloseBrackets:
                        return '\'';
                    case Keys.OemQuotes:
                        return ']'; // ä
                    case Keys.OemBackslash:
                        return '<';
                }
            }
            else
            {
                switch (key)
                {
                    case Keys.D0:
                        return '=';
                    case Keys.D1:
                        return '!';
                    case Keys.D2:
                        return '"';
                    case Keys.D3:
                        return '§';
                    case Keys.D4:
                        return '$';
                    case Keys.D5:
                        return '%';
                    case Keys.D6:
                        return '&';
                    case Keys.D7:
                        return '/';
                    case Keys.D8:
                        return '(';
                    case Keys.D9:
                        return ')';
                    case Keys.OemSemicolon:
                        return '\\'; // Ü
                    case Keys.OemPlus:
                        return '*';
                    case Keys.OemComma:
                        return ';';
                    case Keys.OemMinus:
                        return '_';
                    case Keys.OemPeriod:
                        return ':';
                    case Keys.OemQuestion:
                        return '\'';
                    case Keys.OemTilde:
                        return '{'; // Ö
                    case Keys.OemOpenBrackets:
                        return '?';
                    case Keys.OemPipe:
                        return '°'; // °
                    case Keys.OemCloseBrackets:
                        return '~';
                    case Keys.OemQuotes:
                        return '}'; // Ä
                    case Keys.OemBackslash:
                        return '>';
                }
            }
            return ' ';
        }

        private static char KeyToCharEnglish(Keys key, bool caps, bool shift)
        {
            //Symbols and uppercase numbers : EN-US layout

            if (caps || !shift)
            {
                switch (key)
                {
                    case Keys.OemSemicolon:
                        return ';';
                    case Keys.OemPlus:
                        return '=';
                    case Keys.OemComma:
                        return ',';
                    case Keys.OemMinus:
                        return '-';
                    case Keys.OemPeriod:
                        return '.';
                    case Keys.OemQuestion:
                        return '/';
                    case Keys.OemTilde:
                        return '~';
                    case Keys.OemOpenBrackets:
                        return '[';
                    case Keys.OemPipe:
                        return '\\';
                    case Keys.OemCloseBrackets:
                        return ']';
                    case Keys.OemQuotes:
                        return '\'';
                    case Keys.OemBackslash:
                        return '\\';
                }
            }
            else
            {
                switch (key)
                {
                    case Keys.D0:
                        return ')';
                    case Keys.D1:
                        return '!';
                    case Keys.D2:
                        return '@';
                    case Keys.D3:
                        return '#';
                    case Keys.D4:
                        return '$';
                    case Keys.D5:
                        return '%';
                    case Keys.D6:
                        return '^';
                    case Keys.D7:
                        return '&';
                    case Keys.D8:
                        return '*';
                    case Keys.D9:
                        return '(';
                    case Keys.OemSemicolon:
                        return ':';
                    case Keys.OemPlus:
                        return '+';
                    case Keys.OemComma:
                        return '<';
                    case Keys.OemMinus:
                        return '_';
                    case Keys.OemPeriod:
                        return '>';
                    case Keys.OemQuestion:
                        return '?';
                    case Keys.OemTilde:
                        return '~';
                    case Keys.OemOpenBrackets:
                        return '{';
                    case Keys.OemPipe:
                        return '|';
                    case Keys.OemCloseBrackets:
                        return '}';
                    case Keys.OemQuotes:
                        return '"';
                    case Keys.OemBackslash:
                        return '|';
                }
            }
            return ' ';
        }

        private static char KeyToCharCommon(Keys key, bool caps, bool shift)
        {
            //Excluding capitalized numbers and both lower and uppercase symbols

            if (caps)
            {
                switch (key)
                {
                    case Keys.D0:
                        return '0';
                    case Keys.D1:
                        return '1';
                    case Keys.D2:
                        return '2';
                    case Keys.D3:
                        return '3';
                    case Keys.D4:
                        return '4';
                    case Keys.D5:
                        return '5';
                    case Keys.D6:
                        return '6';
                    case Keys.D7:
                        return '7';
                    case Keys.D8:
                        return '8';
                    case Keys.D9:
                        return '9';
                    case (Keys)58:
                    case (Keys)59:
                    case (Keys)60:
                    case (Keys)61:
                    case (Keys)62:
                    case (Keys)63:
                    case (Keys)64:
                    case Keys.LeftWindows:
                    case Keys.RightWindows:
                    case Keys.Apps:
                    case (Keys)94:
                    case Keys.Sleep:
                        break;
                    case Keys.A:
                        return 'A';
                    case Keys.B:
                        return 'B';
                    case Keys.C:
                        return 'C';
                    case Keys.D:
                        return 'D';
                    case Keys.E:
                        return 'E';
                    case Keys.F:
                        return 'F';
                    case Keys.G:
                        return 'G';
                    case Keys.H:
                        return 'H';
                    case Keys.I:
                        return 'I';
                    case Keys.J:
                        return 'J';
                    case Keys.K:
                        return 'K';
                    case Keys.L:
                        return 'L';
                    case Keys.M:
                        return 'M';
                    case Keys.N:
                        return 'N';
                    case Keys.O:
                        return 'O';
                    case Keys.P:
                        return 'P';
                    case Keys.Q:
                        return 'Q';
                    case Keys.R:
                        return 'R';
                    case Keys.S:
                        return 'S';
                    case Keys.T:
                        return 'T';
                    case Keys.U:
                        return 'U';
                    case Keys.V:
                        return 'V';
                    case Keys.W:
                        return 'W';
                    case Keys.X:
                        return 'X';
                    case Keys.Y:
                        return 'Y';
                    case Keys.Z:
                        return 'Z';
                    case Keys.NumPad0:
                        return '0';
                    case Keys.NumPad1:
                        return '1';
                    case Keys.NumPad2:
                        return '2';
                    case Keys.NumPad3:
                        return '3';
                    case Keys.NumPad4:
                        return '4';
                    case Keys.NumPad5:
                        return '5';
                    case Keys.NumPad6:
                        return '6';
                    case Keys.NumPad7:
                        return '7';
                    case Keys.NumPad8:
                        return '8';
                    case Keys.NumPad9:
                        return '9';
                }
            }
            else if (shift)
            {
                switch (key)
                {
                    case (Keys)58:
                    case (Keys)59:
                    case (Keys)60:
                    case (Keys)61:
                    case (Keys)62:
                    case (Keys)63:
                    case (Keys)64:
                    case Keys.LeftWindows:
                    case Keys.RightWindows:
                    case Keys.Apps:
                    case (Keys)94:
                    case Keys.Sleep:
                        break;
                    case Keys.A:
                        return 'A';
                    case Keys.B:
                        return 'B';
                    case Keys.C:
                        return 'C';
                    case Keys.D:
                        return 'D';
                    case Keys.E:
                        return 'E';
                    case Keys.F:
                        return 'F';
                    case Keys.G:
                        return 'G';
                    case Keys.H:
                        return 'H';
                    case Keys.I:
                        return 'I';
                    case Keys.J:
                        return 'J';
                    case Keys.K:
                        return 'K';
                    case Keys.L:
                        return 'L';
                    case Keys.M:
                        return 'M';
                    case Keys.N:
                        return 'N';
                    case Keys.O:
                        return 'O';
                    case Keys.P:
                        return 'P';
                    case Keys.Q:
                        return 'Q';
                    case Keys.R:
                        return 'R';
                    case Keys.S:
                        return 'S';
                    case Keys.T:
                        return 'T';
                    case Keys.U:
                        return 'U';
                    case Keys.V:
                        return 'V';
                    case Keys.W:
                        return 'W';
                    case Keys.X:
                        return 'X';
                    case Keys.Y:
                        return 'Y';
                    case Keys.Z:
                        return 'Z';
                    case Keys.NumPad0:
                        return '0';
                    case Keys.NumPad1:
                        return '1';
                    case Keys.NumPad2:
                        return '2';
                    case Keys.NumPad3:
                        return '3';
                    case Keys.NumPad4:
                        return '4';
                    case Keys.NumPad5:
                        return '5';
                    case Keys.NumPad6:
                        return '6';
                    case Keys.NumPad7:
                        return '7';
                    case Keys.NumPad8:
                        return '8';
                    case Keys.NumPad9:
                        return '9';
                }
            }
            else
            {
                switch (key)
                {
                    case Keys.D0:
                        return '0';
                    case Keys.D1:
                        return '1';
                    case Keys.D2:
                        return '2';
                    case Keys.D3:
                        return '3';
                    case Keys.D4:
                        return '4';
                    case Keys.D5:
                        return '5';
                    case Keys.D6:
                        return '6';
                    case Keys.D7:
                        return '7';
                    case Keys.D8:
                        return '8';
                    case Keys.D9:
                        return '9';
                    case (Keys)58:
                    case (Keys)59:
                    case (Keys)60:
                    case (Keys)61:
                    case (Keys)62:
                    case (Keys)63:
                    case (Keys)64:
                    case Keys.LeftWindows:
                    case Keys.RightWindows:
                    case Keys.Apps:
                    case (Keys)94:
                    case Keys.Sleep:
                        break;
                    case Keys.A:
                        return 'a';
                    case Keys.B:
                        return 'b';
                    case Keys.C:
                        return 'c';
                    case Keys.D:
                        return 'd';
                    case Keys.E:
                        return 'e';
                    case Keys.F:
                        return 'f';
                    case Keys.G:
                        return 'g';
                    case Keys.H:
                        return 'h';
                    case Keys.I:
                        return 'i';
                    case Keys.J:
                        return 'j';
                    case Keys.K:
                        return 'k';
                    case Keys.L:
                        return 'l';
                    case Keys.M:
                        return 'm';
                    case Keys.N:
                        return 'n';
                    case Keys.O:
                        return 'o';
                    case Keys.P:
                        return 'p';
                    case Keys.Q:
                        return 'q';
                    case Keys.R:
                        return 'r';
                    case Keys.S:
                        return 's';
                    case Keys.T:
                        return 't';
                    case Keys.U:
                        return 'u';
                    case Keys.V:
                        return 'v';
                    case Keys.W:
                        return 'w';
                    case Keys.X:
                        return 'x';
                    case Keys.Y:
                        return 'y';
                    case Keys.Z:
                        return 'z';
                    case Keys.NumPad0:
                        return '0';
                    case Keys.NumPad1:
                        return '1';
                    case Keys.NumPad2:
                        return '2';
                    case Keys.NumPad3:
                        return '3';
                    case Keys.NumPad4:
                        return '4';
                    case Keys.NumPad5:
                        return '5';
                    case Keys.NumPad6:
                        return '6';
                    case Keys.NumPad7:
                        return '7';
                    case Keys.NumPad8:
                        return '8';
                    case Keys.NumPad9:
                        return '9';
                }
            }
            return ' ';
        }

        private static readonly FieldInfo ignoreCore = MserGlobals.GetField(typeof(Keyboard), "ignoreCore");
        private static readonly FieldInfo keyState = MserGlobals.GetField(typeof(Keyboard), "_keyState");

        /// <summary>
        /// Returns a KeyStateInfo about all currently pressed keys and key states.
        /// </summary>
        /// <returns></returns>
        public static KeyStateInfo GetPressedKeys()
        {
            ignoreCore.SetValue(null, true);
            var state = new KeyStateInfo();
            state.Shift = Keyboard.Down(Keys.LeftShift) || Keyboard.Down(Keys.RightShift);
            state.Capslock = Console.CapsLock;

            //Avoid nullref
            if (KeyboardInstance == null)
                KeyboardInstance = GetKeyboardInstance();
            if (KeyboardInstance == null)
                return default(KeyStateInfo);

            //Get keystate
            var st = (KeyboardState)keyState.GetValue(null);
            state.KeyList = st.GetPressedKeys().Where(key => KeyboardInstance.MapPressed((int)key)).Cast<Keys>();
            ignoreCore.SetValue(null, false);
            return state;
        }

        private static Keyboard KeyboardInstance;

        private static Keyboard GetKeyboardInstance()
        {
            return Input.GetDevice<Keyboard>();
        }
    }

    /// <summary>
    /// Contains all info about the currently pressed keys.
    /// </summary>
    public struct KeyStateInfo
    {
        /// <summary>
        /// Bit-mask of pressed keys.
        /// </summary>
        public IEnumerable<Keys> KeyList;

        /// <summary>
        /// Whether capslock is enabled.
        /// </summary>
        public bool Capslock;

        /// <summary>
        /// Whether shift is held.
        /// </summary>
        public bool Shift;

        /// <summary>
        /// Converts this KeyStateInfo's Keys bitmask to a string, ordered by the key scancodes.
        /// </summary>
        /// <returns></returns>
        public string KeyMaskToString()
        {
            if (!this.KeyList.Any())
                return string.Empty;

            var str = string.Empty;
            foreach (var key in this.KeyList)
            {
                if (key == Keys.Space)
                {
                    //Add space, since convert method doesnt support it.
                    str += ' ';
                }
                else if (key == Keys.Enter)
                {
                    //Add newline
                    str += '\n';
                }
                else
                {
                    var keych = KeyLayoutManager.ConvertKeyToChar(key, this.Capslock, this.Shift);
                    if (keych != ' ')
                    {
                        str += keych.ToString();
                    }
                }
            }
            return str;
        }

        /// <summary>
        /// Whether the specified key is pressed in this state info.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool IsPressed(Keys key)
        {
            return this.KeyList.Contains(key);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DuckGame.MserDuck.CommandHandlers;
using DuckGame.MserDuck.ConfigEntries;
using IniParser;
using IniParser.Model;

namespace DuckGame.MserDuck
{
    public static class ConfigManager
    {
        //IMP: ConfigManager - editing comments of a ConfigEntry (return value of GetDescription()) does not update the comment in the file

        /// <summary>
        /// List of commands to autoexecute.
        /// </summary>
        public static List<string> AutoexecutesList = new List<string>();

        /// <summary>
        /// Path to the config file.
        /// </summary>
        public const string configFile = "config.cfg";

        /// <summary>
        /// String to use as comment symbol(s).
        /// </summary>
        public const string commentString = "#";

        /// <summary>
        /// Whether the config file should get autosaved on close. Set using <see cref="ConfigEntries.AutoSaveEntry"/> in config.
        /// </summary>
        public static bool AutoSave = true;

        /// <summary>
        /// List of entries below the [Autoexec] line, to copy over when saving.
        /// </summary>
        private static List<string> _keptAutoexec = new List<string>();

        /// <summary>
        /// Writes the specified value to the config entry, writing to the config.cfg.
        /// </summary>
        /// <param name="entry"></param>
        /// <param name="value"></param>
        private static void WriteValue(ConfigEntry entry, object value)
        {
            var res = entry.WriteCategory(_configData);
            if (!res)
                return;

            if (value == null)
                return;

            if (_configData.Sections.AddSection(entry.GetCategory()))
            {
                if (entry.GetCategory() == "General")
                {
                    _configData.Sections.GetSectionData(entry.GetCategory()).LeadingComments.Add(" Config file for MserDuck. Now with auto-updating!");
                    _configData.Sections.GetSectionData(entry.GetCategory()).LeadingComments.Add(" Use a " + commentString + " at the start of lines for commenting.");
                }
            }

            var sec = _configData.Sections.GetSectionData(entry.GetCategory());
            if (sec.Keys.AddKey(entry.GetName(), entry.GetDefaultValue().ToString()))
            {
                //TODO: ConfigManager - A commented out key should not be recreated
                foreach (var comment in entry.GetDescription())
                {
                    sec.Keys.GetKeyData(entry.GetName()).Comments.Add(" " + comment);
                }
                if (entry.ShowDefaultValue())
                    sec.Keys.GetKeyData(entry.GetName()).Comments.Add(" Default Value: " + entry.GetDefaultValue().ToString());
            }
            else
            {
                //Simply set value
                sec.Keys.GetKeyData(entry.GetName()).Value = value.ToString();
            }
        }

        /// <summary>
        /// Reads the value of the config entry from the config.cfg, returning it.
        /// </summary>
        /// <param name="entry"></param>
        /// <param name="generate">Whether to allow generating new config data, if required.</param>
        /// <returns></returns>
        private static object ReadValue(ConfigEntry entry, bool generate = true)
        {
            var res = entry.ReadCategory(_configData);
            if (!res)
                return new NoValue();

            if (generate && _configData.Sections.AddSection(entry.GetCategory()))
            {
                if (entry.GetCategory() == "General")
                {
                    _configData.Sections.GetSectionData(entry.GetCategory()).LeadingComments.Add(" Config file for MserDuck. Now with auto-updating!");
                    _configData.Sections.GetSectionData(entry.GetCategory()).LeadingComments.Add(" Use a " + commentString + " at the start of lines for commenting.");
                }
            }

            var sec = _configData.Sections.GetSectionData(entry.GetCategory());
            var def = entry.GetDefaultValue();
            if (def == null)
                def = string.Empty;
            if (generate && sec.Keys.AddKey(entry.GetName(), def.ToString()))
            {
                foreach (var comment in entry.GetDescription())
                {
                    sec.Keys.GetKeyData(entry.GetName()).Comments.Add(" " + comment);
                }
                if (entry.ShowDefaultValue())
                    sec.Keys.GetKeyData(entry.GetName()).Comments.Add(" Default Value: " + def.ToString());
                return def.ToString();
            }

            return sec.Keys.GetKeyData(entry.GetName()).Value;
        }

        /// <summary>
        /// A list of all config entries as instances.
        /// </summary>
        public static readonly List<ConfigEntry> ConfigEntries = new List<ConfigEntry>(MserGlobals.GetSubInstances<ConfigEntry>());

        /// <summary>
        /// A list of config entries to be added during a Read/Write operation as instances.
        /// </summary>
        public static readonly List<ConfigEntry> NewConfigEntries = new List<ConfigEntry>(); 

        private static FileIniDataParser _config;
        private static IniData _configData;

        //private static IniFile _config = new IniFile(ConfigManager.configFile);

        private static void SetupParser()
        {
            _config = new FileIniDataParser();
            _config.Parser.Configuration.CommentString = commentString;
            _config.Parser.Configuration.CaseInsensitive = true;
            _config.Parser.Configuration.SkipInvalidLines = true;

            if (!File.Exists(configFile))
                CreateDefaultConfigFile();

            _configData = _config.ReadFile(ConfigManager.configFile);
        }

        /// <summary>
        /// Loads all config entries to list. Returns an error string to display, if any.
        /// </summary>
        /// <param name="generate">Should the default values be generated if not existing? This may cause errors!</param>
        public static string LoadConfigFile(bool generate)
        {
            DebugTest("LoadConfigFile");

            SetupParser();

            var exceptionlist = ReadList(ConfigManager.ConfigEntries, generate);
            exceptionlist.AddRange(ReadList(ConfigManager.NewConfigEntries, generate));
            ConfigManager.NewConfigEntries.Clear();

            var error = HandleExceptionList(exceptionlist, "loading");

            if (generate || error != null)
            {
                //Write if needed
                WriteToConfig();
            }

            return error;
        }

        /// <summary>
        /// Saves all config entries to file. Returns an error string to display, if any.
        /// </summary>
        public static string SaveConfigFile()
        {
            DebugTest("SAVE FILE");

            SetupParser();

            var exceptionlist = WriteList(ConfigManager.ConfigEntries);
            exceptionlist.AddRange(WriteList(ConfigManager.NewConfigEntries));
            ConfigManager.NewConfigEntries.Clear();

            var err = HandleExceptionList(exceptionlist, "saving");

            //Write changes to file
            WriteToConfig();

            return err;
        }

        /// <summary>
        /// Creates the default config file. Returns an error string to display, if any.
        /// </summary>
        public static string CreateDefaultConfigFile()
        {
            DebugTest("hahah default");

            //Create the file to avoid a crash
            File.WriteAllText(ConfigManager.configFile, "");

            if (_configData == null)
                _configData = _config.ReadFile(ConfigManager.configFile);

            var exceptionlist = DefaultsList(ConfigManager.ConfigEntries);
            exceptionlist.AddRange(DefaultsList(ConfigManager.NewConfigEntries));

            var err = HandleExceptionList(exceptionlist, "creating the default");

            //Write changes to file
            WriteToConfig();

            return err;
        }

        /// <summary>
        /// Loads the Autoexec section of the config.cfg into the Autoexec list.
        /// </summary>
        public static void LoadAutoexecutes(bool reloadfile = true)
        {
            //IMP: ConfigManager - when loading a /chain with % delimiting (how detecty?), replace it with the new one by commenting out the old one!
            DebugTest("loadautos");

            if (reloadfile)
                SetupParser();

            //Create the autoexec section if needed
            if (!_configData.Sections.ContainsSection("Autoexec"))
            {
                DebugTest("RECRETEIN SECTION AT LOADIN");
                ConfigManager.RecreateAutoexec();
                return;
            }

            //Clear current autoexec's
            ConfigManager.AutoexecutesList.Clear();
            ConfigManager._keptAutoexec.Clear();

            //This has to be manually read due to missing = signs
            var lines = File.ReadAllLines(ConfigManager.configFile);
            var autoexec = false;
            var removelast = false;
            foreach (var line in lines)
            {
                if (autoexec)
                    ConfigManager._keptAutoexec.Add(line);

                if (line.StartsWith(commentString))
                    continue;

                if (string.IsNullOrWhiteSpace(line))
                    continue;

                if (!autoexec)
                {
                    //TODO: ConfigManager - Better [Autoexec] detection here
                    if (line.Equals("[Autoexec]", StringComparison.InvariantCultureIgnoreCase))
                    {
                        autoexec = true;
                    }
                }
                else
                {
                    //Read lines until not in autoexec anymore
                    if (line.StartsWith("["))
                    {
                        removelast = true;
                        break;
                    }

                    ConfigManager.AutoexecutesList.Add(line);
                }
            }

            //Remove last since it's already a new category
            if (removelast)
                ConfigManager._keptAutoexec.RemoveAt(ConfigManager._keptAutoexec.Count - 1);

            DebugTest("DEBUGGG: After loadautos " + string.Join(";", ConfigManager._keptAutoexec));
        }

        /// <summary>
        /// Runs all autoexecute entries, returning how many of them were executed.
        /// </summary>
        /// <param name="bindsOnly">Only run commands that are marked with the IAutoRun interface.</param>
        /// <returns></returns>
        public static int RunAutoexecutes(bool bindsOnly)
        {
            DebugTest("runautos");

            var i = 0;

            foreach (var exec in ConfigManager.AutoexecutesList)
            {
                var split = ChatManager.GetSplitText(exec);
                var cmd = split[0];
                var args = split.GetRange(1, split.Count - 1);
                var cmdtype = ChatManager.GetCommandByName(cmd);

                //Skip if should bind and isn't to be autorunned
                if (bindsOnly && !(cmdtype is IAutoRun))
                    continue;

                if (cmdtype == null)
                {
                    //Unknown command
                    Program.LogLine("In autoexec section: Unknown command \"" + cmd + "\".");
                }
                else
                {
                    ChatManager.EnterChatMessage(string.Format("/{0} {1}", cmd, string.Join(" ", args)));
                    i++;
                }
            }

            return i;
        }

        /// <summary>
        /// Creates the [Autoexec] segment if needed, adding any otherwise lost commands saved from LoadAutoexecutes.
        /// </summary>
        private static void RecreateAutoexec()
        {
            try
            {
                ConfigManager.SetupParser();
                DebugTest("DEBUGGG: On recreateautos " + string.Join(";", ConfigManager._keptAutoexec));

                if (!ConfigManager._configData.Sections.ContainsSection("Autoexec"))
                {
                    File.AppendAllLines(ConfigManager.configFile, new []
                    {
                        "",
                        ConfigManager.commentString + " Any command in this section automatically executes.",
                        ConfigManager.commentString + " Simply enter the commands of your choice, excluding the / at the beginning of each.",
                        ConfigManager.commentString + " Note that a bind can only run one command, unless you use /chain or /addbind instead.",
                        "[Autoexec]"
                    });
                }
                else
                {
                    if (ConfigManager._keptAutoexec.Count == 0) return;

                    //Readd everything below [Autoexec]
                    var lines = File.ReadAllLines(ConfigManager.configFile).ToList();
                    int startInd;
                    int lastInd;
                    DebugTest("########LINES START");
                    DebugTest(string.Join("\r\n", lines.Select((s, i) => i + ": " + s)));
                    DebugTest("########LINES END");

                    var res = GetIndsForAutoexec(lines, out startInd, out lastInd);

                    //DebugTest(string.Format("autoexec={0} with ln={1}, end={2} with ln={3}", startInd, lines[startInd], lastInd, lines[lastInd]));

                    if (res)
                    {
                        lines.RemoveRange(startInd, Math.Max(0, lastInd - startInd + 1));
                        lines.InsertRange(startInd, ConfigManager._keptAutoexec);

                        File.WriteAllLines(ConfigManager.configFile, lines);
                    }
                    else
                    {
                        Program.LogLine("Some shizz happened!");
                    }
                }
            }
            catch (Exception e)
            {
                Program.LogLine("Couldn't recreate autoexec. Sorry! " + e.Message + e.StackTrace);
                Program.LogLine("Salvage what you can:\r\n" + string.Join("\r\n", ConfigManager._keptAutoexec));
            }
        }

        public static bool GetIndsForAutoexec(List<string> lines, out int startInd, out int lastInd)
        {
            startInd = -1;
            lastInd = -1;

            for (var i = 0; i < lines.Count; i++)
            {
                if (startInd < 0)
                {
                    if (lines[i].Equals("[Autoexec]", StringComparison.InvariantCultureIgnoreCase))
                    {
                        startInd = i + 1;
                    }
                }
                else
                {
                    if (lines[i].StartsWith("["))
                    {
                        lastInd = i - 1;
                        DebugTest("Set lastint based on new [Block].");
                        break;
                    }
                }
            }

            if (startInd == lines.Count) //If autoexec is last, avoid negative ranges
            {
                lastInd = startInd - 1;
                DebugTest("Set lastint based being last line (no lines below).");
            }

            if (lastInd < 0) //Is last block, do until end of file
            {
                lastInd = lines.Count - 1;
                DebugTest("Set lastint based on being last block (no [Block] below).");
            }

            if (startInd < 0)
                return false;
                //throw new FormatException("Could not find [Autoexec] block. Odd formatting used?");

            if (lastInd < startInd)
                return false;
                //throw new ArgumentOutOfRangeException("lastint", "Invalid range calculated. What is go on??");

            return true;
        }

        /// <summary>
        /// Handles an exception list, providing detailed info in the config and log if needed.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        private static string HandleExceptionList(List<ConfigError> list, string where)
        {
            if (list == null || list.Count == 0)
                return null;

            var str = "Error while " + where + " config file!";
            Program.LogLine(str);

            //Error reporting
            foreach (var error in list)
            {
                try
                {
                    string type = null;
                    string name = null;
                    string value = null;
                    string message = null;
                    var stacktrace = "No stacktrace.";

                    if (error.Entry != null)
                    {
                        type = error.Entry.GetType().Name;
                        name = error.Entry.GetName();
                        var val = error.Entry.Value;
                        if (val != null)
                            value = val.ToString();
                    }

                    if (error.Exception != null)
                    {
                        message = error.Exception.Message;
                        stacktrace = error.Exception.StackTrace;
                    }

                    var ln = Extensions.SafeFormat("... error in {0} ({1}) - last value \"{3}\": {2}", type, name, message, value);
                    Program.LogLine(ln);
                    Program.LogLine(stacktrace);
                }
                catch (Exception e)
                {
                    Program.LogLine("So bad, crashing while crashing... " + e.Message + e.StackTrace);
                }
            }

            return str;
        }

        /// <summary>
        /// Writes any _configData to the config file and recreates the [Autoexec] segment.
        /// </summary>
        private static void WriteToConfig()
        {
            DebugTest("CONFO WRITO");

            //Load in order to get un-fuckled autoexecs cached
            ConfigManager.LoadAutoexecutes(false);

            //Write to config, this fuckles the autoexecs
            ConfigManager._config.WriteFile(ConfigManager.configFile, ConfigManager._configData);

            //Luckily, this method fixes any fuckled autoexecs
            ConfigManager.RecreateAutoexec();
        }

        /// <summary>
        /// This is left over to show the painful 2+ hours of debugging I have had to go through to get this going.
        /// </summary>
        /// <param name="str"></param>
        private static void DebugTest(string str)
        {
            //Program.LogLine("DEBUBUBGUGBUBGUGBU: " + str);
        }

        /// <summary>
        /// Returns a list of commands inside of the Autoexec file.
        /// <para />Note that binds will use the fitting BoundCommand syntax - can be distinguished by validating for Keys.None!
        /// <para />Will return null if no [Autoexec] block exists.
        /// </summary>
        /// <returns></returns>
        public static List<BoundCommand> GetAutoexecCommands()
        {
            var inautoexec = false;
            var lines = File.ReadAllLines(ConfigManager.configFile).ToList();
            var cmdlist = new List<BoundCommand>();
            for (var i = 0; i < lines.Count; i++)
            {
                if (string.IsNullOrWhiteSpace(lines[i]))
                    continue;

                if (lines[i].StartsWith(ConfigManager.commentString))
                    continue;

                if (!inautoexec)
                {
                    if (lines[i].Equals("[Autoexec]", StringComparison.InvariantCultureIgnoreCase))
                    {
                        //Entered autoexec
                        inautoexec = true;
                    }
                }
                else
                {
                    if (lines[i].StartsWith("["))
                    {
                        //Exited autoexec
                        break;
                    }
                    else
                    {
                        //Get command format
                        var split = ChatManager.GetSplitText(lines[i]);
                        var type = ChatManager.GetCommandByName(split[0]);
                        if (type is BindCommand)
                        {
                            //Only /(add)bind should be counted as a "binding" command
                            var args = split.Count <= 2 ? new List<string>() : split.GetRange(2, split.Count - 2);
                            cmdlist.Add(new BoundCommand(type, args, BindManager.GetKeyByString(split[1]), i));
                        }
                        else
                        {
                            //Should be a boundcommand with no key
                            var args = split.Count <= 1 ? new List<string>() : split.GetRange(1, split.Count - 1);
                            cmdlist.Add(new BoundCommand(type, args, Keys.None, i));
                        }
                    }
                }
            }

            //Return null if it didn't ever find an autoexec section.
            if (!inautoexec)
                return null;

            return cmdlist;
        }

        /// <summary>
        /// Returns the first bound command using the specified key. Line number included!
        /// <para />If no bind exists on the specified key, null is returned instead!
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static BoundCommand GetAutoexecBindByKey(Keys key)
        {
            var cmds = GetAutoexecCommands();

            if (cmds == null)
                return null;

            return cmds.FirstOrDefault(cmd => cmd.Key == key);
        }

        private static List<ConfigError> ReadList(IEnumerable<ConfigEntry> list, bool generate)
        {
            var exceptionlist = new List<ConfigError>();

            foreach (var entry in list)
            {
                try
                {
                    var res = entry.BeforeRead();
                    if (!res)
                        continue;

                    var value = ConfigManager.ReadValue(entry, generate);

                    if (!(value is NoValue))
                        entry.Value = value as string;

                    if (value != null)
                        entry.AfterRead();
                }
                catch (Exception e)
                {
                    exceptionlist.Add(new ConfigError(entry, e));
                }
            }

            return exceptionlist;
        }

        private static List<ConfigError> WriteList(IEnumerable<ConfigEntry> list)
        {
            var exceptionlist = new List<ConfigError>();

            foreach (var entry in list)
            {
                try
                {
                    var res = entry.BeforeWrite();
                    if (!res)
                        continue;

                    ConfigManager.WriteValue(entry, entry.Value);

                    if (entry.Value != null)
                        entry.AfterWrite();
                }
                catch (Exception e)
                {
                    exceptionlist.Add(new ConfigError(entry, e));
                }
            }

            return exceptionlist;
        }

        private static List<ConfigError> DefaultsList(IEnumerable<ConfigEntry> list)
        {
            var exceptionlist = new List<ConfigError>();

            foreach (var entry in list)
            {
                try
                {
                    var res = entry.BeforeWrite();
                    if (!res)
                        continue;

                    ConfigManager.WriteValue(entry, entry.GetDefaultValue());
                    entry.AfterWrite();
                }
                catch (Exception e)
                {
                    exceptionlist.Add(new ConfigError(entry, e));
                }
            }

            return exceptionlist;
        }

        private class ConfigError
        {
            public ConfigEntry Entry;
            public Exception Exception;

            public ConfigError(ConfigEntry entry, Exception exception)
            {
                this.Entry = entry;
                this.Exception = exception;
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using DuckGame.MserDuck.CommandHandlers;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Container for all cached data, and helper methods for cache management.
    /// <para />Be sure to add your own cached data into here to let it be managable by the Cache Manager (see <code>DebugCacheManager.cs</code>!)
    /// <para />Every field in this class is considered a holder of cache, but one can skip this behaviour using a not-yet-existing attribute.
    /// </summary>
    public static class CacheManager
    {
        private static Type _lastObjectSpawn;
        private static Type _lastObjectCall;
        private static Type _lastObjectSet;

        //HINT: Put all cached data at the top, to make it easier to find
        private static IEnumerable<AutoCompleteEntry> _cachedSkins;
        private static AutoCompleteEntry[] _cachedSounds;
        private static AutoCompleteEntry[] _cachedTextures;
        private static AutoCompleteEntry[] _cachedSongs;
        private static AutoCompleteEntry[] _cachedLevels;
        private static AutoCompleteEntry[] _cachedModifiers;
        private static AutoCompleteEntry[] _cachedColors;
        private static AutoCompleteEntry[] _cachedSpawnTypes;
        private static AutoCompleteEntry[] _cachedSpawnTypesNoSpec;
        private static IEnumerable<MemberInfo> _cachedInfoSet;
        private static IEnumerable<ConstructorInfo> _cachedInfoSpawn;
        private static IEnumerable<MethodInfo> _cachedInfoCall;
        private static string _cachedSongsFolder;
        private static string[] _cachedSongsDir;
        private static IEnumerable<ArgumentError> _cachedErrors;
        private static List<string> _cachedCommands;
        private static IEnumerable<AutoCompleteEntry> _cachedEntries;
        private static IEnumerable<AutoCompleteEntry> _configEntries;
        private static IEnumerable<AutoCompleteEntry> _rolesEntries;
        private static IEnumerable<AutoCompleteEntry> _usersEntries;

        /// <summary>
        /// List of types cached from <code>MserGlobals.GetType</code> calls. The key is the type's <code>.Name</code> property.
        /// </summary>
        public static readonly Dictionary<string, Type> CachedTypes = new Dictionary<string, Type>();

        /// <summary>
        /// A list of member info caches from <code>MserGlobals.GetMember</code> calls. The key is the owning type's <code>.Name</code> property. 
        /// </summary>
        public static readonly Dictionary<string, MemberInfoCache> MemberInfoCaches = new Dictionary<string, MemberInfoCache>();

        /// <summary>
        /// List of snippets known by the client. Saved/loaded from the snippets.dat file.
        /// </summary>
        public static List<CodeSnippet> KnownCodeSnippets;

        //////////////////////////
        // CacheManager-specific
        //////////////////////////

        public static int CountEntries(FieldInfo field)
        {
            //Get value
            var value = field.GetValue(null);

            var enumerable = value as IEnumerable;
            if (enumerable != null)
                return enumerable.Cast<object>().Count();
            return 0;
        }

        private static IEnumerable<FieldInfo> _monitoredFields;

        public static IEnumerable<FieldInfo> CachedMonitoredFields()
        {
            if (CacheManager._monitoredFields == null)
            {
                //Update em
                CacheManager._monitoredFields = typeof(CacheManager).GetFields(MserGlobals.GeneralFlags)
                    .Where(f => !f.Name.Contains("<>"))/*.Where(ValidField)*/.ToList();
            }
            return CacheManager._monitoredFields;
        }

        /////////////////////////////
        // Start other methods here
        /////////////////////////////

        public static IEnumerable<MethodInfo> GetCallAutoCompleteInfo(Type currentObject)
        {
            if (CacheManager._lastObjectCall != currentObject || CacheManager._cachedInfoCall == null)
            {
                //Reload cache
                CacheManager._lastObjectCall = currentObject;
                var t = currentObject;

                //Disallow interface
                if (!t.IsClass)
                    return null;

                var m = t.GetMethods(MserGlobals.GeneralFlags);
                CacheManager._cachedInfoCall = m;
            }
            return CacheManager._cachedInfoCall;
        }

        public static AutoCompleteEntry[] GetColorAutoCompleteInfo()
        {
            if (CacheManager._cachedColors == null)
            {
                CacheManager._cachedColors = ColorCommand.ColorNames.Select(x => x.Key.FirstLetterToUpper())
                    .Concat(CommandHelpers.GetListOfMicrosoftColors())
                    .Select(GetColorAutoCompleteEntry).Distinct(AutoCompleteEntry.DefaultDisplayComparer).ToArray();
            }

            return CacheManager._cachedColors;
        }

        public static AutoCompleteEntry GetColorAutoCompleteEntry(string color)
        {
            return GetColorAutoCompleteEntry(new List<string> {color});
        }

        public static AutoCompleteEntry GetColorAutoCompleteEntry(List<string> color)
        {
            var name = string.Join(" ", color);
            return new AutoCompleteEntry(name, name, false, rect =>
            {
                //Draw color in rectangle
                var namedcolor = CommandHelpers.GetColorFromString(color);
                if (namedcolor != null)
                {
                    Graphics.DrawRect(rect, new Color(namedcolor.Value / byte.MaxValue));
                }
            });
        }

        public static AutoCompleteEntry[] GetLevelAutoCompleteInfo()
        {
            if (CacheManager._cachedLevels == null)
                CacheManager._cachedLevels = Editor.GetSubclasses(typeof(Level))
                    .Select(t => t.Name).Concat(new [] {"Lobby", "RockThrow"}).Concat(IterateLevels())
                    .Select(e => (AutoCompleteEntry)e).ToArray();
            return CacheManager._cachedLevels;
        }

        private static IEnumerable<string> IterateLevels()
        {
            var basedir = Path.Combine(MserGlobals.GetDuckGamePath(), "Content/levels/");
            foreach (var file in Directory.EnumerateFiles(basedir, "*.lev", SearchOption.AllDirectories))
            {
                if (file.Contains("procedural"))
                    continue;

                var local = file.Replace(basedir, "").Replace('\\', '/');
                yield return local.Substring(0, local.Length - 4);
            }

            var custdir = DuckFile.levelDirectory;
            foreach (var file in Directory.EnumerateFiles(custdir, "*.lev", SearchOption.AllDirectories))
            {
                if (file.Contains("custommaps"))
                    continue;

                var local = file.Replace(custdir, "").Replace('\\', '/');
                yield return local.Substring(0, local.Length - 4);
            }
        }

        public static void ClearModifierAutoCompleteEntries()
        {
            CacheManager._cachedModifiers = null;
        }

        public static AutoCompleteEntry[] GetModifierAutoCompleteEntries()
        {
            if (CacheManager._cachedModifiers == null)
                CacheManager._cachedModifiers = UpdateModifierAutoCompleteEntries().ToArray();
            return CacheManager._cachedModifiers;
        }

        public static IEnumerable<AutoCompleteEntry> UpdateModifierAutoCompleteEntries()
        {
            foreach (var mod in ModifierManager.Modifiers)
            {
                var name = mod.GetName();
                yield return new AutoCompleteEntry(name, name.Replace(" ", ""), false, CommandHelpers.GetModifierIconDrawDelegate(mod.Enabled));
            }

            foreach (var mod in Unlocks.GetUnlocks(UnlockType.Modifier))
            {
                //Do not include MserDuck Options
                if (mod.id == "QWOPPY")
                    continue;

                var name = mod.shortName;
                yield return new AutoCompleteEntry(name, name.Replace(" ", ""), false, CommandHelpers.GetModifierIconDrawDelegate(mod.enabled));
            }
        }

        public static AutoCompleteEntry[] GetMusicAutoCompleteInfo()
        {
            if (CacheManager._cachedSongs == null)
                CacheManager._cachedSongs = CacheManager.IterateSongs().ToArray();
            return CacheManager._cachedSongs;
        }

        private static IEnumerable<AutoCompleteEntry> IterateSongs()
        {
            var basedir = Directory.GetCurrentDirectory() + "/Content/Audio/Music/";
            foreach (var file in Directory.EnumerateFiles(basedir, "*.ogg", SearchOption.AllDirectories))
            {
                var local = file.Replace(basedir, "").Replace('\\', '/');
                yield return new AutoCompleteEntry(Path.GetFileNameWithoutExtension(local), local.Substring(0, local.Length - 4), false);
            }

            var customdir = MserGlobals.GetContentDir() + "Music/";
            foreach (var file in Directory.EnumerateFiles(customdir, "*.ogg", SearchOption.AllDirectories))
            {
                var local = file.Replace(customdir, "").Replace('\\', '/');
                yield return new AutoCompleteEntry(Path.GetFileNameWithoutExtension(local), local.Substring(0, local.Length - 4), false);
            }
        }

        public static IEnumerable<MemberInfo> GetSetAutoCompleteInfo(Type currentObject)
        {
            if (CacheManager._lastObjectSet != currentObject || CacheManager._cachedInfoSet == null)
            {
                //Reload cache
                CacheManager._lastObjectSet = currentObject;
                var t = currentObject;

                //Disallow interface
                if (!t.IsClass)
                    return null;

                var f = t.GetFields(MserGlobals.GeneralFlags).Cast<MemberInfo>();
                var p = t.GetProperties(MserGlobals.GeneralFlags).Cast<MemberInfo>();
                CacheManager._cachedInfoSet = f.Concat(p);
            }
            return CacheManager._cachedInfoSet;
        }

        public static void ClearSkinAutoCompleteInfo()
        {
            CacheManager._cachedSkins = null;
        }

        public static IEnumerable<AutoCompleteEntry> GetSkinAutoCompleteInfo()
        {
            if (CacheManager._cachedSkins == null)
                CacheManager._cachedSkins = CacheManager.IterateSkinFolders();
            return CacheManager._cachedSkins;
        }

        private static IEnumerable<AutoCompleteEntry> IterateSkinFolders()
        {
            if (!Directory.Exists(Skin.SkinsDirectory))
                return null;

            return Directory.EnumerateDirectories(Skin.SkinsDirectory, "*", SearchOption.TopDirectoryOnly)
                            .Select(s => new AutoCompleteEntry(new DirectoryInfo(s).Name));
        }

        public static AutoCompleteEntry[] GetSoundAutoCompleteInfo()
        {
            if (CacheManager._cachedSounds == null)
                CacheManager._cachedSounds = CacheManager.IterateSounds().Select(e => (AutoCompleteEntry)e).ToArray();
            return CacheManager._cachedSounds;
        }

        private static IEnumerable<string> IterateSounds()
        {
            var basedir = Directory.GetCurrentDirectory() + "/Content/Audio/SFX/";
            foreach (var file in Directory.EnumerateFiles(basedir, "*.xnb"))
            {
                var local = file.Replace(basedir, "").Replace('\\', '/');
                yield return local.Substring(0, local.Length - 4);
            }

            var custdir = MserGlobals.GetContentDir() + "sounds/";
            foreach (var file in Directory.EnumerateFiles(custdir, "*.wav"))
            {
                var local = file.Replace(custdir, "").Replace('\\', '/');
                yield return local.Substring(0, local.Length - 4);
            }
        }

        public static AutoCompleteEntry[] GetTexturesAutoCompleteInfo()
        {
            if (CacheManager._cachedTextures == null)
                CacheManager._cachedTextures = CacheManager.IterateTextures().Select(e => (AutoCompleteEntry)e).ToArray();
            return CacheManager._cachedTextures;
        }

        private static IEnumerable<string> IterateTextures()
        {
            var basedir = Directory.GetCurrentDirectory() + "/Content/";
            foreach (var file in Directory.EnumerateFiles(basedir, "*.xnb", SearchOption.AllDirectories))
            {
                if (file.Contains("Audio"))
                    continue;

                var local = file.Replace(basedir, "").Replace('\\', '/');
                yield return local.Substring(0, local.Length - 4);
            }

            var custdir = MserGlobals.GetContentDir();
            foreach (var file in Directory.EnumerateFiles(custdir, "*.png"))
            {
                var local = file.Replace(custdir, "").Replace('\\', '/');
                yield return local.Substring(0, local.Length - 4);
            }
        }

        /// <summary>
        /// Returns AutoCompleteEntries for the all cached types of things.
        /// </summary>
        /// <param name="includeSpecial">Whether to include any special string things, such as "ragdoll" for a NetworkRagdoll.</param>
        /// <returns></returns>
        public static AutoCompleteEntry[] GetTypeAutoCompleteInfo(bool includeSpecial)
        {
            //TODO: /spawn - Rendering preview of items

            if (includeSpecial)
            {
                if (CacheManager._cachedSpawnTypes == null)
                    CacheManager._cachedSpawnTypes =
                        Editor.GetSubclasses(typeof(Thing)).Select(t => t.Name).Concat(CommandHelpers._specialThings)
                              .Select(e => new AutoCompleteEntry(e, e, false, CommandHelpers.GetThingDrawDelegate(e))).ToArray();
                return CacheManager._cachedSpawnTypes;
            }
            else
            {
                if (CacheManager._cachedSpawnTypesNoSpec == null)
                    CacheManager._cachedSpawnTypesNoSpec =
                        Editor.GetSubclasses(typeof(Thing)).Select(t => t.Name)
                              .Select(e => new AutoCompleteEntry(e, e, false, CommandHelpers.GetThingDrawDelegate(e))).ToArray();
                return CacheManager._cachedSpawnTypesNoSpec;
            }
        }

        public static IEnumerable<ConstructorInfo> GetSpawnAutoCompleteInfo(Type currentObject)
        {
            if (CacheManager._lastObjectSpawn != currentObject || CacheManager._cachedInfoSpawn == null)
            {
                //Reload cache
                CacheManager._lastObjectSpawn = currentObject;
                var t = currentObject;
                var p = t.GetConstructors(MserGlobals.GeneralFlags);
                CacheManager._cachedInfoSpawn = p;
            }
            return CacheManager._cachedInfoSpawn;
        }

        /// <summary>
        /// Container for member info cache data. Used by <code>MserGlobals.GetMember</code> and similar.
        /// </summary>
        public class MemberInfoCache
        {
            public Dictionary<string, MethodInfo> Methods;
            public Dictionary<string, FieldInfo> Fields;
            public Dictionary<string, PropertyInfo> Properties;
            public Dictionary<string, MethodInfo> MethodsLowercase;
            public Dictionary<string, FieldInfo> FieldsLowercase;
            public Dictionary<string, PropertyInfo> PropertiesLowercase;
        }

        public static string[] GetSongList(string folder)
        {
            if (CacheManager._cachedSongsDir == null || CacheManager._cachedSongsFolder != folder)
            {
                CacheManager._cachedSongsFolder = folder;
                CacheManager._cachedSongsDir = CacheManager.IterateSongDir(folder).ToArray();
            }
            return CacheManager._cachedSongsDir;
        }

        private static IEnumerable<string> IterateSongDir(string folder)
        {
            var basedir = Directory.GetCurrentDirectory() + "/Content/Audio/Music/" + folder + "/";
            foreach (var file in Directory.EnumerateFiles(basedir, "*.ogg", SearchOption.AllDirectories))
            {
                var local = file.Replace(basedir, "").Replace('\\', '/');
                yield return Path.GetFileNameWithoutExtension(local);
            }

            var customdir = MserGlobals.GetContentDir() + "Music/" + folder + "/";
            foreach (var file in Directory.EnumerateFiles(customdir, "*.ogg", SearchOption.AllDirectories))
            {
                var local = file.Replace(customdir, "").Replace('\\', '/');
                yield return Path.GetFileNameWithoutExtension(local);
            }
        }

        /// <summary>
        /// Returns a list of argument errors for the currently entered chat text.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<ArgumentError> GetArgumentErrorsList()
        {
            if (CacheManager._cachedErrors == null)
            {
                CacheManager.UpdateArgumentErrorsList();
            }

            return CacheManager._cachedErrors;
        }

        public static void UpdateArgumentErrorsList()
        {
            var reg = MserDebug.StartProfiling("UpdateArgumentErrorsList");

            try
            {
                if (String.IsNullOrWhiteSpace(DuckNetwork.core.currentEnterText))
                {
                    CacheManager._cachedErrors = new List<ArgumentError>();
                    return;
                }

                var split = ChatManager.GetSplitChat();

                if (split.Count == 0)
                {
                    CacheManager._cachedErrors = new List<ArgumentError>();
                    return;
                }

                var cmd = ChatManager.GetCommandByName(split[0]);

                if (cmd == null)
                {
                    CacheManager._cachedErrors = new List<ArgumentError>();
                    return;
                }

                var args = split.GetRange(1, split.Count - 1);

                var valid = cmd.CheckArgumentsValid(args);
                if (valid == null)
                {
                    CacheManager._cachedErrors = new List<ArgumentError>();
                }
                else
                {
                    CacheManager._cachedErrors = valid.ToList();
                }

                if (cmd.IsNotImplemented())
                {
                    CacheManager._cachedErrors = CacheManager._cachedErrors.ToList();
                    ((List<ArgumentError>)CacheManager._cachedErrors).Add(new ArgumentError(-1, "This command is currently not implemented!", Severity.Warning));
                }

                //May execute??
                var role = PermissionManager.GetRole(MserGlobals.GetLocalDuck().profile);
                if (!role.IsPermittedToExecute(cmd))
                {
                    //DAM
                    ((List<ArgumentError>)CacheManager._cachedErrors).Add(new ArgumentError(-69,
                        string.Format("You may not execute this command, since you are assigned to the role \"{0}\".", role.Name)));
                }
            }
            catch (Exception ex)
            {
                Program.LogLine("Exception caught while updating argument errors: " + ex.Message + "\n" + ex.StackTrace);
                CacheManager._cachedErrors = new List<ArgumentError>();
            }

            MserDebug.EndProfiling(reg);
        }

        /// <summary>
        /// Returns a list of auto complete commands, as to be displayed in the list, for the currently entered chat text.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetCommandAutoCompleteList()
        {
            if (CacheManager._cachedCommands == null)
            {
                CacheManager.UpdateCommandAutoCompleteList();
            }

            return CacheManager._cachedCommands;
        }

        public static void UpdateCommandAutoCompleteList()
        {
            var reg = MserDebug.StartProfiling("UpdateCommandAutoCompleteList");

            if (String.IsNullOrWhiteSpace(DuckNetwork.core.currentEnterText))
            {
                CacheManager._cachedCommands = new List<string>();
                return;
            }

            CacheManager._cachedCommands = ChatManager.Commands.OrderBy(c => c.GetNames()[0]).Select(cmd => "/" + cmd.GetNames()[0])
                .Where(name => name.StartsWith(DuckNetwork.core.currentEnterText, StringComparison.InvariantCultureIgnoreCase)).ToList();
            MserDebug.EndProfiling(reg);
        }

        /// <summary>
        /// Returns a list of auto complete arguments, as to be displayed in the list, for the currently entered chat text.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<AutoCompleteEntry> GetArgumentAutoCompleteList()
        {
            if (CacheManager._cachedEntries == null)
            {
                CacheManager.UpdateArgumentAutoCompleteList();
            }

            return CacheManager._cachedEntries;
        }

        private static int _lastAACindex = -1;

        public static void UpdateArgumentAutoCompleteList()
        {
            var reg = MserDebug.StartProfiling("UpdateArgumentAutoCompleteList");

            //Not this kinda autocomplete for empty chat texts
            if (String.IsNullOrWhiteSpace(DuckNetwork.core.currentEnterText))
            {
                CacheManager._cachedEntries = new List<AutoCompleteEntry>();
                return;
            }

            //Needs at least the command name
            var split = ChatManager.GetSplitChat();
            if (split.Count < 1)
            {
                CacheManager._cachedEntries = new List<AutoCompleteEntry>();
                return;
            }

            //Needs to be a known command
            var cmd = ChatManager.GetCommandByName(split[0]);
            if (cmd == null)
            {
                CacheManager._cachedEntries = new List<AutoCompleteEntry>();
                return;
            }

            //Has to return any kind of commands
            var index = split.Count - 2;
            if (index != _lastAACindex)
            {
                _lastAACindex = index;

                //Update the topindex etc
                ChatManager.ResetAutoCompleteScrolling();
            }
            var nulllist = cmd.GetAutoCompleteEntries(index, split.GetRange(1, split.Count - 1));
            if (nulllist == null || !nulllist.Any())
            {
                CacheManager._cachedEntries = new List<AutoCompleteEntry>();
                return;
            }

            //Filtery filter
            var megalist = nulllist.Where(x => split.Count <= 1 || x.Display.StartsWith(split.Last(), StringComparison.InvariantCultureIgnoreCase))
                                   .OrderBy(x => x.SortByValue ? x.Value : x.Display)
                                   .Distinct(AutoCompleteEntry.DefaultDisplayComparer)
                                   .ToList();

            CacheManager._cachedEntries = megalist;

            MserDebug.EndProfiling(reg);
        }

        public static IEnumerable<AutoCompleteEntry> GetConfigEntriesAutoCompleteInfo()
        {
            if (_configEntries == null)
            {
                _configEntries = CacheConfigEntries();
            }
            return _configEntries;
        }

        private static IEnumerable<AutoCompleteEntry> CacheConfigEntries()
        {
            return ConfigManager.ConfigEntries.Select(config => new AutoCompleteEntry(String.Format("{0} ({1})", config.GetName(), config.Value), config.GetName()));
        }

        public static IEnumerable<AutoCompleteEntry> GetRolesAutoCompleteInfo()
        {
            if (_rolesEntries == null)
            {
                UpdateRolesEntries();
            }
            return _rolesEntries;
        }

        public static void UpdateRolesEntries()
        {
            _rolesEntries = PermissionManager.Roles.Select(r => new AutoCompleteEntry(r.Name, r.Name, false, rect =>
            {
                //Draw color rectangle and icon
                Graphics.DrawRect(rect, r.Color, 0.8f);
                if (r.Icon != null)
                {
                    r.Icon.position = new Vec2(rect.x + 2, rect.y + 2);
                    r.Icon.Draw();
                }
            }));
        }

        public static IEnumerable<AutoCompleteEntry> GetUsersAutoCompleteInfo()
        {
            if (_usersEntries == null)
            {
                UpdateUsersEntries();
            }
            return _usersEntries;
        }

        public static void UpdateUsersEntries()
        {
            _usersEntries = MserGlobals.GetListOfProfiles().Where(p => p.steamID > 0)
                .Select(p => new AutoCompleteEntry(string.Format("{0} ({1})", p.name, p.steamID), p.steamID.ToString(), false,
                rect => {
                    var x = rect.x + 2;
                    var y = rect.y + 2;
                    if (p.localPlayer)
                    {
                        //Draw a heart
                        var ico = IconManager.GetIconFromSet(IconManager.TinyIcons, (int)IconManager.TinyIconsFrames.Heart, new Vec2(x, y), new Vec2(2), 0.98f);
                        ico.Draw();

                        if (MserGlobals.IsHosting(p))
                        {
                            //Draw a shield
                            var ico2 = IconManager.GetIconFromSet(IconManager.TinyIcons, (int)IconManager.TinyIconsFrames.Shield, new Vec2(x - 18, y), new Vec2(2), 0.98f);
                            ico2.Draw();
                        }
                    }
                }));
        }
    }
}

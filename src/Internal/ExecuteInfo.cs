﻿namespace DuckGame.MserDuck
{
    /// <summary>
    /// An error to display when a command's Execute did not run successfully, as well as any info for when it ran successfully.
    /// </summary>
    public class ExecuteInfo
    {
        private readonly int _index;

        /// <summary>
        /// Which argument causes the error.
        /// </summary>
        public int Index
        {
            get { return this._index; }
        }

        private readonly string _message;

        /// <summary>
        /// The string to display/associate with the error. Should be as descriptive as possible, and may include the argument's value in question.
        /// </summary>
        public string Message
        {
            get { return this._message; }
        }

        private readonly Severity _severity;

        /// <summary>
        /// Severity of the ExecuteInfo. Changes output format and severity for handling.
        /// </summary>
        public Severity Severity
        {
            get { return this._severity; }
        }

        /// <summary>
        /// Creates a new error ExecuteInfo, which should tell the user what happened so that the command couldn't execute as expected and what he has to do to fix it.
        /// </summary>
        /// <param name="message">The string to display. Should be as descriptive as possible, and may include the argument's value in question.</param>
        /// <param name="index">Which argument causes the error. Use -1 to not include this info, since the error isn't directly caused by an argument.</param>
        /// <param name="severity">If required, overrides the severity.</param>
        public ExecuteInfo(string message, int index, Severity severity = Severity.Error)
        {
            this._message = message;
            this._index = index;
            this._severity = severity;
        }

        /// <summary>
        /// Creates a new informing ExecuteInfo, which should tell the user anything about the command's execution he may want to know.
        /// </summary>
        /// <param name="message">The string to display. Should be as descriptive as possible.</param>
        /// <param name="severity">If required, overrides the severity.</param>
        public ExecuteInfo(string message, Severity severity = Severity.Info)
        {
            this._message = message;
            this._severity = severity;
        }

        /// <summary>
        /// Returns an ExecuteInfo notifying the user that they may not execute a certain command due to the role they are a part of.
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public static ExecuteInfo NoPermissions(PermissionRole role)
        {
            return new ExecuteInfo(string.Format("You may not execute this command, since you are assigned to the role \"{0}\".", role.Name), -1);
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            return this.Severity + ": " + this.Message;
        }
    }
}

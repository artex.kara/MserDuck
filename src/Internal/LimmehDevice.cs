namespace DuckGame.MserDuck
{
    public class LimmehDevice : Holdable
    {
        public readonly Duck MyDuck;

        private bool _pickedup;

        public SpriteMap _sprite;

        public LimmehDevice(float x, float y, Duck duck) : base(x, y)
        {
            this.MyDuck = duck;

            this.weight = 5;
            //TODO: IntermissionLimmeh / LimmehDevice - sprite for the device
            this._sprite = new SpriteMap(this.GetPath("robotBeacon"), 16, 16);
            this.graphic = this._sprite;
        }

        public override void CheckIfHoldObstructed()
        {
            if (this.duck != null)
            {
                this.duck.holdObstructed = false;
            }
        }

        public override void Update()
        {
            if (!this._pickedup && this.duck != null)
            {
                //Picked up!!
                this._pickedup = true;

                //Pls rate me, limmeh-sama
                var lvl = Level.current as IntermissionLimmeh;
                if (lvl != null)
                {
                    lvl.RateScore(this.MyDuck);
                }
            }

            base.Update();
        }
    }
}
﻿using System.Linq;
using System.Reflection;

namespace DuckGame.MserDuck
{
    /// <summary>
    /// Animation data for a skin, because the Animation class is internal. :(
    /// </summary>
    public struct SkinAnimation
    {
        public SkinAnimation(string format)
        {
            var split = format.Split(',').ToList();
            this.Name = split[0];
            this.Speed = SkinInfo.FloatOrRange(split[1]);
            this.Looping = bool.Parse(split[2]);
            this.Frames = split.GetRange(3, split.Count - 3).Select(int.Parse).ToArray();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public SkinAnimation(string name, float speed, bool looping, int[] frames)
        {
            this.Name = name;
            this.Speed = speed;
            this.Looping = looping;
            this.Frames = frames;
        }

        public string Name;

        public float Speed;

        public bool Looping;

        public int[] Frames;

        /// <summary>
        /// Returns a SkinAnimation instance from an Animation instance, fucking hell dev.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static SkinAnimation ReflectAnimation(object obj)
        {
            var anim = new SkinAnimation();
            anim.Name = (string)SkinAnimation._nameField.GetValue(obj);
            anim.Speed = (float)SkinAnimation._speedField.GetValue(obj);
            anim.Looping = (bool)SkinAnimation._loopingField.GetValue(obj);
            anim.Frames = (int[])SkinAnimation._framesField.GetValue(obj);
            return anim;
        }

        /// <summary>
        /// Returns the fully qualified type name of this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"/> containing a fully qualified type name.
        /// </returns>
        public override string ToString()
        {
            return this.Name + " (speed " + this.Speed + (this.Looping ? ", looping" : string.Empty) + " - frames: " + string.Join(",", this.Frames) + ")";
        }

        public static readonly FieldInfo _nameField = MserGlobals.GetField("Animation", "name");
        public static readonly FieldInfo _speedField = MserGlobals.GetField("Animation", "speed");
        public static readonly FieldInfo _loopingField = MserGlobals.GetField("Animation", "looping");
        public static readonly FieldInfo _framesField = MserGlobals.GetField("Animation", "frames");
    }
}

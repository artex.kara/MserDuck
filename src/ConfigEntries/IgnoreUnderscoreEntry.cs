﻿namespace DuckGame.MserDuck.ConfigEntries
{
    public class IgnoreUnderscoreEntry : ConfigEntry
    {
        /// <summary>
        /// The name of the ConfigEntry in the config.cfg file.
        /// </summary>
        public override string GetName()
        {
            return "IgnoreUnderscore";
        }

        /// <summary>
        /// The category of this config entry.
        /// </summary>
        /// <returns></returns>
        public override string GetCategory()
        {
            return "Debugging";
        }

        /// <summary>
        /// The description comments of the config entry, to add above it. The commenting string (ConfigManager.commentString) is added on its own.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[]
            {
                "When inspecting a thing or object, whether members starting with an underscore (such as _hasTrigger, ...)",
                "should be sorted as expected (true) or to be placed in front of regular entries (false)."
            };
        }

        /// <summary>
        /// The default value of the ConfigEntry when first generated.
        /// </summary>
        /// <returns></returns>
        public override object GetDefaultValue()
        {
            return true;
        }

        /// <summary>
        /// Gets invoked after the value is read. If the key does not exist, this does not get executed.
        /// </summary>
        public override void AfterRead()
        {
            DebugThingBox.IgnoreUnderscore = bool.Parse(this.Value.ToString());
        }
    }
}

﻿namespace DuckGame.MserDuck.ConfigEntries
{
    public class ListSizeEntry : ConfigEntry
    {
        /// <summary>
        /// The name of the ConfigEntry in the config.cfg file.
        /// </summary>
        public override string GetName()
        {
            return "ListSize";
        }

        /// <summary>
        /// The default value of the ConfigEntry when first generated.
        /// </summary>
        /// <returns></returns>
        public override object GetDefaultValue()
        {
            return 10;
        }

        /// <summary>
        /// The description comments of the config entry, to add above it. The commenting // are added on their own.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[] { "Changes the amount of entries to show in the auto-complete list, chat history and help command." };
        }

        /// <summary>
        /// The category of this config entry.
        /// </summary>
        /// <returns></returns>
        public override string GetCategory()
        {
            return "Chat";
        }

        /// <summary>
        /// Gets invoked after the value is read.
        /// </summary>
        public override void AfterRead()
        {
            ChatManager.Entries = int.Parse(this.Value.ToString());
        }
    }
}

﻿using System;
using System.Linq;
using IniParser.Model;
using System.Text.RegularExpressions;

namespace DuckGame.MserDuck.ConfigEntries
{
    public class RolesEntry : ConfigEntry
    {
        /// <summary>
        /// The name of the ConfigEntry in the config.cfg file.
        /// </summary>
        public override string GetName()
        {
            return null;
        }

        /// <summary>
        /// The category of this config entry.
        /// </summary>
        /// <returns></returns>
        public override string GetCategory()
        {
            return "Roles";
        }

        /// <summary>
        /// Whether a comment displaying the default value should be appended.
        /// </summary>
        public override bool ShowDefaultValue()
        {
            return false;
        }

        /// <summary>
        /// Custom reading code of this ConfigEntry's category, using the specified IniData which can be read or written to.
        /// <para />Return true to execute old reading code AFTER this custom code, false to skip it entriely (not changing the <code>this.Value</code> property).
        /// </summary>
        /// <returns></returns>
        public override bool ReadCategory(IniData data)
        {
            //Do not read permission data if not hosting!
            if (!Network.isServer)
                return false;

            //Read sections / roles
            data.Sections.AddSection(this.GetCategory());

            var catdat = data.Sections.GetSectionData(this.GetCategory());
            if (!catdat.LeadingComments.Any())
            {
                catdat.LeadingComments.Add(" List of roles and users assigned to them. The server is always assigned to the Admin role, and anyone who's left is assigned to Default.");
                catdat.LeadingComments.Add(" Split the list of users (steamIDs) and permissions with spaces, and specify a color known by /color.");
            }

            //Add default keys
            catdat.Keys.AddKey("AdminUsers", "");
            catdat.Keys.AddKey("DefaultPermissions", string.Join(" ", PermissionManager.GetRoleByName("Default").Permissions));

            foreach (var key in catdat.Keys)
            {
                var name = key.KeyName;
                var value = key.Value;
                string role = null;

                //Skip if unknown entry
                if (!name.EndsWith("users", StringComparison.InvariantCultureIgnoreCase)
                    && !name.EndsWith("color", StringComparison.InvariantCultureIgnoreCase)
                    && !name.EndsWith("permissions", StringComparison.InvariantCultureIgnoreCase))
                    continue;

                //Skip if invalid entry
                if (name.EqualsTo(true, "DefaultUsers")|| name.EqualsTo(true, "DefaultColor")
                    || name.EqualsTo(true, "AdminColor") || name.EqualsTo(true, "AdminPermissions"))
                    continue;

                //Get role name
                role = Regex.Replace(name, @"(users|color|permissions)$", "", RegexOptions.IgnoreCase);

                //Create if not existing
                var myrole = PermissionManager.Roles.FirstOrDefault(r => r.Name.EqualsTo(true, role));
                if (myrole == null)
                {
                    myrole = new PermissionRole(role, Color.White);
                    PermissionManager.Roles.Add(myrole);
                }

                //Defined properties for role
                if (name.EndsWith("color", StringComparison.InvariantCultureIgnoreCase))
                {
                    //GOT THE COLOR
                    Color col;
                    if (MserGlobals.TryParseColor(value, out col, true))
                    {
                        myrole.Color = col;
                    }
                }
                else if (name.EndsWith("users", StringComparison.InvariantCultureIgnoreCase))
                {
                    //Remove all users of this role
                    foreach (var user in PermissionManager.GetUserIdsOfRole(myrole).ToList())
                    {
                        PermissionManager.AddUserToRole(user, PermissionManager.GetRoleByName("Default"), false);
                    }

                    //GOT THE USERS
                    foreach (var idstr in value.Split(new [] {' '}, StringSplitOptions.RemoveEmptyEntries))
                    {
                        ulong id;
                        if (ulong.TryParse(idstr, out id))
                        {
                            PermissionManager.AddUserToRole(id, myrole, false);
                        }
                    }
                }
                else if (name.EndsWith("permissions", StringComparison.InvariantCultureIgnoreCase))
                {
                    //Remove all permissions
                    myrole.Permissions.Clear();

                    //GOT THE PERMS
                    foreach (var permstr in value.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries))
                    {
                        var perm = Permission.ParsePermission(permstr);
                        if (perm != null)
                        {
                            myrole.Permissions.Add(perm);
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Custom writing code of this ConfigEntry's category, using the specified IniData which can be read or written to.
        /// <para />Return true to execute old writing code AFTER this custom code, false to skip it entriely.
        /// </summary>
        /// <returns></returns>
        public override bool WriteCategory(IniData data)
        {
            //Write all role settings
            data.Sections.AddSection(this.GetCategory());

            var sec = data.Sections.GetSectionData(this.GetCategory());
            foreach (var role in PermissionManager.Roles)
            {
                if (!(role is PermissionRoleAdmin))
                {
                    //Only store color if not default role
                    if (!(role is PermissionRoleDefault))
                        if (!sec.Keys.AddKey(role.Name + "Color", MserGlobals.GetNameOfColor(role.Color, ReturnValueFormat.HexOnly)))
                        {
                            //Already exists, set value
                            sec.Keys.GetKeyData(role.Name + "Color").Value = MserGlobals.GetNameOfColor(role.Color, ReturnValueFormat.HexOnly);
                        }

                    //Store permissions if not admin
                    var permissions = string.Join(" ", role.Permissions);
                    if (!sec.Keys.AddKey(role.Name + "Permissions", permissions))
                    {
                        //Already exists, set value
                        sec.Keys.GetKeyData(role.Name + "Permissions").Value = permissions;
                    }
                    continue;
                }

                //Store users list for all roles
                var users = PermissionManager.AssignedUsers.Where(kv => kv.Value == role).Select(kv => kv.Key).ToList();
                if (!sec.Keys.AddKey(role.Name + "Users", string.Join(" ", users)))
                {
                    //Already exists, set value
                    sec.Keys.GetKeyData(role.Name + "Users").Value = string.Join(" ", users);
                }
            }

            return false;
        }
    }
}

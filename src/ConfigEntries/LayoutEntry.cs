﻿using System;
using System.Linq;

namespace DuckGame.MserDuck.ConfigEntries
{
    public class LayoutEntry : ConfigEntry
    {
        /// <summary>
        /// The name of the ConfigEntry in the config.cfg file.
        /// </summary>
        public override string GetName()
        {
            return "Layout";
        }

        /// <summary>
        /// The default value of the ConfigEntry when first generated.
        /// </summary>
        /// <returns></returns>
        public override object GetDefaultValue()
        {
            return "DETECT";
        }

        /// <summary>
        /// The description comments of the config entry, to add above it. The commenting // are added on their own.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[]
            {
                "Override the detected keyboard layout",
                "Supported: " + string.Join("; ", KeyLayoutManager.LayoutsList.OrderBy(x => x))
            };
        }

        /// <summary>
        /// The category of this config entry.
        /// </summary>
        /// <returns></returns>
        public override string GetCategory()
        {
            return "Input";
        }

        /// <summary>
        /// Gets invoked after the value is read.
        /// </summary>
        public override void AfterRead()
        {
            var value = this.Value.ToString();
            if (value.Equals("DETECT", StringComparison.InvariantCultureIgnoreCase))
                value = KeyLayoutManager.GetDetectedKeyLayout();
            KeyLayoutManager.Layout = value;
        }
    }
}

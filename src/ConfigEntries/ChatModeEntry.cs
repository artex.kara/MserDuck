﻿namespace DuckGame.MserDuck.ConfigEntries
{
    public class ChatModeEntry : ConfigEntry
    {
        /// <summary>
        /// The name of the ConfigEntry in the config.cfg file.
        /// </summary>
        public override string GetName()
        {
            return "ChatMode";
        }

        /// <summary>
        /// The default value of the ConfigEntry when first generated.
        /// </summary>
        /// <returns></returns>
        public override object GetDefaultValue()
        {
            return 1;
        }

        /// <summary>
        /// The description comments of the config entry, to add above it. The commenting // are added on their own.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[]
            {
                "Change the chat mode",
                "0 = Chat opens and sends with chat key only. Cancel with escape.",
                "1 = Chat opens with chat key, but sends with enter. Cancel with escape.",
                "2 = Chat opens with chat key, and sends with either chat key or enter. Cancel with escape.",
                "As of now, you will not be able to rebind the enter or escape keys mentioned above."
            };
        }

        /// <summary>
        /// The category of this config entry.
        /// </summary>
        /// <returns></returns>
        public override string GetCategory()
        {
            return "Chat";
        }

        /// <summary>
        /// Gets invoked after the value is read.
        /// </summary>
        public override void AfterRead()
        {
            ChatManager.ChatMode = (ChatManager.ChatModeSetting)int.Parse(this.Value.ToString());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace DuckGame.MserDuck.ConfigEntries
{
    public class MatchSettingsEntry : ConfigEntry
    {
        private static bool _addedEntries = false;
        private bool _alreadyRead = false;

        private int num = 0;

        private static readonly IEnumerable<MatchSetting> settings = TeamSelect2.matchSettings.Concat(TeamSelect2.onlineSettings).OrderBy(x => x.id);

        private MatchSetting GetSetting()
        {
            return settings.ElementAtOrDefault(this.num);
        }

        /// <summary>
        /// The name of the ConfigEntry in the config.cfg file.
        /// </summary>
        public override string GetName()
        {
            if (this.GetSetting() == null)
                return "error";
            return this.GetSetting().id;
        }

        /// <summary>
        /// The category of this config entry.
        /// </summary>
        /// <returns></returns>
        public override string GetCategory()
        {
            return "MatchSettings";
        }

        /// <summary>
        /// The default value of the ConfigEntry when first generated.
        /// </summary>
        /// <returns></returns>
        public override object GetDefaultValue()
        {
            if (this.GetSetting() == null)
                return null;
            return this.GetSetting().defaultValue;
        }

        /// <summary>
        /// Whether a comment displaying the default value should be appended.
        /// </summary>
        public override bool ShowDefaultValue()
        {
            return false;
        }

        /// <summary>
        /// The description comments of the config entry, to add above it. The commenting string (ConfigManager.commentString) is added on its own.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            if (this.num == 0)
                return new[] {"Default values for the match settings. Modifiers can be set using autoexec."};

            return null;
        }

        /// <summary>
        /// Gets invoked before the value is read. Return false to cancel the read operation.
        /// </summary>
        public override bool BeforeRead()
        {
            if (MatchSettingsEntry._addedEntries)
                return true;

            //Add new config entries
            MatchSettingsEntry._addedEntries = true;
            for (var i = 1; i < settings.Count(); i++)
            {
                if (settings.ElementAt(i).id == "modifiers")
                    continue;

                var e = new MatchSettingsEntry {num = i};
                ConfigManager.NewConfigEntries.Add(e);
            }

            return true;
        }

        /// <summary>
        /// Gets invoked after the value is read. If the key does not exist, this does not get executed.
        /// </summary>
        public override void AfterRead()
        {
            var converted = Extensions.ConvertTo(this.Value, this.GetDefaultValue().GetType());

            if (converted == null)
                throw new FormatException(string.Format("Could not convert value \"{0}\" to type \"{1}\".", this.Value, this.GetDefaultValue().GetType()));

            if (!this._alreadyRead)
            {
                this._alreadyRead = true;

                if (this.GetSetting() == null)
                    return;

                this.GetSetting().value = converted;
                this.GetSetting().defaultValue = converted;
            }
        }
    }
}

﻿using IniParser.Model;

namespace DuckGame.MserDuck.ConfigEntries
{
    /// <summary>
    /// A config entry inside of the config.cfg file, with a value of type T. By inheriting this class, the subclass will be considered a config entry automatically and added to the config.cfg.
    /// </summary>
    public abstract class ConfigEntry
    {
        //HINT: Add any new class inheriting from ConfigEntry to the ConfigEntries namespace in order for it to be recognized as a config.cfg entry.

        /// <summary>
        /// The name of the ConfigEntry in the config.cfg file.
        /// </summary>
        public virtual string GetName()
        {
            return null;
        }

        /// <summary>
        /// The description comments of the config entry, to add above it. The commenting string (ConfigManager.commentString) is added on its own.
        /// </summary>
        /// <returns></returns>
        public virtual string[] GetDescription()
        {
            return null;
        }

        /// <summary>
        /// The category of this config entry.
        /// </summary>
        /// <returns></returns>
        public virtual string GetCategory()
        {
            return "General";
        }

        /// <summary>
        /// The default value of the ConfigEntry when first generated.
        /// </summary>
        /// <returns></returns>
        public virtual object GetDefaultValue()
        {
            return null;
        }

        /// <summary>
        /// The cached value of the ConfigEntry. Setting/Getting does not invoke any I/O calls.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Whether a comment displaying the default value should be appended.
        /// </summary>
        public virtual bool ShowDefaultValue()
        {
            return true;
        }

        /// <summary>
        /// Gets invoked before the value is read. Return false to cancel the read operation.
        /// </summary>
        public virtual bool BeforeRead()
        {
            return true;
        }

        /// <summary>
        /// Gets invoked before the value is written. Return false to cancel the write operation.
        /// </summary>
        public virtual bool BeforeWrite()
        {
            return true;
        }

        /// <summary>
        /// Gets invoked after the value is read. If the key does not exist, this does not get executed.
        /// </summary>
        public virtual void AfterRead()
        {
            
        }

        /// <summary>
        /// Gets invoked after the value is written. If the value was null, this does not get executed.
        /// </summary>
        public virtual void AfterWrite()
        {
            
        }

        /// <summary>
        /// Custom reading code of this ConfigEntry's category, using the specified IniData which can be read or written to.
        /// <para />Return true to execute old reading code AFTER this custom code, false to skip it entriely (not changing the <code>this.Value</code> property).
        /// </summary>
        /// <returns></returns>
        public virtual bool ReadCategory(IniData data)
        {
            return true;
        }

        /// <summary>
        /// Custom writing code of this ConfigEntry's category, using the specified IniData which can be read or written to.
        /// <para />Return true to execute old writing code AFTER this custom code, false to skip it entriely.
        /// </summary>
        /// <returns></returns>
        public virtual bool WriteCategory(IniData data)
        {
            return true;
        }
    }
}

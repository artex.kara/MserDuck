﻿namespace DuckGame.MserDuck.ConfigEntries
{
    public class ClearLogEntry : ConfigEntry
    {
        /// <summary>
        /// The name of the ConfigEntry in the config.cfg file.
        /// </summary>
        public override string GetName()
        {
            return "ClearLog";
        }

        /// <summary>
        /// The category of this config entry.
        /// </summary>
        /// <returns></returns>
        public override string GetCategory()
        { 
            return "General";
        }

        /// <summary>
        /// The description comments of the config entry, to add above it. The commenting string (ConfigManager.commentString) is added on its own.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[]
            {
                "When set, removes the ducklog.txt in certain cases.",
                "Possible values: Never; OnStart; OnQuit; <FileSize> (e.g. 250kb or 1mb / The filesize check is always done on startup.)"
            };
        }

        /// <summary>
        /// The default value of the ConfigEntry when first generated.
        /// </summary>
        /// <returns></returns>
        public override object GetDefaultValue()
        {
            return "Never";
        }

        /// <summary>
        /// Gets invoked after the value is read.
        /// </summary>
        public override void AfterRead()
        {
            //Parse the value
            switch (this.Value.ToString().ToLowerInvariant())
            {
                case "never":
                    DuckLogManager.RemoveLog = DuckLogManager.RemoveLogMode.Never;
                    break;
                case "onstart":
                    DuckLogManager.RemoveLog = DuckLogManager.RemoveLogMode.OnStart;
                    break;
                case "onquit":
                    DuckLogManager.RemoveLog = DuckLogManager.RemoveLogMode.OnQuit;
                    break;
                default:
                    DuckLogManager.MaxFileSize = FileSize.Parse(this.Value.ToString());
                    DuckLogManager.RemoveLog = DuckLogManager.RemoveLogMode.FileSize;
                    break;
            }
        }
    }
}

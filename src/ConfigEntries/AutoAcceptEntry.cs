﻿namespace DuckGame.MserDuck.ConfigEntries
{
    public class AutoAcceptEntry : ConfigEntry
    {
        /// <summary>
        /// The name of the ConfigEntry in the config.cfg file.
        /// </summary>
        public override string GetName()
        {
            return "AutoAccept";
        }

        /// <summary>
        /// The default value of the ConfigEntry when first generated.
        /// </summary>
        /// <returns></returns>
        public override object GetDefaultValue()
        {
            return true;
        }

        /// <summary>
        /// The description comments of the config entry, to add above it. The commenting // are added on their own.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[]
            {
                "Enables auto-accept for expressions. Disable this for online play!"
            };
        }

        /// <summary>
        /// Gets invoked before the value is written. Return false to cancel the write operation.
        /// </summary>
        public override bool BeforeWrite()
        {
            //TODO: AutoAcceptEntry - Notify user before write / after read (before setting) about the dangers?

            return base.BeforeWrite();
        }

        /// <summary>
        /// Gets invoked after the value is read.
        /// </summary>
        public override void AfterRead()
        {
            CodeSnippet._autoAccept = bool.Parse(this.Value.ToString());
        }
    }
}

﻿using System;

namespace DuckGame.MserDuck.ConfigEntries
{
    public class ConnectionStatusEntry : ConfigEntry
    {
        /// <summary>
        /// The name of the ConfigEntry in the config.cfg file.
        /// </summary>
        public override string GetName()
        {
            return "ScoreboardKey";
        }

        /// <summary>
        /// The default value of the ConfigEntry when first generated.
        /// </summary>
        /// <returns></returns>
        public override object GetDefaultValue()
        {
            return "F1";
        }

        /// <summary>
        /// The description comments of the config entry, to add above it. The commenting string (ConfigManager.commentString) is added on its own.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new [] {"The key for showing the connection status UI / scoreboard."};
        }

        /// <summary>
        /// The category of this config entry.
        /// </summary>
        /// <returns></returns>
        public override string GetCategory()
        {
            return "Input";
        }

        /// <summary>
        /// Gets invoked after the value is read. If the key does not exist, this does not get executed.
        /// </summary>
        public override void AfterRead()
        {
            var key = BindManager.GetKeyByString(this.Value.ToString());
            if (key == Keys.None)
            {
                throw new FormatException("Could not find a key with the specified name.");
            }
            else
            {
                ChatManager.ScoreboardKey = key;
            }
        }
    }
}

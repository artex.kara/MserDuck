﻿namespace DuckGame.MserDuck.ConfigEntries
{
    public class SmartColorEntry : ConfigEntry
    {
        /// <summary>
        /// The name of the ConfigEntry in the config.cfg file.
        /// </summary>
        public override string GetName()
        {
            return "SmartColor";
        }

        /// <summary>
        /// The default value of the ConfigEntry when first generated.
        /// </summary>
        /// <returns></returns>
        public override object GetDefaultValue()
        {
            return false;
        }

        /// <summary>
        /// The description comments of the config entry, to add above it. The commenting // are added on their own.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[]
            {
                "Enables a high contrast text color for chat instead of the regular black/white only"
            };
        }

        /// <summary>
        /// The category of this config entry.
        /// </summary>
        /// <returns></returns>
        public override string GetCategory()
        {
            return "Chat";
        }

        /// <summary>
        /// Gets invoked after the value is read.
        /// </summary>
        public override void AfterRead()
        {
            ChatManager.SmartColor = bool.Parse(this.Value.ToString());
        }
    }
}

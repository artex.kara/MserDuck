﻿namespace DuckGame.MserDuck.ModifierHandlers
{
    public class ShuffleModifier : ModifierHandler
    {
        /// <summary>
        /// Returns the name of the modifier.
        /// </summary>
        /// <returns></returns>
        public override string GetName()
        {
            return "Shuffle";
        }

        /// <summary>
        /// What category this modifier should be found inside of.
        /// </summary>
        public override string GetCategory()
        {
            return "General";
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[]
            {
                "Controls are randomly",
                "shuffled every level."
            };
        }

        /// <summary>
        /// Whether this modifier is not implemented yet, and as such should not be used.
        /// <para />Selecting it with /modifier or in the modifier list will show a warning about the modifier not being implemented.
        /// <para />You may implement certain update behaviour regardless.
        /// </summary>
        /// <returns></returns>
        public override bool IsNotImplemented()
        {
            return true;
        }

        //NYI: Shuffle Modifier - at start of every level, randomly shuffle the controls of every duck
        //  Probably keyboard-only for now, randomize order of input keys as they are set (be sure to reset whenever not ingame anymore!!)
    }
}

﻿using System.Reflection;

namespace DuckGame.MserDuck.ModifierHandlers
{
    public class ManualControlModifier : ModifierHandler
    {
        /// <summary>
        /// What category this modifier should be found inside of.
        /// </summary>
        public override string GetCategory()
        {
            return "Intermissions";
        }

        public override string GetName()
        {
            return "Manual Control";
        }

        private static readonly FieldInfo _scroll = MserGlobals.GetField("RockScoreboard", "_desiredScroll");

        public override void Update()
        {
            if (MserGlobals.GetGameState() == GameState.RockThrow)
            {
                var xadd = 0f;
                var xcnt = 0;
                var ducks = Level.current.things[typeof(Duck)];

                foreach (Duck duck in ducks)
                {
                    //Allow control
                    duck.mindControl = null;

                    //Average the camera position
                    xadd += duck.x;
                    xcnt++;

                    //TODO: ManualControl - Fix ragdoll rendering (would need to draw custom renderer on top of default)
                }

                if (xcnt > 0)
                {
                    //Set camera position
                    _scroll.SetValue(Level.current, xadd / xcnt);
                }
            }
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[] { "Move around freely during the intermissions." };
        }
    }
}

﻿namespace DuckGame.MserDuck.ModifierHandlers
{
    public class DualWieldingModifier : ModifierHandler
    {
        /// <summary>
        /// Returns the name of the modifier.
        /// </summary>
        /// <returns></returns>
        public override string GetName()
        {
            return "Dual Wielding";
        }

        /// <summary>
        /// Whether this modifier is not implemented yet, and as such should not be used.
        /// <para />Selecting it with /modifier or in the modifier list will show a warning about the modifier not being implemented.
        /// <para />You may implement certain update behaviour regardless.
        /// </summary>
        /// <returns></returns>
        public override bool IsNotImplemented()
        {
            return true;
        }

        /// <summary>
        /// What category this modifier should be found inside of.
        /// </summary>
        public override string GetCategory()
        {
            return "Weapons";
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new []
            {
                "Why only use one weapon",
                "if you have two hands?"
            };
        }

        //NYI: DualWieldingModifier - override grabbing code to allow for a second holdObject to be defined.
        //  Set holdable's properties accordingly, and pass OnPressAction etc over manually when a second key is pressed.
        //  Configurable "drop other weapon" key as well. Option to, instead of using second set of keys, change behaviour by holding a modifier key (e.g. strafe)
    }
}

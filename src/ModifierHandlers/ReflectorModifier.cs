﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.ModifierHandlers
{
    public class ReflectorModifier : ModifierHandler
    {
        private static Type _refType = MserGlobals.GetType("ReflectorBlock");

        /// <summary>
        /// Returns the name of the modifier.
        /// </summary>
        /// <returns></returns>
        public override string GetName()
        {
            return "Reflectiveness";
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[] { "Everything reflects bullets." };
        }

        /// <summary>
        /// Whether this modifier is not implemented yet, and as such should not be used.
        /// <para />Selecting it with /modifier or in the modifier list will show a warning about the modifier not being implemented.
        /// <para />You may implement certain update behaviour regardless.
        /// </summary>
        /// <returns></returns>
        public override bool IsNotImplemented()
        {
            return true;
        }

        /// <summary>
        /// Called every tick if the modifier is enabled and a game is currently in progress. No checks for game state are being done!
        /// </summary>
        /// <returns></returns>
        public override void Update()
        {
            if (!MserGlobals.GetListOfBlocks(false).Any(b => b.GetType().Name == "ReflectorBlock"))
            {
                ReplaceAll();
            }
        }

        public static void ReplaceAll()
        {
            if (_refType == null || MserGlobals.GetGameState() != GameState.Playing) return;

            var poslist = new List<Vec2>();

            foreach (var gr in MserGlobals.GetListOfBlockGroups())
            {
                //Add AutoBlocks from BlockGroups to remove.
                foreach (var bl in gr.blocks)
                {
                    var pos = ReplaceBlock(bl, false);
                    if (pos != null)
                        poslist.Add(pos.Value);
                }

                //Tell the blockgroup to wreck the marked blocks
                gr.Wreck();
            }

            foreach (var bl in MserGlobals.GetListOfBlocks(false))
            {
                var pos = ReplaceBlock(bl, true);
                if (pos != null)
                    poslist.Add(pos.Value);
            }

            foreach (var pos in poslist)
            {
                var newblock = (Block)Activator.CreateInstance(_refType, pos.x, pos.y);
                Level.Add(newblock);
            }
        }

        private static Vec2? ReplaceBlock(Block bl, bool skipWreck)
        {
            //Shouldn't be a reflectorblock and not any block containing things
            var name = bl.GetType().Name;
            if (name == "ReflectorBlock" || bl is Door || bl is VerticalDoor || bl is Window || bl is IContainAThing)
                return null;

            if (skipWreck)
                bl.skipWreck = true;

            bl.shouldWreck = true;

            //Create block at same spot
            return bl.position;
        }
    }
}

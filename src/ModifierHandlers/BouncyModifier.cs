﻿namespace DuckGame.MserDuck.ModifierHandlers
{
    public class BouncyModifier : ModifierHandler
    {
        /// <summary>
        /// What category this modifier should be found inside of.
        /// </summary>
        public override string GetCategory()
        {
            return "Physics";
        }

        public override string GetName()
        {
            return "Bouncy Stuff";
        }

        public override void Update()
        {
            SetBouncy(3);
        }

        /// <summary>
        /// Called when the modifier has been disabled.
        /// </summary>
        public override void OnDisabled()
        {
            //Since the last bouncy per-object is unknown, use a fallback value
            SetBouncy(0);
        }

        private static void SetBouncy(float value)
        {
            var thingList = Level.current.things[typeof(MaterialThing)];
            if (thingList == null)
                return;

            foreach (MaterialThing thing in thingList)
            {
                if (thing is Duck)
                    continue;
                thing.bouncy = value;
            }
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[] { "Makes EVERYTHING bounce around." };
        }
    }
}

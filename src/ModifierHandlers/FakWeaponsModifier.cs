﻿namespace DuckGame.MserDuck.ModifierHandlers
{
    public class FakWeaponsModifier : ModifierHandler
    {
        /// <summary>
        /// What category this modifier should be found inside of.
        /// </summary>
        public override string GetCategory()
        {
            return "Weapons";
        }

        /// <summary>
        /// Returns the name of the modifier.
        /// </summary>
        /// <returns></returns>
        public override string GetName()
        {
            return "Fak Weapons";
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[] { "Weapons randomly turn into fake variants." };
        }

        //Implementation in WeaponSubstitutionHandler.cs
    }
}

﻿namespace DuckGame.MserDuck.ModifierHandlers
{
    public class SlipperyModifier : ModifierHandler
    {
        /// <summary>
        /// What category this modifier should be found inside of.
        /// </summary>
        public override string GetCategory()
        {
            return "Physics";
        }

        public override string GetName()
        {
            return "Slippery";
        }

        public override void Update()
        {
            SetFriction(0f);
        }

        /// <summary>
        /// Called when the modifier has been disabled.
        /// </summary>
        public override void OnDisabled()
        {
            //Reset as best as possible
            SetFriction(0.1f);
        }

        private static void SetFriction(float value)
        {
            var thingList = Level.current.things[typeof(PhysicsObject)];
            if (thingList == null)
                return;

            foreach (PhysicsObject thing in thingList)
            {
                thing.friction = value;
            }
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[] { "Who needs friction anyway." };
        }
    }
}
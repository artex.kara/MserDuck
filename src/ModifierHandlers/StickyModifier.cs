﻿namespace DuckGame.MserDuck.ModifierHandlers
{
    public class StickyModifier : ModifierHandler
    {
        /// <summary>
        /// What category this modifier should be found inside of.
        /// </summary>
        public override string GetCategory()
        {
            return "Physics";
        }

        public override string GetName()
        {
            return "Sticky Ragdolls";
        }

        public override void Update()
        {
            SetBouncy(-5f);
        }

        /// <summary>
        /// Called when the modifier has been disabled.
        /// </summary>
        public override void OnDisabled()
        {
            SetBouncy(0.6f);
        }

        private static void SetBouncy(float value)
        {
            var thingList = Level.current.things[typeof(RagdollPart)];
            if (thingList == null)
                return;

            foreach (RagdollPart thing in thingList)
            {
                thing.bouncy = value;
            }
        }

        /// <summary>
        /// Description for this modifier, to be shown when toggling it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetDescription()
        {
            return new[] { "Ragdolls like to stick to everything." };
        }
    }
}

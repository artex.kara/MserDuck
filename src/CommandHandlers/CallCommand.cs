﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class CallCommand : CommandHandler
    {
        //TODO: /call - allow calls to STATIC methods (add another icon to the autocomplete entries)

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            var errors = new List<ArgumentError>();
            try
            {
                if (args.Count < 2)
                {
                    errors.Add(ArgumentError.InvalidArgumentCount(2, args.Count, true));
                }
                else
                {
                    //Check if part of info list
                    var tuple = CommandHelpers.HandleObjectListByName(args[0], MserGlobals.GetLocalDuck());
                    if (tuple != null)
                    {
                        if (tuple.Item1.Count == 0)
                        {
                            errors.Add(new ArgumentError(0, "No thing of this type were found in the level.", Severity.Warning));
                        }

                        var type = tuple.Item2;
                        var cachedInfo = CacheManager.GetCallAutoCompleteInfo(type);
                        if (cachedInfo == null)
                            return errors;

                        var info = cachedInfo.Where(i => i.Name.Equals(args[1], StringComparison.InvariantCultureIgnoreCase)).ToList();
                        if (info.Count == 0)
                        {
                            errors.Add(new ArgumentError(1, string.Format("The specified method is not a part of \"{0}\".", type)));
                        }
                        else
                        {
                            //Check if too many arguments
                            var paranums = info.Select(i => i.GetParameters().Length);
                            var realnum = args.Count - 2;

                            if (!paranums.Contains(realnum))
                            {
                                errors.Add(new ArgumentError(args.Count - 1,
                                                             string.Format("No overload of the specified method accepts {0}.", Extensions.GetPlural(realnum, "parameter"))));
                            }

                            //Check datatype
                            var failedoverloads = 0;
                            foreach (var member in info)
                            {
                                //Try method overloads for fitting types
                                var paras = member.GetParameters();

                                if (paras.Length != args.Count - 2)
                                {
                                    //Too few/many params for overload, as stated above
                                    continue;
                                }

                                for (var i = 0; i < paras.Length; i++)
                                {
                                    var expect = paras[i].ParameterType;
                                    var objs = CommandHelpers.ParseStringToObjects(new[] {args[i + 2]}, MserGlobals.GetLocalDuck());
                                    var result = objs == null ? null : ((objs.Count == 1) ? objs[0].GetType() : objs.GetType());

                                    if (expect != result && !Extensions.CanConvert(expect, result))
                                    {
                                        //Invalid parameter type, try next overload
                                        failedoverloads++;
                                        break;
                                    }
                                }
                            }

                            if (failedoverloads == info.Count)
                            {
                                //All overloads are fuckled
                                var overlist = info.Select(member => member.GetParameters())
                                    .Select(paras => string.Join(", ", paras.Select(p => string.Format("{0} {1}", p.ParameterType.Name, p.Name)))).ToList();
                                var overloads = string.Join("\n", overlist);
                                errors.Add(new ArgumentError(1, "No overload of the specified method accepts these parameters. Expected one of:\n" + overloads));
                            }
                        }
                    }
                    else
                    {
                        errors.Add(new ArgumentError(0, "The specified type of thing is unknown."));
                    }
                }
            }
            catch (Exception e)
            {
                Program.LogLine("Fokd the Callcommand ArgumentErrors: " + e.Message + e.StackTrace);
            }
            return errors;
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player (or the local player, if IsLocalOnly returns true).
        /// <para />Return null to indicate a successful execute (command executed as expected), an informing ExecuteInfo about the command's actions,
        /// or an error ExecuteInfo to indicate failure (specified parameters caused exception).
        /// <para />Do not use ExecuteErrors for parameter validation (this is done by CheckArgumentsValid before execution), but only for anything that can only be determined on Execute!
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            try
            {

                object[] pars = null;
                string objName = args[0];

                if (args.Count > 2)
                {
                    List<string> strList = args.GetRange(2, args.Count - 2);
                    pars = CommandHelpers.ParseStringToObjects(strList).ToArray();
                }

                var tuple = CommandHelpers.HandleObjectListByName(objName, sender.duck);

                var objects = tuple.Item1;
                var ty = tuple.Item2;

                if (ty == null || objects == null)
                {
                    return new ExecuteInfo("Invalid type specified.", 0);
                }

                if (objects.Count == 0)
                {
                    return new ExecuteInfo("No things of this type found.", Severity.Warning);
                }

                foreach (var th in objects)
                {
                    if (th == null) continue;

                    var method = MserGlobals.GetMethod(ty, args[1]);
                    if (method == null)
                    {
                        return new ExecuteInfo("The specified method cannot be found.", 1);
                    }
                    else
                    {
                        method.Invoke(th, pars);
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                Program.LogLine("Error in CallCommand: " + ex.Message + ex.StackTrace);
                return new ExecuteInfo("Unexpected error calling the specified method.", -1);
            }
        }

        /// <summary>
        /// Returns a list containing the expected arguments, as displayed in the "usage" message for this command, as well as for use for naming or type checking.
        /// <para />The order of the CommandArguments in the list is obviously important.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            return new[] {new CommandArgument("Thing") {DisplayText = "[all/near]<Thing>[!]"}, "Method", "?Parameters",};
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new string[] { "Calls the specified method with the given parameters on the specified object.",
                                  "Add ! to exclude sender from all/near calls." };
        }

        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new string[] { "call", "invoke", "cl", "inv" };
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered. Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (index <= 0)
            {
                //Some neat autocompleteyo
                string prefix = null;
                if (args.Count > 0 && args[0].StartsWith("all"))
                {
                    prefix = "all";
                }
                else if (args.Count > 0 && args[0].StartsWith("near"))
                {
                    prefix = "near";
                }

                if (prefix != null)
                {
                    //CLASS NAMES QUIK!
                    return Editor.GetSubclasses(typeof(Thing)).Select(t => new AutoCompleteEntry(prefix + t.Name));
                }

                return CommandHelpers.ObjectListNames.Select(s => (AutoCompleteEntry)s).Concat(new[]
                {
                    new AutoCompleteEntry("<all...>", "all") {InsertSpace = false},
                    new AutoCompleteEntry("<near...>", "near") {InsertSpace = false},
                });
            }
            else if (index == 1)
            {
                //Get properties and fields
                try
                {
                    var stuf = CommandHelpers.HandleObjectListByName(args[0], MserGlobals.GetLocalDuck());
                    if (stuf == null)
                        return null;

                    var type = stuf.Item2;
                    var info = CacheManager.GetCallAutoCompleteInfo(type);
                    if (info == null)
                        return null;

                    return info.DistinctBy(i => i.Name).Select(i => new AutoCompleteEntry(i.Name, drawDelegate: CommandHelpers.GetInfoIconDrawDelegate(i)));
                }
                catch
                {
                    //broken *11
                }
            }

            return null;
        }
    }
}

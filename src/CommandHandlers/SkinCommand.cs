﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class SkinCommand : CommandHandler
    {
        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] {"skin", "sprite", "spr"};
        }

        /// <summary>
        /// Returns a string containing the expected arguments, as displayed in the "usage" message for this command.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            return new CommandArgument[] {"Skin Name"};
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new []
            {
                "Replaces your duck's sprites with the specified set.",
                "For help on how to set this up, check /opendir skins"
            };
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player.
        /// <para />Return true to indicate success, false to indicate failure (will print usage).
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            if (args == null || !args.Any())
            {
                //Show currently selected skin
                if (!sender.localPlayer)
                    return null;

                if (!Skin.SelectedSkins.ContainsKey(sender) || Skin.SelectedSkins[sender] == Skin.DefaultSkin)
                    return new ExecuteInfo("No skin is currently selected.");

                return new ExecuteInfo("Currently selected skin: " + Skin.SelectedSkins[sender]);
            }
            else
            {
                //Select a new skin
                var str = string.Join(" ", args);
                try
                {
                    Skin skin;
                    if (str.EqualsTo(true, Skin.DefaultSkinName))
                        skin = Skin.DefaultSkin;
                    else
                        skin = new Skin(str);

                    var load = skin.Load();
                    if (load == null)
                    {
                        //Apply it!
                        var apply = skin.Apply(sender);
                        if (apply != null)
                        {
                            return new ExecuteInfo("Could not apply skin: " + apply, -1);
                        }
                        return null;
                    }
                    else
                    {
                        return new ExecuteInfo("Could not load skin: " + load, -1);
                    }
                }
                catch (Exception ex)
                {
                    Program.LogLine("Skin trouble: " + ex.Message + ex.StackTrace);
                    return new ExecuteInfo("Could not create skin: " + ex.Message, -1);
                }
            }
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            if (args.Count > 0)
            {
                var str = string.Join(" ", args);
                if (!CacheManager.GetSkinAutoCompleteInfo().Any(f => f.Value.Equals(str, StringComparison.InvariantCultureIgnoreCase)))
                {
                    //Try to update this data first
                    CacheManager.ClearSkinAutoCompleteInfo();
                    if (!CacheManager.GetSkinAutoCompleteInfo().Any(f => f.Value.Equals(str, StringComparison.InvariantCultureIgnoreCase)))
                        yield return new ArgumentError(0, "The specified skin can not be found.");
                }
            }
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered. Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            //Show folder names (always show since spaces could be a thing)
            return CacheManager.GetSkinAutoCompleteInfo();
        }
    }
}

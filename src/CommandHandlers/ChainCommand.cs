﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class ChainCommand : CommandHandler
    {
        private static readonly List<ChainedCommand> _chainList = new List<ChainedCommand>();

        private class ChainedCommand
        {
            public readonly List<string> Aliases;

            public int CurrentDelay = 0;

            public int Index = 0;

            public ChainedCommand(IEnumerable<string> aliases)
            {
                this.Aliases = aliases.ToList();
            }
        }

        /// <summary>
        /// Whether this command should execute only for the client that entered it, or for every connected user as well.
        /// </summary>
        /// <returns></returns>
        public override bool IsLocalOnly()
        {
            return true;
        }

        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] {"chain", "ch"};
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player.
        /// <para />Return true to indicate success, false to indicate failure (will print usage).
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            ChainCommand._chainList.Add(new ChainedCommand(args));
            return null;
        }

        internal static void Update()
        {
            if (ChainCommand._chainList.Count == 0) return;

            var removelist = new List<ChainedCommand>();
            foreach (var chain in ChainCommand._chainList)
            {
                //Reduce delay
                if (chain.CurrentDelay > 0)
                {
                    chain.CurrentDelay--;
                    continue;
                }

                //Parse the bits
                do
                {
                    try
                    {
                        var name = chain.Aliases[chain.Index];
                        if (name.StartsWith("delay"))
                        {
                            //Add the delay
                            try
                            {
                                chain.Index++;
                                chain.CurrentDelay = int.Parse(chain.Aliases[chain.Index]);
                            }
                            catch
                            {
                                ChatManager.Echo("Unable to parse chain's delay (must be an integer, in game ticks): \"" + chain.Aliases[chain.Index] + "\".");
                            }
                        }
                        else
                        {
                            //Get params
                            var alias = AliasManager.GetAlias(name);
                            if (alias != null)
                            {
                                var paras = AliasManager.CountParams(alias);
                                if (paras == 0)
                                {
                                    //Execute the alias
                                    AliasManager.ExecuteAlias(name);
                                }
                                else
                                {
                                    //Collect paras
                                    var paralist = new List<string>();
                                    do
                                    {
                                        chain.Index++;
                                        paralist.Add(chain.Aliases[chain.Index]);
                                    } while (paralist.Count < paras);
                                    AliasManager.ExecuteAlias(name, paralist);
                                }
                            }
                        }

                        //Reset queue if last bit
                        if (chain.Index >= chain.Aliases.Count - 1)
                        {
                            removelist.Add(chain);
                            break;
                        }

                        //Cancel if delaying
                        if (chain.CurrentDelay > 0)
                            break;
                    }
                    catch (Exception ex)
                    {
                        ChatManager.Echo("General chain error. See ducklog!");
                        Program.LogLine("General chain error: " + ex.Message + ex.StackTrace);
                    }
                    chain.Index++;
                } while (chain.Index < chain.Aliases.Count);
            }

            //Remove entries
            foreach (var rem in removelist)
            {
                ChainCommand._chainList.Remove(rem);
            }
        }

        /// <summary>
        /// Returns a string containing the expected arguments, as displayed in the "usage" message for this command.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            return new CommandArgument[] { "Aliases" };
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[]
            {
                "Executes multiple aliases in the specified order at once.",
                "Optional parameters are required in this syntax.",
                "Use \"delay <num>\" in order to insert a pause of num ticks."
            };
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered. Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            //TODO: /chain autocomplete - inform about aliases' param names, probably need to do it the way it's done in /alias currently (un-autocompleteable entries)

            //List of aliases
            return AliasManager.Aliases.Select(a => new AutoCompleteEntry(a.Name)).Concat(new[] {new AutoCompleteEntry("delay")});
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc. together with a specific recursed depth.
        /// <para />Only use this overload if this command has to be able to call CheckArgumentsValid as well. For implementation, check /chain or /bind.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            if (args == null || !args.Crop().Any())
            {
                yield return ArgumentError.InvalidArgumentCount();
            }
            else
            {
                var isDelay = false;
                var argCount = -1;
                var lastAlias = "";
                for (int i = 0; i < args.Count; i++)
                {
                    var arg = args[i];
                    var alias = AliasManager.GetAlias(arg);
                    if (alias == null)
                    {
                        //"Unknown alias" context
                        if (isDelay)
                        {
                            //Check if parsable as a delay
                            isDelay = false;
                            int delay;
                            if (!int.TryParse(arg, out delay) || delay <= 0)
                            {
                                //Invalid delay
                                yield return new ArgumentError(i, "Unable to parse delay: needs an integer value, in game ticks, that is > 0");
                            }
                        }
                        else if (arg.EqualsTo(true, "delay"))
                        {
                            //TODO: /chain validation - allow delay as parameter, if context allows it

                            if (argCount > 0)
                                yield return new ArgumentError(i, string.Format("Invalid amount of parameters for alias \"{0}\" specified.", lastAlias));

                            //Delay command!
                            isDelay = true;
                        }
                        else
                        {
                            if (argCount > 0)
                            {
                                //Parameter specified
                                argCount--;
                                continue;
                            }

                            if (arg.StartsWith("delay", StringComparison.InvariantCultureIgnoreCase))
                            {
                                //Info about delay cmd usage
                                yield return new ArgumentError(i, "In order to specify a delay, keep a space between \"delay\" and the amount.", Severity.Info);
                            }

                            //Unknown alias
                            yield return new ArgumentError(i, string.Format("Unknown alias \"{0}\" specified.", arg));
                        }
                    }
                    else
                    {
                        //Found alias
                        argCount = AliasManager.CountParams(alias);
                        if (argCount > 0)
                            lastAlias = arg;
                    }
                }

                if (isDelay)
                {
                    //Didn't finish delay cmd
                    yield return new ArgumentError(args.Count - 1, "Delay does not specify an amount.");
                }

                if (argCount != 0 && !string.IsNullOrWhiteSpace(lastAlias))
                {
                    yield return new ArgumentError(args.Count - 1, string.Format("Invalid amount of parameters for alias \"{0}\" specified.", lastAlias));
                }
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class RoleCommand : CommandHandler
    {
        //TODO: /role execute - allow specifying which permissions a role has:
        //  Either a command's name OR a keyword such as DebugMode

        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] {"role", "roles", "permission", "permissions", "permit"};
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player (or the local player, if IsLocalOnly returns true).
        /// <para />Return null to indicate a successful execute (command executed as expected), an informing ExecuteInfo about the command's actions,
        /// or an error ExecuteInfo to indicate failure (specified parameters caused exception).
        /// <para />Do not use ExecuteErrors for parameter validation (this is done by CheckArgumentsValid before execution), but only for anything that can only be determined on Execute!
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            var name = args[1];
            var role = PermissionManager.GetRoleByName(name);
            if (args[0].EqualsToAny(true, "create", "add"))
            {
                if (role != null)
                    return new ExecuteInfo("Role already exists.", 1);

                var color = Color.White;
                var users = new List<ulong>();
                if (args.Count > 2)
                {
                    //Specified a color?
                    MserGlobals.TryParseColor(args[2], out color, true);

                    if (args.Count > 3)
                    {
                        //Specified users?
                        for (var i = 3; i < args.Count; i++)
                        {
                            users.Add(MserGlobals.ParseProfile(args[i], false).steamID);
                        }
                    }
                }

                //Create the role!
                var newrole = PermissionManager.CreateRole(name, color);
                foreach (var user in users)
                {
                    PermissionManager.AddUserToRole(user, newrole, true);
                }

                CacheManager.UpdateRolesEntries();

                return new ExecuteInfo(string.Format("Created new role \"{0}\"!", name));
            }
            else if (args[0].EqualsToAny(true, "remove", "delete"))
            {
                if (role == null)
                    return new ExecuteInfo("Role doesn't exist.", 1);

                //Reassign users of role
                var users = PermissionManager.GetUsersOfRole(role);
                foreach (var user in users)
                {
                    PermissionManager.AddUserToRole(user, PermissionManager.GetRoleByName("Default"), true);
                }

                //Remove the role!
                PermissionManager.Roles.Remove(role);

                CacheManager.UpdateRolesEntries();

                return new ExecuteInfo(string.Format("Removed the role \"{0}\".", name));
            }
            else if (args[0].EqualsToAny(true, "set", "assign"))
            {
                if (role == null)
                    return new ExecuteInfo("Role doesn't exist.", 1);

                //Assign all users to new role
                for (var i = 2; i < args.Count; i++)
                {
                    var user = MserGlobals.ParseProfile(args[i], false).steamID;
                    PermissionManager.AddUserToRole(user, role, true);
                }

                return new ExecuteInfo(string.Format("User{1} now part of role \"{0}\".", name, args.Count == 3 ? " is" : "s are"));
            }
            else
            {
                //Unknown action
                return new ExecuteInfo("Unknown action specified.", 0);
            }
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[]
            {
                "Simple management of different roles for the permission system.",
                "Allows creating new roles, editing existing ones, and assigning users to them."
            };
        }

        /// <summary>
        /// Returns a list containing the expected arguments, as displayed in the "usage" message for this command, as well as for use for naming or type checking.
        /// <para />The order of the CommandArguments in the list is obviously important.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            if (args.Count > 0 && args[0].EqualsToAny(true, "create", "add"))
            {
                return new CommandArgument[] {"Action/create", "Name", "?Color", "?User(s)" };
            }
            else if (args.Count > 0 && args[0].EqualsToAny(true, "remove", "delete"))
            {
                return new CommandArgument[] { "Action/remove", "Role" };
            }
            else if (args.Count > 0 && args[0].EqualsToAny(true, "set", "assign"))
            {
                return new CommandArgument[] {"Action/set", "Role", "User(s)" };
            }

            return new CommandArgument[] {"Action/create/set/remove", "?..." };
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            if (!args.Crop().Any())
            {
                //SPEZIALO
                yield return new ArgumentError(-1, "Your current role: " + PermissionManager.GetRole(MserGlobals.GetLocalDuck().profile).Name, Severity.Info);
            }

            if (args.Count < 2)
            {
                //Always too few
                yield return ArgumentError.InvalidArgumentCount(2, args.Count, true);
            }
            else
            {
                //Different actions
                var role = PermissionManager.GetRoleByName(args[1]);
                if (args[0].EqualsToAny(true, "create", "add"))
                {
                    //Needs name, may have color and users
                    if (role != null)
                    {
                        yield return new ArgumentError(1, "A role with the specified name already exists!", Severity.Warning);
                    }

                    if (args.Count > 2)
                    {
                        //Check valid color
                        Color color;
                        if (!MserGlobals.TryParseColor(args[2], out color, true))
                        {
                            //NOPE
                            yield return new ArgumentError(2, "The specified color is invalid.");
                        }

                        if (args.Count > 3)
                        {
                            //Check if users exist
                            for (var i = 3; i < args.Count; i++)
                            {
                                var prof = MserGlobals.ParseProfile(args[i], true);
                                if (prof == null)
                                {
                                    //CREP
                                    yield return new ArgumentError(i, "The specified profile is not on this server.", Severity.Warning);
                                }

                                //Change role info
                                var userrole = PermissionManager.GetRole(prof);
                                if (userrole is PermissionRoleAdmin && MserGlobals.IsHosting(prof))
                                {
                                    //Mayn't change server role
                                    yield return new ArgumentError(i, "May not change role of server!");
                                }
                                else
                                {
                                    yield return new ArgumentError(i, "Current role: " + userrole.Name, Severity.Info);
                                }
                            }
                        }
                    }
                }
                else if (args[0].EqualsToAny(true, "remove", "delete"))
                {
                    if (args.Count > 2)
                    {
                        yield return ArgumentError.ArgumentsNotUsed(true, 2);
                    }

                    //Has to exist
                    if (role != null)
                    {
                        //May not remove default or server role
                        if (role is PermissionRoleDefault || role is PermissionRoleAdmin)
                        {
                            yield return new ArgumentError(1, "May not remove standard roles!");
                        }
                        else
                        {
                            //Check who is in it
                            var users = PermissionManager.GetUsersOfRole(role).ToList();
                            if (!users.Any())
                            {
                                yield return new ArgumentError(1, "No users are part of this role.", Severity.Info);
                            }
                            else
                            {
                                //People!!
                                //TODO: /role argerrors - truncate list of users since it'll probably get rly long
                                string display;
                                if (users.Any(p => p.localPlayer))
                                {
                                    //Contains you
                                    if (users.Count == 1)
                                    {
                                        //Contains ONLY you
                                        display = "You are part of this role.";
                                    }
                                    else
                                    {
                                        //Contains you AND others
                                        users.RemoveAll(p => p.localPlayer);
                                        display = string.Format("You and {0} part of this role:\n{1}",
                                                                Extensions.GetPlural(users.Count, "other user is", "other users are"),
                                                                string.Join(", ", users.Select(Extensions.ToNeatString)));
                                    }
                                }
                                else
                                {
                                    //Contains random users
                                    display = string.Format("{0} part of this role:\n{1}",
                                                            Extensions.GetPlural(users.Count, "user is", "users are"),
                                                            string.Join(", ", users.Select(Extensions.ToNeatString)));
                                }

                                //Display who is part of the role you are about to delete
                                yield return new ArgumentError(1, display, Severity.Info);
                            }
                        }
                    }
                    else
                    {
                        yield return new ArgumentError(1, "The specified role does not exist.");
                    }
                }
                else if(args[0].EqualsToAny(true, "set", "assign"))
                {
                    //Has to exist
                    if (role != null)
                    {
                        //Print current role of user(s)
                        for (var i = 2; i < args.Count; i++)
                        {
                            //BUG: /role argerrors - prof never equals null, not even if entering total garbage
                            var prof = MserGlobals.ParseProfile(args[i], true);
                            if (prof == null)
                            {
                                //CREP
                                yield return new ArgumentError(i, "The specified profile is not on this server.", Severity.Warning);
                            }
                            else
                            {
                                //Change role info
                                var userrole = PermissionManager.GetRole(prof);
                                if (userrole is PermissionRoleAdmin && MserGlobals.IsHosting(prof))
                                {
                                    //Mayn't change server role
                                    yield return new ArgumentError(i, "May not change role of server!");
                                }
                                else
                                {
                                    yield return new ArgumentError(i, "Current role: " + userrole.Name, Severity.Info);
                                }
                            }
                        }
                    }
                    else
                    {
                        yield return new ArgumentError(1, "The specified role does not exist.\nYou can create roles using /role create");
                    }
                }
                else
                {
                    //Unknown
                    yield return ArgumentError.ArgumentUnknown(0, "action", true, "create", "set", "remove");
                }
            }
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered, separated by spaces after the commandName.
        /// <para />Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (index == 0)
            {
                //EZPZ!
                return new AutoCompleteEntry[] {"create", "set", "remove"};
            }
            else if (args.Any())
            {
                //OK HERE WE GO
                if (args[0].EqualsToAny(true, "create", "add"))
                {
                    if (index == 1)
                    {
                        //Nothing for name
                        return null;
                    }
                    else if (index == 2)
                    {
                        //Color list
                        if (args[2].StartsWith("#"))
                            return new [] { CacheManager.GetColorAutoCompleteEntry(args[2]) };

                        return CacheManager.GetColorAutoCompleteInfo();
                    }
                    else
                    {
                        //User list
                        //TODO: /role autocompletes - dont show users which were already specified before (same below)
                        //  -> create a commandargument type which accepts a list of parameters (like C#'s params - only allow at end, etc)
                        return CacheManager.GetUsersAutoCompleteInfo();
                    }
                }
                else if (args[0].EqualsToAny(true, "remove", "delete"))
                {
                    //Nothing more for role removal
                    if (index == 1)
                    {
                        return CacheManager.GetRolesAutoCompleteInfo();
                    }
                    return null;
                }
                else if (args[0].EqualsToAny(true, "set", "assign"))
                {
                    if (index == 1)
                    {
                        return CacheManager.GetRolesAutoCompleteInfo();
                    }

                    //User list <------------------------------------------------------------------------------- here
                    return CacheManager.GetUsersAutoCompleteInfo();
                }
            }

            //Unknown action or no args
            return null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class AutoRunCommand : CommandHandler, IBind, IAutoRun
    {
        /// <summary>
        /// List of aliases that get executed on level start.
        /// </summary>
        public static List<string> ExecuteList = new List<string>();

        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] {"autorun"};
        }

        /// <summary>
        /// Whether this command should execute only for the client that entered it, or for every connected user as well.
        /// </summary>
        /// <returns></returns>
        public override bool IsLocalOnly()
        {
            return true;
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            if (args == null || !args.Crop().Any())
            {
                yield return ArgumentError.InvalidArgumentCount();
            }
            else
            {
                //Check similar to /chain
                var argCount = -1;
                var lastAlias = "";
                for (int i = 0; i < args.Count; i++)
                {
                    var arg = args[i];
                    var alias = AliasManager.GetAlias(arg);
                    if (alias == null)
                    {
                        if (argCount > 0)
                        {
                            //Parameter specified
                            argCount--;
                            continue;
                        }

                        //Unknown alias
                        yield return new ArgumentError(i, string.Format("Unknown alias \"{0}\" specified.", arg));
                    }
                    else
                    {
                        //Found alias
                        argCount = AliasManager.CountParams(alias);
                        if (argCount > 0)
                            lastAlias = arg;
                    }
                }
                
                if (argCount != 0 && !string.IsNullOrWhiteSpace(lastAlias))
                {
                    yield return new ArgumentError(args.Count - 1, string.Format("Invalid amount of parameters for alias \"{0}\" specified.", lastAlias));
                }
            }
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player.
        /// <para />Return true to indicate success, false to indicate failure (will print usage).
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            AutoRunCommand.ExecuteList.Clear();

            //TODO: /autobind - Mode to execute a modibind either after the "get ready" or once the transition is over/level just loaded

            if (args[0].ToLowerInvariant() == "reset" || args[0].ToLowerInvariant() == "none")
            {
                return new ExecuteInfo("Reset all autoruns.");
            }

            foreach (var name in args)
            {
                //Don't do alias check, since parameters are checked for in validation step already
                AutoRunCommand.ExecuteList.Add(name);
            }

            if (AutoRunCommand.ExecuteList.Count > 0)
            {
                return new ExecuteInfo(string.Format("Added {0} to execute on level change.", Extensions.GetPlural(AutoRunCommand.ExecuteList.Count, "alias", "aliases")));
            }
            else
            {
                return new ExecuteInfo("No aliases have been added.", Severity.Warning);
            }
        }

        /// <summary>
        /// Returns a string containing the expected arguments, as displayed in the "usage" message for this command.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            return new[] {new CommandArgument("Aliases") {DisplayText = "<aliases>/reset"}, };
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new []
            {
                "Executes the specified aliases on the start of every level.",
                "Optional parameters are required in this syntax.",
                "Use reset to run nothing."
            };
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered, separated by spaces after the commandName.
        /// <para />Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            //TODO: /autorun autocomplete - inform about param names, probably need to do it the way it's done in /alias or /chain (un-autocompleteable entries)
            return AliasManager.Aliases.Select(a => new AutoCompleteEntry(a.Name));
        }
    }
}

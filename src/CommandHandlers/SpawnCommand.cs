﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class SpawnCommand : CommandHandler
    {
        //TODO: /spawn command - type list for anything that should not be spawnable without /spawnex
        //  It won't show those types in the AutoComplete list, and entering their name would show an AE warning informing that it isn't recommended to spawn it
        //  Best example is all those UI-related elements which no one needs, or anything desynced (should there be different "reasons" for not recommending spawn??)

        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] { "spawn", "create", "s" };
        }

        /// <summary>
        /// Returns a list containing the expected arguments, as displayed in the "usage" message for this command, as well as for use for naming or type checking.
        /// <para />The order of the CommandArguments in the list is obviously important.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            return new CommandArgument[] {"Thing Name", "?Count", "?X Coord", "?Y Coord"};
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[] { "Spawns any amount of things at your location.",
                           "Thing name must match the class name of an object type.",
                           "If no position is specified, spawns the thing at your location.",
                           "X and Y are offsets to your duck's position." };
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            var errors = new List<ArgumentError>();
            try
            {
                if (args.Count == 0)
                {
                    errors.Add(ArgumentError.InvalidArgumentCount());
                }
                if (args.Count > 0)
                {
                    //Check if valid thing
                    Type thingType = null;
                    var specialthing = false;
                    Profile profile = null;
                    if (MserGlobals.GetLocalDuck() != null)
                        profile = MserGlobals.GetLocalDuck().profile;
                    var thing = CommandHelpers.CreateSpecialThing(args[0].ToLowerInvariant(), Vec2.Zero, profile);
                    if (thing == null)
                    {
                        thingType = MserGlobals.GetType(args[0]);

                        //Type has to inherit from thing
                        if (thingType != null && !thingType.IsSubclassOf(typeof(Thing)))
                        {
                            errors.Add(new ArgumentError(0, "Type to spawn has to inherit from \"Thing\"."));
                        }
                    }
                    else
                    {
                        thingType = thing.GetType();
                        specialthing = true;
                    }

                    if (thingType == null)
                    {
                        errors.Add(new ArgumentError(0, "Unknown type of thing to spawn specified!"));
                    }
                    else if (!specialthing)
                    {
                        //Check if valid constructor exists
                        var cons = CacheManager.GetSpawnAutoCompleteInfo(thingType);
                        var foundcons = cons.Select(con => con.GetParameters())
                                            .Any(para => para.Length == 2 && para[0].ParameterType == typeof(float) && para[1].ParameterType == typeof(float));

                        if (!foundcons)
                        {
                            errors.Add(new ArgumentError(0, "The specified type does not have the default constructor for things (xpos and ypos).\n" +
                                                            "You will have to use /spawnex with a custom constructor instead."));
                        }
                    }

                    if (args.Count > 1)
                    {
                        //Check count
                        int count;
                        if (!int.TryParse(args[1], out count))
                        {
                            errors.Add(new ArgumentError(1, "The specified amount of things is invalid."));
                        }
                        else if (count <= 0)
                        {
                            errors.Add(new ArgumentError(1, "The specified amount of things has to be >0"));
                        }
                    }
                    if (args.Count > 2)
                    {
                        //Check x position
                        float xpos;
                        if (!float.TryParse(args[2], out xpos))
                        {
                            errors.Add(new ArgumentError(2, "The specified x coord is invalid."));
                        }
                    }
                    if (args.Count > 3)
                    {
                        //Check y position
                        float ypos;
                        if (!float.TryParse(args[3], out ypos))
                        {
                            errors.Add(new ArgumentError(3, "The specified y coord is invalid."));
                        }
                    }
                }
                if (args.Count > 4)
                {
                    errors.Add(ArgumentError.ArgumentsNotUsed(true, 4));
                }
            }
            catch (Exception e)
            {
                Program.LogLine("Fokd the SpawnCommand ArgumentErrors: " + e.Message + e.StackTrace);
            }
            return errors;
        }

        /// <summary>
        /// Whether this command should execute only for the client that entered it, or for every connected user as well.
        /// </summary>
        /// <returns></returns>
        public override bool IsLocalOnly()
        {
            return true;
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player (or the local player, if IsLocalOnly returns true).
        /// <para />Return null to indicate a successful execute (command executed as expected), an informing ExecuteInfo about the command's actions,
        /// or an error ExecuteInfo to indicate failure (specified parameters caused exception).
        /// <para />Do not use ExecuteErrors for parameter validation (this is done by CheckArgumentsValid before execution), but only for anything that can only be determined on Execute!
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            Vec2 spawnPoint;

            Vec2 posget = sender.duck.position;

            if (sender.duck.ragdoll != null)
            {
                posget = sender.duck.ragdoll.position;
            }
            if (sender.duck._trapped != null)
            {
                posget = sender.duck._trapped.position;
            }

            try
            {
                if (args.Count == 4)
                {
                    spawnPoint.x = posget.x + Convert.ToSingle(args[2]);
                    spawnPoint.y = posget.y + Convert.ToSingle(args[3]);
                }
                else if (args.Count == 1 || args.Count == 2)
                {
                    spawnPoint.x = posget.x;
                    spawnPoint.y = posget.y;
                }
                else
                {
                    return new ExecuteInfo("Invalid amount of parameters specified.", -1);
                }

                //Special names
                Thing specialThing = CommandHelpers.CreateSpecialThing(args[0].ToLowerInvariant(), spawnPoint, sender);
                if (specialThing != null)
                {
                    CommandHelpers.SpawnThing(specialThing);
                    return null;
                }

                //Generic name
                var thingType = MserGlobals.GetType(args[0]);
                if (thingType != null)
                {
                    var count = 1;
                    if (args.Count > 1)
                    {
                        count = int.Parse(args[1]);
                    }

                    for (int i = 0; i < count; i++)
                    {
                        var spawned = (Thing)Activator.CreateInstance(thingType, spawnPoint.x, spawnPoint.y);
                        CommandHelpers.SpawnThing(spawned);
                    }
                    return null;
                }
                else
                {
                    return new ExecuteInfo("Can't spawn an unknown type.", 0);
                }
            }
            catch (Exception ex)
            {
                Program.LogLine("General spawning error: " + ex.Message + ex.StackTrace);
                return new ExecuteInfo("General spawning error (try using /spawnex with a special constructor). See ducklog for more infos!", -1);
            }
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered, separated by spaces after the commandName.
        /// <para />Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (index > 0)
            {
                return null;
            }
            else
            {
                //Only provide object list for now
                return CacheManager.GetTypeAutoCompleteInfo(true);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class FindCommand : CommandHandler
    {
        //TODO: /find Support for types, modifiers, etc (mostly whatever gets /dumped)

        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] {"find", "f"};
        }

        /// <summary>
        /// Returns a string containing the expected arguments, as displayed in the "usage" message for this command.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            return new CommandArgument[] {"Search Text"};
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new [] {"Searches for commands based on their name and help text."};
        }

        /// <summary>
        /// Whether this command should execute only for the client that entered it, or for every connected user as well.
        /// </summary>
        /// <returns></returns>
        public override bool IsLocalOnly()
        {
            return true;
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            if (args.Count == 0)
            {
                yield return ArgumentError.InvalidArgumentCount();
            }
            else
            {
                var str = string.Join(" ", args);
                if (string.IsNullOrWhiteSpace(str))
                    yield return ArgumentError.InvalidArgumentCount();
            }
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player.
        /// <para />Return true to indicate success, false to indicate failure (will print usage).
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            var str = string.Join(" ", args);
            if (string.IsNullOrWhiteSpace(str)) return new ExecuteInfo("Tried searching for nothing.");

            var found = new List<CommandHandler>();

            foreach (var cmd in ChatManager.Commands)
            {
                found.AddRange(from name in cmd.GetNames() where name.IndexOf(str, StringComparison.InvariantCultureIgnoreCase) >= 0 select cmd);
                found.AddRange(from help in cmd.GetHelpMessage() where help.IndexOf(str, StringComparison.InvariantCultureIgnoreCase) >= 0 select cmd);
            }

            if (found.Count == 0)
            {
                return new ExecuteInfo(string.Format("Could not find \"{0}\" in any command name or help text.", str));
            }
            else
            {
                //Remove duplicates
                found = found.Distinct().ToList();

                var chatlist = new List<string>();
                int i;
                for (i = 0; i < Math.Min(found.Count, ChatManager.Entries); i++)
                {
                    chatlist.Add(ChatManager.GetShortCommandText(found[i]));
                }

                if (found.Count > ChatManager.Entries)
                {
                    chatlist.Add(string.Format("... and {0} more", found.Count - ChatManager.Entries));
                }

                return new ExecuteInfo(string.Join("\n", chatlist));
            }
        }
    }
}

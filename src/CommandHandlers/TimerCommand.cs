﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class TimerCommand : CommandHandler
    {
        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] {"timer", "interval"};
        }

        /// <summary>
        /// Returns a string containing the expected arguments, as displayed in the "usage" message for this command.
        /// <para />Use [] for optional parameters, &lt;&gt; for specific parameters, and a/b for plaintext choices.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            if (args.Count > 0 && args[0].Equals("start", StringComparison.InvariantCultureIgnoreCase))
            {
                return new CommandArgument[] {"Action/start", "Timer Name", "Interval", "Amount", "Command", "?Arguments"};
            }
            else if (args.Count > 0 && args[0].Equals("stop", StringComparison.InvariantCultureIgnoreCase))
            {
                return new CommandArgument[] {"Action/stop", "Timer Name"};
            }

            return new CommandArgument[] {"Action/start/stop", "Timer Name", "?..."};
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new []
            {
                "Starts or stops a timer, which executes a command",
                "in regular time intervals, for a specified amount of times.",
                "Use amount = -1 to run it infinitely until manually stopped."
            };
        }

        /// <summary>
        /// Whether this command should execute only for the client that entered it, or for every connected user as well.
        /// </summary>
        /// <returns></returns>
        public override bool IsLocalOnly()
        {
            return true;
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc. together with a specific recursed depth.
        /// <para />Only use this overload if this command has to be able to call CheckArgumentsValid as well. For implementation, check /chain or /bind.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            var errorlist = new List<ArgumentError>();

            if (args.Count < 2)
            {
                errorlist.Add(ArgumentError.InvalidArgumentCount(2, args.Count, true));
            }

            if (args.Count > 0 && args[0].Equals("stop", StringComparison.InvariantCultureIgnoreCase))
            {
                if (args.Count > 2)
                {
                    errorlist.Add(ArgumentError.ArgumentsNotUsed(true, 2));
                }

                if (args.Count > 1 && !TimerEntry.Timers.Any(t => t.Name.Equals(args[1], StringComparison.InvariantCultureIgnoreCase)))
                {
                    errorlist.Add(new ArgumentError(1, "No timer with the specified name is running.", Severity.Warning));
                }
            }
            else if (args.Count > 0 && (args[0].Equals("start", StringComparison.InvariantCultureIgnoreCase) ||
                                        args[0].Equals("new", StringComparison.InvariantCultureIgnoreCase)))
            {
                if (TimerEntry.Timers.Any(t => t.Name.Equals(args[1], StringComparison.InvariantCultureIgnoreCase)))
                {
                    //TODO: /timer argumenterrors - show what the timer is currently set to similarly to /bind
                    errorlist.Add(new ArgumentError(1, "A timer with the same name is already running and will be restarted.", Severity.Info));
                }

                //Interval and amount checks
                int interval = 1;
                int amounts;
                if (args.Count > 2 && !int.TryParse(args[2], out interval))
                {
                    errorlist.Add(new ArgumentError(2, "Could not parse specified \"interval\" value as int."));
                }
                else if (interval <= 0)
                {
                    errorlist.Add(new ArgumentError(2, "Interval has to be >0"));
                }
                if (args.Count > 3 && !int.TryParse(args[3], out amounts))
                {
                    errorlist.Add(new ArgumentError(3, "Could not parse specified \"amount\" value as int."));
                }

                if (args.Count <= 4)
                    return errorlist;

                var cmd = ChatManager.GetCommandByName(args[4]);
                if (cmd == null)
                {
                    errorlist.Add(ArgumentError.CommandUnknown(4));
                }
                else
                {
                    if (cmd is IBind)
                    {
                        errorlist.Add(new ArgumentError(4, "The specified command may not be used in a timer."));
                    }

                    var errors = cmd.CheckArgumentsValid(args.GetRange(5, args.Count - 5));
                    foreach (var err in errors)
                    {
                        err.Index += 6;
                        if (err.Severity == Severity.Error)
                            err.Severity = Severity.Warning;
                        errorlist.Add(err);
                    }
                }
            }
            else if (args.Count > 0)
            {
                errorlist.Add(new ArgumentError(0, "Unknown action specified. Supported: start, stop"));
            }

            return errorlist;
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player.
        /// <para />Return true to indicate success, false to indicate failure (will print usage).
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            if (args[0].Equals("start", StringComparison.InvariantCultureIgnoreCase) ||
                args[0].Equals("new", StringComparison.InvariantCultureIgnoreCase))
            {
                //Read params
                var interval = int.Parse(args[2]);
                var amounts = int.Parse(args[3]);

                if (amounts == 0)
                {
                    amounts = -1;
                }

                var cmd = ChatManager.GetCommandByName(args[4]);
                var cmdargs = new List<string>();
                if (args.Count > 5)
                {
                    cmdargs = args.GetRange(5, args.Count - 5);
                }

                //Start timer
                var timer = new TimerEntry();
                timer.Name = args[1];
                timer.Interval = interval;
                timer.Amounts = amounts;
                timer.Command = new BoundCommand(cmd, cmdargs, Keys.None);

                var foundtimer = TimerEntry.Timers.FirstOrDefault(t => t.Name.Equals(timer.Name, StringComparison.InvariantCultureIgnoreCase));
                if (foundtimer != null)
                {
                    //Warn user about restart
                    //ChatManager.Echo(string.Format("NOTE: A timer with the name \"{0}\" already existed and has been replaced.", timer.Name));
                    TimerEntry.Timers.Remove(foundtimer);
                }
                TimerEntry.Timers.Add(timer);

                return null;
            }
            else if (args[0].Equals("stop", StringComparison.InvariantCultureIgnoreCase))
            {
                //Stop specified timer
                var name = args[1];
                var foundtimer = TimerEntry.Timers.FirstOrDefault(t => t.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
                if (foundtimer == null)
                {
                    return new ExecuteInfo(string.Format("No timer with the name \"{0}\" could be found!", name), 1);
                }
                else
                {
                    TimerEntry.Timers.Remove(foundtimer);
                    return null;
                }
            }
            return new ExecuteInfo("Invalid amount of parameters specified.", -1);
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered. Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (index <= 0)
            {
                return new AutoCompleteEntry[] {"start", "stop"};
            }
            if (args.Count == 2 && args[0].Equals("stop", StringComparison.InvariantCultureIgnoreCase))
            {
                //Show existing names
                //TODO: /timer autocomplete should show a timer icon for current timer state! <3
                return TimerEntry.Timers.Select(t => new AutoCompleteEntry(t.Name));
            }
            else if (args.Count > 0 && (args[0].Equals("start", StringComparison.InvariantCultureIgnoreCase) ||
                     args[0].Equals("new", StringComparison.InvariantCultureIgnoreCase)))
            {
                if (index == 1)
                {
                    return new [] { new AutoCompleteEntry("<New Timer Name>", null, false, null, false) }
                        .Concat(TimerEntry.Timers.Select(t => new AutoCompleteEntry(t.Name)));
                }
                else if (index == 4)
                {
                    //Show command name
                    return ChatManager.Commands.Where(c => !(c is IBind)).Select(c => new AutoCompleteEntry(c.GetNames()[0]));
                }
                else if (index > 4)
                {
                    //Show command args autocomplete
                    var cmd = ChatManager.GetCommandByName(args[4]);
                    if (cmd != null)
                        return cmd.GetAutoCompleteEntries(index - 5, args.GetRange(5, index - 4));
                }
                else
                {
                    //Show info of selected timer
                    var foundtimer = TimerEntry.Timers.FirstOrDefault(t => t.Name == args[1]);
                    if (foundtimer != null)
                    {
                        if (index == 2)
                            return new[] {new AutoCompleteEntry(args[1] + ": " + foundtimer.Interval, foundtimer.Interval.ToString(), false, null, false)};
                        if (index == 3)
                            return new[] {new AutoCompleteEntry(args[1] + ": " + foundtimer.Amounts, foundtimer.Amounts.ToString(), false, null, false)};
                    }
                }
            }
            return null;
        }
    }
}

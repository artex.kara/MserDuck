﻿using System.Collections.Generic;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class HoldBindCommand : RebindCommand
    {
        /// <summary>
        /// Returns the "help" message for this command.
        /// <para/>
        /// Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns/>
        public override string[] GetHelpMessage()
        {
            return new[] {"Executes a bind for as long as the specified key is held."};
        }

        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns/>
        public override string[] GetNames()
        {
            return new[] {"bindhold", "hold", "holdbind"};
        }

        /// <summary>
        /// Returns a string containing the expected arguments, as displayed in the "usage" message for this command.
        /// <para/>
        /// Use [] for optional parameters, &lt;&gt; for specific parameters, and a/b for plaintext choices.
        /// </summary>
        /// <returns/>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            return new CommandArgument[] {"Key", "Interval", "Command", "?Arguments"};
        }

        /// <summary>
        /// Whether this command should execute only for the client that entered it, or for every connected user as well.
        /// </summary>
        /// <returns></returns>
        public override bool IsLocalOnly()
        {
            return true;
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            var errors = new List<ArgumentError>();
            var modargs = new List<string>(args);
            if (args.Count > 1)
            {
                //Error int
                int ass;
                if (!int.TryParse(args[1], out ass))
                {
                    errors.Add(new ArgumentError(1, "Invalid interval specified."));
                }
                else if (ass <= 0)
                {
                    errors.Add(new ArgumentError(1, "Interval has to be > 0."));
                }
                //Remove to do base check
                modargs.RemoveAt(1);
            }
            errors.AddRange(base.CheckArgumentsValid(modargs));
            return errors;
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player.
        /// <para/>
        /// Return true to indicate success, false to indicate failure (will print usage).
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param><param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns/>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            //Do a regular bind
            var interval = int.Parse(args[1]);
            var modargs = new List<string>(args);
            modargs.RemoveAt(1);
            var ret = base.Execute(sender, modargs);
            if (!ChatManager.ExecuteRanSuccessfully(ret))
                return ret;

            //Modify new boundcommand
            BindManager.LastBound.Interval = interval;
            return null;
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered. Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (index <= 0)
            {
                return base.GetAutoCompleteEntries(index, args);
            }
            if (index == 1)
            {
                return null;
            }

            args.RemoveAt(1);
            return base.GetAutoCompleteEntries(index - 1, args);
        }
    }
}

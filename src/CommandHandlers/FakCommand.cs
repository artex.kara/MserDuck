﻿using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class FakCommand : CommandHandler
    {
        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] {"fak", "fake"};
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new []
            {
                "Turns the currently held weapon into its fake variant and vice versa.",
                "Use the all parameter to do so for all weapons in the level instead."
            };
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc. together with a specific recursed depth.
        /// <para />Only use this overload if this command has to be able to call CheckArgumentsValid as well. For implementation, check /chain or /bind.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            if (args.Count > 0 && !args[0].EqualsToAny(true, "all", "every", "any", "level"))
            {
                yield return new ArgumentError(0, "Invalid action specified. Supported value: all");
            }

            if (args.Count > 1)
            {
                yield return ArgumentError.ArgumentsNotUsed(true, 1);
            }
        }

        /// <summary>
        /// Returns a list containing the expected arguments, as displayed in the "usage" message for this command, as well as for use for naming or type checking.
        /// <para />The order of the CommandArguments in the list is obviously important.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            yield return "?Action/all";
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered, separated by spaces after the commandName.
        /// <para />Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (index == 0)
            {
                yield return new AutoCompleteEntry("all");
            }
        }

        /// <summary>
        /// Whether this command should execute only for the client that entered it, or for every connected user as well.
        /// </summary>
        /// <returns></returns>
        public override bool IsLocalOnly()
        {
            return true;
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player.
        /// <para />Return true to indicate success, false to indicate failure (will print usage).
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            //Convert all to fak?
            if (args.Count > 0 && args[0].EqualsTo(true, "all"))
            {
                //Count how many things are fak, then toggle based on majority
                var phys = MserGlobals.GetListOfThings<PhysicsObject>().Where(WeaponFakker.HasFakVersion).ToList();
                var fakcnt = phys.Count(WeaponFakker.IsFak);
                var totalcnt = phys.Count;

                //If more than half is a fak, make not-fak
                var makefak = !(fakcnt > totalcnt / 2);
                if (makefak)
                {
                    foreach (var p in phys)
                    {
                        WeaponFakker.ConvertToFak(p);
                    }
                }
                else
                {
                    foreach (var p in phys)
                    {
                        WeaponFakker.ConvertFromFak(p);
                    }
                }
                return null;
            }

            var d = sender.duck;
            if (d == null)
                return new ExecuteInfo("Could not turn weapon into fak: no duck spawned!", -1);

            var held = d.holdObject;
            if (held == null)
                return new ExecuteInfo("Could not turn weapon into fak: no weapon held!", -1);

            //Toggle fak state
            if (!WeaponFakker.ToggleFakState(held))
                return new ExecuteInfo("Could not turn weapon into fak: does not have a fak version!", -1);

            return null;
        }
    }
}

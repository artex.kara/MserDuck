﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class OpenDirCommand : CommandHandler
    {
        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] {"opendir", "opendirectory", "open" };
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[]
            {
                "Opens the specified directory in the explorer.",
                "Only accepts special directory names - see autocompletes!"
            };
        }

        /// <summary>
        /// Whether this command should execute only for the client that entered it, or for every connected user as well.
        /// </summary>
        /// <returns></returns>
        public override bool IsLocalOnly()
        {
            return true;
        }

        /// <summary>
        /// Returns a list containing the expected arguments, as displayed in the "usage" message for this command, as well as for use for naming or type checking.
        /// <para />The order of the CommandArguments in the list is obviously important.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            return new CommandArgument[] { "Directory/gamedirectory/moddirectory/gamecontent/modcontent/skins" };
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered, separated by spaces after the commandName.
        /// <para />Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (index == 0)
            {
                return new AutoCompleteEntry[] {"gamedirectory", "moddirectory", "gamecontent", "modcontent", "skins"};
            }

            return null;
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            if (!args.Any())
            {
                yield return ArgumentError.InvalidArgumentCount();
            }
            else
            {
                if (args.Count > 1)
                {
                    yield return ArgumentError.InvalidArgumentCount(1, args.Count);
                }

                if (!args[0].EqualsToAny(true, "gamedirectory", "moddirectory", "gamecontent", "modcontent", "skins", "gamedir", "moddir", "gameassets", "modassets", "modskins"))
                {
                    yield return ArgumentError.ArgumentUnknown(0, "directory", true, "gamedirectory", "moddirectory", "gamecontent", "modcontent", "skins");
                }
            }
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player (or the local player, if IsLocalOnly returns true).
        /// <para />Return null to indicate a successful execute (command executed as expected), an informing ExecuteInfo about the command's actions,
        /// or an error ExecuteInfo to indicate failure (specified parameters caused exception).
        /// <para />Do not use ExecuteErrors for parameter validation (this is done by CheckArgumentsValid before execution), but only for anything that can only be determined on Execute!
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            var dir = args[0].ToLowerInvariant();
            string path;

            switch (dir)
            {
                case "gamedirectory":
                case "gamedir":
                    path = MserGlobals.GetDuckGamePath();
                    break;
                case "moddirectory":
                case "moddir":
                    path = MserGlobals.GetModDir();
                    break;
                case "gamecontent":
                case "gameassets":
                    path = Path.Combine(MserGlobals.GetDuckGamePath(), "content");
                    break;
                case "modcontent":
                case "modassets":
                    path = Path.Combine(MserGlobals.GetModDir(), "content");
                    break;
                case "modskins":
                case "skins":
                    path = Path.Combine(MserGlobals.GetModDir(), "content", "skins");
                    break;
                default:
                    return null;
            }

            var trailpath = path.EndsWith(Path.DirectorySeparatorChar.ToString()) ? path : path + Path.DirectorySeparatorChar;
            Process.Start("explorer.exe", string.Format("\"{0}\"", trailpath));
            return null;
        }
    }
}

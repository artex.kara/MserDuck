﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class ModifierCommand : CommandHandler
    {
        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] {"modifier", "mod", "md"};
        }

        /// <summary>
        /// Returns a string containing the expected arguments, as displayed in the "usage" message for this command.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            return new CommandArgument[] {"Modifier", "?Value/on/off/toggle"};
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[]
            {
                "Enables or disables the specified modifier.",
                "Accepts names or IDs. Check /dump for more info."
            };
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            if (args.Count > 2)
                yield return ArgumentError.InvalidArgumentCount(2, args.Count);

            if (args.Count > 0)
            {
                //Do shit checks
                var existsA = ModifierManager.Modifiers.FirstOrDefault(mod => args[0].Equals(mod.GetName(), StringComparison.InvariantCultureIgnoreCase) ||
                                                             args[0].Equals(mod.GetName().Replace(" ", ""), StringComparison.InvariantCultureIgnoreCase));
                if (existsA == null)
                {
                    //Check two
                    var existsB = Unlocks.GetUnlocks(UnlockType.Modifier).FirstOrDefault(mod => args[0].Equals(mod.shortName, StringComparison.InvariantCultureIgnoreCase) ||
                                                                         args[0].Equals(mod.id, StringComparison.InvariantCultureIgnoreCase) ||
                                                                         args[0].Equals(mod.shortName.Replace(" ", ""), StringComparison.InvariantCultureIgnoreCase));
                    if (existsB == null)
                    {
                        yield return new ArgumentError(0, "The specified modifier does not exist.");
                    }
                }
                else
                {
                    //May be notimplemented
                    if (existsA.IsNotImplemented())
                    {
                        yield return new ArgumentError(0, "This modifier is not implemented!", Severity.Warning);
                    }

                    //Show current state
                    yield return new ArgumentError(0, string.Join(" ", existsA.GetDescription()), Severity.Info);
                }
            }
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player.
        /// <para />Return true to indicate success, false to indicate failure (will print usage).
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            //HINT: REMINDER /modifier - Instead of having to remove the spaces, try to detect the last parameter being a value or not, or allow "quotes for spaces"!

            CacheManager.ClearModifierAutoCompleteEntries();

            if (!args.Any())
            {
                //Return all modified modifier states
                var mods = ModifierManager.Modifiers.Where(m => m.Enabled).Select(m => new KeyValuePair<string, bool>(m.GetName(), m.Enabled))
                    .Concat(Unlocks.GetUnlocks(UnlockType.Modifier).Where(m => m.enabled).Select(m => new KeyValuePair<string, bool>(m.shortName, m.enabled))).ToList();
                var strings = new string[mods.Count];
                for (var i = 0; i < Math.Min(ChatManager.Entries, mods.Count); i++)
                {
                    strings[i] = mods[i].Key + " = " + mods[i].Value;
                }
                return new ExecuteInfo(string.Join("\n", strings));
            }

            //Try mserduck mods first
            foreach (var mod in ModifierManager.Modifiers)
            {
                if (args[0].Equals(mod.GetName(), StringComparison.InvariantCultureIgnoreCase) ||
                    args[0].Equals(mod.GetName().Replace(" ", ""), StringComparison.InvariantCultureIgnoreCase))
                {
                    var newval = !mod.Enabled;
                    if (args.Count == 2)
                    {
                        switch (args[1])
                        {
                            case "on":
                            case "true":
                            case "1":
                            case "enable":
                                newval = true;
                                break;
                            case "off":
                            case "false":
                            case "0":
                            case "disable":
                                newval = false;
                                break;
                        }
                    }

                    var state = newval ? "Enabled" : "Disabled";
                    mod.Enabled = newval;
                    var extra = mod.IsNotImplemented() ? " [Not implemented!]" : string.Empty;
                    return new ExecuteInfo(state + " modifier " + mod.GetName() + extra);
                }
            }

            //Try duckgame modifiers
            foreach (var mod in Unlocks.GetUnlocks(UnlockType.Modifier))
            {
                if (args[0].Equals(mod.shortName, StringComparison.InvariantCultureIgnoreCase) ||
                    args[0].Equals(mod.id, StringComparison.InvariantCultureIgnoreCase) ||
                    args[0].Equals(mod.shortName.Replace(" ", ""), StringComparison.InvariantCultureIgnoreCase))
                {
                    var newval = !mod.enabled;

                    if (args.Count == 2)
                    {
                        switch (args[1])
                        {
                            case "on":
                            case "true":
                            case "1":
                            case "enable":
                                newval = true;
                                break;
                            case "off":
                            case "false":
                            case "0":
                            case "disable":
                                newval = false;
                                break;
                        }
                    }

                    var state = newval ? "Enabled" : "Disabled";
                    mod.enabled = newval;
                    TeamSelect2.UpdateModifierStatus();
                    TeamSelect2.SendMatchSettings(null, false);
                    return new ExecuteInfo(state + " modifier " + mod.shortName);
                }
            }

            return new ExecuteInfo("No modifier with the name or ID \"" + args[0] + "\" could be found.", 0);
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered, separated by spaces after the commandName.
        /// <para />Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (index == 1)
            {
                //EZPZ
                return AutoCompleteEntry.Toggles;
            }

            if (index <= 0)
            {
                //Dang
                return CacheManager.GetModifierAutoCompleteEntries();
            }

            return null;
        }
    }
}

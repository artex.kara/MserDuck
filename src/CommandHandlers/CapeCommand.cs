﻿using System;
using System.Collections.Generic;
using System.IO;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class CapeCommand : CommandHandler
    {
        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] {"cape", "cp"};
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            if (args.Count == 0)
                yield return ArgumentError.InvalidArgumentCount();
            if (args.Count > 0 && (args[0] == "add" || args[0] == "set"))
            {
                if (args.Count < 2)
                    yield return ArgumentError.InvalidArgumentCount(2, args.Count, false);
                else if (args.Count > 2)
                    yield return ArgumentError.ArgumentsNotUsed(true, 2);
            }
            if (args.Count > 1 && args[0] == "remove")
                yield return ArgumentError.ArgumentsNotUsed(true, 1);
            if (args.Count > 0 && (args[0] != "add" && args[0] != "set" && args[0] != "remove"))
                yield return new ArgumentError(0, "Unknown action specified. Supported: add, remove");
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player.
        /// <para />Return true to indicate success, false to indicate failure (will print usage).
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            if (args[0] == "remove")
            {
                //Remove cape from duck, if any
                if (CapeHandler.savedProfiles.ContainsKey(sender))
                    CapeHandler.savedProfiles.Remove(sender);

                CapeHandler.DettachCape(sender.duck);
            }
            else
            {
                //Grant cape if valid texture
                var modpath = Mod.GetPath<MserDuck>(args[1]);
                var tex = Content.Load<Tex2D>(modpath);
                if (!File.Exists(modpath))
                {
                    tex = Content.Load<Tex2D>(modpath + ".png");
                    if (!File.Exists(modpath + ".png"))
                    {
                        tex = Content.Load<Tex2D>(args[1]);
                        if (!File.Exists(Content.path + args[1]) && !File.Exists(Content.path + args[1] + ".xnb"))
                        {
                            return new ExecuteInfo("Texture is invalid or can't be found.", 1);
                        }
                    }
                }

                //Remove cape first
                if (CapeHandler.savedProfiles.ContainsKey(sender))
                    CapeHandler.savedProfiles.Remove(sender);
                CapeHandler.DettachCape(sender.duck);

                //Add the new cape
                CapeHandler.savedProfiles.Add(sender, tex);

                //Notify if duck has no hat
                if (sender.localPlayer && !sender.duck.dead && sender.duck.hat == null)
                {
                    return new ExecuteInfo("NOTE: You need to have a hat equipped to see the cape.");
                }
            }
            return null;
        }

        /// <summary>
        /// Returns a string containing the expected arguments, as displayed in the "usage" message for this command.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            if (args.Count > 0 && args[0].Equals("set", StringComparison.InvariantCultureIgnoreCase))
            {
                return new CommandArgument[] {"Action/set", "Texture"};
            }
            else if (args.Count > 0 && args[0].Equals("remove", StringComparison.InvariantCultureIgnoreCase))
            {
                return new CommandArgument[] {"Action/remove"};
            }

            return new CommandArgument[] {"Action/set/remove", "?..."};
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[]
            {
                "Grants or removes a cape from your duck",
                "Texture is specified from inside content path of DuckGame and MserDuck"
            };
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered. Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (index <= 0)
            {
                //Keywords
                return new[] {new AutoCompleteEntry("set"), new AutoCompleteEntry("remove")};
            }

            if (index == 1 && args[0] != "remove")
            {
                //autocomplete texture names
                return CacheManager.GetTexturesAutoCompleteInfo();
            }

            return null;
        }
    }
}

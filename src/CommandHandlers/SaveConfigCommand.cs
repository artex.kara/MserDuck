﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DuckGame.MserDuck.CommandHandlers
{
    public class SaveConfigCommand : CommandHandler
    {
        /// <summary>
        /// Returns a list of names of the command. First entry in the array is the standard name for it.
        /// </summary>
        /// <returns></returns>
        public override string[] GetNames()
        {
            return new[] {"saveconfig", "savecfg", "save", "writeconfig", "writecfg", "write"};
        }

        /// <summary>
        /// Returns the "help" message for this command.
        /// <para />Can be newline-separated for a longer description. Only the first line is displayed in the quick help.
        /// </summary>
        /// <returns></returns>
        public override string[] GetHelpMessage()
        {
            return new[]
            {
                "Saves all binds to the config.cfg file.",
                "Can also set a config key to the specified value."
            };
        }

        /// <summary>
        /// Whether this command should execute only for the client that entered it, or for every connected user as well.
        /// </summary>
        /// <returns></returns>
        public override bool IsLocalOnly()
        {
            return true;
        }

        /// <summary>
        /// Returns a list of ArgumentErrors specifying which argument causes an error, what the error is, etc.
        /// <para />This does not have to guarantee that the Execute does not error, but only that the format as asked for in GetExpectedArguments is correct!
        /// <para />Return null or an empty list if the arguments are valid.
        /// </summary>
        /// <param name="args">A list of all arguments currently entered (separated by spaces after the commandName), or list of arguments to check validity for.</param>
        /// <returns></returns>
        public override IEnumerable<ArgumentError> CheckArgumentsValid(List<string> args)
        {
            if (args.Count > 0)
            {
                //Check key names
                if (!CacheManager.GetConfigEntriesAutoCompleteInfo().Any(e => e.Value.Equals(args[0], StringComparison.InvariantCultureIgnoreCase)))
                {
                    yield return new ArgumentError(0, "No config key with this name exists.");
                }
            }
        }

        /// <summary>
        /// Returns a list of AutoCompleteEntries that represents the auto complete info for the specified argument of the command.
        /// <para />The list will be sorted alphabetically on its own (use SortByValue to determine precise behaviour)!
        /// <para />Since it's called every draw event, be sure to optimize it well (cache if possible).
        /// <para />Note that only entries which start with whatever is being entered as the argument's value will be shown!
        /// <para />Return null to display the generic auto complete text instead.
        /// </summary>
        /// <param name="index">The index of argument currently being edited by the user.</param>
        /// <param name="args">A list of all arguments currently entered, separated by spaces after the commandName.
        /// <para />Use args[index] (or args.Last()) to get the currently modified.</param>
        /// <returns></returns>
        public override IEnumerable<AutoCompleteEntry> GetAutoCompleteEntries(int index, List<string> args)
        {
            if (index <= 0)
            {
                //Config Keys
                foreach (var key in CacheManager.GetConfigEntriesAutoCompleteInfo())
                {
                    yield return key;
                }
            }

            //TODO: /savecfg autocomplete - include common type's values (e.g. true/false for bools) -> put to set/spawn/call/...
        }

        /// <summary>
        /// Returns a string containing the expected arguments, as displayed in the "usage" message for this command.
        /// <para />Use [] for optional parameters, &lt;&gt; for specific parameters, and a/b for plaintext choices.
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<CommandArgument> GetExpectedArguments(List<string> args)
        {
            return new CommandArgument[] {"?Key", "?Value"};
        }

        /// <summary>
        /// Called when a chat message matching the pattern "/commandName" is sent by any player.
        /// <para />Return true to indicate success, false to indicate failure (will print usage).
        /// </summary>
        /// <param name="sender">Profile that executed the command.</param>
        /// <param name="args">List of arguments, separated by spaces after the commandName.</param>
        /// <returns></returns>
        public override ExecuteInfo Execute(Profile sender, List<string> args)
        {
            if (args.Crop().Any())
            {
                //Change a config key
                var key = ConfigManager.ConfigEntries.First(e => e.GetName().Equals(args[0], StringComparison.InvariantCultureIgnoreCase));
                if (args.Count == 1)
                {
                    //Get config key value, default and help
                    return new ExecuteInfo(string.Format("[{0}] {1} = {2} (Default: {3}).\n{4}",
                        key.GetCategory(), key.GetName(), key.Value, key.GetDefaultValue(), string.Join("\n", key.GetDescription())));
                }

                //Set config key
                var value = string.Join(" ", args.GetRange(1, args.Count - 1));
                if (!key.BeforeWrite())
                    return new ExecuteInfo("BeforeWrite returned false - key was not modified.", 0, Severity.Warning);
                key.Value = value;
                key.AfterWrite();

                //Run an AfterRead in order to update values, since the key should "get read again"
                key.AfterRead();
                return null;
            }

            //Save the config
            var error = ConfigManager.SaveConfigFile();

            //Add any binds to the autoexec
            var newlines = new List<string>();
            var lines = File.ReadAllLines(ConfigManager.configFile).ToList();

            foreach (var bind in BindManager.Commands)
            {
                //Get line contents
                string line;
                if (bind.Interval > -1)
                {
                    //Is a bindhold
                    line = string.Format("bindhold {0} {2} {1}", bind.Key, bind.ToString(false, false), bind.Interval);
                }
                else if (bind.CommandList.Count > 1)
                {
                    //NYI: /save - togglebind save context
                    line = "YOU ARE AN ERROR!";
                }
                else
                {
                    //Is a regular bind
                    line = string.Format("bind {0} {1}", bind.Key, bind.ToString(false, false));
                }

                //Get bind key from config file
                var cmd = ConfigManager.GetAutoexecBindByKey(bind.Key);
                if (cmd == null)
                {
                    //New key, append to file
                    newlines.Add(line);
                }
                else
                {
                    //Already in file, replace line
                    lines[cmd.Line] = line;
                }
            }

            //Add the newlines to the autoexec block
            if (newlines.Count > 0)
            {
                int startInd;
                int lastInd;
                var res = ConfigManager.GetIndsForAutoexec(lines, out startInd, out lastInd);
                if (!res)
                {
                    //Fallback last int
                    lastInd = lines.Count;
                }
                lines.InsertRange(lastInd, newlines);
            }

            //Save the final file!
            File.WriteAllLines(ConfigManager.configFile, lines);

            if (error == null)
            {
                return new ExecuteInfo("Saved changes to config file.");
            }
            else
            {
                return new ExecuteInfo(error, -1);
            }
        }
    }
}

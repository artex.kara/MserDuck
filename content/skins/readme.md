Add any folder here for a new duck skin. It can be selected using /skin <foldername>
The following files can be replaced inside each:

```
duck.png (regular animations)
duckArms.png (arms of duck, frames from duck.png copied over)
quackduck.png (quacking spritesheet of duck, frames from duck.png copied over)
controlledDuck.png (mind control spritesheet of duck, frames from duck.png copied over)
seatDuck.png (crowd sprite of duck)
feather.png (feather sprite when near-missed or killed)
fingerPositions.png (only used for the trumpet being played, frames directly set)
```

Each png file can also be accompanied with a .txt file of the same name.
They allow you to specify any metadata that you may want to include. Example:

```py
# Size of each frame in the spritesheet:
# You might want to keep it close to default for gameplay-relevant sprites,
# since there currently is no way to easily change collision boxes!
size = 32x32

# Whether the duck's sprite should be recolored based on /color
# This only replaces the colors used in the default duck, so be sure to color-pick those
recolor = true

# Animation definitions:
# You are better off not changing these, since some code MAY set or compare frame numbers instead of animations by name.
# Format is anim# = name,speed,looping,frames
anim1 = idle,1,true,1,2,3,4,5,6
anim2 = run,1,true,1,2,3,4,5,6
# ... any animation not included here will use default values.

# Used to set the sprite.speed property - slowing down all animations by this factor.
# This gets overwritten for the duck's sprite at all times - use animation times instead!
animspeed = 0.3

# Sets the starting animation. Not guaranteed to be used when selecting the skin.
animdefault = idle
```

By not including a replacement PNG or TXT file, the default variant is used.
See the default folder for the assets, how they should be replaced, and what your skin would fallback to with each file.
You will probably need to check the animation names there to see which frames correspond to what action.